//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbdriverejdb.h
/// Declarations of class
/// TEJDBFile - working with internal EJDB data base
/// TEJDBDrive - DB driver for working with EJDB data base
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef DBDRIVEREJDB_H
#define DBDRIVEREJDB_H

#include "dbbase.h"
#include "json2cfg.h"
#include "ejdb/ejdb.h"

namespace bsonio {

/// Class for EJDB file manipulation
class TEJDBFile: public TFileBase
{
    string version;  ///< Version of EJDB

protected:

    /// Make keyword of DB internal file
    void makeKeyword();

public:
    EJDB *ejDB ;

    /// Constructor
    TEJDBFile( const string& fName, const string& fExt, const string& fDir="");
    /// Constructor from path
    TEJDBFile( const string& path );
    /// Constructor from bson data
    TEJDBFile( const char* bsobj );

    ~TEJDBFile();

    /// Get Version of EJDB
    const string& Version() const
    {   return version; }
    /// Read version from Changelog.txt file
    void readVersion();

    //--- Manipulation

    /// Create PDB file
    void Create( bool isDel );
    /// Close EJ DataBase
    virtual void Close();
    /// Open EJ DataBase
    virtual void Open( int amode = OpenModeTypes::ReadWrite );
};

/// Driver for working with EJDB data base
class TEJDBDrive : public TAbstractDBDriver
{

    EJCOLL *openCollectionFile( const char* collname, bool createifempty );
    void closeCollectionFile( const char* collname );
    void getBsonOid( bson_oid_t *oid, char* pars );
    void setBsonOid( bson_oid_t *oid, KeysSet::iterator& itr );

    TEJDBFile flEJ;

public:

    virtual void resetEJDB( const std::string& theNewPath )
    {
      flEJ.reOpen(theNewPath);
    }

    ///  Constructor
    TEJDBDrive( const string& dbfilePath ):flEJ(dbfilePath)
    { }

   ///  Destructor
   virtual ~TEJDBDrive() {}

    /// Gen new oid or other pointer of location
    virtual string genOid(const char* )
    {
        bson_oid_t oid;
        bson_oid_gen( &oid );
        char bytes[25];
        bson_oid_to_string( &oid, bytes );
        return string(bytes);
   }

   /// Read record from DB to bson
   bson *loadbson( const char* collname,  KeysSet::iterator& it);
   /// Remove current object from collection.
   bool rmbson( const char* collname, KeysSet::iterator& it);
   /// Write record to DB
   bool savebson( const char* collname, KeysSet::iterator& it, bson *bsrec, string& errejdb );
   /// Change current bson before save
   void addtobson( bson* bsdata, char* oid );

   /// Run query
   ///  \param collname - collection name
   ///  \param queryFields - list of fileds selection
   ///  \param queryJson - json string with query  condition (where)
   ///  \param setfnc - function for set up readed data
   void selectQuery( const char* collname, const set<string>& queryFields,
        const string&  queryJson,  SetReadedFunction setfnc );

   /// Run query for read Linked records list
   ///  \param collname - collection name
   ///  \param queryFields - list of fileds selection
   ///  \param setfnc - function for set up Linked records list
   void allQuery( const char* collname,
          const set<string>& queryFields,  SetReadedFunctionKey setfnc );

   /// Run delete query
   ///  \param collname - collection name
   ///  \param query - qury  condition (where)
   void delQuery(const char* collname, bson* query );


   ///  Provides 'distinct' operation over query
   ///  \param fpath Field path to collect distinct values from.
   ///  \param queryJson - json string with where condition
   ///  \param return values by specified fpath and query
   void fpathQuery( const char* collname, const string& fpath,
                    const string& queryJson, vector<string>& values );

};


/// Driver for working with EJDB data base
class TEJDBDriveOne : public TEJDBDrive
{

    ///  Constructor
    TEJDBDriveOne():TEJDBDrive(flEJPath) {}

public:

   static string flEJPath;

   ///  Destructor
   virtual ~TEJDBDriveOne() {}

   friend TEJDBDriveOne& ejdbDriveOne();
};


/// Use only one client object
TEJDBDriveOne& ejdbDriveOne();

} // namespace bsonio

#endif // DBDRIVEREJDB_H
