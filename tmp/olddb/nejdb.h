//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file nejdb.h
/// Declarations of class TDBString
/// for working with simple data base
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TEJDSTRING_H
#define TEJDSTRING_H

#include "dbbase.h"

namespace bsonio {


/// Definition of simple chain (internal data are saved into string )
class TDBString : public TDBAbstract
{
    vector<string> internalData;
    string currentJson;     ///< last read/save record json
    string currentYAML;     ///< last read/save record YAML

protected:

   // internal functions
   /// Load collection keys from one EJDB
   void loadCollectionFile( const set<string>& queryFields );
   void closeCollectionFile() {}
   /// Load records key from bson structure
   void listKeyFromBson( const char* bsdata, char*keydata );

   set<string> listQueryFields( bool addkey, const vector<string>& fields);

   // from TDBAbstract
   void closeCollection()
   {  closeCollectionFile(); }
   void loadCollection();

   /// Save current record to bson structure
   void recToBson( bson *obj, time_t crtt, char* oid );
   /// Load data from bson structure (return read record key)
   string recFromBson( bson *obj );

   /// Run current query, rebuild internal table of values
   void executeQuery();
   /// Run query
   ///  \param _queryJson - json string with where condition
   ///  \param  _queryFields - list of fields into result json ( if empty only _id )
   ///  \return _resultData - list of json strings with query result
   void executeQuery( const string&  _queryJson, const vector<string>& _queryFields,
               vector<string>& _resultData );
   ///  Provides 'distinct' operation over query
   ///  \param fpath Field path to collect distinct values from.
   ///  \param queryJson - json string with where condition
   ///  \return Unique values by specified fpath and query
   vector<string> distinctQuery( const string& fpath, const string&  queryJson="" );

public:

   ///  Constructor
   TDBString( TAbstractDBDriver* adriver, const char* name, const vector<KeyFldsData>& keyFldsInf  ):
       TDBAbstract( adriver, name, keyFldsInf )
     {}

   /// Constructor from bson data
   TDBString( TAbstractDBDriver* adriver, const char* bsobj ):
       TDBAbstract( adriver, bsobj )
     {}

   ///  Destructor
   virtual ~TDBString(){}

   /// Return curent record in json format string
   string GetJson()
   {  return currentJson; }
   /// Set json format string to curent record
   void SetJsonYaml( const string& sjson, bool is_json = true);
   /// Return curent record in YAML format string
   string GetYAML()
   { return currentYAML; }

};

} // namespace bsonio

#endif // TEJDSTRING_H
