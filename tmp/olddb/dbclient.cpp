//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbclient.cpp
/// Implementation of class
/// TDBClient - DB driver for working with thrift server data base
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <cstring>
#include <iostream>
#include "dbclient.h"

#include <thrift/transport/TSocket.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/protocol/TProtocolException.h>
#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>

using namespace apache::thrift;
using namespace apache::thrift::transport;
using namespace apache::thrift::protocol;
using boost::make_shared;
using boost::shared_ptr;

using namespace  dbserver;

namespace bsonio {

// The host that the socket is connected to
std::string TDBClientOne::theHost = "localhost";

// The port that the socket is connected to
int TDBClientOne::thePort = 9090;

TDBClientOne& dbClientOne()
{
 static TDBClientOne drive_client;
 return drive_client;
}

DBServerClient* TDBClient::resetDBServerClient(const std::string& theHost, int thePort)
{
  _host = theHost;
  _port = thePort;

   if( dbClient  )
     delete dbClient;

  //Set the Transport
     trans = boost::make_shared<TSocket>(theHost.c_str(), thePort );
     trans = boost::make_shared<TBufferedTransport>(trans);

   //Set the Protocol
     boost::shared_ptr<TProtocol> proto;
     proto.reset(new TBinaryProtocol(trans));

  dbClient = new DBServerClient(proto);

  // Connect to service
  return dbClient ;
}

// TDBClient ---------------------------------------------

// Gen new oid or other pointer of location
string TDBClient::genOid( const char* collname )
{
    // Get oid of record
    string _oid;

    // read from server
    try{
         trans->open();
         dbClient->GenerateId(_oid, collname );
         trans->close();
    }
    catch (const TException & te)
    {
      cout << "TException: " << te.what() << endl;
      bsonioErr( "DBClient001", te.what() );
    }
    return _oid;
}

// Retrive one record from the collection
bson *TDBClient::loadbson(const char* collname, KeysSet::iterator& itr )
{
    // Get oid of record
    string key;
    getServerKey( key, itr->second.get() );
    ValueResponse _data;

    // read from server
    try{
         trans->open();
         dbClient->ReadRecord(_data, collname, key);
         trans->close();
    }
    catch (const TException & te)
    {
      cout << "TException: " << te.what() << endl;
      bsonioErr( "DBClient001", te.what() );
    }

    // test responseCode
    switch( _data.responseCode )
    {
       case ResponseCode::NameNotFound:
             bsonioErr( "DBClient002", "Collection doesn't exist" );
       case ResponseCode::RecordNotFound:
             bsonioErr( "DBClient003", "Record doesn't exist" );
       case ResponseCode::Error:
             bsonioErr( "DBClient004", _data.errormsg );
       default: break;
    }
    // convert json to bson
    bson *bsrec = bson_create();
    jsonToBson( bsrec, _data.value );
    return bsrec;
}

// Removes record from the collection
bool TDBClient::rmbson( const char* collname,KeysSet::iterator& itr )
{
    // Get oid of record
    string key;
    getServerKey( key, itr->second.get() );
    ResponseCode::type _data;

    // read from server
    try{
         trans->open();
         _data = dbClient->DeleteRecord( collname, key);
         trans->close();
    }
    catch (const TException & te)
    {
      cout << "TException: " << te.what() << endl;
      bsonioErr( "DBClient005", te.what() );
    }

    // test responseCode
    switch( _data )
    {
       case ResponseCode::NameNotFound:
             bsonioErr( "DBClient002", "Collection doesn't exist" );
       case ResponseCode::RecordNotFound:
             bsonioErr( "DBClient003", "Record doesn't exist" );
       case ResponseCode::Error:
             bsonioErr( "DBClient004", "Undefined error" );
       default: break;
    }
    return true;
}

// Save/update record in the collection
bool TDBClient::savebson( const char* collname, KeysSet::iterator& itr, bson *bsrec, string& errejdb )
{
    ResponseCode::type _ret;
    string errormsg = "Undefined error";
    string recJson;
    jsonFromBson( bsrec->data, recJson );

    // read from server
    try{
         trans->open();
         if( itr->second.get() == nullptr ) // new record
         {
           KeyResponse _return;
           dbClient->CreateRecord( _return, collname, recJson);
           _ret =_return.responseCode;
           if( _ret == ResponseCode::Success )
             setServerKey( _return.key, itr );
           else
             errormsg = errejdb = _return.errormsg;
         }
         else
         {
           string key;
           getServerKey( key, itr->second.get() );
          _ret = dbClient->UpdateRecord( collname, key, recJson);
         }
         trans->close();
    }
    catch (const TException & te)
    {
      cout << "TException: " << te.what() << endl;
      bsonioErr( "DBClient006", te.what() );
    }

    // test responseCode
    switch( _ret )
    {
       case ResponseCode::NameNotFound:
             bsonioErr( "DBClient002", "Collection doesn't exist" );
       case ResponseCode::RecordNotFound:
             bsonioErr( "DBClient003", "Record doesn't exist" );
       case ResponseCode::Error:
             bsonioErr( "DBClient004", errormsg );
       default: break;
    }
    return true;
}


// Run query
//  \param collname - collection name
//  \param queryFields - list of fileds selection
//  \param queryJson - json string with query  condition (where)
//  \param setfnc - function for set up readed data
void TDBClient::selectQuery( const char* collname, const set<string>& queryFields,
     const string&  queryJson,  SetReadedFunction setfnc )
{
    /* ?? add collection
     try{
          trans->open();
          dbClient->addCollection( collname );
          trans->close();
     }
     catch (const TException & te)
     {
       cout << "TException: " << te.what() << endl;
       bsonioErr( "DBClient007", te.what() );
     }
     */

    // read from server
    DBRecordListResponse _data;
    try{
         trans->open();
         dbClient->SearchRecords(_data, collname, queryFields, queryJson);
         trans->close();
    }
    catch (const TException & te)
    {
      cout << "TException: " << te.what() << endl;
      bsonioErr( "DBClient007", te.what() );
    }

    // test responseCode
    switch( _data.responseCode )
    {
       case ResponseCode::NameNotFound:
             bsonioErr( "DBClient002", "Collection doesn't exist" );
       case ResponseCode::Error:
             bsonioErr( "DBClient004", _data.errormsg );
       default: break;
    }

    //cout << _data.records.size() << " records in collection " << getKeywd() << endl;
    for (uint i = 0; i < _data.records.size(); ++i)
    {
        bson bsdata;
        jsonToBson( &bsdata, _data.records[i].value );
        setfnc(  bsdata.data );
        bson_destroy(&bsdata);
    }
}

// Run query
//  \param collname - collection name
//  \param queryFields - list of fileds selection
//  \param setfnc - function for set up readed data
void TDBClient::allQuery( const char* collname, const set<string>& queryFields,
    SetReadedFunctionKey setfnc )
{
    /* ?? add collection
     try{
          trans->open();
          dbClient->addCollection( collname );
          trans->close();
     }
     catch (const TException & te)
     {
       cout << "TException: " << te.what() << endl;
       bsonioErr( "DBClient007", te.what() );
     }
     */

    // read from server
    DBRecordListResponse _data;
    try{
         trans->open();
         dbClient->SearchRecords(_data, collname, queryFields, "");
         trans->close();
    }
    catch (const TException & te)
    {
      cout << "TException: " << te.what() << endl;
      bsonioErr( "DBClient007", te.what() );
    }

    // test responseCode
    switch( _data.responseCode )
    {
       case ResponseCode::NameNotFound:
             bsonioErr( "DBClient002", "Collection doesn't exist" );
       case ResponseCode::Error:
             bsonioErr( "DBClient004", _data.errormsg );
       default: break;
    }

    //cout << _data.records.size() << " records in collection " << getKeywd() << endl;
    for (uint i = 0; i < _data.records.size(); ++i)
    {
        bson bsdata;
        jsonToBson( &bsdata, _data.records[i].value );
        // make oid
        char *oidhex = new char[25];
        strncpy(oidhex, _data.records[i].key.c_str(), 25 );
        //cout << _data.records[i].c_str() << endl;
         setfnc(  bsdata.data, oidhex );
         bson_destroy(&bsdata);
    }
}


void TDBClient::delQuery(const char* collname, bson* bsq1 )
{
    string deleteQuery;
    jsonFromBson( bsq1->data, deleteQuery );
    ResponseCode::type _data;

    // read from server
    try{
         trans->open();
         _data = dbClient->DeleteRecordsQuery( collname, deleteQuery);
         trans->close();
    }
    catch (const TException & te)
    {
      cout << "TException: " << te.what() << endl;
      bsonioErr( "DBClient005", te.what() );
    }

    // test responseCode
    switch( _data )
    {
       case ResponseCode::NameNotFound:
             bsonioErr( "DBClient002", "Collection doesn't exist" );
       case ResponseCode::Error:
             bsonioErr( "DBClient004", "Undefined error" );
       default: break;
    }
}

void TDBClient::fpathQuery( const char* collname, const string& fpath,
                            const string& queryJson, vector<string>& values )
{

    /* ?? add collection
     try{
          trans->open();
          dbClient->addCollection( collname );
          trans->close();
     }
     catch (const TException & te)
     {
       cout << "TException: " << te.what() << endl;
       bsonioErr( "DBClient007", te.what() );
     }
     */

    // read from server
    StringListResponse _data;
    try{
         trans->open();
         dbClient->SearchValues(_data, collname, fpath, queryJson);
         trans->close();
    }
    catch (const TException & te)
    {
      cout << "TException: " << te.what() << endl;
      bsonioErr( "DBClient007", te.what() );
    }

    // test responseCode
    switch( _data.responseCode )
    {
       case ResponseCode::NameNotFound:
             bsonioErr( "DBClient002", "Collection doesn't exist" );
       case ResponseCode::Error:
             bsonioErr( "DBClient004", _data.errormsg );
       default: break;
    }
    values = _data.names;
}

} // namespace bsonio
