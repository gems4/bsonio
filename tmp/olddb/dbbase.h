//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbbase.h
/// Declarations of class TDBAbstract - base interface of abstract DB chain
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TDBABSTRACT_H
#define TDBABSTRACT_H

#include <set>
#include <map>
#include <memory>
#include "dbquerydef.h"
#include "dbkeyfields.h"

namespace bsonio {

enum RecStatus {   // states of keys record Data Base
    UNDF_=-7 /* undefined state */, FAIL_=-1 /* access error */,
    NONE_=0 /* no records */, ONEF_ ,
    MANY_,  EMPC_ /* empty chain */
};

using KeysSet = map<IndexEntry, unique_ptr<char>, less<IndexEntry> >;
using  SetReadedFunction = std::function<void( const char* bsdata )>;
using  SetReadedFunctionKey = std::function<void( const char* bsdata, char *keydata )>;

/// Interface for Abstract Database Driver
class TAbstractDBDriver
{

public:

    ///  Constructor
    TAbstractDBDriver( ) {}

   ///  Destructor
   virtual ~TAbstractDBDriver() {}

   /// Gen new oid or other pointer of location
   virtual string genOid(const char* collname) = 0 ;

   /// Read record from DB to bson
   virtual bson *loadbson( const char* collname, KeysSet::iterator& it ) = 0;
   /// Remove current object from collection.
   virtual bool rmbson(const char* collname, KeysSet::iterator& it ) = 0;
   /// Write record to DB
   virtual bool savebson( const char* collname, KeysSet::iterator& it, bson *bsrec, string& errejdb ) = 0;
   /// Change current bson before save
   virtual void addtobson( bson*, char* ) {}


   /// Run query
   ///  \param collname - collection name
   ///  \param queryFields - list of fileds selection
   ///  \param queryJson - json string with query  condition (where)
   ///  \param setfnc - function for set up readed data
   virtual void selectQuery( const char* collname, const set<string>& queryFields,
        const string&  queryJson,  SetReadedFunction setfnc ) = 0;

   /// Run query for read Linked records list
   ///  \param collname - collection name
   ///  \param queryFields - list of fileds selection
   ///  \param setfnc - function for set up Linked records list
   virtual void allQuery( const char* collname,
          const set<string>& queryFields,  SetReadedFunctionKey setfnc ) = 0;

   /// Run delete query
   ///  \param collname - collection name
   ///  \param query - qury  condition (where)
   virtual void delQuery(const char* collname, bson* query ) = 0;

    ///  Provides 'distinct' operation over query
    ///  \param fpath Field path to collect distinct values from.
    ///  \param queryJson - json string with where condition
    ///  \param return values by specified fpath and query
    virtual void fpathQuery( const char* collname, const string& fpath,
                             const string& queryJson, vector<string>& values ) = 0;


};

/// Connect to DB driver by type
TAbstractDBDriver* dbDrive( int type );


/// Definition of Abstract DB chain
class TDBAbstract: public IsUnloadToBson
{

    /// Do it after read record from DB to bson
    virtual void afterLoadbson( KeysSet::iterator&  ) {}
    /// Do it before remove current object from collection.
    virtual void beforeRmbson( KeysSet::iterator&  ) {}
    /// Do it after remove current object from collection.
    virtual void afterRmbson( KeysSet::iterator&  ) {}
    /// Do it before write record to DB
    virtual void beforeSaveUpdatebson( KeysSet::iterator& , bson* /*bsrec*/ ) {}
    /// Do it after write record to DB
    virtual void afterSaveUpdatebson( KeysSet::iterator& , bson* /*bsrec*/ ) {}


 protected:

    TAbstractDBDriver* _dbdriver; ///< Access to the database driver

    string Keywd;   ///< Name of modules DB
    TEJDBKey key;  ///< Definition of record key

    // Definition of record key list
    KeysSet recList;   ///< Linked records list
    KeysSet::iterator itrL; ///< Current index in recList

    // Work data
    RecStatus status;             ///< ? Current states of keys record DB
    time_t crt;                   ///< Current Record Modify time

    //--- Manipulation list of records (tables) query
    string         defqueryJson;     ///< Default query for select record json
    vector<string> defqueryFields;   ///< Default list of fields to query
    DBQueryResult queryResult;

    virtual void closeCollection() = 0;
    virtual void loadCollection() = 0;

    /// Save current record to bson structure
    virtual void recToBson( bson *obj, time_t crtt, char* oid ) = 0;
    /// Load data from bson structure (return read record key)
    virtual string recFromBson( bson *obj ) = 0;

    /// Run current query, rebuild internal table of values
    virtual void executeQuery() =0;
    /// Run query
    ///  \param _queryJson - json string with where condition
    ///  \param  _queryFields - list of fields into result json ( if empty only _id )
    ///  \return _resultData - list of json strings with query result
    virtual void executeQuery( const string&  _queryJson, const vector<string>& _queryFields,
                vector<string>& _resultData ) = 0;

    ///  Provides 'distinct' operation over query
    ///  \param fpath Field path to collect distinct values from.
    ///  \param queryJson - json string with where condition
    ///  \return Unique values by specified fpath and query
    virtual vector<string> distinctQuery( const string& fpath, const string&  queryJson="" ) = 0;

 public:

    ///  Constructor
    TDBAbstract( TAbstractDBDriver* adriver, const char* name, const vector<KeyFldsData>& keyFldsInf  );
    ///  Destructor
    virtual ~TDBAbstract()
    { }
    /// Constructor from bson data
    TDBAbstract( TAbstractDBDriver* adriver, const char* bsobj );

    virtual void toBson( bson *obj ) const;
    virtual void fromBson( const char *obj );

    /// Open Data Base files and build linked record list
    virtual void Open();
    /// Close all Data Base files and free linked record list
    virtual void Close();


    //--- Manipulation records

    /// Find record with key into internal record keys list
    bool Find( const char *key );
    /// Test state of record with key key_ as template.
    /// in field field setted any(*) data
    bool FindPart( const char *key_, int field );
    /// Retrive one record from the collection
    void GetRecord( const char *key );
    /// Removes record from the collection
    void DelRecord( const char *key );
    /// Save/update record in the collection
    void SaveRecord( const char* key  );
    /// Save new record in the collection
    void InsertRecord( const char *key=0 );
    /// Test state of record with key pkey.
    /// If mode == 1 and one record, read this record.
    RecStatus Rtest( const char *key, int mode );

    /// Return curent record in json format string
    virtual string GetJson() = 0;
    /// Set json format string to curent record
    virtual void SetJsonYaml( const string& sjson, bool is_json = true) = 0;
    /// Return curent record in YAML format string
    virtual string GetYAML() = 0;

    /// Get record key from bson structure
    virtual string getKeyFromBson( const char* bsdata );
    /// Add current record key to bson structure
    virtual void addKeyToBson( bson *obj );

    //--- Manipulation list of records (tables)


    /// Set default json query string for collection
    virtual void SetDefaultQueryJson( const string& qrjson);
    /// Set default query fields to table
    void SetDefaultQueryFields( const vector<string>&  fieldsList )
    {
       defqueryFields = fieldsList;
    }

    const DBQueryDef& getQuery()
    {
       return queryResult.getQuery();
    }
    void setQuery( const DBQueryDef& qrdef )
    {
       queryResult.setQuery(qrdef);
    }
    /// Set query json string for collection
    virtual void SetQueryJson( const string& qrjson)
    {
       queryResult.setEJDBQuery(qrjson);
    }
    /// Return curent query json string
    string GetLastQuery()
    {
        return  queryResult.getQuery().getEJDBQuery();
    }
    /// Set query fields to table
    void SetQueryFields( const vector<string>&  fieldsList )
    {
       queryResult.setFieldsCollect( fieldsList );
    }
    /// Get query fields list
    const vector<string>& GetQueryFields()
    {
       return queryResult.getQuery().getFieldsCollect();
    }

    /// Run current query, rebuild internal table of values
    void runQuery()
    {
      executeQuery();
    }

    /// Run current query, rebuild internal table of values
    void runQuery( const DBQueryDef&  _query  )
    {
       queryResult.setQuery(_query);
       executeQuery();
    }

    /// Run query
    ///  \param _queryJson - json string with where condition
    ///  \param  _queryFields - list of fields into result json ( if empty only _id )
    ///  \return _resultData - list of json strings with query result
    void runQuery( const string&  _queryJson, const vector<string>& _queryFields,
                vector<string>& _resultData )
    {
        executeQuery( _queryJson, _queryFields, _resultData );
    }

    /// Run query
    ///  \param _query - selection query
    ///  \return _resultData - list of json strings with query result
    void runQuery( const DBQueryDef&  _query, vector<string>& _resultData )
    {
       executeQuery( _query.getEJDBQuery(), _query.getFieldsCollect(), _resultData );
    }

    /// Run query  \return _resultData - list of json strings with query result
    void runQuery( vector<string>& _resultData )
    {
       runQuery( queryResult.getQuery(), _resultData );
    }

    ///  Provides 'distinct' operation over query
    ///  \param fpath Field path to collect distinct values from.
    ///  \param queryJson - json string with where condition
    ///  \return Unique values by specified fpath and query
    vector<string> fieldQuery( const string& fpath, const string&  queryJson="" )
    {
       return  distinctQuery( fpath, queryJson );
    }

    /// Get query result table
    const TableOfValues& GetQueryResult()
    {
       return  queryResult.getQueryResult();
    }

    /// Get all keys list for current query
    int GetKeyValueList( vector<string>& aKeyList,
           vector<vector<string>>& aValList )
    {
      return  queryResult.getKeyValueList(aKeyList,aValList);
    }


    /// Get keys list for current query according pattern
    int GetKeyValueList( const char* keypat,  vector<string>& aKeyList,
           vector<vector<string>>& aValList )
    {
       CompareTemplateFunction compareFunc = [=]( const char* keypat, const char* akey)
       {
           TEJDBKey currentKey(key);
           currentKey.setKey( akey);
           TEJDBKey wkey(key);
           wkey.setKey( keypat );
           return wkey.compareTemplate( currentKey.retIndex() );
       };

       return  queryResult.getKeyValueList(aKeyList,aValList, keypat, compareFunc );
    }

    /// Find key from data
    string GetKeyFromValue( const char *bsdata )
    {
       return  queryResult.getKeyFromValue(bsdata);
    }

    /// Get key list for a wildcard search
    virtual int GetKeyList( const char *keypat,
        vector<string>& aKeyList, bool retUnpackform = true );


    //--- Selectors

    /// Get name of modules DB
    const char* getKeywd() const
    {  return Keywd.c_str();   }
    /// Get current record status
    RecStatus GetStatus() const
       {  return status;  }
    /// Set current record status
    void SetStatus( RecStatus stt )
       {   status = stt;   }
    /// Get records count in opened files
    int getRecCount() const
       {  return recList.size(); }
    /// Time of current record
    time_t Rtime() const
        {  return crt;  }


    //--- Selectors for key

    /// Return current record key in packed form
    const char *packKey()
        { return key.packKey(); }
    /// Return current record key in unpacked form
    const char *unpackKey()
        { return key.unpackKey(); }
    /// Return record key field i
    const char *keyFld( int i ) const
        { return key.keyFld(i); }
    /// Return number or record key fields
    uint keyNumFlds() const
        { return key.keyNumFlds();  }
    /// Access to TEJDBKey
    const TEJDBKey& getDBKey() const
        { return key; }
    /// Putting DB record key pkey to internal structure
    virtual void setKey( const char *ckey )
        {  key.setKey(ckey);  }
    /// Change i-th field of TEJDBKey to key
    void setKeyFld( uint i, const char *fld )
        {  key.setFldKey( i, fld ); }
    /// Return record key field name i
    const char* keyFldName(int i) const
        { return key.keyFldName(i); }
    /// Return short record key field name i
    const char* keyFldShortName(int i) const
    {   return key.keyFldShortName(i); }
    const char* keyFldDesc(int i) const
    {   return key.keyFldDesc(i); }
    /// Return record key field size
    uint keyFldSize(int i) const
    {   return key.keyFldSize(i); }
    /// Return full record key size
    int keySize() const
    {   return key.keySize(); }

};

} // namespace bsonio

#endif // TDBABSTRACT_H
