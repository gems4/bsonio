//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file nejdb.cpp
/// Implementation of class TDBString
/// for working with simple data base
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <cstring>
#include <iostream>
#include "dbdriverejdb.h"
#include "nejdb.h"
#include "v_yaml.h"

namespace bsonio {

// Save current record to bson structure
void TDBString::recToBson( bson *obj, time_t /*crtt*/, char* pars )
{
    bson_init( obj );

    if( !currentJson.empty() )
    { ParserJson pars;
      pars.setJsonText( currentJson.substr( currentJson.find_first_of('{')+1 ) );
      pars.parseObject(  obj );
    }
    else // yaml
      {
        ParserYAML::parseYAMLToBson( currentYAML, obj );
      }

    // added Modify time
    // bson_append_time_t( obj , "mtime", crtt );

    // added oid if not
    _dbdriver->addtobson( obj,  pars );
    bson_finish( obj );

    //get key from object
    string keyStr = getKeyFromBson( obj->data );
    if(keyStr.find("*")!=std::string::npos  )
    { // undefined key
      bsonioErr("TEJDB0044", keyStr+" Undifined record key" );
    }

    if( !pars )
       key.setKey( keyStr.c_str() );
    else if( (key.packKey() !=  keyStr) )
    {
       string mess = " Try to update record with changed key fields\n";
              mess += " You must use command Insert";
       bsonioErr( key.packKey(), mess );
    }
}

// Load data from bson structure (return read record key)
string TDBString::recFromBson( bson *obj )
{
    // record to json string
    ParserJson pars;
    pars.printBsonObjectToJson( currentJson, obj->data );
    // record to YAML string
    ParserYAML::printBsonObjectToYAML( currentYAML, obj->data );
    // Get key of record
    string keyStr = getKeyFromBson( obj->data );
    return keyStr;
}


// Set json format string to curent record
void TDBString::SetJsonYaml( const string& sjson, bool is_json )
{
    if( is_json )
    { currentJson = sjson;
      currentYAML = "";
    }
    else
    { currentYAML = sjson;
      currentJson = "";
    }
}

set<string> TDBString::listQueryFields( bool addkey, const vector<string>& fields)
{
   uint ii;
   set<string> queryFields;
   // add key fields
   if( addkey)
     for(uint ii=0; ii<keyNumFlds(); ii++ )
     {
         queryFields.insert( key.keyFldName(ii) );
         queryFields.insert( key.keyFldShortName(ii) );
     }
   // select view fields
   for( ii=0; ii<fields.size(); ii++ )
       queryFields.insert(  fields[ii] );

   return queryFields;
}


void TDBString::loadCollection()
{
    set<string> keys = listQueryFields(  true,  vector<string>() );
    loadCollectionFile( keys );

}

void TDBString::listKeyFromBson( const char* bsdata, char *keydata)
{
    // Get key of record
    string keyStr = getKeyFromBson( bsdata );
    key.setKey( keyStr.c_str() );
    if( key.isPattern() )
      bsonioErr("TEJDB0110", "Cannot save under record key template" );

    // make oid
    //bson_iterator it;
    //bson_find_from_buffer(&it, bsdata, JDBIDKEYNAME );
    //char *oidhex = new char[25];
    //bson_oid_to_string(bson_iterator_oid(&it), oidhex);

    // Try add key to list
    pair<KeysSet::iterator,bool> ret;
    ret = recList.insert( pair<IndexEntry,unique_ptr<char> >( key.retIndex(), unique_ptr<char>(keydata) ) );
    itrL = ret.first;
    // Test unique keys name before add the record(s)
    if( ret.second == false)
    {
        string erstr = "Cannot add new record:\n";
               erstr += keyStr;
               erstr += ".\nTwo records with the same key!";
        bsonioErr("TEJDB0014", erstr );
    }
}

// Working with collections
void TDBString::loadCollectionFile(  const set<string>& queryFields )
{
    SetReadedFunctionKey setfnc = [=]( const char* bsdata, char *keydata )
    {
       listKeyFromBson( bsdata, keydata );
    };
    _dbdriver->allQuery( getKeywd(), queryFields,  setfnc );
}

// Working with collections
void TDBString::executeQuery()
{
    // init query to fields
    set<string> fields = listQueryFields(  true,   queryResult.getQuery().getFieldsCollect() );
     queryResult.clearResults();
    SetReadedFunction setfnc = [=]( const char* bsdata )
    {
        string keyStr = getKeyFromBson( bsdata );
        queryResult.addLine( keyStr,  (const char *)bsdata, false );
    };
    _dbdriver->selectQuery( getKeywd(), fields,  queryResult.getQuery().getEJDBQuery(), setfnc );

}


// Run query
//  \param _queryJson - json string with where condition
//  \param  _queryFields - list of fields into result json ( if empty only _id )
//  \return _resultData - list of json strings with query result
void TDBString::executeQuery( const string&  qrjson,
        const vector<string>& _queryFields,  vector<string>& _resultData )
{
    // init query to fields
    set<string> fields = listQueryFields(  false,  _queryFields );

    internalData.clear();
    SetReadedFunction setfnc = [=]( const char* bsdata )
    {
        // added new record to table
        string fieldsQuery;
        jsonFromBson( bsdata, fieldsQuery );
        //cout << fieldsQuery.c_str() << endl;
        internalData.push_back(fieldsQuery);
    };

    _dbdriver->selectQuery( getKeywd(), fields, qrjson, setfnc );
    _resultData = internalData;
}

vector<string> TDBString::distinctQuery( const string& fpath, const string&  queryJson )
{
    vector<string> values;
    _dbdriver->fpathQuery( getKeywd(), fpath, queryJson, values );
    return values;
}

} // namespace bsonio
