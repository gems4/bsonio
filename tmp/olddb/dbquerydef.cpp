//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbquerydef.cpp
/// Implementation of QueryLine, DBQueryDef and DBQueryResult
/// classes to work with data base queries
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include "dbquerydef.h"

namespace bsonio {

// set data to bson
void DBQueryDef::toBson( bson *fbson )
{
     // set data to bson
     bson_init(fbson);

     bson_append_string( fbson, "name", keyName.c_str() );
     bson_append_string( fbson, "comment", comment.c_str() );
     bson_append_string( fbson, "qschema", toschema.c_str() );
     bson_append_int( fbson, "style", style );

     bson_append_start_array(fbson, "collect");
     for(uint ii=0; ii<fieldsCollect.size(); ii++)
             bson_append_string( fbson, to_string(ii).c_str(), fieldsCollect[ii].c_str() );
     bson_append_finish_array(fbson);
     //jsonToBson( &whilebson, findcond );
     //bson_append_bson( &fbson, "find", &whilebson  );
     bson_append_string( fbson, "find", findCondition.c_str()  );

     bson_finish(fbson);
}

void DBQueryDef::fromBson( const char *fbson )
{
   if(!bson_find_string( fbson, "name", keyName ) )
             keyName = "undefined";
   if(!bson_find_string( fbson, "comment", comment ) )
             comment = "";
   if(!bson_find_string( fbson, "qschema", toschema ) )
             toschema = "";
   if(!bson_find_value( fbson, "style", style ) )
             style = 0;

   fieldsCollect.clear();
   const char *arr  = bson_find_array(  fbson, "collect"  );
   bson_iterator i;
   bson_iterator_from_buffer(&i, arr);
   while (bson_iterator_next(&i))
      {
             bson_type t = bson_iterator_type(&i);
             bsonioErrIf( t != BSON_STRING, "collect", "Error file format..." );
             const char* data = bson_iterator_string(&i);
             fieldsCollect.push_back(data);
      }
   // read condition
   if( !bson_to_string( fbson, "find", findCondition ))
           findCondition = "";
}


//---------------------------------------------------------------

// Make line to view table
void DBQueryResult::bsonToValues( const char *bsdata, vector<string>& values )
{
   values.clear();
   string kbuf;

   for(uint ii=0; ii< query.getFieldsCollect().size(); ii++ )
   {
     if( !bson_to_key( bsdata, query.getFieldsCollect()[ii].c_str(), kbuf ) )
                 kbuf = "---";
     strip( kbuf );
     values.push_back(kbuf);
   }
}

// Make line to view table
void DBQueryResult::nodeToValues( SchemaNode *node, vector<string>& values )
{
   values.clear();
   string kbuf;
   SchemaNode *data;

   for(uint ii=0; ii< query.getFieldsCollect().size(); ii++ )
   {
       data = node->field(  query.getFieldsCollect()[ii] );
       if( data == nullptr || !data->getValue( kbuf ) )
            kbuf = "---";
       strip( kbuf );
       values.push_back(kbuf);
   }
}

// Add line to view table
void DBQueryResult::addLine( const string& keyStr, const char *bsdata, bool isupdate )
{
   vector<string> values;
   bsonToValues( bsdata, values );

   if( isupdate)
   { auto it =  queryValues.find(keyStr);
     if( it != queryValues.end() )
     {   it->second = values;
         return;
     }
   }

   queryValues.insert(pair<string,vector<string>>(keyStr, values ));
}

// Delete line from view table
void DBQueryResult::deleteLine( const string& keyStr )
{
    auto it =  queryValues.find(keyStr);
    if( it != queryValues.end() )
      queryValues.erase(it);
}


int DBQueryResult::getKeyValueList( vector<string>& aKeyList,
       vector<vector<string>>& aValList )
{
    aKeyList.clear();
    aValList.clear();


    auto it = queryValues.begin();
    while( it != queryValues.end() )
    {
        aKeyList.push_back( it->first );
        aValList.push_back( it->second );
        it++;
    }
    return aKeyList.size();
}

int DBQueryResult::getKeyValueList( vector<string>& aKeyList,  vector<vector<string>>& aValList,
                              const char* keypart, CompareTemplateFunction compareTemplate )
{
    aKeyList.clear();
    aValList.clear();

    auto it = queryValues.begin();
    while( it != queryValues.end() )
    {
        string key =it->first;
        if( compareTemplate(keypart, key.c_str() ) )
        {
          aKeyList.push_back( key );
          aValList.push_back( it->second );
        }
        it++;
    }
    return aKeyList.size();
}


string DBQueryResult::getKeyFromValue( const char *bsdata )
{
    vector<string> value;
    bsonToValues( bsdata, value );
    auto it = queryValues.begin();
    while( it != queryValues.end() )
    {
       if( it->second == value )
          return it->first;
       it++;
    }
    return "";
}

string DBQueryResult::getKeyFromValue( SchemaNode *node )
{
    vector<string> value;
    nodeToValues( node, value );
    auto it = queryValues.begin();
    while( it != queryValues.end() )
    {
       if( it->second == value )
          return it->first;
       it++;
    }
    return "";
}

//-----------------------------------------------------------------
QueryLine::QueryLine( int andx, const string& akey, QueryLine* parentline):
  ndx( andx), keyname( akey), type( tField ), value( "" )
{
    type = typeFromKey( keyname );
    if( !parentline )
        type = tTop;
    parent = parentline;
    if( parent)
    {
      parent->children.push_back( unique_ptr<QueryLine>(this) );
      schdata = parent->schdata;
    }
}

/*QueryLine::QueryLine( const QueryLine* data ):
  ndx( data->ndx), keyname( data->keyname), type( data->type ), value( data->value)
{
    parent = data->parent;
    // copy childrens
    for(uint ii=0; ii<data->children.size(); ii++ )
    {
        QueryLine *line = new QueryLine( data->children[ii].get() );
        line->parent = this;
        children.push_back( unique_ptr<QueryLine>(line) );
    }
}*/

QueryLine::~QueryLine()
{
    children.clear();
    // qDeleteAll(children);
}

QueryLine::fieldType QueryLine::typeFromKey( const string& akey )
{
  fieldType type_ = tField; // field name

  if( akey.empty())
      type_ = tString;
  else
    if( akey=="$exists")
     {
        type_ = tObject;
        value = "true";
     }
     else if( akey == "$begin" || akey == "$gt" || akey == "$gte"
      || akey == "$lt" || akey == "$lte" || akey == "$eq"   )
      type_ = tObject;
  else if( akey=="$and" || akey == "$or" )
    type_ = tConditionAdd;
    else if( akey=="$not" || akey == "$icase" )
       type_ = tConditionOne;
      else if( akey=="$in" || akey == "$nin" )
           type_ = tList;
         else if( akey=="$root")
            type_ = tTop;

   return type_;
}

void QueryLine::value2bson( const ThriftSchema *aschema, bson* bsonobj )
{
    string fldkey = keyname;
    int bstype = getBsonType( schdata.thrifttype );

    switch (bstype)
    {
      // impotant datatypes
      case BSON_NULL:
           bson_append_null(bsonobj, fldkey.c_str() );
           break;
      case BSON_BOOL:
           { bson_append_bool(bsonobj, fldkey.c_str(),
              ( value == "true"? true : false));
           }
           break;
      case BSON_INT:
           { int val;
             getValue( val );
             if( !schdata.enumname.empty() )
             {
              ThriftEnumDef* enumdef = aschema->getEnum( schdata.enumname );
              if( enumdef != nullptr )
              {
                  val = enumdef->getId( value );
              }
             }
             bson_append_int(bsonobj, fldkey.c_str(), val );
           }
           break;
      case BSON_LONG:
           { long val;
             getValue( val );
             bson_append_long(bsonobj, fldkey.c_str(), val );
           }
           break;
      case BSON_DOUBLE:
           { double val;
             getValue( val );
             bson_append_double(bsonobj, fldkey.c_str(), val );
           }
           break;
      case BSON_OBJECT:
      case BSON_ARRAY:
      case BSON_OID:
      case BSON_STRING:
             {
               if( value == JDBIDKEYNAME )
               {  // oid
                  if( value.empty() || value == emptiness )
                    break;
                  bson_oid_t oid;
                  bson_oid_from_string( &oid, value.c_str() );
                  bson_append_oid( bsonobj, JDBIDKEYNAME, &oid);
               }
               else
                   bson_append_string(bsonobj, fldkey.c_str(), value.c_str() );

              }
              break;
      default:
        break;
    }
}

void QueryLine::query2bson( const ThriftSchema *aschema, bson* bsonobj )
{
    uint ii;
    switch (type)
    {
      // impotant datatypes
      case tTop:
            bson_init( bsonobj );
            for(uint ii=0; ii< children.size(); ii++ )
               children[ii]->query2bson( aschema, bsonobj );
            bson_finish( bsonobj );
           break;
      case tConditionAdd:
           bson_append_start_array(bsonobj, keyname.c_str() );
           for( ii=0; ii< children.size(); ii++ )
           {   string nkey = std::to_string(ii);
               bson_append_start_object(bsonobj, nkey.c_str() );
               children[ii]->query2bson( aschema, bsonobj );
               bson_append_finish_object( bsonobj );
           }
           bson_append_finish_array( bsonobj );
           break;
      case tList:
           bson_append_start_array(bsonobj, keyname.c_str() );
           for( ii=0; ii< children.size(); ii++ )
              children[ii]->value2bson( aschema, bsonobj );
           bson_append_finish_array( bsonobj );
           break;
      case tString:
      case tObject:
           value2bson(  aschema, bsonobj );
           break;
      case tConditionOne:
      case tField:
           {
             if( children.size() > 0 )
             {
               bson_append_start_object(bsonobj, keyname.c_str() );
               for( ii=0; ii< children.size(); ii++ )
                   children[ii]->query2bson( aschema, bsonobj );
               bson_append_finish_object( bsonobj );
             }
             if(!value.empty())
               value2bson(  aschema, bsonobj );
           }
           break;
    }
}


} // namespace bsonio


