//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbbase.cpp
/// Implementation of class TDBAbstract - base interface of abstract DB chain
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include <cstring>
#include "dbbase.h"

namespace bsonio {

//-------------------------------------------------------------
// TDBAbstract - This class contains the structure of Abstract Data Base
//-------------------------------------------------------------


// Default configuration of the Data Base
TDBAbstract::TDBAbstract(  TAbstractDBDriver* adriver,
       const char* name, const vector<KeyFldsData>& keyFldsInf  ):
    _dbdriver(adriver), Keywd( name ), key( keyFldsInf ),
    status( UNDF_), defqueryJson(""), queryResult(DBQueryDef(defqueryFields))
{
    crt = time(NULL);
}


// Configurator by readind from bson object
TDBAbstract::TDBAbstract(TAbstractDBDriver* adriver, const char* bsobj):
   _dbdriver(adriver), key( bsobj ), status( UNDF_), queryResult(DBQueryDef(defqueryFields))
{
    crt = time(NULL);
    fromBson(bsobj);
}

// Open EJDB files and build linked record list
void TDBAbstract::Open()
{
    recList.clear();
    itrL= recList.end();
    loadCollection();
}

// Close files all EJDB files
void TDBAbstract::Close()
{
    closeCollection();
    recList.clear();
    itrL= recList.end();
}

// Writes data to bson
void TDBAbstract::toBson( bson *obj ) const
{
    key.toBson(obj);
    bson_append_string( obj, "Keywd", Keywd.c_str() );
    bson_append_string( obj, "QueryJson", defqueryJson.c_str() );
    bson_append_start_array(obj, "QueryFields");
    for(uint ii=0; ii<defqueryFields.size(); ii++)
       bson_append_string( obj, to_string(ii).c_str(), defqueryFields[ii].c_str() );
    bson_append_finish_array(obj);
}

// Reads data from bson
void TDBAbstract::fromBson( const char* bsobj )
{
    if(!bson_find_string( bsobj, "Keywd", Keywd ) )
        bsonioErr( "E010BSon: ", "Undefined Keywd.");
    if(!bson_find_string( bsobj, "QueryJson", defqueryJson ) )
        defqueryJson="";
    defqueryFields.clear();
    const char *arr  = bson_find_array(  bsobj, "QueryFields"  );
    bson_iterator i;
    bson_iterator_from_buffer(&i, arr);
    while (bson_iterator_next(&i))
    {
        bson_type t = bson_iterator_type(&i);
        bsonioErrIf( t != BSON_STRING, "fromBson", "Error reading cfg file..." );
        const char* data = bson_iterator_string(&i);
        defqueryFields.push_back(data);
    }
}


// Add current record key to bson structure
void TDBAbstract::addKeyToBson( bson *obj )
{
    for( uint ii=0; ii < keyNumFlds(); ii++)
       bson_append_string( obj, keyFldShortName(ii), keyFld(ii) );
}

// Get current record key from bson structure
string TDBAbstract::getKeyFromBson( const char* bsdata )
{
    //get key from object
    string keyStr = "", kbuf;
    for(uint ii=0; ii<keyNumFlds(); ii++ )
    {
        if( !bson_to_key( bsdata, keyFldShortName(ii), kbuf ) )
            if( !bson_to_key( bsdata, keyFldName(ii), kbuf ) )
                 kbuf = "*";
        strip( kbuf );
        keyStr += kbuf;
        keyStr += ":";
    }
    return keyStr;
}

// Seach record index with key pkey.
bool TDBAbstract::Find( const char *pkey )
{
    if(recList.empty() )
      return false;
    TEJDBKey wkey(key);
    wkey.setKey( pkey );
    itrL = recList.find( wkey.retIndex() );

    if( itrL == recList.end() )
        return  false;
    else
        return true;
}

// Retrive one record from the collection
void TDBAbstract::GetRecord( const char *pkey )
{
    key.setKey( pkey );
    if( key.isPattern() )
      bsonioErr("TEJDB0010", "Cannot get under record key template" );

    itrL = recList.find( key.retIndex() );
    if( itrL == recList.end() )
    {
       string erstr = pkey;
              erstr += "\nrecord to retrive does not exist!";
       bsonioErr("TEJDB0001", erstr );
    }

    // Read record from DB to bson
    bson *bsrec = _dbdriver->loadbson(getKeywd(), itrL);

     if( !bsrec )
     {  string errejdb = "Error Loading record ";
               errejdb+= pkey;
               errejdb+= " from Data Base";
        bsonioErr( "TEJDB0025",  errejdb );
     }

     // Save bson structure to internal arrays
     status = UNDF_;
     recFromBson( bsrec );
     bson_del( bsrec );

     afterLoadbson( itrL  );
     // Set up internal data
     status = ONEF_;
}

// Removes record from the collection
void TDBAbstract::DelRecord( const char *pkey )
{
    key.setKey( pkey );
    if( key.isPattern() )
      bsonioErr("TEJDB0010", "Cannot delete under record key template" );

    itrL = recList.find( key.retIndex() );
    if( itrL == recList.end() )
    {
       string erstr = pkey;
              erstr+= "\nrecord to delete does not exist!";
       bsonioErr("TEJDB0002", erstr );
    }

    beforeRmbson( itrL );
    // Remove BSON object from collection.
    bool iRet = _dbdriver->rmbson(getKeywd(), itrL);

    if( !iRet )
    {  string errejdb = "Error deleting of record ";
              errejdb+= pkey;
              errejdb+= " from Data Base ";
       bsonioErr( "TEJDB0024",  errejdb );
    }

    afterRmbson( itrL );
    // Set up internal data
    queryResult.deleteLine( key.packKey() );
    recList.erase(itrL);
    itrL = recList.end();
    status = UNDF_;
}

// Save new record in the collection
void TDBAbstract::InsertRecord( const char *pkey )
{
    // Try to insert new record to list
    if( pkey )
      key.setKey( pkey );

    bson bsrec;
    // Get bson structure from internal data
    recToBson( &bsrec, time(NULL), nullptr );

    //if( !pkey ) protect different data into bsrec and key
    key.setKey( getKeyFromBson( bsrec.data ).c_str() );
    if( key.isPattern() )
      bsonioErr("TEJDB0010", "Cannot save under record key template" );

    pair<KeysSet::iterator,bool> ret;
    ret = recList.insert( pair<IndexEntry,unique_ptr<char>>(key.retIndex(), nullptr ) );
    itrL = ret.first;
    // Test unique keys name before add the record(s)
    if( ret.second == false)
    {
        string erstr = "Cannot insert record:\n";
               erstr += key.packKey();
               erstr += ".\nTwo records with the same key!";
        bsonioErr("TEJDB0004", erstr );
    }

   try{
        beforeSaveUpdatebson( itrL, &bsrec);
       // save record to data base
       string errejdb;
       bool retSave = _dbdriver->savebson( getKeywd(), itrL, &bsrec, errejdb );
       if( !retSave )
        bsonioErr( "TEJDB0021",  errejdb );
    }
    catch(...)
    {
       bson_destroy(&bsrec);
       recList.erase(itrL);
       status = UNDF_;
       throw;
    }

    afterSaveUpdatebson( itrL, &bsrec);
    // Set up internal data
    string keyStr = getKeyFromBson( bsrec.data );
    queryResult.addLine( keyStr ,bsrec.data, false );
    bson_destroy(&bsrec);
    status = ONEF_;
}

// Save/update record in the collection
void TDBAbstract::SaveRecord(const char* pkey )
{
    // Try to insert new record to list
    key.setKey( pkey );
    if( key.isPattern() )
      bsonioErr("TEJDB0011", "Cannot save under record key template" );

   pair<KeysSet::iterator,bool> ret;
   ret = recList.insert( pair<IndexEntry,unique_ptr<char>>(key.retIndex(), nullptr ) );
   itrL = ret.first;

   bson bsrec;
   try{
       // Get bson structure from internal data
        recToBson( &bsrec, time(NULL), itrL->second.get() );

      beforeSaveUpdatebson( itrL, &bsrec);
      // save record to data base
      string errejdb;
      bool retSave = _dbdriver->savebson( getKeywd(), itrL, &bsrec, errejdb );
      if( !retSave )
       bsonioErr( "TEJDB0022",  errejdb );
   }
   catch(...)
   {
      bson_destroy(&bsrec);
      if( ret.second == true )
        recList.erase(itrL);
      throw;
   }
   afterSaveUpdatebson( itrL, &bsrec);
   // Set up internal data
   string keyStr = getKeyFromBson( bsrec.data );
   queryResult.addLine( keyStr, bsrec.data, true );
   bson_destroy(&bsrec);
   status = ONEF_;
}


//Test state of record with key key_ as template.
// in field field setted any(*) data
bool TDBAbstract::FindPart( const char *pkey, int field )
{
    TEJDBKey wkey(key);
    wkey.setKey( pkey );
    wkey.setFldKey(field,"*");
    RecStatus iRet = Rtest( wkey.packKey(), 0 );
    return ( iRet==MANY_ || iRet==ONEF_ );
}

// Test state of record with key pkey.
// If mode == 1 and one record, read this record.
// returns state of record
RecStatus TDBAbstract::Rtest( const char *key, int mode )
{
    int iRec;
    const char *pkey;
    bool OneRec = true;

    if( key != 0 && *key )
        pkey = key;
    else
        pkey = packKey();

    if( strpbrk( pkey, "*?" ))
        OneRec = false; // pattern

    status = UNDF_;

    if( getRecCount() <= 0 )
        return EMPC_;

    if( OneRec )
    {
       if( !Find( pkey )  )
            return NONE_;
       if( mode == 1 )
          GetRecord( pkey );
       return ONEF_;
    }
    else // template
    {   vector<string> aKeyList;
        iRec = GetKeyList( pkey, aKeyList, false );
        if( iRec == 0 )
            return NONE_;
        if( iRec == 1 )
        {
            if( mode == 1 )
                GetRecord( aKeyList[0].c_str() );
            return ONEF_;
        }
        return MANY_;
    }
}

int TDBAbstract::GetKeyList( const char *keypat,
        vector<string>& aKeyList, bool retUnpackform )
{
    // Set key template
    TEJDBKey wkey(key);
    wkey.setKey( keypat );
    aKeyList.clear();

    bool OneRec = !wkey.isPattern(),
         AllRecs = wkey.isAll();

    KeysSet::iterator it;
    it = recList.begin();
    while( it != recList.end() )
    {
        if( !AllRecs )
            if( !wkey.compareTemplate( it->first ) )
              { it++;
                continue;
              }
        if( retUnpackform )
           aKeyList.push_back( wkey.unpackKey( it->first ) );
        else
           aKeyList.push_back( wkey.packKey( it->first ) );
        if( OneRec )
         break;

        it++;
    }
    return aKeyList.size();
}

//-----------------------------------------------------------------


// Set json format string to curent record
void TDBAbstract::SetDefaultQueryJson( const string& qrjson)
{
    defqueryJson = /*queryJson =*/ replace_all( qrjson, "\'", "\"");
}


} // namespace bsonio
