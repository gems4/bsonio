//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbschema.cpp
/// Implementation of class TDBSchema - working with database
/// Used  SchemaNode class - API for direct code access to internal
/// DOM based on our JSON schemas.
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include "dbschema.h"
#include "v_yaml.h"

namespace bsonio {

// TDBSchema ---------------------------------------------


TDBSchema* TDBSchema::newDBClient( const ThriftSchema *aschema,
    TAbstractDBDriver* dbdriver, const string& collcName, const string& schemaName,
      const vector<KeyFldsData>& keyFldsInf, const string& queryString  )
{
   if( schemaName.empty() || collcName.empty() )
          return nullptr;

   TDBSchema* newClient =  new TDBSchema( dbdriver, collcName.c_str(),
             keyFldsInf, aschema, schemaName );

   if( !queryString.empty() )
            newClient->SetQueryJson(queryString);
   newClient->runQuery();
   newClient->Open();
   return newClient;
}


///  Constructor
TDBSchema::TDBSchema( TAbstractDBDriver* adriver, const char* name, const vector<KeyFldsData>& keyFldsInf,
                      const ThriftSchema* aschema, const string& aschemaName  ):
  TDBAbstract( adriver, name, keyFldsInf ),
  schema(aschema), schemaName(aschemaName), currentData(nullptr)
{
   resetSchema( aschemaName, true );
   // set query data
}


/// Constructor from bson data
TDBSchema::TDBSchema( TAbstractDBDriver* adriver, const char* bsobj, const ThriftSchema* aschema ):
    TDBAbstract( adriver, bsobj ), schema(aschema), currentData(nullptr)
{
    fromBson(bsobj);
    resetSchema( schemaName, true );
    // set query data
    SetDefaultQueryFields(makeDefaultQueryFields(schemaName));
}


// Writes DOD data to ostream file
void TDBSchema::toBson( bson *obj ) const
{
    TDBAbstract::toBson( obj );
    bson_append_string( obj, "SchemaName", schemaName.c_str() );
}

// Reads DOD data from istream file
void TDBSchema::fromBson( const char* bsobj )
{
    TDBAbstract::fromBson( bsobj );
    if(!bson_find_string( bsobj, "SchemaName", schemaName ) )
        bsonioErr( "TGRDB0001: ", "Undefined SchemaName.");
}


vector<string>  TDBSchema::makeDefaultQueryFields(const string& schName )
{
   vector<string> keyFields;
   uint ii;
   ThriftFieldDef*  fldDef;

   ThriftStructDef* strDef = schema->getStruct(schName);
   if( strDef == nullptr )
       return keyFields;

   if( strDef->getSelectSize() > 0 )
     {
       string fldKey, to_select_name, to_select_doc;
       for( ii=0; ii<strDef->getSelectSize(); ii++ )
       {
         fldKey = strDef->keyFldIdName(ii);
         schema->getSelectData(fldKey, strDef, to_select_name, to_select_doc );
         keyFields.push_back(to_select_name);
       }
     }
     else // old type of keys
      {
        for( ii=0; ii<strDef->fields.size(); ii++ )
        {
           fldDef = &strDef->fields[ii];
           if( fldDef->fRequired == fld_required )
               keyFields.push_back(fldDef->fName);
        }
      }
   return keyFields;
}


void TDBSchema::resetSchema( const string& aschemaName, bool change_queries )
{
    //if( schemaName != aschemaName )
    {
      schemaName = aschemaName;
      currentData.reset( SchemaNode::newSchemaStruct( schemaName ) );
    }
    if( change_queries )
    {
        SetDefaultQueryFields(makeDefaultQueryFields(schemaName));
        queryResult.setFieldsCollect(defqueryFields);
        queryResult.setToSchema(schemaName);
    }
}


void TDBSchema::recToBson( bson *obj, time_t /*crtt*/, char* pars )
{
     // test oid
    SchemaNode* oidNode = currentData.get()->field("_id");
    string oidval;
    if(oidNode!=nullptr)
       oidNode->getValue( oidval );

    if( oidNode==nullptr ) // no "_id" into schema definition
    {
       bson bsdata;
        // make bson from current data
       currentData.get()->getStructBson( &bsdata );
       bson bskey;
       bson_init(&bskey);
       _dbdriver->addtobson( &bskey,  pars );
       bson_finish(&bskey);
       bson_init( obj );
       bson_merge(&bsdata, &bskey, false, obj);
       bson_finish( obj);
       bson_destroy(&bsdata);
       bson_destroy(&bskey);
    }
    else
    {
       if( pars )
       {
        if( !oidval.empty() )
        {
          if( oidval != string((char *)pars) )
            bsonioErr("TGRDB0003", "Changed record oid " + string((char *)pars) );
         } else // add oid
          {
              oidNode->setValue( string((char *)pars) );
          }
       }
       else
            if( oidval.empty() )
            {
                oidNode->setValue( genOid() ); // new oid
            }
        // make bson from current data
        currentData.get()->getStructBson( obj );
    }

}

// Load data from bson structure (return read record key)
string TDBSchema::recFromBson( bson *obj )
{
    currentData.get()->setStructBson( obj->data );
    // Get key of record
    string keyStr = getKeyFromBson( obj->data );
    return keyStr;
}

// Return curent record in json format string
string TDBSchema::GetJson()
{
  bson obj;
  GetBson( &obj );
  string jsonstr;
  jsonFromBson( obj.data, jsonstr );
  bson_destroy(&obj);
  return  jsonstr;
}

// Return curent record in YAML format string
string TDBSchema::GetYAML()
{
    bson obj;
    GetBson( &obj );
    string yamlstr;
    // record to YAML string
    ParserYAML::printBsonObjectToYAML( yamlstr, obj.data );
    bson_destroy(&obj);
    return  yamlstr;
}


// Set json format string to curent record
void TDBSchema::SetJsonYaml( const string& sjson, bool is_json )
{
    bson obj;

    if( is_json )  // json
     jsonToBson( &obj, sjson );
    else // yaml
      {
        bson_init(&obj);
        ParserYAML::parseYAMLToBson(sjson, &obj );
        bson_finish(&obj);
      }
    SetBson( &obj );
    bson_destroy(&obj);
}

set<string> TDBSchema::listQueryFields( bool addkey, const vector<string>& fields)
{
   uint ii;
   set<string> queryFields;
   // add key fields
   if( addkey)
     for(uint ii=0; ii<keyNumFlds(); ii++ )
     {
         queryFields.insert( key.keyFldName(ii) );
         queryFields.insert( key.keyFldShortName(ii) );
     }
   // select view fields
   for( ii=0; ii<fields.size(); ii++ )
       queryFields.insert(  fields[ii] );

   return queryFields;
}


void TDBSchema::loadCollection()
{
    set<string> keys = listQueryFields(  true,  vector<string>() );
    loadCollectionFile( keys );
}

// Load records key from bson structure
void TDBSchema::listKeyFromBson( const char* bsdata, char*keydata)
{
    // Get key of record
    string keyStr = getKeyFromBson( bsdata );
    key.setKey( keyStr.c_str() );
    if( key.isPattern() )
      bsonioErr("TGRDB0005", "Cannot save under record key template" );


    // Try add key to list
    pair<KeysSet::iterator,bool> ret;
    ret = recList.insert( pair<IndexEntry,unique_ptr<char> >( key.retIndex(), unique_ptr<char>(keydata) ) );
    itrL = ret.first;
    // Test unique keys name before add the record(s)
    if( ret.second == false)
    {
        string erstr = "Cannot add new record:\n";
               erstr += keyStr;
               erstr += ".\nTwo records with the same key!";
        bsonioErr("TGRDB0006", erstr );
    }
}

// Working with collections
void TDBSchema::loadCollectionFile(  const set<string>& queryFields )
{
    SetReadedFunctionKey setfnc = [=]( const char* bsdata, char*keydata )
    {
        listKeyFromBson( bsdata, keydata );
    };
    _dbdriver->allQuery( getKeywd(), queryFields, setfnc );
}

// Working with collections
void TDBSchema::executeQuery()
{
    // init query to fields
    set<string> fields = listQueryFields(  true,   queryResult.getQuery().getFieldsCollect() );
     queryResult.clearResults();
    SetReadedFunction setfnc = [=]( const char* bsdata )
    {
        string keyStr = getKeyFromBson( bsdata );
        queryResult.addLine( keyStr,  (const char *)bsdata, false );
    };
    _dbdriver->selectQuery( getKeywd(), fields,  queryResult.getQuery().getEJDBQuery(), setfnc );
}


// Run query
//  \param _queryJson - json string with where condition
//  \param  _queryFields - list of fields into result json ( if empty only _id )
//  \return _resultData - list of json strings with query result
void TDBSchema::executeQuery( const string&  qrjson,
        const vector<string>& _queryFields,  vector<string>& _resultData )
{
    // init query to fields
    set<string> fields = listQueryFields(  false,  _queryFields );

    internalData.clear();
    SetReadedFunction setfnc = [=]( const char* bsdata )
    {
        // added new record to table
        string fieldsQuery;
        jsonFromBson( bsdata, fieldsQuery );
        //cout << fieldsQuery.c_str() << endl;
        internalData.push_back(fieldsQuery);
    };

    _dbdriver->selectQuery( getKeywd(), fields, qrjson, setfnc );
    _resultData = internalData;
}

vector<string> TDBSchema::distinctQuery( const string& fpath, const string&  queryJson )
{
   vector<string> values;
   _dbdriver->fpathQuery( getKeywd(), fpath, queryJson, values );
   return values;
}

void TDBSchema::SaveBson( bson* obj, bool overwrite, bool testValues )
{
  string key = SetBson(obj);

  if( !Find( key.c_str() ))
  {
     if(overwrite )
     {
       if( !( key.empty() ||  key.find_first_of("*?") != std::string::npos )  )
       {
         SaveRecord( key.c_str() );
         return;
       }
       if( testValues )
       {
           key = GetKeyFromValue(obj->data);
           if( !key.empty() )
           {
             SaveRecord( key.c_str() );
             return;
           }
       }
     }
     InsertRecord();
  }
  else
  {
    if( overwrite )
      SaveRecord( key.c_str() );
  }
}

string TDBSchema::InsertCurrent( bool testValues )
{
   if( testValues )
   {
     string key = GetKeyFromValueNode(currentData.get());
     if( !key.empty() )
     {
        SaveRecord( key.c_str() );
        strip_all( key, ":" );
        return key;
      }
   }
   string stroid = genOid();
   setValue("_id", stroid );
   InsertRecord();
   return stroid;
}

// Get current record key from internal data
string TDBSchema::getKeyFromCurrent()
{
    string keyStr = "", kbuf;
    for(uint ii=0; ii<keyNumFlds(); ii++ )
    {
        if( !getValue( keyFldShortName(ii), kbuf  ) )
            if( !getValue( keyFldName(ii), kbuf  ) )
          kbuf = "---";
        strip( kbuf );
        if( kbuf.empty() )
           kbuf = "*";
        keyStr += kbuf;
        keyStr += ":";
    }
    return keyStr;
}


void TDBSchema::SaveCurrent( bool overwrite, bool testValues )
{
  string key = getKeyFromCurrent();

  if( !Find( key.c_str() ))
  {
     if(overwrite )
     {
       if( !( key.empty() ||  key.find_first_of("*?") != std::string::npos )  )
       {
         SaveRecord( key.c_str() );
         return;
       }
       InsertCurrent( testValues );
       return;
     }
     InsertRecord();
  }
  else
  {
    if( overwrite )
      SaveRecord( key.c_str() );
  }
}

} // namespace bsonio
