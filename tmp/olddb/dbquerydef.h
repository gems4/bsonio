//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbquerydef.h
/// Declaration of QueryLine, DBQueryDef and DBQueryResult
/// classes to work with data base queries
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef DBQUERYDEF_H
#define DBQUERYDEF_H

#include <vector>
#include <map>
#include <memory>
#include "ar2base.h"
#include "thrift_node.h"

namespace bsonio {

using TableOfValues = map<string, vector<string> >;
using  CompareTemplateFunction = std::function<bool( const char* keypat, const char* akey )>;


struct SchInternalData
{
    int thrifttype = 0;   ///< Thrift type of field
    string enumname = ""; ///< Name of enum
};

/// \class QueryLine represents a query item in a tree view.
/// Item data defines one query object
class QueryLine
{

public:

    enum fieldType
    { tTop, tField, tConditionAdd, tConditionOne, tList, tObject, tString };

    QueryLine( int ndx, const string& akey, QueryLine* parentline );
    // QueryLine( const QueryLine* data );
    ~QueryLine();
    void query2bson( const ThriftSchema *aschema, bson* bsonobj );

    int ndx;
    string keyname; ///< Name of field
    fieldType type; ///< Type of field
    string value;   ///< Value of field
    SchInternalData schdata; ///< Internal data from thrift schema

    QueryLine *parent;
    vector<unique_ptr<QueryLine>> children;

private:

    /// Get Value from string
    /// If field is not type T, the false will be returned.
    template <class T>
    bool getValue( T& val  )
    {   return  bsonio::TArray2Base::string2value( value, val );  }

    fieldType typeFromKey( const string& akey );
    void value2bson( const ThriftSchema *aschema, bson* bsonobj );
};


/// \class DBQueryDef description query into Database
class DBQueryDef
{

  string keyName;          ///< Short name of query (used as key field)
  string comment;          ///< Description of query
  string toschema;         ///< Schema for query about
  int    style;            ///<  Query style (QueryType)
  string findCondition;          ///< String with query in json format
  vector<string> fieldsCollect;  ///< List of fieldpaths to collect

  // internal
  QueryLine *treeDescription=0;

public:

  enum QueryType
  { qEJDB = 0, qSparQl /* now optional */ };

  DBQueryDef( const vector<string>& fields, const string& condition = "", int astyle = qEJDB ):
   style(astyle), findCondition(condition),  fieldsCollect(fields)
  { }
  DBQueryDef( const string& qschema, const vector<string>& fields,
              const string& condition = "", int astyle = qEJDB ):
   toschema(qschema), style(astyle), findCondition(condition),  fieldsCollect(fields)
  { }

  void toBson( bson *fbson );
  void fromBson( const char *fbson );

  void setKeyName( const string& name )
    { keyName = name;  }
  const string& getKeyName() const
    { return keyName; }

  void setComment( const string& comm )
    { comment = comm;  }
  const string& getComment() const
    { return comment; }

  void setToSchema( const string& shname )
    { toschema = shname;  }
  const string& getToSchema() const
    { return toschema; }

  void setStyle( int astyle )
    { style = astyle;  }
  int getStyle() const
    { return style; }

  void setEJDBQuery( const string& query )
  {
    style = qEJDB;
    findCondition =  replace_all( query, "\'", "\"");
  }
  string getEJDBQuery() const
   {
      if(style == qEJDB)
         return findCondition;
      else
         return "";
   }

  void setFieldsCollect(const vector<string>& collect )
    { fieldsCollect = collect;  }
  void addFieldsCollect(const vector<string>& collect )
    { fieldsCollect.insert(fieldsCollect.end(), collect.begin(), collect.end()); }
  const vector<string>& getFieldsCollect() const
    { return fieldsCollect; }

};

/// \class  DBQueryResult used to store query definition and result
class DBQueryResult
{

  DBQueryDef query;
  TableOfValues  queryValues;    ///< Table of values were gotten from query

  /// Make line to view table
  void bsonToValues( const char *bsdata, vector<string>& values );
  void nodeToValues( SchemaNode *node, vector<string>& values );

 public:

  DBQueryResult( const DBQueryDef& aquery ):
      query(aquery)  {}
  ~DBQueryResult() {}


  const DBQueryDef& getQuery()
  {
     return query;
  }
  void setQuery( const DBQueryDef& qrdef)
  {
     query = qrdef;
  }
  void setEJDBQuery( const string& jsonquery)
  {
     query.setEJDBQuery(jsonquery);
  }
  void setFieldsCollect(const vector<string>& collect )
  {
     query.setFieldsCollect(collect);
  }
  /// Define schema for query about
  void setToSchema(const string& schemaname )
  {
     query.setToSchema(schemaname);
  }

  /// Add line to view table
  void addLine( const string& keyStr, const char *bsdata, bool isupdate );
  /// Delete line from view table
  void deleteLine( const string& keyStr );

  /// Get query result table
  const TableOfValues& getQueryResult()
  {
     return queryValues;
  }
  /// Clear result table
  void clearResults()
  {
    queryValues.clear();
  }

  ///  Get all keys list for current query
  int getKeyValueList( vector<string>& aKeyList,
         vector<vector<string>>& aValList );

  /// Get keys list for current query and defined condition function
  int getKeyValueList( vector<string>& aKeyList,
         vector<vector<string>>& aValList, const char* keypart, CompareTemplateFunction compareTemplate );

  /// Extract key from data
  string getKeyFromValue( const char *bsdata );
  /// Extract key from data
  string getKeyFromValue( SchemaNode *node );

};

} // namespace bsonio


#endif // DBQUERYDEF_H
