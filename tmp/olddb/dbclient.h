//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbclient.h
/// Declarations of class
/// TDBClient - DB driver for working with thrift server data base
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TDBCLIEBT_H
#define TDBCLIEBT_H

#include "dbbase.h"
#include "DBServer.h"

namespace bsonio {

/// Definition of graph database chain used EJDB (?) server
class TDBClient : public TAbstractDBDriver
{
    /// The host that the socket is connected to
    std::string _host;
    /// The port that the socket is connected to
    int _port;
    boost::shared_ptr<apache::thrift::transport::TTransport> trans;
    dbserver::DBServerClient* dbClient = 0;

    void getServerKey( string& key, char* pars )
    {
      key = (char *)pars;
    }
    void setServerKey( const string& key, KeysSet::iterator& itr )
    {
        char *bytes = new char[25];
        strncpy( bytes, key.c_str(), 25 );
        itr->second = unique_ptr<char>(bytes);
    }

public:

    ///  Constructor
    TDBClient( const std::string& theHost, int thePort )
    { resetDBServerClient(theHost, thePort); }

    virtual dbserver::DBServerClient* resetDBServerClient( const std::string& theHost, int thePort);

    ///  Destructor
   virtual ~TDBClient()
    {
       if(dbClient)
         delete dbClient;
    }

   /// Gen new oid or other pointer of location
   virtual string genOid(const char* collname);

   /// Read record from DB to bson
   bson *loadbson( const char* collname,  KeysSet::iterator& it);
   /// Remove current object from collection.
   bool rmbson( const char* collname, KeysSet::iterator& it);
   /// Write record to DB
   bool savebson( const char* collname, KeysSet::iterator& it, bson *bsrec, string& errejdb );

   /// Run query
   ///  \param collname - collection name
   ///  \param queryFields - list of fileds selection
   ///  \param queryJson - json string with query  condition (where)
   ///  \param setfnc - function for set up readed data
   void selectQuery( const char* collname, const set<string>& queryFields,
         const string&  queryJson,  SetReadedFunction setfnc );

   /// Run query for read Linked records list
   ///  \param collname - collection name
   ///  \param queryFields - list of fileds selection
   ///  \param setfnc - function for set up Linked records list
    void allQuery( const char* collname,
           const set<string>& queryFields,  SetReadedFunctionKey setfnc );

   /// Run delete query
   ///  \param collname - collection name
   ///  \param query - qury  condition (where)
    void delQuery(const char* /*collname*/, bson* /*query*/ );

    ///  Provides 'distinct' operation over query
    ///  \param fpath Field path to collect distinct values from.
    ///  \param queryJson - json string with where condition
    ///  \param return values by specified fpath and query
    void fpathQuery( const char* collname, const string& fpath,
                     const string& queryJson, vector<string>& values );

};


/// Definition of graph database chain used EJDB (?) server
class TDBClientOne : public TDBClient
{

 private:

    ///  Constructor
    TDBClientOne():TDBClient( theHost, thePort )
    {}

 public:

    /// The host that the socket is connected to
   static std::string theHost;

   /// The port that the socket is connected to
   static int thePort;

   ///  Destructor
   virtual ~TDBClientOne() {}

   friend TDBClientOne& dbClientOne();

};

/// Use only one client object
TDBClientOne& dbClientOne();


} // namespace bsonio

#endif // TDBCLIEBT_H
