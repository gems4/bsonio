//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbgraph.cpp
/// Implementation of classes TGraphAbstract - working with graph databases (OLTP)
/// TGraphEJDB - working with internal EJDB data base in graph mode
/// Used  SchemaNode class - API for direct code access to internal
/// DOM based on our JSON schemas.
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include "dbgraph.h"
#include "v_yaml.h"
#include <regex>

namespace bsonio {

// TGraphAbstract ---------------------------------------------

vector<KeyFldsData> keyFldsInf = { KeyFldsData( "_id", "_id", "Only id" )};

TDBGraph* TDBGraph::newDBClient( const ThriftSchema *aschema,
    TAbstractDBDriver* dbdriver, const string& collcName,
    const string& schemaName, const string& queryString  )
{
   if( schemaName.empty() || collcName.empty() )
          return nullptr;

   TDBGraph* newClient  =  new TDBGraph( dbdriver, collcName.c_str(),
                                    aschema, schemaName );

   if( !queryString.empty() )
            newClient->SetQueryJson(queryString);
   newClient->runQuery();
   newClient->Open();
   return newClient;
}

///  Constructor
TDBGraph::TDBGraph( TAbstractDBDriver* adriver, const char* name, const ThriftSchema* aschema, const string& aschemaName  ):
  TDBSchema( adriver, name, keyFldsInf, aschema, aschemaName ),
  changeSchemaMode(false)
{
   resetSchema( aschemaName, true );
}


/// Constructor from bson data
TDBGraph::TDBGraph( TAbstractDBDriver* adriver, const char* bsobj, const ThriftSchema* aschema ):
    TDBSchema( adriver, bsobj, aschema )
{
    resetSchema( schemaName, true );
    // set query data
    SetDefaultQueryFields(makeDefaultQueryFields(schemaName));
    SetDefaultQueryJson(makeDefaultQuery());
}

string TDBGraph::makeDefaultQuery()
{
   string queryJson =  "{'_label': '";
    queryJson += _label;
    queryJson += "' }";

   return queryJson;
}

void TDBGraph::resetSchema( const string& aschemaName, bool change_queries )
{
    //if( schemaName != aschemaName )
    {
      schemaName = aschemaName;
      currentData.reset( SchemaNode::newSchemaStruct( schemaName ) );
      schemaType = rUndef;
      SchemaNode* type_node = currentData->field("_type");
      _type = "";
      if( type_node != nullptr )
      {
        type_node->getValue(_type);
        if( _type == "edge")
           schemaType = rEdge;
        else
            if( _type == "vertex")
               schemaType = rVertex;
       }
       type_node = currentData->field("_label");
       _label = "";
       if( type_node != nullptr )
         type_node->getValue(_label);
    }
    if( change_queries )
    {
        SetDefaultQueryFields(makeDefaultQueryFields(schemaName));
        SetDefaultQueryJson(makeDefaultQuery());
        queryResult.setFieldsCollect(defqueryFields);
        queryResult.setEJDBQuery(defqueryJson);
        queryResult.setToSchema(schemaName);
    }

    // load unique map
    loadUniqueFields();
}

// Test true type and label for schema
void TDBGraph::testSchema( const char *bsobj )
{
    string newtype, newlabel;

    if(!bson_find_string( bsobj, "_type", newtype ) )
        newtype = "";
    if(!bson_find_string( bsobj, "_label", newlabel ) )
        newlabel = "";

    if( changeSchemaMode )
    {
      string newSchema;
      if( newtype == "edge")
       newSchema = schema->getEdgeName(newlabel);
      else
        if( newtype == "vertex")
         newSchema = schema->getVertexName(newlabel);
      bsonioErrIf(newSchema.empty(),  "TGRDB0007",
                  "Undefined record: " + newtype + " type " + newlabel + " label" );
      resetSchema( newSchema, false );
    } else
      {
        if( newtype != _type || newlabel != _label )
          bsonioErr("TGRDB0004", "Illegal record type: " + newtype + " or label: " + newlabel );
      }
}


void TDBGraph::loadUniqueFields()
{
    uniqueFieldsNames.clear();
    uniqueFieldsValues.clear();

    ThriftStructDef* strDef = schema->getStruct(schemaName);
    if( strDef == nullptr )
        return;

    uniqueFieldsNames = strDef->getUniqueList();
    if( uniqueFieldsNames.empty() )
       return;

    uniqueFieldsNames.push_back("_id");
    ValuesTable uniqueVal = loadRecords( makeDefaultQuery(), uniqueFieldsNames );

    for( auto row: uniqueVal)
    {
      auto idkey = row.back();
      row.pop_back();

      if( !uniqueFieldsValues.insert( std::pair< vector<string>, string>(row, idkey)).second )
      {
          cout << "Not unique values: " << string_from_vector( row ) << endl;
      }
   }
}

void TDBGraph::recToBson( bson *obj, time_t /*crtt*/, char* pars )
{
     // test oid
    SchemaNode* oidNode = currentData.get()->field("_id");
    bsonioErrIf(oidNode==nullptr, "TGRDB0002", "No oid in record" );
    string oidval;
    oidNode->getValue( oidval );

    if( pars )
    {
        if( !oidval.empty() )
        {
          if( oidval != string((char *)pars) )
            bsonioErr("TGRDB0003", "Changed record oid " + string((char *)pars) );
        } else // add oid
          {
            oidNode->setValue( string((char *)pars) );
          }
     }
     else
         if( oidval.empty() )
         {
             oidNode->setValue( genOid() ); // new oid
         }

    // added Modify time ??? Special strucrure use
    // bson_append_time_t( obj , "mtime", crtt );

    // make bson from current data
    currentData.get()->getStructBson( obj );
}

// Load data from bson structure (return read record key)
string TDBGraph::recFromBson( bson *obj )
{
    testSchema( obj->data );
    currentData.get()->setStructBson( obj->data );
    // Get key of record
    string keyStr = getKeyFromBson( obj->data );
    return keyStr;
}

/*void TGraphAbstract::buildFieldsQuery(  bson* bshits1, bool addkey, const vector<string>& _queryFields )
{
   // init query to fields
   uint ii;
   bson_init_as_query(bshits1);
   bson_append_start_object(bshits1, "$fields");
   bson_append_int(bshits1, JDBIDKEYNAME, 1);
   // add key fields
   if( addkey)
     for( ii=0; ii<keyNumFlds(); ii++ )
       bson_append_int(bshits1, key.keyFldName(ii), 1);
   // select view fields
   for( ii=0; ii<_queryFields.size(); ii++ )
       bson_append_int(bshits1, _queryFields[ii].c_str(), 1);
    bson_append_finish_object(bshits1);
    bson_finish(bshits1);
}*/


void TDBGraph::beforeRmbson( KeysSet::iterator& itr )
{
    string  _vertexid =itr->second.get();

    // init query to fields
    //bson bshits1;
    //bson_init_as_query(&bshits1);
    //bson_append_start_object(&bshits1, "$fields");
    //bson_append_int(&bshits1, JDBIDKEYNAME, 1);
    //bson_append_finish_object(&bshits1);
    //bson_finish(&bshits1);

    // delete all edges query
    bson bsq1;
    bson_init_as_query(&bsq1);
    bson_append_string(&bsq1, "_type", "edge");
    bson_append_start_array(&bsq1, "$or");
    bson_append_start_object(&bsq1, "0" );
    bson_append_string(&bsq1, "_inV", _vertexid.c_str() );
    bson_append_finish_object( &bsq1 );
    bson_append_start_object(&bsq1, "1" );
    bson_append_string(&bsq1, "_outV", _vertexid.c_str() );
    bson_append_finish_object( &bsq1 );
    bson_append_finish_array(&bsq1);
    bson_append_bool(&bsq1, "$dropall", true);
    bson_finish(&bsq1);

    _dbdriver->delQuery( getKeywd(), &bsq1 );

    //bson_destroy(&bshits1);
    bson_destroy(&bsq1);
}

void TDBGraph::afterRmbson( KeysSet::iterator& itr )
{

    // delete from unique map
    if( !uniqueFieldsNames.empty() )
    {
      string  _vertexid =itr->second.get();
      auto itrow = uniqueLinebyId( _vertexid );
      if( itrow != uniqueFieldsValues.end() )
         uniqueFieldsValues.erase(itrow);
    }
}

UniqueFieldsMap::iterator TDBGraph::uniqueLinebyId( const string& idschem )
{
    UniqueFieldsMap::iterator itrow = uniqueFieldsValues.begin();
    while( itrow != uniqueFieldsValues.end() )
    {
      if( itrow->second == idschem )
          break;
      itrow++;
    }
    return itrow;
}

void TDBGraph::beforeSaveUpdatebson( KeysSet::iterator& , bson* bsrec )
{
  if( !uniqueFieldsNames.empty() )
  {
     FieldSetMap uniqfields = extractFields( uniqueFieldsNames, bsrec );
     vector<string> uniqValues;
     for( uint ii=0; ii<uniqueFieldsNames.size()-1; ii++ )
       uniqValues.push_back( uniqfields[uniqueFieldsNames[ii]] );

     auto itfind = uniqueFieldsValues.find(uniqValues);
     if( itfind != uniqueFieldsValues.end() )
     {
       if( itfind->second != uniqfields["_id"] )
         bsonioErr("TGRDB0009", "Not unique values: " + string_from_vector( uniqValues ) );
     }
  }
}

void TDBGraph::afterSaveUpdatebson( KeysSet::iterator& , bson* bsrec )
{
  if( !uniqueFieldsNames.empty() )
  {
      FieldSetMap uniqfields = extractFields( uniqueFieldsNames, bsrec );

      // delete old
      auto itrow = uniqueLinebyId( uniqfields["_id"] );
      if( itrow != uniqueFieldsValues.end() )
         uniqueFieldsValues.erase(itrow);

      // insert new
      vector<string> uniqValues;
      for( uint ii=0; ii<uniqueFieldsNames.size()-1; ii++ )
         uniqValues.push_back( uniqfields[uniqueFieldsNames[ii]] );
      if( !uniqueFieldsValues.insert( std::pair< vector<string>, string>(uniqValues, uniqfields["_id"])).second )
      {
          cout << "Not unique values: " << string_from_vector( uniqValues ) << endl;
      }
  }
}

// Test existence records
bool TDBGraph::existKeysByQuery( const string& query )
{
    vector<string> _queryFields = { "_id"};
    vector<string> _resultData;
    runQuery( query,  _queryFields, _resultData );
    return !_resultData.empty();
}

// Build keys list by query
vector<string> TDBGraph::getKeysByQuery( const string& query )
{
    vector<string> _keys;
    vector<string> _queryFields = { "_id"};
    vector<string> _resultData;
    runQuery( query,  _queryFields, _resultData );

    std::regex re("\\{\\s*\"_id\"\\s*:\\s*\"([^\"]*)\"\\s*\\}");
    std::string replacement = "$1:";
    for( uint ii=0; ii<_resultData.size(); ii++)
    {
      string akey = std::regex_replace(_resultData[ii], re, replacement);
      //cout << _resultData[ii] << "      " << akey << endl;
      _keys.push_back(akey);
    }
    return _keys;
}

// Extract the string value by key from query
string TDBGraph::extractToken( const string& key, const string& query )
{
  string token;
  string query2 =  replace_all( query, "\'", "\"");
  string regstr =  string(".*\"")+key+"\"\\s*:\\s*\"([^\"]*)\".*";
  std::regex re( regstr );
  std::smatch match;

  if( std::regex_search( query2, match, re ))
  {
     if (match.ready())
        token = match[1];
  }
  //cout << key << "  token " << token  << endl;
  return token;
}

// Extract label by id
string TDBGraph::extractLabelById( const string& id )
{
  string token;
  string query =  idQuery( id );
  vector<string> _queryFields = { "_label"};
  vector<string> _resultData;
  runQuery( query,  _queryFields, _resultData );
  if( _resultData.size()>0  )
    token = extractToken( "_label", _resultData[0] );
  return token;
}

string TDBGraph::getSchemaFromKey( const string& id  )
{
    GetRecord( (id+":").c_str() );
    string label, schemaName = "";
    getValue("_label", label);
    if( !( label.empty() ) )
       schemaName = schema->getVertexName(label);
    return schemaName;
}

// build functions

// Define new Vertex
void TDBGraph::setVertex( const string& aschemaName, const FieldSetMap& fldvalues )
{
    // check schema
    if( schemaName != aschemaName )
    {
      if( changeSchemaMode )
          resetSchema( aschemaName, false );
      else
        bsonioErr("TGRDB0007", "Illegal record schame: " + aschemaName + " current schema: " + schemaName );
    }
   currentData->clearNode(); // set default values

   for(auto const &ent : fldvalues)
     setValue( ent.first, ent.second  );
}

// Update current schema data
void TDBGraph::updateVertex( const string& aschemaName, const FieldSetMap& fldvalues )
{
   // check schema
   if( schemaName != aschemaName )
        bsonioErr("TGRDB0008", "Illegal record schame: " + aschemaName + " current schema: " + schemaName );
   for(auto const &ent : fldvalues)
     setValue( ent.first, ent.second  );
}


// Define new Edge
void TDBGraph::setEdge( const string& aschemaName, const string& outV,
              const string& inV, const FieldSetMap& fldvalues )
{
    // check schema
    if( schemaName != aschemaName )
    {
      if( changeSchemaMode )
          resetSchema( aschemaName, false );
      else
        bsonioErr("TGRDB0007", "Illegal record schame: " + aschemaName + " current schema: " + schemaName );
    }

   currentData->clearNode(); // set default values
   setValue("_inV", inV);
   setValue("_outV", outV);
   for(auto const &ent : fldvalues)
     setValue( ent.first, ent.second  );
}

ValuesTable TDBGraph::loadRecords( const string& query, const vector<string>& queryFields )
{
    ValuesTable recordsValues;
    FieldSetMap  fldsValues;

    vector<string> resultData, rowData;
    runQuery( query,  queryFields, resultData );

    for( const auto& subitem: resultData )
    {
      fldsValues = extractFields( queryFields, subitem );

      rowData.clear();
      for( const auto& fld: queryFields)
        rowData.push_back( fldsValues[fld] );
      recordsValues.push_back(rowData);
    }

    return recordsValues;
}

ValuesTable TDBGraph::loadRecords( const vector<string>& ids, const vector<string>& queryFields )
{
    ValuesTable recordsValues;
    FieldSetMap  fldsValues;
    vector<string> rowData;

    for( const auto& id_: ids )
    {
      bson bobj;
      GetRecord( (id_+":").c_str() );
      GetBson( &bobj );
      fldsValues = extractFields( queryFields, &bobj );

      rowData.clear();
      for( const auto& fld: queryFields)
        rowData.push_back( fldsValues[fld] );
      recordsValues.push_back(rowData);
      bson_destroy(&bobj);
    }

    return recordsValues;
}

FieldSetMap TDBGraph::loadRecordFields( const string& id, const vector<string>& queryFields )
{
    bson bobj;
    GetRecord( (id+":").c_str() );
    GetBson( &bobj );
    FieldSetMap fldsValues = extractFields( queryFields, &bobj );
    bson_destroy(&bobj);
    return fldsValues;
}

// extern functions ---------------------------------------

// Extract the string value by key from query
int extractIntField( const string& key, const string& jsondata )
{
  string token;
  string query2 =  replace_all( jsondata, "\'", "\"");
  string regstr =  string(".*\"")+key+"\"\\s*:\\s*([+-]?[1-9]\\d*|0).*";
  std::regex re( regstr );
  std::smatch match;

  if( std::regex_search( query2, match, re ))
  {
     if (match.ready())
        token = match[1];
  }
  //cout << key << "  token " << token  << endl;
  return stoi(token);
}

// Extract the string value by key from query
string extractStringField( const string& key, const string& jsondata )
{
  string token;
  string query2 =  replace_all( jsondata, "\'", "\"");
  string regstr =  string(".*\"")+key+"\"\\s*:\\s*\"([^\"]*)\".*";
  std::regex re( regstr );
  std::smatch match;

  if( std::regex_search( query2, match, re ))
  {
     if (match.ready())
        token = match[1];
  }
  //cout << key << "  token " << token  << endl;
  return token;
}

FieldSetMap extractFields( const vector<string> queryFields, bson *bobj )
{
    string valbuf;
    FieldSetMap res;
    for( auto& ent: queryFields )
    {
        bson_to_key( bobj->data, ent, valbuf );
        res[ ent] = valbuf;
    }
    return res;
}


FieldSetMap extractFields( const vector<string> queryFields, const string& jsondata )
{
    bson bobj;
    jsonToBson(&bobj, jsondata);
    FieldSetMap res = extractFields( queryFields, &bobj );
    bson_destroy(&bobj);
    return res;
}

void addFieldsToQuery( string& query, const FieldSetMap& fldvalues )
{
   cout << query << endl;

   bson oldobj;
   jsonToBson(&oldobj, query);

   bson addobj;
   bson_init_as_query(&addobj);
   for(auto& ent : fldvalues)
    addScalar( ent.first.c_str(), ent.second, &addobj );
   bson_finish(&addobj);

   // add two bson
   bson fullobj;
   bson_init_as_query(&fullobj);
   bson_merge(&oldobj, &addobj, false, &fullobj);
//   bson_check_duplicate_keys(&fullobj);
   bson_finish( &fullobj);

   bson_destroy(&oldobj);
   bson_destroy(&addobj);

   jsonFromBson( fullobj.data, query );
   cout << " Result: " <<  query << endl;
   bson_destroy(&fullobj);

}


} // namespace bsonio
