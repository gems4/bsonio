//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbgraph.h
/// Declarations of class TDBGraph - working with graph databases (OLTP)
/// Used  SchemaNode class - API for direct code access to internal
/// DOM based on our JSON schemas.
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TDBGRAPH_H
#define TDBGRAPH_H

#include "dbschema.h"

namespace bsonio {

using FieldSetMap = map<string, string >; ///< map fieldpath<->value
using UniqueFieldsMap = map<vector<string>, string >;  ///< map unique fields values<-> record key

/// A graph is a structure composed of vertices and edges.
/// Both vertices and edges can have an arbitrary number of
/// key/value-pairs called properties.
enum  GraphDBRecordType
{
     rVertex = 0,
     rEdge = 1,
     rQuery = 2,  // future use
     rUndef = 10
};

int extractIntField( const string& key, const string& query );
string extractStringField( const string& key, const string& query );
FieldSetMap extractFields( const vector<string> queryFields, bson *bobj );
FieldSetMap extractFields( const vector<string> queryFields, const string& jsondata );
void addFieldsToQuery( string& query, const FieldSetMap& fldvalues );

/// Definition of graph databases chain
class TDBGraph : public TDBSchema
{

    virtual void beforeRmbson( KeysSet::iterator&  );
    virtual void beforeSaveUpdatebson( KeysSet::iterator& , bson* bsrec );

    virtual void afterRmbson( KeysSet::iterator&  );
    virtual void afterSaveUpdatebson( KeysSet::iterator& , bson* bsrec );

    UniqueFieldsMap::iterator uniqueLinebyId( const string& idschem );


 protected:

    // Internal data
    bool changeSchemaMode = false;  ///< When read new record from DB, schemaName can be changed
    string _type;
    string _label;
    int schemaType;  // Vertex, Edge, Query, Undef,

    vector<string>  uniqueFieldsNames;               ///< names of fields to be unique
    UniqueFieldsMap uniqueFieldsValues;              ///< map to save unique fields values

    /// Init uniqueFields when load collection
    void loadUniqueFields();

    // internal functions
    /// Test true type and label for schema
    void testSchema( const char *bsobj );

    /// Build default json query string for collection
    string makeDefaultQuery();
    //void buildFieldsQuery(  bson* bshits1, bool addkey, const vector<string>& queryFields );

    /// Save current record to bson structure
    void recToBson( bson *obj, time_t crtt, char* oid );
    /// Load data from bson structure (return read record key)
    string recFromBson( bson *obj );

 public:

    /// Create new TDBGraph for struct name
   static  TDBGraph* newDBClient( const ThriftSchema *aschema, TAbstractDBDriver* dbdriver,
      const string& collcName, const string& schemaName, const string& queryString = "" );

    ///  Constructor
    TDBGraph( TAbstractDBDriver* adriver, const char* name, const ThriftSchema* aschema, const string& schemaName  );
    /// Constructor from bson data
    TDBGraph( TAbstractDBDriver* adriver, const char* bsobj, const ThriftSchema* aschema );

    ///  Destructor
    virtual ~TDBGraph(){}

    /// Change current schema mode
    /// If true,  current schemaName can be changed when read new record
    void resetMode( bool mode )
    {
      changeSchemaMode = mode;
    }

    /// Change current schema
    void resetSchema( const string& aschemaName, bool change_queries );

    // query functions

    /// Make query by keyword
    string idQuery( const string& id ) const
    {
       return  string("{ '_id' : '")+ id + "' }";
    }
    /// Make Follow Outgoing Edges query
    string outEdgesQuery( const string& id ) const
    {
        return string("{'_type': 'edge', '_outV': '")+ id + "' }";
    }
    /// Make Follow Incoming Edges query
    string inEdgesQuery( const string& id ) const
    {
        return string("{'_type': 'edge', '_inV': '")+ id + "' }";
    }

    /// Test existence Outgoing Edges
    bool existOutEdges( const string& id )
    {
      return existKeysByQuery( outEdgesQuery( id ) );
    }
    /// Build Outgoing Edges keys list
    vector<string> getOutEdgesKeys( const string& id )
    {
      return getKeysByQuery( outEdgesQuery( id ) );
    }

    /// Test existence Incoming Edges
    bool existInEdges( const string& id )
    {
       return existKeysByQuery( inEdgesQuery( id ) );
    }
    /// Build Incoming Edges keys list
    vector<string> getInEdgesKeys( const string& id )
    {
       return getKeysByQuery( inEdgesQuery( id ) );
    }

    /// Test existence records by query
    bool existKeysByQuery( const string& query );
    /// Build keys list by query
    vector<string> getKeysByQuery( const string& query );


    // build functions

    /// Define new Vertex
    void setVertex( const string& aschemaName, const FieldSetMap& fldvalues );
    /// Add new Vertex to database
    /// Return oid of new record
    string addNewVertex( const string& aschemaName, const FieldSetMap& fldvalues, bool testValues )
    {
        setVertex( aschemaName, fldvalues );
        return InsertCurrent( testValues );
    }

    /// Define new Edge
    void setEdge( const string& aschemaName, const string& outV,
                  const string& inV, const FieldSetMap& fldvalues );
    /// Add new Edge to database
    /// Return oid of new record
    string addNewEdge( const string& aschemaName, const string& outV,
                         const string& inV, const FieldSetMap& fldvalues, bool testValues )
    {
        setEdge( aschemaName, outV, inV, fldvalues );
        return InsertCurrent( testValues );
    }

    /// Update current schema data
    void updateVertex( const string& aschemaName, const FieldSetMap& fldvalues );
    /// Update&Save currentData to database
    void UpdateCurrent( const string& aschemaName, const FieldSetMap& fldvalues,
                      bool overwrite, bool testValues )
    {
      updateVertex( aschemaName, fldvalues );
      SaveCurrent( overwrite,  testValues );
    }


    // service functions

    /// Extract the string value by key from query string
    string extractToken( const string& key, const string& query );
    /// Extract label by id  ( using query )
    string extractLabelById( const string& id );

    /// Build table of fields values by query
    ValuesTable loadRecords( const string& query, const vector<string>& queryFields );
    /// Build table of fields values by ids list
    ValuesTable loadRecords( const vector<string>& ids, const vector<string>& queryFields );
    /// Build map of fields-value pairs
    FieldSetMap loadRecordFields( const string& id, const vector<string>& queryFields );

    /// Extract schema by id  ( using query )
    string  extractSchemaFromKey( const string& id  )
    {
      return schema->getVertexName( extractLabelById( id ) );
    }
    /// Get schema by id (with reading record )
    string  getSchemaFromKey( const string& id  );
};

} // namespace bsonio

#endif // TDBGRAPH_H
