/** Apache Thrift IDL definition for the query service interface */
/** PMATCH++ prototype   January 2016 */
/** Revised by SD 13-10-2016 */

namespace * query

/** Classes of query style */
enum QueryStyle {
    /** EJDB-style query */
    EJDB
    /** SPARQL-style query */
    SPARQL
}


 /** description of query record */
struct Query {   // Database record of type query
    /** id of this query record or 0 if unknown */
    1: required string _id
    /** short name of query (used as key field) */
    2: required string name
    /** description of query */
    3: string comment
    /** query style */
    4: required  QueryStyle style
    /** query for schema */
    5: string qschema
    /** list of fields to collect */
    6: required list<string> collect
    /** query string */
    7: string find
}


// End of file query.thrift - - - - - - - - - - - - - - - - - - - - - - - - - -
