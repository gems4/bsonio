/** Apache Thrift IDL definition for the query service interface */
/** bsonio prototype   January 2017 */
/** Revised by SD 20-01-2017 */

namespace * docpages


/** Types of Help files */
enum DocFileTypes {
    /** Markdown text file */
    Markdown
    /** Image binary file */
    Image
}

 /** description of  Help record ( Markdown or image file ) */
struct DocPages {   // Database record for help content
    /** id of this help record or 0 if unknown */
    1: required string _id
    /** name of file (used as key field) */
    2: required string name
    /** extension of file (used as key field) */
    3: required string ext
    /** type of content (used as key field) */
    4: required DocFileTypes type
    /** markdown content  */
    5: string markdown
    /** image content  */
    6: string image
}


// End of file docpages.thrift - - - - - - - - - - - - - - - - - - - - - - - - - -
