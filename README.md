BSONIO
=========

BSONIO is a library and API providing a generic interface for exchanging the structured data between JSON, YAML, or XML files, NoSQL database back-ends, and Thrift-based RPC (remote procedure calls). The data munging is based on JSON schemas connected with the internal BSON data and DOM objects. A JSON schema can be easily generated from a Thrift data structure definition (*.thrift file) for a given structured data type.   

### What BSONIO does? ###

* BSON is a binary image of JSON. BSON is used for processing JSON files and for storing JSON data in NoSQL databases such as MongoDB and EJDB. 
* BSONIO implements the BSON rendering to/from file or text stream into EJDB and/or JSON, YAML and XML formats. Public API methods allow the following conversions:
    - bson to/from JSON
    - bson to/from subset of YAML
    - bson to/from subset of XML
* BSONIO can also be used for connecting any user-defined C++ data structures to a GUI editor widget and graphics (using the BSONUI widget API).
* BSONIO is written in C/C++ using open-source libraries EJDB, YAML-CPP, Pugixml, Thrift and Lua.
* Version: currently 0.1.
* Will be distributed as is (no liability) under the terms of Lesser GPL v.3 license. 

### How to get BSONIO source code ###

* In your home directory, make a folder named e.g. ~/bsonio.
* Change into ~/bsonio and clone this repository from https://bitbucket.org/gems4/bsonio.git  using a preinstalled free git client SourceTree or SmartGit (the best way on Windows). 
* Alternatively on Mac OS X or linux, open a terminal and type in the command line (do not forget a period):
~~~
git clone https://bitbucket.org/gems4/bsonio.git . 
~~~

### How to build the BSONIO library ###

* Building BSONIO requires [CMake](http://www.cmake.org/), Apache Thrift, Lua, EJDB, PugiXML, YAML-CPP.  
* Make sure you have cmake, Thrift and Lua installed before building BSONIO. On Ubuntu linux terminal, this can be done using the following commands:
~~~
sudo apt-get install cmake
sudo apt-get install lua5.2 lua5.2-dev
sudo apt-get install thrift-compiler
~~~

* Apache Thrift can also be built and installed by cloning it from Github using git:
~~~
sudo apt-get install libssl-dev libtool byacc flex
cd ~
mkdir thrift
cd thrift
git clone http://github.com/apache/thrift . -b 0.11.0
./bootstrap.sh
./configure
sudo make install
sudo ldconfig
thrift -version
~~~

* Navigate to the directory where this README file is located and type in terminal:
~~~
cd bsonio
mkdir build && cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
~~~

* This will create a `build` directory where BSONIO and its third-party libraries EJDB, PugiXML, YAML-CPP are built locally. To install them in the system path, execute
~~~
sudo make install
~~~

### How to use BSONIO (use cases) ###

#### [Doxygen](http://www.stack.nl/~dimitri/doxygen/index.html) documentation #####

Install doxygen

~~~
sudo apt install doxygen
sudo apt install graphviz
~~~

Generate html help

~~~
doxygen  bsonio-doxygen.conf
~~~

### How to generate JSON schemas using thrift ###

Use the following command:

~~~
thrift -r --gen json schema.thrift
~~~

The option -r (recursively) is necessary for generating json files from potential includes in the schema.thrift file. 

* TBD