# Include the ExternalProject module
include(ExternalProject)

# Ignore all warning from the third-party libraries
if(CMAKE_COMPILER_IS_GNUCXX)
  add_definitions("-w")
endif()

# Set the list of compiler flags for the external projects
if(NOT ${CMAKE_SYSTEM_NAME} MATCHES "Windows")
    set(CXXFLAGS "-fPIC")
endif()


# Download and install the yaml-cpp library
ExternalProject_Add(YAMLCPP
    PREFIX thirdparty
    GIT_REPOSITORY https://github.com/jbeder/yaml-cpp.git
    UPDATE_COMMAND ""
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${THIRDPARTY_DIR}
               -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
 #              -DCMAKE_CXX_FLAGS=${CXXFLAGS}
               -DCMAKE_INSTALL_INCLUDEDIR=include
               -DCMAKE_INSTALL_LIBDIR=lib
               -DCMAKE_INSTALL_BINDIR=bin
               -DBUILD_SHARED_LIBS=ON
               -DYAML_CPP_BUILD_TOOLS=OFF
               -DYAML_CPP_BUILD_CONTRIB=OFF
)

# Download and install the EJDB library
ExternalProject_Add(EJDB
    PREFIX thirdparty
    GIT_REPOSITORY https://github.com/Softmotions/ejdb.git
    UPDATE_COMMAND ""
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${THIRDPARTY_DIR}
               -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
               -DCMAKE_CXX_FLAGS=${CXXFLAGS}
               -DCMAKE_INSTALL_INCLUDEDIR=include
               -DCMAKE_INSTALL_LIBDIR=lib
               -DCMAKE_INSTALL_BINDIR=bin
               -DBUILD_SHARED_LIBS=ON
)

# Download and install the pugixml library
ExternalProject_Add(PUGIXML
    PREFIX thirdparty
    GIT_REPOSITORY https://github.com/zeux/pugixml.git
    UPDATE_COMMAND ""
#    CONFIGURE_COMMAND ${CMAKE_COMMAND} <SOURCE_DIR>/scripts
    CMAKE_ARGS    -DCMAKE_INSTALL_PREFIX=${THIRDPARTY_DIR}
        -DCMAKE_GENERATOR=${CMAKE_GENERATOR}
        -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
        -DCMAKE_INSTALL_INCLUDEDIR=include
        -DCMAKE_INSTALL_LIBDIR=lib
        -DCMAKE_INSTALL_BINDIR=bin
        -DBUILD_SHARED_LIBS=ON
)

# Create the install target for the third-party libraries
install(DIRECTORY ${THIRDPARTY_DIR}/lib 
    DESTINATION .)
install(DIRECTORY ${THIRDPARTY_DIR}/include 
    DESTINATION .)
# Create the install target for the third-party libraries
#install(DIRECTORY ${THIRDPARTY_DIR}/
#    DESTINATION .)
