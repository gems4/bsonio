//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbconnect.h
/// Declarations of class TDBAbstract - base interface of abstract DB chain
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TDBCONNECT_H
#define TDBCONNECT_H

//#include <set>
//#include <map>
#include <memory>
#include "dbcollection.h"

namespace bsonio {

/// Default host that the socket is connected to
const string defHost = "localhost";

/// Default port that the socket is connected to
const int defPort = 9090;

/// Default path to EJDB
const string defEJDBPath =  "ejdbdata";

/// Definition of Abstract DataBase connections
class TDataBase
{
    /// Flag for using local EJDB or connect to Database server
    bool _isLocalDB;

    /// Parh to local EJDB Database
    string _dbfilePath;

    /// The host that the socket is connected to
    std::string _host;
    /// The port that the socket is connected to
    int _port;

    /// List of opened collections from Database
    mutable map< string, shared_ptr<TDBCollection> > _collections;

    void getFromSettings();

 public:

    ///  Constructor used internal preferences to BSONIO
    TDataBase()
    {
      getFromSettings();
    }

    ///  Constructor - connections to Database server
    TDataBase( int thePort, const std::string& theHost = "localhost" ):
      _isLocalDB( false), _dbfilePath(defEJDBPath), _host(theHost),_port(thePort)
    { }

    ///  Constructor - connections to local EJDB
    TDataBase( const string& adbfilePath ):
      _isLocalDB( true), _dbfilePath(adbfilePath), _host(defHost), _port(defPort)
    { }

    /// Destructor of TDataBase
    virtual ~TDataBase()
    { }

    /// Update DataBase connections using settings
    void updateSettings();

    TAbstractDBDriver* newDrive() const;

    /// Link to collection into DataBase
    TDBCollection *getCollection( const string& type, const string& colname,
                                  const vector<KeyFldsData>& keyFldsInf  ) const
    {
      auto itr = _collections.find(colname);
      if( itr != _collections.end() )
        return itr->second.get();

      return addCollection( type, colname, keyFldsInf );
    }

    /// Create link to collection
    /// \param type - type of collection ( "undef", "schema", "vertex", "edge" )
    /// \param colname - name of collection
    /// \param keyFldsInf - definition of record key (for Vertexes and Edges predefined values )
    TDBCollection *addCollection( const string& type, const string& colname,
                                  const vector<KeyFldsData>& keyFldsInf  ) const ;
};

} // namespace bsonio

#endif // TDBCONNECT_H
