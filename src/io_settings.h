//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file io_settings.h
/// Declaration of BsonioSettings object for storing preferences to BSONIO
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef IOSETTINGS_H
#define IOSETTINGS_H

#include "json2cfg.h"
#include "thrift_schema.h"
#include "dbconnect.h"

namespace bsonio {

//TDBSchema* newDBImpexClient(  const string& queryString  );
//TDBSchema* newDBQueryClient(  const string& queryString  );


/// \class BsonioSettings - storing preferences to BSONIO
class BsonioSettings
{
    friend BsonioSettings& ioSettings();

    /// Read data from settings
    void getDataFromPreferences();

    /// Read all thrift schemas from Dir
    void readSchemaDir( const string& dirPath );

    /// Constructor
    BsonioSettings( const string& inifile );

public:

  /// Task settings file name
  static string settingsFileName;

  /// Create new graph database object
  //TDBGraph* newDBGraphClient(const string& schemaName_, const string& queryString ="" ) const;
  /// Create new schema based database object
  //TDBSchema* newDBSchemaClient(  const string& collcName, const string& schemaName,
  //   const vector<bsonio::KeyFldsData>& keyFldsInf, const string& queryString =""  ) const;

  /// Link to Thrift schema definition
  const ThriftSchema*  Schema() const
  {
      return &schema;
  }

  /// updatre user directory
  void setUserDir( const string& dirPath);

  /// Set addition settings
  template <class T>
  void setValue( const char *key, const T& value )
  {
     mainSettings.setValue( key, value);
     mainSettings.saveCfg();
  }

  /// Get value from settings
  template <class T>
  T value(const string& key, const T& defaultValue ) const
  {
    return mainSettings.getValue( key, defaultValue);
  }

  bool getBool( const char *key, const bool& defvalue ) const
  {
    return  mainSettings.getBool( key, defvalue );
  }

  /// Overwrite mode when loading data
  bool overwrite() const
  {
    return mainSettings.getBool("ImportFormatOverwrite", false);
  }

  ///  If true is used internal EJDB database otherwise used server database
  bool isLocalDB() const
  {
    return mainSettings.getBool("UseLocalDB", false);
  }

  /// Graph database collection name (by default "data")
  string collection() const
  {
    return mainSettings.getString("LocalDBCollection", "data");
  }

  /// Get internal EJDB database file and location
  string localDBPath() const;

  /// Get server DataBase host
  string dbSocketHost() const
  {
    return mainSettings.getString("DBSocketHost", defHost );
  }

  /// Get server DataBase port
  int dbSocketPort() const
  {
    return mainSettings.getValue("DBSocketPort", defPort );
  }

  /// Path to lua lib files
  string luaLibPath() const
  {
     string lua_lib_path =  mainSettings.getString("ResourcesDirectory", "");
         lua_lib_path += "/lua/";
     return lua_lib_path;
  }

  /// Current work directory
  string userDir() const
  {
      return UserDir;
  }

  /// Current schema directory
  string schemDir() const
  {
      return SchemDir;
  }

  /// Connect localDB to new path
  bool updateSchemaDir()
  {
    string newSchema =  mainSettings.getString("SchemasDirectory", "");
    if( newSchema != SchemDir )
    {
      SchemDir  = newSchema;
      readSchemaDir( SchemDir );
      return true;
    }
    return false;
  }

  /// Connect localDB to new path
  bool updateLocalDB();

  /// Update server connect
  bool updateClientDB();

private:

    // Internal settings
   ConfigJson mainSettings;        ///< Properties for program
   string SchemDir;               ///< Path to schemas directory
   string UserDir;                ///< Current work directory

    // Internal data
   ThriftSchema schema;    ///< Current schema structure

};

/// Function to connect to only one Preferences object
extern BsonioSettings& ioSettings();

} // namespace bsonio

#endif // IOSETTINGS_H
