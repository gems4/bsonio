//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file v_json.h
/// Declaration API to work with bson data
/// Declarations of functions for data exchange to/from json format
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef __v_json_h_
#define __v_json_h_

#include <fstream>
#include <vector>
#include <string>
#include "ejdb/ejdb.h"
#include "nservice.h"
using namespace std;

namespace bsonio {

class GemDataStream;

/// Parse one json object from string to bson structure
void jsonToBson( bson *obj, const string& recJson );
/// Parse one json  string to bson array
void jsonToBsonArray( bson *obj, const string& recJson );

/// Print bson structure to json format string
void jsonFromBson( const char *objdata, string& recJson );
/// Print bson array to json format string
void jsonArrayFromBson( const char *objdata, string& recJson );

/// Set default data to bson
void bson_reset_data(bson *b);

/**
 * Get OID value from the '_id' field of specified bson object.
 * @param bson[in] BSON object
 * @param oid[out] Pointer to OID type
 * @return Type if bson object
 */
bson_type _bsonoidkey(bson *bs, bson_oid_t *oid);

/// Get string from bson structure by name
bool bson_find_string( const char *obj, const char *name, string& str );
///   Get string from bson structure by name (any bson type data)
bool bson_to_string( const char *obj, const char *name, string& str );
///   Get string from bson iterator
bool bson_to_string( bson_type type,  bson_iterator *it, string& str );
///   Get string from bson structure by name
///  (any bson type data, special data for object and array)
bool bson_to_key( const char *obj, string name, string& str );
/// Get array from bson structure by name
const char* bson_find_array( const char *obj, const char *name );
/// Get object from bson structure by name
const char* bson_find_object( const char *obj, const char *name );
/// Get array from bson structure by name ( may be null array )
const char* bson_find_array_null( const char *obj, const char *name );
/// Get object from bson structure by name ( may be null object )
const char* bson_find_object_null( const char *obj, const char *name );
/// Get time from bson structure by name
time_t bson_find_time_t( const char *obj, const char *name );
/// Read array from bson structure by name to string list
void bson_read_array( const char *obj, const char *name, vector<string>& lst );
/// Read array from bson structure by name path to string list
bool bson_read_array_path( const char *obj, const char *namepath, vector<string>& lst );

/// Get value from bson structure by name
template <class T>
bool bson_find_value( const char *obj, const char *name, T& value )
{
    bson_iterator it;
    bson_type type;
    type =  bson_find_from_buffer(&it, obj, name );

    switch( type )
    {
      // impotant datatypes
      case BSON_BOOL:
           value = bson_iterator_bool(&it);
           break;
      case BSON_INT:
           value = bson_iterator_int(&it);
           break;
      case BSON_LONG:
           value =  bson_iterator_long(&it);
           break;
      case BSON_DOUBLE:
           value = bson_iterator_double(&it);
           break;
     case BSON_NULL:
        // return false;
     default: // error
         // bson_errprintf("can't print type : %d\n", type);
         return false;
   }
   return true;
}

/// Read array from bson structure by name to value vector
template<class T>
void bson_read_array( const char *obj, const char *name, vector<T>& lst )
{
    lst.clear();

    bson_iterator it;
    bson_type type;
    type =  bson_find_from_buffer(&it, obj, name );
    if( type == BSON_NULL || type ==BSON_EOO )
      return;
    bsonioErrIf( type != BSON_ARRAY,
                 "E015BSon: ", string("Must be array ")+name );
    T value;
    bson_iterator i;
    bson_iterator_from_buffer(&i, bson_iterator_value(&it));
    while (bson_iterator_next(&i))
    {
        //bson_type t = bson_iterator_type(&i);
        const char* key = bson_iterator_key(&i);
        //const char* data = bson_iterator_value(&i);
        if( !bson_find_value( bson_iterator_value(&it), key, value ) )
          value = 0;
        lst.push_back(value);
    }
 }

/// Read map from bson structure by name to value map
template <class T>
bool bson_read_map_path( const char *obj, const char *namepath, map<string,T>& newmap )
{
    newmap.clear();

    bson_iterator it;
    bson_type type;
    bson_iterator_from_buffer(&it, obj );
    type =  bson_find_fieldpath_value( namepath, &it );
    if( type != BSON_OBJECT && type != BSON_NULL  )
        return false;

    T value;
    if( type == BSON_OBJECT  )
    {
      bson_iterator itobj;
      bson_iterator_from_buffer(&itobj, bson_iterator_value(&it));
      while( bson_iterator_next(&itobj) )
      {
          const char *key = bson_iterator_key(&itobj);
          if( bson_find_value( bson_iterator_value(&it), key, value ) )
            newmap[key] = value;
      }
    }
    return true;
 }
template <> bool bson_read_map_path( const char *obj,
                  const char *namepath, map<string,string>& newmap );


extern void print_bson_to_json(FILE *f, const bson *b);
/// Print bson object to text file
extern void print_bson_object_to_json(FILE *f, const bson *b);
/// Print bson object to binary file
extern void print_bson_object_to_bin( GemDataStream& ff, const bson *b);
/// Load bson object from binary file
extern char * read_bson_object_from_bin( GemDataStream& ff );

enum {
    jsBeginArray = '[',    //0x5b,
    jsBeginObject = '{',   //0x7b,
    jsEndArray = ']',      //0x5d,
    jsEndObject = '}',     //0x7d,
    jsNameSeparator = ':', //0x3a,
    jsValueSeparator = ',',//0x2c,
    jsQuote = '\"'      //0x22
};

/// Class for read/write bson structure from/to text file or string
class ParserJson
{
protected:

  string jsontext;
  const char *curjson;
  const char *end;

  bool xblanc();
  void getString( string& str );
  void getNumber( double& value, int& type );
  void parseValue( const char *name, bson *brec );

 public:

  static void bson_print_raw_txt( iostream& os, const char *data, int depth, int datatype );


  ParserJson():curjson(0), end(0)
    {}
  virtual ~ParserJson()
    {}

  /// Parse internal jsontext string to bson structure (without first {)
  void parseObject( bson *brec );
  void parseArray( bson *brec );

  /// Print bson structure to JSON string
  void printBsonObjectToJson( string& resStr, const char *b);
  /// Print bson array to JSON string
  void printBsonArrayToJson( string& resStr, const char *b);

  /// Read one json object from text file
  string readObjectText( fstream& fin );
  /// Set up internal jsontext before parsing
  void  setJsonText( const string& json );

};

/// Interface for future using in/out cfg files
class IsUnloadToBson
{
 public:
   IsUnloadToBson() {}
   virtual ~IsUnloadToBson() {}

   virtual void toBson( bson *obj ) const = 0;
   virtual void fromBson( const char *obj )= 0;
};


} // namespace bsonio

#endif	// __v_json_h_
