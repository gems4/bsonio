//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file traversal.cpp
/// Implementation of class GraphTraversal - Graph Traversal for Database.
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include "traversal.h"
#include "io_settings.h"
#include "json2cfg.h"

namespace bsonio {

GraphTraversal::GraphTraversal( const TDataBase* dbconnect )
{
   string  schemaName = ioSettings().Schema()->getVertexesList()[0];

   TDBVertexDocument* verDoc = new TDBVertexDocument( schemaName, dbconnect  );
   verDoc->resetMode(true);
   _vertexdoc.reset( verDoc );

   schemaName = ioSettings().Schema()->getEdgesList()[0];
   TDBEdgeDocument* edgeDoc = new TDBEdgeDocument( schemaName, dbconnect  );
   edgeDoc->resetMode(true);
   _edgedoc.reset( edgeDoc );

}

// visit Vertex and all linked Edges
bool GraphTraversal::parseVertex( const string& idVertex, GraphElementFunction afunc  )
{
  bson obj;

  // Test Vertex is parsed before
  if( _vertexList.find( idVertex ) !=  _vertexList.end() )
    return false;

  _vertexList.insert(idVertex);
  _vertexdoc->GetRecord( (idVertex+":").c_str() );
  _vertexdoc->GetBson(&obj);
  afunc( true, &obj );
  bson_destroy(&obj);

  // read all edges
  vector<string> edgeslst;

  if( travType&trOut )
  {
    edgeslst = _edgedoc->getOutEdgesKeys( idVertex );
    for(auto const &ent : edgeslst)
       parseEdge( ent,  afunc  );
  }

  if( travType&trIn )
  {
    edgeslst = _edgedoc->getInEdgesKeys( idVertex );
    for(auto const &ent : edgeslst)
      parseEdge( ent, afunc  );
  }
  return true;

}

//  visit Edge and all linked Vertexes
bool GraphTraversal::parseEdge( const string& idEdge, GraphElementFunction afunc  )
{
    string vertexId;
    bson obj;

    // Test Edge is parsed before
    if( _edgeList.find( idEdge ) !=  _edgeList.end() )
      return false;

    _edgeList.insert(idEdge);
    _edgedoc->GetRecord( (idEdge+":").c_str() );
    _edgedoc->GetBson(&obj);

    if( (travType&trIn) )
    {
      if( _edgedoc->getValue("_outV", vertexId) )
      {
        if( _vertexdoc->Find( (vertexId+":").c_str() ) )
           parseVertex( vertexId, afunc );
      }
    }

    if( (travType&trOut) )
    {
     if( _edgedoc->getValue("_inV", vertexId) )
     {
      if( _vertexdoc->Find( (vertexId+":").c_str() ) )
       parseVertex( vertexId, afunc );
     }
    }

    afunc( false, &obj );
    bson_destroy(&obj);
    return true;
}

void GraphTraversal::RestoreGraphFromFile( const string& filePath, bool overwrite )
{
    string type;
    bson curRecord;
    bson_init(&curRecord);
    bson_finish(&curRecord);

    FJsonArray file( filePath);
    file.Open( OpenModeTypes::ReadOnly );
    while( file.LoadNext( &curRecord ) )
    {
        if( !bson_to_key( curRecord.data, "_type", type ))
           continue;  // undefined type
        if( type == "edge" )
           _edgedoc->SaveBson( &curRecord, overwrite, false );
        else
            if( type == "vertex" )
               _vertexdoc->SaveBson( &curRecord, overwrite, false );
    }
    file.Close();
}


} // namespace bsonio
