//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file nservice.cpp
/// Implementation of  service functions
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <cstring>
#include <ctime>
#include <boost/regex.hpp>
#include "nservice.h"

namespace bsonio {

void delColumn( ValuesTable& matr, int column )
{
    for( uint ii=0; ii< matr.size(); ii++)
      matr[ii].erase(matr[ii].begin()+column);
}

void addColumn( ValuesTable& matr, int column, const string& fillvalue )
{
    for( uint ii=0; ii< matr.size(); ii++)
      matr[ii].insert( matr[ii].begin()+column, fillvalue );
}

template <>
string vectorToJson( const vector<string>& elems )
{
  string genjson = "[ ";

  for( auto elm: elems )
       genjson += "\n \""+ elm + "\",\n";
  genjson.pop_back();
  genjson += "\n]";

  return genjson;
}


template <>
string mapToJson( const map<string,string>& elems )
{
  string genjson = "{ ";

  for( auto row: elems )
      genjson += "\n \""+ row.first+ "\" : \"" + row.second + "\",";
  genjson.pop_back();
  genjson += "\n}";

  return genjson;
}

void strip(string& str)
{
  if( str.empty())
   return;
  string::size_type pos1 = str.find_first_not_of(' ');
  string::size_type pos2 = str.find_last_not_of(' ');
  str = str.substr( (pos1 == string::npos ? 0 : pos1),
    (pos2 == string::npos ? str.length() - 1 : pos2 - pos1 + 1));
}

void strip_all(string& str, const string& valof  )
{
  if( str.empty())
   return;
  string::size_type pos1 = str.find_first_not_of(valof);
  string::size_type pos2 = str.find_last_not_of(valof);
  str = str.substr( (pos1 == string::npos ? 0 : pos1),
    (pos2 == string::npos ? str.length() - 1 : pos2 - pos1 + 1));
}

// The method replace() returns a copy of the string
// in which the first occurrence of old have been replaced with new
string replace( string str, const char* old_part, const char* new_part)
{
    size_t pos = str.find( old_part );
    if( pos == string::npos )
      return str;

    string res = str.substr(0, pos);
            res += new_part;
            res += str.substr( pos+strlen(old_part));
    return res;
}


// The method replace() returns a copy of the string
// in which all occurrences of old have been replaced with new
string replace_all( string str, const char* old_part, const char* new_part)
{
    size_t pos = str.find( old_part );
    string res = "";

    while( pos != string::npos )
    {
        res += str.substr(0, pos);
        res += new_part;
        str = str.substr( pos+strlen(old_part));
        pos = str.find( old_part );
    }
    return res+str;
}

void replaceall(string& str, char ch1, char ch2)
{
  for(size_t ii=0; ii<str.length(); ii++ )
   if( str[ii] == ch1 )
            str[ii] = ch2;
}

void replaceall(string& str, const string& allReplased, char ch2)
{
  for(size_t ii=0; ii<str.length(); ii++ )
  {
      std::size_t found = allReplased.find_first_of(str[ii]);
      if( found!=std::string::npos )
            str[ii] = ch2;

  }
}

void convertReadedString(string& strvalue )
{
  if(strvalue.empty())
    return;

  if( strvalue.find_first_of("\\") != string::npos )
  {
    string resstr = "";

    size_t ii=0;

    while( ii<strvalue.length()  )
    {
      if( strvalue[ii]  == '\\')
      {
        ii++;
        switch( strvalue[ii++] )
        {
        case '\"': resstr += '\"'; break;
        case '\'': resstr += '\''; break;
        case '\\': resstr += '\\'; break;
        case '/':  resstr += '/';  break;
        case 'n':  resstr += '\n'; break;
        case 'r':  resstr += '\r'; break;
        case 't':  resstr += '\t'; break;
        case 'b':  resstr += '\b'; break;
        case 'f':  resstr += '\f'; break;
        case 'u': // unicode not realized
        default:
           bsonioErr( "convertReadedString","Bad escape sequence in string", strvalue );
        }
     }
     else
       resstr += strvalue[ii++];
  }
  strvalue =  resstr;
 }
}

void convertStringToWrite( string& strvalue )
{
   if( strvalue.empty() )
      return;
   // Not sure how to handle unicode...
   if( strvalue.find_first_of("\"\'\\\b\f\n\r\t") != string::npos )
   {
      string resstr = "";
      auto pos = strvalue.begin();
      while( pos != strvalue.end() )
      {
        switch(*pos)
        {
         case '\"': resstr+= "\\\""; break;
         case '\'': resstr+= "\\\'"; break;
         case '\\': resstr+= "\\\\"; break;
         case '\b': resstr+= "\\b"; break;
         case '\f': resstr+= "\\f"; break;
         case '\n': resstr+= "\\n"; break;
         case '\r': resstr+= "\\r"; break;
         case '\t': resstr+= "\\t"; break;
         default:
            resstr+= *pos;
            break;
      }
      pos++;
    }
    strvalue = resstr;
   }
}


// "1;2;3" to array { 1, 2, 3 }
queue<int> string2intarray( const string& str_data, const string& delimiters )
{
    queue<int> res;
    string str = str_data;

    size_t pos = str.find( delimiters );
    while( pos != string::npos )
    {
        res.push( stoi( str.substr(0, pos) ) );
        str = str.substr( pos + delimiters.length() );
        pos = str.find( delimiters );
    }
    res.push( stoi( str ) );
    return res;
}

// "a;b;c" to array { "a", "b", "c" }
queue<string> split(const string& str, const string& delimiters)
{
    queue<string> v;
    string::size_type start = 0;
    auto pos = str.find_first_of(delimiters, start);
    while(pos != string::npos)
    {
        //if(pos != start) // ignore empty tokens
        {
          string vv = string(str, start, pos - start);
          strip(vv);
          v.push( vv );
        }
        start = pos + 1;
        pos = str.find_first_of(delimiters, start);
    }
    //if(start < str.length()) // ignore trailing delimiter
        v.push( string (str, start, str.length() - start) );
    return v;
}

//  Function that can be used to split text using regexp
vector<string> regexp_split(const string& str, string rgx_str)
{
  vector<string> lst;

  boost::regex rgx(rgx_str);

  boost::sregex_token_iterator iter(str.begin(), str.end(), rgx, -1);
  boost::sregex_token_iterator end;

  while (iter != end)
  {
    lst.push_back(*iter);
    strip_all(lst.back(), " \t\n");
    ++iter;
  }

  return lst;
}

//  Function that can be used to extract tokens using regexp
vector<string> regexp_extract(const string& str, string rgx_str)
{
  vector<string> lst;

  boost::regex rgx(rgx_str);

  boost::sregex_token_iterator iter(str.begin(), str.end(), rgx, 0);
  boost::sregex_token_iterator end;

  while (iter != end)
  {
    lst.push_back(*iter);
    strip_all(lst.back(), " \t\n");
    ++iter;
  }

  return lst;
}


//  Function that can be used to replase text using regexp
std::string regexp_replace(const string& instr, const string& rgx_str, const string& replacement )
{
   boost::regex re(rgx_str);
   std::string output_str = regex_replace(instr, re, replacement);
   return output_str;
}

string string_from_vector(const vector<string>& pieces, const string& delimiter )
{
  string ss;

  for(vector<string>::const_iterator itr = pieces.begin();
      itr != pieces.end(); ++itr)
  {
    if( !ss.empty() )
      ss+=  delimiter;
    ss += *itr;
  }

  return ss;
}

// Extract coefficient from strings like +10.7H2O
string extractCoef( const string& data, double& coef )
{
  //cout << "data " << data << endl;
  coef = 1.;
  if( data.empty() || isalpha( data[0] ) || data[0] == '('  )
   return data;
  if( data[0] == '+' && ( isalpha( data[1] ) || data[1] == '(')  )
   return data.substr(1);
  if( data[0] == '-' && ( isalpha( data[1] ) || data[1] == '(')  )
  {  coef = -1.; return data.substr(1); }

  string::size_type sz;
  coef = stod(data,&sz);
  return data.substr(sz);
}

string u_makepath(const string& dir,
           const string& name, const string& ext)
{
    string Path(dir);
    if( dir != "")
      Path += "/";
    Path += name;
    Path += ".";
    Path += ext;

    return Path;
}

void u_splitpath(const string& aPath, string& dir,
            string& name, string& ext)
{
    string Path = aPath;
    replaceall( Path, '\\', '/');
    size_t pos = Path.rfind("/");
    if( pos != string::npos )
        dir = Path.substr(0, pos), pos++;
    else
        dir = "",    pos = 0;

    size_t pose = Path.rfind(".");
    if( pose != string::npos )
    {
        ext = Path.substr( pose+1, string::npos );
        name = Path.substr(pos, pose-pos);
    }
    else
    {
        ext = "";
        name = Path.substr(pos, string::npos);
    }
}

string curDate()
{
    struct tm *time_now;
    time_t secs_now;

    tzset();
    time(&secs_now);
    time_now = localtime(&secs_now);

    char tstr[100];

    strftime(tstr, 11,
             "%d/%m/%Y",
             time_now);

    return tstr;
}

string curDateSmol(char ch )
{
    struct tm *time_now;
    time_t secs_now;

    tzset();
    time(&secs_now);
    time_now = localtime(&secs_now);

    char tstr[100];

    string frm = "%d" + string(1,ch)+ "%m" + string(1,ch) + "%y";
    strftime(tstr, 9, frm.c_str(),
             // "%d/%m/%y",
             time_now);

    return tstr;
}

string curTime()
{
    struct tm *time_now;
    time_t secs_now;

    tzset();
    time(&secs_now);
    time_now = localtime(&secs_now);

    char tstr[100];
    strftime(tstr, 6,
             "%H:%M",
             time_now);

    return tstr;
}

}  // bsonio
