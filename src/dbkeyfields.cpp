//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbkeyfields.cpp
/// Implementation of class TEJDBKey - description the key of record
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include "dbkeyfields.h"

namespace bsonio {


//-------------------------------------------------------------
// IndexEntry  - Element in sequence of record keys
//-------------------------------------------------------------

bool operator <( const IndexEntry& iEl,  const IndexEntry& iEr)
{
    for( uint ii=0; ii<iEl.keyFlds.size(); ii++ )
    {
        if(iEl.keyFlds[ii] == iEr.keyFlds[ii])
          continue;
        return ( iEl.keyFlds[ii] < iEr.keyFlds[ii] ? true: false);
    }
    return false;
}

bool operator >( const IndexEntry& iEl,  const IndexEntry& iEr)
{
    for( uint ii=0; ii<iEl.keyFlds.size(); ii++ )
    {
        if(iEl.keyFlds[ii] == iEr.keyFlds[ii])
          continue;
        return ( iEl.keyFlds[ii] > iEr.keyFlds[ii] ? true: false);
    }
    return false;
}

bool operator ==( const IndexEntry& iEl,  const IndexEntry& iEr)
{
    return equal( iEl.keyFlds.begin(), iEl.keyFlds.end(), iEr.keyFlds.begin());
}

bool operator !=( const IndexEntry& iEl,  const IndexEntry& iEr)
{
    return !equal( iEl.keyFlds.begin(), iEl.keyFlds.end(), iEr.keyFlds.begin());
}

//-------------------------------------------------------------
// TEJDBKey  - working with DB record key
//-------------------------------------------------------------

// Default configuration
TEJDBKey::TEJDBKey( const vector<KeyFldsData>& keyFldsInf  )
{
    for( uint ii=0; ii<keyFldsInf.size(); ii++)
    {
        rkDesc.push_back( keyFldsInf[ii] );
        rkFlds.push_back( "*" );
    }
   setUseSize();
}

// Copy struct
TEJDBKey::TEJDBKey( const TEJDBKey& dbKey )
{
    for(uint ii=0; ii<dbKey.keyNumFlds(); ii++)
    {
        rkDesc.push_back( dbKey.rkDesc[ii] );
        rkFlds.push_back( dbKey.rkFlds[ii]  );
    }
    pKey = dbKey.pKey;
    uKey = dbKey.uKey;
    usesize_ = dbKey.usesize_;
}

// Writes DOD data to ostream file
void TEJDBKey::toBson( bson *obj ) const
{
  bson_append_start_array(obj, "keydef");
   for(uint ii=0; ii<keyNumFlds(); ii++)
   {
     bson_append_start_object( obj, to_string(ii).c_str() );
     bson_append_string( obj, "name", keyFldName(ii) );
     bson_append_string( obj, "shortName", keyFldShortName(ii) );
     bson_append_string( obj, "descr", keyFldDesc(ii) );
     bson_append_int( obj, "size", keyFldSize(ii) );
     bson_append_finish_object(obj);
    }
  bson_append_finish_array(obj);
}

// Reads DOD data from istream file
void TEJDBKey::fromBson( const char* bsobj )
{
    string name, shname, descr;
    int size;
    const char *arr  = bson_find_array(  bsobj, "keydef"  );
    bson_iterator i;
    //const char *key;
    bson_iterator_from_buffer(&i, arr);
    while (bson_iterator_next(&i))
    {
        bson_type t = bson_iterator_type(&i);
        bsonioErrIf( t != BSON_OBJECT, "bson2Array", "Error reading cfg file..." );
        //key = bson_iterator_key(&i);
        const char* data = bson_iterator_value(&i);
        if(!bson_find_string( data, "name", name ) )
            bsonioErr( "E010BSon: ", "Undefined Name.");
        if(!bson_find_string( data, "shortName", shname ) )
            shname = name;
        if(!bson_find_string( data, "descr", descr ) )
            descr = "";
        if(!bson_find_value( data, "size", size ) )
            size=-1;
        rkDesc.push_back( KeyFldsData( name, shname,descr, size ) );
        rkFlds.push_back( "*" );
    }
}

// Return record key in packed form
const char *TEJDBKey::pack( const vector<string>& akeyFlds )
{
    string sp;
    pKey = "";
    for(uint ii=0; ii<keyNumFlds(); ii++)
    {
        sp = akeyFlds[ii];
        //strip( sp );
        pKey += sp;
        if( !usesize_ || sp.length() < keyFldSize(ii) )
            pKey+= ":";
    }
    return pKey.c_str();
}

// Return record key in packed form
const char *TEJDBKey::unpack( const vector<string>& akeyFlds )
{
    if( !usesize_ )
       return pack( akeyFlds );

    string sp;
    uKey = "";
    for(uint ii=0; ii<keyNumFlds(); ii++)
    {
        sp = akeyFlds[ii];
        //strip( sp );
        if(  sp.length() < keyFldSize(ii) )
          sp.append( keyFldSize(ii)-sp.length(), ' ' );
        uKey += sp;
    }
    return uKey.c_str();
}


// Putting DB record key key to internal structure
void TEJDBKey::setKey( const char *key )
{
    bsonioErrIf( key==0, "TEJDBKey", "Undefined key of record.");
    rkFlds.clear();
    string sp = key;

    if( sp == S_ANY )
    {
        for(uint ii=0; ii<keyNumFlds(); ii++)
           rkFlds.push_back( "*" );
    }
    else
    {
        if( !usesize_ )
        { for(uint ii=0; ii<keyNumFlds(); ii++)
          {
             size_t pos = sp.find_first_of(':');
             if( pos == string::npos )
             {
              if( ii < keyNumFlds()-1)
                  bsonioErr( key, "Invalid packed record key.");
             }
             rkFlds.push_back( sp.substr(0, pos) );
             strip( rkFlds[ii] );
             if( sp[pos] == ':' )
                pos++;
             sp = sp.substr(pos);
         }
        } else
          {
            for(uint ii=0; ii<keyNumFlds(); ii++)
            {
              size_t pos = sp.find_first_of(':');
              if( pos == string::npos )
              {
                  pos = sp.length();
                  if( ii < keyNumFlds()-1)
                      if( pos < keyFldSize(ii) )
                       bsonioErrIf( pos < keyFldSize(ii), key, "Invalid packed record key.");
              }
              pos =  min( static_cast<uint>(pos), keyFldSize(ii) );
              rkFlds.push_back( sp.substr(0, pos) );
              strip( rkFlds[ii] );
              if( sp[pos] == ':' )
                  pos++;
              sp = sp.substr(pos);
            }
          }
    }
 }

// Change i-th field of TEJDBKey to key
void TEJDBKey::setFldKey( uint i, const char *key )
{
    bsonioErrIf( i>= keyNumFlds() , key, "Invalid key field number");
    string sp = key;
    strip( sp );
    if( usesize_ && sp.length() > keyFldSize(i) )
      sp.erase( keyFldSize(i) );
    rkFlds[i] = sp;
}

// Check if pattern in record key
bool TEJDBKey::isPattern()
{
  bool OneRec =  true;
  for(uint ii=0; ii<keyNumFlds(); ii++)
      if( rkFlds[ii].find_first_of("*?") != std::string::npos )
      {
          OneRec = false;
          break;
      }
  return !OneRec;
}

// Setted S_ANY
bool TEJDBKey::isAll()
{
  bool AllRecs = true;
  for(uint ii=0; ii<keyNumFlds(); ii++)
      if( rkFlds[ii] != S_ANY )
      {
          AllRecs = false;
          break;
      }
  return AllRecs;
}

bool TEJDBKey::compareTemplate( const IndexEntry& elm )
{
  uint i, j, rklen;
  string kpart, kelm;

  for( j=0; j<keyNumFlds(); j++)
    {
        kpart = keyFld(j);
        kelm = elm.getKeyField(j);

        if( kpart == "*" )
            continue; //to next field

        if( kpart.length() > kelm.length() )
            return false;

        rklen = kpart.length();

        for( i=0; i<rklen; i++ )
        {
            if( kpart[i] == '*' ) // next field
              break;
            switch( kpart[i] )
            {
             case '?': // no ' '
                if( kelm[i] == ' ' )
                    return false;
                break;
             default:
                if( kpart[i] != kelm[i] )
                    return false;
            }
        }
        if( kpart[i] != '*' && i < kelm.length())
           return false;
    }
  return true;
}

}   // namespace bsonio

