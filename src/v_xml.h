//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file v_xml.h
/// Declarations of functions for data exchange to/from XML format
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef AR2XML_H
#define AR2XML_H

#include <vector>
#include "ejdb/bson.h"
#include "pugixml.hpp"
#include "ar2base.h"
using namespace pugi;

namespace bsonio {

namespace ParserXML
{
    /// Print bson structure to XML structure
    void bson_emitter( pugi::xml_node& out, const char *data, int datatype );

    /// Parse one XML object from string to bson structure
    bool parseXMLToBson( const string& currentXML, bson *obj );

    /// Read one XML object from text file and parse to bson structure
    bool parseXMLToBson( const char *fname, bson *brec );

    /// Print bson structure to xml file
    void printBsonObjectToXml(const char* file_name, const char *aobj);

    /// Print bson array to xml file
    void printBsonArrayToXml(const char* file_name, const char *aobj);
}


} // namespace bsonio

#endif // AR2XML_H
