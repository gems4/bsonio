//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file nservice.h
/// Declarations of bsonio_exeption class and service functions
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef NSERVICE_H
#define NSERVICE_H

#include <sstream>
#include <string>
#include <vector>
#include <queue>
#include <map>
#include <set>
using namespace std;

namespace bsonio {

using ValuesTable = vector< vector<string> >; ///< Lines of colums values table

void delColumn( ValuesTable& matr, int column );

void addColumn( ValuesTable& matr, int column, const string& fillvalue = "" );


class bsonio_exeption: public exception
{

public:

    string mess_;
    string title_;
    string field_;  ///< Last readed field

    bsonio_exeption()
    {}

    bsonio_exeption(const string& titl, const string& msg, const string& fld):
            mess_(msg),  title_(titl), field_(fld)
    {}

    const char* what() const throw()
   {
      return mess_.c_str();
   }

   const char* title() const
   {
      return title_.c_str();
   }

   const char* field() const
   {
      return field_.c_str();
   }

};

/// Throw  bsonio_exeption
inline void bsonioErr(const string& title, const string& message, const string& fld= "")
{
    throw bsonio_exeption(title, message, fld);
}

/// Throw condition bsonio_exeption
inline void bsonioErrIf(bool error, const string& title, const string& message, const string& fld = "")
{
    if(error) throw bsonio_exeption(title, message, fld);
}

/// Whitespace characters have been stripped from the beginning and the end of the string.
   void strip(string& str);
/// All whitespace characters have been stripped from the beginning and the end of the string.
   void strip_all(string& str, const string& valof =  " \n\t\r"  );
/// The method replace() returns a copy of the string
/// in which the first occurrence of old have been replaced with new
   string replace( string str, const char* old_part, const char* new_part);
/// The method replace() returns a copy of the string
/// in which all occurrences of old have been replaced with new
   string replace_all( string str, const char* old_part, const char* new_part);
   void replaceall(string& str, char ch1, char ch2);
   void replaceall(string& str, const string& allReplased, char ch2);

   void convertReadedString(string& strvalue );
   void convertStringToWrite( string& strvalue );

   /// Split string to int array
   queue<int> string2intarray( const string& str_data, const string& delimiters );
   /// A split function
   queue<string> split(const string& str, const string& delimiters);
   ///  Function that can be used to split text using regexp
   vector<string> regexp_split(const string& str, string rgx_str = "\\s+");
   ///  Function that can be used to extract tokens using regexp
   vector<string> regexp_extract(const string& str, string rgx_str);
   ///  Function that can be used to replase text using regexp
   string regexp_replace(const string& instr, const string& rgx_str, const string& replacement );

   /// Convert vector to string
   string string_from_vector(const vector<string> &pieces, const string& delimiter=" " );
   /// Extract coefficient from strings like +10.7H2O
   string extractCoef( const string& data, double& coef );

   /// Splits full pathname to path, directory, name and extension
   void u_splitpath(const string& Path, string& dir, string& name, string& ext);
   /// Combines path, directory, name and extension to full pathname
   string u_makepath(const string& dir, const string& name, const string& ext);

   inline int ROUND( double x )
   {
    return static_cast<int>(x>0?(x)+.5:(x)-0.4);
   }

   template <class T>
   inline void fillValue( T* arr, T value, int size )
   {
      if( !arr )  return;
      for(int ii=0; ii<size; ii++)
        arr[ii] = value;
   }

   template <class T>
   inline void copyValues( T* arr, T* data, int size )
   {
      if( !arr || !data )  return;
      for(int ii=0; ii<size; ii++)
       arr[ii] = data[ii];
   }

   template <class D, class F>
   inline void copyValues( D* arr, F* data, int size )
   {
      if( !arr || !data )  return;
      for(int ii=0; ii<size; ii++)
       arr[ii] = data[ii];
   }

   template <typename T>
   bool is( T& x, const std::string& s)
   {
     std::istringstream iss(s);
     //char c;
     return iss >> x && !iss.ignore();
  }

   /// Test function if testVector is subset of fullSet (to bsonio )
   template <typename T>
   bool isSubset(const std::vector<T>& testVector, const std::set<T>& fullSet)
   {
       for( auto val: testVector)
       {
         if( fullSet.find(val) == fullSet.end() )
             return false;
       }
       return true;
   }


   template <typename T>
   string vectorToJson( const vector<T>& elems )
   {
     string genjson = "[ ";

     for( auto elm: elems )
          genjson += "\n "+to_string(elm) + ",";
     genjson.pop_back();
     genjson += "\n]";

     return genjson;
   }
   template <> string vectorToJson( const vector<string>& elems );

   template <typename T>
   string mapToJson( const map<string,T>& elems )
   {
     string genjson = "{ ";

     for( auto row: elems )
         genjson += "\n \""+row.first+ "\" : " + to_string(row.second) + ",";
     genjson.pop_back();
     genjson += "\n}";

     return genjson;
   }
   template <>  string mapToJson( const map<string,string>& elems );

   /// Returns string representation of current date in dd/mm/yyyy format
   string curDate();

   /// Returns string representation of current date in dd/mm/yy format
   string curDateSmol(char ch = '/');

   /// Returns string representation of current time in HH:MM  format
   string curTime();

} // namespace bsonio

#endif // NSERVICE_H_H

