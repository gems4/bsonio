//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbschema.cpp
/// Implementation of class TDBSchema - working with database
/// Used  SchemaNode class - API for direct code access to internal
/// DOM based on our JSON schemas.
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include "io_settings.h"
#include "dbschemadoc.h"
#include "v_yaml.h"

namespace bsonio {


vector<KeyFldsData> defSchemaKeyFldsInf = { KeyFldsData( "name", "name", "Only name" )};


TDBSchemaDocument* TDBSchemaDocument::newDBSchemaDocument( const TDataBase* dbconnect,
                    const string& schemaName, const string& collcName,
                    const vector<KeyFldsData>& keyFldsInf, const string& queryString  )
{
   if( schemaName.empty() || collcName.empty() )
          return nullptr;
   TDBSchemaDocument* newDoc =  new TDBSchemaDocument( schemaName,
                    dbconnect, collcName.c_str(), keyFldsInf );
   if( !queryString.empty() )
            newDoc->SetQueryJson(queryString);
   newDoc->runQuery();
   return newDoc;
}

//  Constructor
TDBSchemaDocument::TDBSchemaDocument( const string& aschemaName, TDBCollection* collection  ):
  TDBDocumentBase( collection ),
  _schema(ioSettings().Schema()), _schemaName(aschemaName), _currentData(nullptr)
{
   resetSchema( aschemaName, true );
   // set query data
}

TDBSchemaDocument::TDBSchemaDocument( const string& aschemaName, const TDataBase* dbconnect,
                                      const string& coltype, const string& colname,
                                      const vector<KeyFldsData>& keyFldsInf ):
  TDBDocumentBase( dbconnect, coltype, colname, keyFldsInf ),
  _schema(ioSettings().Schema()), _schemaName(aschemaName), _currentData(nullptr)
{}


/// Constructor from bson data
TDBSchemaDocument::TDBSchemaDocument( TDBCollection* collection, const char* bsobj ):
    TDBDocumentBase( collection, bsobj ), _schema(ioSettings().Schema()), _currentData(nullptr)
{
    fromBson(bsobj);
    resetSchema( _schemaName, true );
    // set query data
    SetDefaultQueryFields(makeDefaultQueryFields(_schemaName));
}


// Writes DOD data to ostream file
void TDBSchemaDocument::toBson( bson *obj ) const
{
    TDBDocumentBase::toBson( obj );
    bson_append_string( obj, "SchemaName", _schemaName.c_str() );
}

// Reads DOD data from istream file
void TDBSchemaDocument::fromBson( const char* bsobj )
{
    TDBDocumentBase::fromBson( bsobj );
    if(!bson_find_string( bsobj, "SchemaName", _schemaName ) )
        bsonioErr( "TGRDB0001: ", "Undefined SchemaName.");
}


vector<string>  TDBSchemaDocument::makeDefaultQueryFields(const string& schName )
{
   vector<string> keyFields;
   uint ii;
   ThriftFieldDef*  fldDef;

   ThriftStructDef* strDef = _schema->getStruct(schName);
   if( strDef == nullptr )
       return keyFields;

   if( strDef->getSelectSize() > 0 )
     {
       string fldKey, to_select_name, to_select_doc;
       for( ii=0; ii<strDef->getSelectSize(); ii++ )
       {
         fldKey = strDef->keyFldIdName(ii);
         _schema->getSelectData(fldKey, strDef, to_select_name, to_select_doc );
         keyFields.push_back(to_select_name);
       }
     }
     else // old type of keys
      {
        for( ii=0; ii<strDef->fields.size(); ii++ )
        {
           fldDef = &strDef->fields[ii];
           if( fldDef->fRequired == fld_required )
               keyFields.push_back(fldDef->fName);
        }
      }
   return keyFields;
}


void TDBSchemaDocument::resetSchema( const string& aschemaName, bool change_queries )
{
    /// !!! test not changed collection name for schema
    /// if changed -> error
    //if( schemaName != aschemaName )
    {
      _schemaName = aschemaName;
      _currentData.reset( SchemaNode::newSchemaStruct( _schemaName ) );
    }
    if( change_queries )
    {
        SetDefaultQueryFields(makeDefaultQueryFields(_schemaName));
        _queryResult.setFieldsCollect(_defqueryFields);
        _queryResult.setToSchema(_schemaName);
    }
}


/*void TDBSchemaDocument::recToBson( bson *obj, time_t *crtt*, char* pars )
{
     // test oid
    SchemaNode* oidNode = currentData.get()->field("_id");
    string oidval;
    if(oidNode!=nullptr)
       oidNode->getValue( oidval );

    if( oidNode==nullptr ) // no "_id" into schema definition
    {
       bson bsdata;
        // make bson from current data
       currentData.get()->getStructBson( &bsdata );
       bson bskey;
       bson_init(&bskey);
       _dbdriver->addtobson( &bskey,  pars );
       bson_finish(&bskey);
       bson_init( obj );
       bson_merge(&bsdata, &bskey, false, obj);
       bson_finish( obj);
       bson_destroy(&bsdata);
       bson_destroy(&bskey);
    }
    else
    {
       if( pars )
       {
        if( !oidval.empty() )
        {
          if( oidval != string((char *)pars) )
            bsonioErr("TGRDB0003", "Changed record oid " + string((char *)pars) );
         } else // add oid
          {
              oidNode->setValue( string((char *)pars) );
          }
       }
       else
            if( oidval.empty() )
            {
                oidNode->setValue( genOid() ); // new oid
            }
       // make bson from current data
       currentData.get()->getStructBson( obj );
    }
}*/

void TDBSchemaDocument::recToBson( bson *obj, time_t /*crtt*/, char* pars )
{
     // test oid
    SchemaNode* oidNode = _currentData.get()->field("_id");
    string oidval;
    if(oidNode!=nullptr)
       oidNode->getValue( oidval );

    if( oidNode==nullptr || oidval.empty() ) // no "_id" into schema definition
    {
        if( pars )
            oidNode->setValue( string((char *)pars) );
        else
            oidNode->setValue( _collection->genOid() ); // new oid
    }
    else
    {
      if( pars )
      {
        if( oidval != string((char *)pars) )
            bsonioErr("TGRDB0003", "Changed record oid " + string((char *)pars) );
       }
    }
   // make bson from current data
   _currentData.get()->getStructBson( obj );
}


// Load data from bson structure (return read record key)
string TDBSchemaDocument::recFromBson( bson *obj )
{
    _currentData.get()->setStructBson( obj->data );
    // Get key of record
    string keyStr = getKeyFromBson( obj->data );
    return keyStr;
}

// Return curent record in json format string
string TDBSchemaDocument::GetJson()
{
  bson obj;
  GetBson( &obj );
  string jsonstr;
  jsonFromBson( obj.data, jsonstr );
  bson_destroy(&obj);
  return  jsonstr;
}

// Return curent record in YAML format string
string TDBSchemaDocument::GetYAML()
{
    bson obj;
    GetBson( &obj );
    string yamlstr;
    // record to YAML string
    ParserYAML::printBsonObjectToYAML( yamlstr, obj.data );
    bson_destroy(&obj);
    return  yamlstr;
}


// Set json format string to curent record
void TDBSchemaDocument::SetJsonYaml( const string& sjson, bool is_json )
{
    bson obj;

    if( is_json )  // json
     jsonToBson( &obj, sjson );
    else // yaml
      {
        bson_init(&obj);
        ParserYAML::parseYAMLToBson(sjson, &obj );
        bson_finish(&obj);
      }
    SetBson( &obj );
    bson_destroy(&obj);
}

set<string> TDBSchemaDocument::listQueryFields( bool addkey, const vector<string>& fields)
{
   set<string> queryFields;
   // add key fields
   if( addkey)
     queryFields = _collection->listQueryFields();

   // select view fields
   for(uint ii=0; ii<fields.size(); ii++ )
       queryFields.insert(  fields[ii] );

   return queryFields;
}


// Working with collections
void TDBSchemaDocument::executeQuery()
{
    // init query to fields
    set<string> fields = listQueryFields(  true,  _queryResult.getQuery().getFieldsCollect() );
    _queryResult.clearResults();
    SetReadedFunction setfnc = [=]( const char* bsdata )
    {
        string keyStr = getKeyFromBson( bsdata );
        _queryResult.addLine( keyStr,  (const char *)bsdata, false );
    };
    _collection->selectQuery( fields,  _queryResult.getQuery().getEJDBQuery(), setfnc );
}


// Run query
//  \param _queryJson - json string with where condition
//  \param  _queryFields - list of fields into result json ( if empty only _id )
//  \return _resultData - list of json strings with query result
void TDBSchemaDocument::executeQuery( const string&  qrjson,
        const vector<string>& queryFields,  vector<string>& resultData )
{
    // init query to fields
    set<string> fields = listQueryFields(  false,  queryFields );

    _internalData.clear();
    SetReadedFunction setfnc = [=]( const char* bsdata )
    {
        // added new record to table
        string fieldsQuery;
        jsonFromBson( bsdata, fieldsQuery );
        //cout << fieldsQuery.c_str() << endl;
        _internalData.push_back(fieldsQuery);
    };

    _collection->selectQuery( fields, qrjson, setfnc );
    resultData = _internalData;
}

vector<string> TDBSchemaDocument::distinctQuery( const string& fpath, const string&  queryJson )
{
   vector<string> values;
   _collection->fpathQuery( fpath, queryJson, values );
   return values;
}

void TDBSchemaDocument::SaveBson( bson* obj, bool overwrite, bool testValues )
{
  string key = SetBson(obj);

  if( !Find( key.c_str() ))
  {
     if(overwrite )
     {
       if( !( key.empty() ||  key.find_first_of("*?") != std::string::npos )  )
       {
         SaveRecord( key.c_str() );
         return;
       }
       if( testValues )
       {
           key = GetKeyFromValue(obj->data);
           if( !key.empty() )
           {
             SaveRecord( key.c_str() );
             return;
           }
       }
     }
     InsertRecord();
  }
  else
  {
    if( overwrite )
      SaveRecord( key.c_str() );
  }
}

string TDBSchemaDocument::InsertCurrent( bool testValues )
{
   string stroid;
   if( testValues )
   {
     string key = GetKeyFromValueNode(_currentData.get());
     if( !key.empty() )
     {
        SaveRecord( key.c_str() );
        getValue( "_id", stroid );
        //strip_all( key, ":" );
        return stroid;
      }
   }
   stroid = genOid();
   setValue("_id", stroid );
   InsertRecord();
   return stroid;
}

// Get current record key from internal data
string TDBSchemaDocument::getKeyFromCurrent()
{
    string keyStr = "", kbuf;
    for(uint ii=0; ii<_collection->keyNumFlds(); ii++ )
    {
        if( !getValue( _collection->keyFldShortName(ii), kbuf  ) )
            if( !getValue( _collection->keyFldName(ii), kbuf  ) )
          kbuf = "---";
        strip( kbuf );
        if( kbuf.empty() )
           kbuf = "*";
        keyStr += kbuf;
        keyStr += ":";
    }
    return keyStr;
}


void TDBSchemaDocument::SaveCurrent( bool overwrite, bool testValues )
{
  string key = getKeyFromCurrent();

  if( !Find( key.c_str() ))
  {
     if(overwrite )
     {
       if( !( key.empty() ||  key.find_first_of("*?") != std::string::npos )  )
       {
         SaveRecord( key.c_str() );
         return;
       }
       InsertCurrent( testValues );
       return;
     }
     InsertRecord();
  }
  else
  {
    if( overwrite )
      SaveRecord( key.c_str() );
  }
}

} // namespace bsonio
