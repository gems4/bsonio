//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file TBSONSerializer.cpp
/// Implementation of bson+schema protocol for Thrift
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include "TBSONSerializer.h"
#include "ar2base.h"
#include "v_json.h"

#include <limits>
#include <locale>
#include <sstream>
#include <cmath>

namespace bsonio {

TBSONSerializer::TBSONSerializer( const ThriftSchema* aschema):
    TVirtualProtocol<TBSONSerializer>(0),
    schema(aschema), bobj_tansport(0)
{}

TBSONSerializer::~TBSONSerializer()
{}


uint32_t TBSONSerializer::writeMessageBegin(const std::string& name,
                                          const TMessageType messageType,
                                          const int32_t seqid)
{
  uint32_t result = 0;
  tranWriteStart();

  contexts_.push( TBSONWriteContext2( name.c_str(), T_LIST, 0, 4 ));
  bson_append_start_array(getBson(), name.c_str() );
  bson_append_int( getBson(), "0", 1 /*kThriftVersion1*/);
  bson_append_string( getBson(), "1", name.c_str() );
  bson_append_int( getBson(), "2", messageType );
  bson_append_int( getBson(), "3", seqid );
  return result;
}

uint32_t TBSONSerializer::writeMessageEnd()
{
  bson_append_finish_array(getBson());
  contexts_.pop();
  return transWriteEnd();
}

uint32_t TBSONSerializer::writeStructBegin(const char* name)
{
  tranWriteStart();
  strSchema.push( schema->getStruct(name));
  if( contexts_.size() > 0 )
      bson_append_start_object(getBson(), contexts_.top().getKey().c_str() );
  return 0;
}

uint32_t TBSONSerializer::writeStructEnd()
{
  strSchema.pop();
  if( contexts_.size() > 0 )
    bson_append_finish_object(getBson() );
  return transWriteEnd();
}

uint32_t TBSONSerializer::writeFieldBegin(const char* name,
                                        const TType fieldType,
                                        const int16_t fieldId)
{
  contexts_.push( TBSONWriteContext2( name, fieldType, fieldId, 0 ));
  return 0;
}

uint32_t TBSONSerializer::writeFieldEnd()
{
  contexts_.pop();
  return 0;
}

uint32_t TBSONSerializer::writeFieldStop()
{
  return 0;
}

uint32_t TBSONSerializer::writeMapBegin(const TType /*keyType*/,
                                      const TType /*valType*/,
                                      const uint32_t /*size*/)
{
  bson_append_start_object(getBson(), contexts_.top().getName() );
  return 0;
}

uint32_t TBSONSerializer::writeMapEnd()
{
  bson_append_finish_object(getBson() );
  return 0;
}

uint32_t TBSONSerializer::writeListBegin(const TType /*elemType*/, const uint32_t /*size*/)
{
  bson_append_start_array(getBson(), contexts_.top().getName() );
  return 0;
}

uint32_t TBSONSerializer::writeListEnd()
{
  bson_append_finish_array(getBson() );
  return 0;
}

uint32_t TBSONSerializer::writeSetBegin(const TType /*elemType*/, const uint32_t /*size*/)
{
   bson_append_start_array(getBson(), contexts_.top().getName() );
   return 0;
}

uint32_t TBSONSerializer::writeSetEnd()
{
    bson_append_finish_array(getBson() );
    return 0;
}

uint32_t TBSONSerializer::writeBool(const bool value)
{
  string key = contexts_.top().getKey();
  if( key == ismapkey2)
    contexts_.top().setMapKey( to_string(value) );
  else
    bson_append_bool( getBson(), key.c_str(), value );
  return 0;
}

uint32_t TBSONSerializer::writeByte(const int8_t byte)
{
  // writeByte() must be handled specially because boost::lexical cast sees
  // int8_t as a text type instead of an integer type
    string key = contexts_.top().getKey();
    if( key == ismapkey2)
      contexts_.top().setMapKey( to_string(byte) );
    else
      bson_append_int( getBson(), key.c_str(), (int16_t)byte );
  return 0;
}

uint32_t TBSONSerializer::writeI16(const int16_t i16)
{
    string key = contexts_.top().getKey();
    if( key == ismapkey2)
      contexts_.top().setMapKey( to_string(i16) );
    else
      bson_append_int( getBson(), key.c_str(), i16 );
  return 0;
}

uint32_t TBSONSerializer::writeI32(const int32_t i32)
{
    string key = contexts_.top().getKey();
    if( key == ismapkey2)
      contexts_.top().setMapKey( to_string(i32) );
    else
       bson_append_int( getBson(), key.c_str(), i32 );
  return 0;
}

uint32_t TBSONSerializer::writeI64(const int64_t i64)
{
    string key = contexts_.top().getKey();
    if( key == ismapkey2)
      contexts_.top().setMapKey( to_string(i64) );
    else
      bson_append_long( getBson(), key.c_str(), i64 );
  return 0;
}

uint32_t TBSONSerializer::writeDouble(const double dub)
{
    string key = contexts_.top().getKey();
    if( key == ismapkey2)
      contexts_.top().setMapKey( to_string(dub) );
    else
      bson_append_double( getBson(), key.c_str(), dub );
  return 0;
}

uint32_t TBSONSerializer::writeString(const std::string& str)
{
    string key = contexts_.top().getKey();
    if( key == ismapkey2)
      contexts_.top().setMapKey( str );
    else
        if( key == JDBIDKEYNAME  )
        {  // oid
            if( !str.empty() && str != bsonio::emptiness)
            { bson_oid_t oid;
              bson_oid_from_string( &oid, str.c_str() );
              bson_append_oid( getBson(), JDBIDKEYNAME, &oid);
            }
        }
       else
          bson_append_string( getBson(), key.c_str(), str.c_str() );
  return 0;
}

uint32_t TBSONSerializer::writeBinary(const std::string& str)
{
    bson_append_binary( getBson(), contexts_.top().getKey().c_str(),
                        0, str.c_str(), str.length() );
    return 0;
}

/**
 * Reading functions
 */

// not implemented only for structures
uint32_t TBSONSerializer::readMessageBegin(std::string& /*name*/,
                                         TMessageType& /*messageType*/,
                                         int32_t& /*seqid*/)
{
   uint32_t returns = 0; /*transReadStart();

    // get array data
    bson_iterator_next(&bitr.top());
    bson_iterator i = bitr.top();
    name = bson_iterator_key(&i);
    bson_type type = bson_iterator_type(&i);

    if ( type != BSON_ARRAY )
        throw TProtocolException(TProtocolException::INVALID_DATA,
              "Expected BSON array; field \"" + name + "\"");

   const char* arrdata = bson_iterator_value(&i);
   bson_iterator itval;
   bson_iterator_from_buffer(&itval, arrdata);
   readcntxt_.push( TBSONReadContext2( name.c_str(), itval, true,  false ) );

   // read first  4 fields
   int kThriftVersion1 = readInt();
   if ( 1 != kThriftVersion1) {
     throw TProtocolException(TProtocolException::BAD_VERSION, "Message contained bad version.");
   }
   readString( name );
   messageType = static_cast<TMessageType>(readInt());
   seqid = readInt();
*/
    return returns;
}

uint32_t TBSONSerializer::readMessageEnd()
{
  //transReadEnd();
  return 0;
}

uint32_t TBSONSerializer::readStructBegin(std::string& /*name*/)
{
    uint32_t returns = transReadStart();
    return returns;
}

uint32_t TBSONSerializer::readStructEnd()
{
    transReadEnd();
    return 0;
}

uint32_t TBSONSerializer::readFieldBegin(std::string& name, TType& fieldType, int16_t& fieldId)
{
   if( bson_iterator_next(&bitr.top()))
   {
       bson_iterator it = bitr.top();
       name = bson_iterator_key(&it);
       //bson_type type = bson_iterator_type(&it);

       // Get field definition by name
       ThriftFieldDef fldinf = strSchema.top()->getSchema( name );
       fieldId = fldinf.fId;
       if( fieldId >=  0 )
         fieldType = static_cast<TType>(fldinf.fTypeId[0]);

       readcntxt_.push( TBSONReadContext2( fldinf, it ) );

   } else // stop process
      {
        fieldType = ::apache::thrift::protocol::T_STOP;
      }
  return 0;
}

uint32_t TBSONSerializer::readFieldEnd()
{
  readcntxt_.pop();
  return 0;
}

uint32_t TBSONSerializer::readMapBegin(TType& keyType, TType& valType, uint32_t& size)
{
   readcntxt_.top().getMap( keyType, valType,size );
   return 0;
}

uint32_t TBSONSerializer::readMapEnd()
{
  readcntxt_.top().endMap();
  return 0;
}

uint32_t TBSONSerializer::readListBegin(TType& elemType, uint32_t& size)
{
   readcntxt_.top().getList( elemType, size );
   return 0;
}

uint32_t TBSONSerializer::readListEnd()
{
  readcntxt_.top().endList();
  return 0;
}

uint32_t TBSONSerializer::readSetBegin(TType& elemType, uint32_t& size)
{
  readcntxt_.top().getList( elemType, size );
  return 0;
}

uint32_t TBSONSerializer::readSetEnd()
{
  readcntxt_.top().endList();
  return 0;
}

uint32_t TBSONSerializer::readBool(bool& value)
{
    std::string mapkey;
    bson_iterator *it = readcntxt_.top().getIterator( mapkey );
    if( mapkey.empty() )
     value = bson_iterator_bool(it);
    else
     value = std::stoi(mapkey);
    return 0;
}

int TBSONSerializer::readInt()
{
    int ii=0;
    std::string mapkey;
    bson_iterator *it = readcntxt_.top().getIterator( mapkey );
    if( mapkey.empty() )
     ii = bson_iterator_int(it);
    else
     ii = std::stoi(mapkey);
  return ii;
}


// readByte() must be handled properly because boost::lexical cast sees int8_t
// as a text type instead of an integer type
uint32_t TBSONSerializer::readByte(int8_t& byte)
{
  byte = readInt();
  return 0;
}

uint32_t TBSONSerializer::readI16(int16_t& i16)
{
  i16 = readInt();
  return 0;
}

uint32_t TBSONSerializer::readI32(int32_t& i32)
{
  i32 = readInt();
  return 0;
}

uint32_t TBSONSerializer::readI64(int64_t& i64)
{
    std::string mapkey;
    bson_iterator *it = readcntxt_.top().getIterator( mapkey );
    if( mapkey.empty() )
     i64 = bson_iterator_long(it);
    else
     i64 = std::stol(mapkey);
  return 0;
}

uint32_t TBSONSerializer::readDouble(double& dub)
{
    std::string mapkey;
    bson_iterator *it = readcntxt_.top().getIterator( mapkey );
    if( mapkey.empty() )
     dub = bson_iterator_double(it);
    else
     dub = std::stod(mapkey);
  return 0;
}

uint32_t TBSONSerializer::readString(std::string& str)
{
   std::string mapkey;
   bson_iterator *it = readcntxt_.top().getIterator( mapkey );
   if( mapkey.empty() )
   {
       if(  readcntxt_.top().getName() == JDBIDKEYNAME   )
       {  // oid
           char oidhex[25];
           bson_oid_to_string(bson_iterator_oid(it), oidhex);
           str = oidhex;
       }
      else
       // str = bson_iterator_string(it);
       {
           bson_type t = bson_iterator_type(it);
           if( !bsonio::bson_to_string( t, it, str ) )
             str = "";
       }
    }
   else
    str = mapkey;
   return 0;
}

uint32_t TBSONSerializer::readBinary(std::string& str)
{
    std::string mapkey;
    bson_iterator *it = readcntxt_.top().getIterator( mapkey );
    str = bson_iterator_bin_data(it);
    return 0;
}

} // namespace bsonio
