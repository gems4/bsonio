//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbcollection.h
/// Declarations of class TDBAbstract - base interface of abstract DB chain
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TDCOLLECTION_H
#define TDCOLLECTION_H

//#include <set>
//#include <map>
#include <memory>
#include "dbkeyfields.h"

namespace bsonio {

class TDBDocumentBase;
class TDataBase;

using KeysSet = map<IndexEntry, unique_ptr<char>, less<IndexEntry> >;
using  SetReadedFunction = std::function<void( const char* bsdata )>;
using  SetReadedFunctionKey = std::function<void( const char* bsdata, char *keydata )>;


/// Interface for Abstract Database Driver
class TAbstractDBDriver
{

public:

    ///  Constructor
    TAbstractDBDriver( ) {}

   ///  Destructor
   virtual ~TAbstractDBDriver() {}

   /// Gen new oid or other pointer of location
   virtual string genOid(const char* collname) = 0 ;

   /// Read record from DB to bson
   virtual bson *loadbson( const char* collname, KeysSet::iterator& it ) = 0;
   /// Remove current object from collection.
   virtual bool rmbson(const char* collname, KeysSet::iterator& it ) = 0;
   /// Write record to DB
   virtual bool savebson( const char* collname, KeysSet::iterator& it, bson *bsrec, string& errejdb ) = 0;

   /// Run query
   ///  \param collname - collection name
   ///  \param queryFields - list of fileds selection
   ///  \param queryJson - json string with query  condition (where)
   ///  \param setfnc - function for set up readed data
   virtual void selectQuery( const char* collname, const set<string>& queryFields,
        const string&  queryJson,  SetReadedFunction setfnc ) = 0;

   /// Run query for read Linked records list
   ///  \param collname - collection name
   ///  \param queryFields - list of fileds selection
   ///  \param setfnc - function for set up Linked records list
   virtual void allQuery( const char* collname,
          const set<string>& queryFields,  SetReadedFunctionKey setfnc ) = 0;

   /// Run delete query
   ///  \param collname - collection name
   ///  \param query - qury  condition (where)
   virtual void delQuery(const char* collname, bson* query ) = 0;

    ///  Provides 'distinct' operation over query
    ///  \param fpath Field path to collect distinct values from.
    ///  \param queryJson - json string with where condition
    ///  \param return values by specified fpath and query
    virtual void fpathQuery( const char* collname, const string& fpath,
                             const string& queryJson, vector<string>& values ) = 0;

};

/// Definition of DB collection
class TDBCollection: public IsUnloadToBson
{

    friend class TDBDocumentBase;

    string _type = "undef";
    string _name;                  ///< Name of modules DB
    TEJDBKey _key;                 ///< Definition of record key
    const TDataBase *_dbconnect; ///< Access to the database
    TAbstractDBDriver *_dbdriver; ///< Access to the database driver

    // Definition of record key list
    KeysSet _recList;        ///< Linked records list
    KeysSet::iterator _itrL; ///< Current index in recList

    set<TDBDocumentBase*> _documents; ///< Linked documents

    // internal functions

    /// Load all keys from one collection
    virtual void loadCollectionFile(const set<string>& queryFields );
    virtual void closeCollectionFile() {}

    void closeCollection()
    {
        closeCollectionFile();
    }

    void loadCollection()
    {
        set<string> keys = listQueryFields();
        loadCollectionFile( keys );
    }

    /// Get record key from bson structure
    virtual string getKeyFromBson( const char* bsdata );
    /// Add current record key to bson structure
    virtual void addKeyToBson( bson *obj );

    /// Add key from bson structure to recList
    void listKeyFromBson( const char* bsdata, char* keydata );

 public:

    enum RecStatus {   // states of keys record Data Base
        UNDF_=-7 /* undefined state */, FAIL_=-1 /* access error */,
        NONE_=0 /* no records */, ONEF_ ,
        MANY_,  EMPC_ /* empty chain */
    };

    ///  Constructor
    TDBCollection( const TDataBase* adbconnect, const char* name, const vector<KeyFldsData>& keyFldsInf  );
    ///  Destructor
    virtual ~TDBCollection()
    { }

    /// Constructor from bson data
    TDBCollection( const TDataBase* adbconnect, const char* bsobj );
    virtual void toBson( bson *obj ) const;
    virtual void fromBson( const char *obj );

    /// Open Data Base files and build linked record list
    void Open();
    /// Close all Data Base files and free linked record list
    void Close();

    /// Reconnect DataBase (collection)
    void changeDriver( TAbstractDBDriver* adriver );
    /// reload keys list and update queryes
    void reloadCollection();

    void addDocument( TDBDocumentBase* doc)
    {
      _documents.insert(doc);
    }
    void removeDocument( TDBDocumentBase* doc)
    {
      auto itr =  _documents.find(doc);
      if( itr != _documents.end() )
         _documents.erase(doc);
    }
    /// Update type of collection
    void setType( const string& coltype )
    {
      _type = coltype;
    }
    string type() const
    {
      return _type;
    }

    //--- Manipulation records

    /// Find record with key into internal record keys list
    bool Find( const char *key );
    /// Test state of record with key key_ as template.
    /// in field field setted any(*) data
    bool FindPart( const char *key_, int field );
    /// Retrive one record from the collection
    void GetRecord( TDBDocumentBase* document, const char *key );
    /// Removes record from the collection
    void DelRecord( const char *key );
    /// Save/update record in the collection
    void SaveRecord( TDBDocumentBase* document, const char* key );
    /// Save new record in the collection
    void InsertRecord( TDBDocumentBase* document, const char *key=0 );
    /// Test state of record with key pkey.
    RecStatus Rtest( const char *pkey );

    /// Gen new oid or other pointer of location
    virtual string genOid()
    {
        return _dbdriver->genOid(getName());
    }

    //--- Manipulation list of records (tables)

    /// Build list of key fields for query
    virtual set<string> listQueryFields();

    /// Run query
    ///  \param queryFields - list of fileds selection
    ///  \param queryJson - json string with query  condition (where)
    ///  \param setfnc - function for set up readed data
    void selectQuery( const set<string>& queryFields,
         const string&  queryJson,  SetReadedFunction setfnc )
    {
       _dbdriver->selectQuery( getName(), queryFields, queryJson, setfnc );
    }

    /// Run query for read Linked records list
    ///  \param queryFields - list of fileds selection
    ///  \param setfnc - function for set up Linked records list
    void allQuery( const set<string>& queryFields,
                          SetReadedFunctionKey setfnc )
    {
        _dbdriver->allQuery( getName(), queryFields, setfnc );
    }

    /// Run delete query
    ///  \param query - qury  condition (where)
    void delQuery( bson* query )
    {
       _dbdriver->delQuery( getName(), query );
       reloadCollection();
    }

     ///  Provides 'distinct' operation over query
     ///  \param fpath Field path to collect distinct values from.
     ///  \param queryJson - json string with where condition
     ///  \param return values by specified fpath and query
     void fpathQuery( const string& fpath, const string& queryJson, vector<string>& values )
     {
       _dbdriver->fpathQuery( getName(), fpath, queryJson, values );
     }

    /// Get key list for a wildcard search
    virtual int GetKeyList( const char *keypat,
        vector<string>& aKeyList, bool retUnpackform = true );


    //--- Selectors

    /// Get name of modules DB
    const char* getName() const
    {  return _name.c_str();   }
    /// Get records count in opened files
    int getRecCount() const
       {  return _recList.size(); }
    bool CompareTemplate( const char* keypat, const char* akey);


    //--- Selectors for key

    /// Return current record key in packed form
    const char *packKey()
        { return _key.packKey(); }
    /// Return current record key in unpacked form
    const char *unpackKey()
        { return _key.unpackKey(); }
    /// Return record key field i
    const char *keyFld( int i ) const
        { return _key.keyFld(i); }
    /// Return number or record key fields
    uint keyNumFlds() const
        { return _key.keyNumFlds();  }
    /// Access to TEJDBKey
    const TEJDBKey& getDBKey() const
        { return _key; }
    /// Putting DB record key pkey to internal structure
    virtual void setKey( const char *ckey )
        {  _key.setKey(ckey);  }
    /// Change i-th field of TEJDBKey to key
    void setKeyFld( uint i, const char *fld )
        {  _key.setFldKey( i, fld ); }
    /// Return record key field name i
    const char* keyFldName(int i) const
        { return _key.keyFldName(i); }
    /// Return short record key field name i
    const char* keyFldShortName(int i) const
    {   return _key.keyFldShortName(i); }
    const char* keyFldDesc(int i) const
    {   return _key.keyFldDesc(i); }
    /// Return record key field size
    uint keyFldSize(int i) const
    {   return _key.keyFldSize(i); }
    /// Return full record key size
    int keySize() const
    {   return _key.keySize(); }

};

} // namespace bsonio

#endif // TDCOLLECTION_H
