//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbserver.cpp
/// Implementation of class DBServerHandler - a service interface
/// for RPC client and server to communicate over.
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include "dbserverejdb.h"
#include "json2cfg.h"

using namespace ::apache::thrift::stdcxx;

TSimpleServer* makeServer( const char *cfg_path )
{
  // read a Cfg file for the file names list file
  bsonio::ConfigJson cfgfile( cfg_path );

  string DBpath = cfgfile.getString("LocalDBDirectory", ".");
  string DBname = cfgfile.getString("LocalDBName", "data");
  int port =      cfgfile.getValue("port", (int)9090 ); //9090
  DBpath += "/"+DBname;

  /*boost::shared_ptr<EJDBServerHandler> handler(new EJDBServerHandler(DBpath.c_str()));
  boost::shared_ptr<TProcessor> processor(new DBServerProcessor(handler));
  boost::shared_ptr<TServerTransport> serverTransport(new TServerSocket(port));
  boost::shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());
  boost::shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());
  */
  ::apache::thrift::stdcxx::shared_ptr<EJDBServerHandler> handler(new EJDBServerHandler(DBpath.c_str()));
  ::apache::thrift::stdcxx::shared_ptr<TProcessor> processor(new DBServerProcessor(handler));
  ::apache::thrift::stdcxx::shared_ptr<TServerTransport> serverTransport(new TServerSocket(port));
  ::apache::thrift::stdcxx::shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());
  ::apache::thrift::stdcxx::shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

  return new TSimpleServer(processor, serverTransport, transportFactory, protocolFactory);
}

ResponseCode::type EJDBServerHandler::ping()
{
  cout << "ping" << endl;
  return ResponseCode::Success;
}

void EJDBServerHandler::stop()
{
  cout << "stop" << endl;
  // Your implementation goes here
}

ResponseCode::type EJDBServerHandler::addCollection(const std::string& colName)
{
  cout << "addCollection " << colName << endl;
  //boost::unique_lock< boost::shared_mutex> writeLock(mutex_);;
  auto itr = collecs_.find(colName);
  if (itr != collecs_.end())
      return ResponseCode::NameExists;

  try{
       // create collection
       getCollection(colName, true );
       // init new DB collection
       collecs_.insert( colName);
       return ResponseCode::Success;
      }
   catch( ... ) {
           return ResponseCode::Error;
         }
}

ResponseCode::type EJDBServerHandler::dropCollection(const std::string& colName)
{
  cout << "dropCollection " << colName << endl;
  // boost::unique_lock< boost::shared_mutex> writeLock(mutex_);;
  auto itr = collecs_.find(colName);
  if (itr == collecs_.end())
         return ResponseCode::NameNotFound;

  try{
      /// ??? delete collection into EJDB
      collecs_.erase(itr);
     }
   catch( ... ) {
       return ResponseCode::Error;
     }
   return ResponseCode::Success;
}

void EJDBServerHandler::listCollections(StringListResponse& _return)
{
   cout << "dropCollection " << endl;
  // boost::unique_lock< boost::shared_mutex> writeLock(mutex_);;
  auto itr = collecs_.begin();
  while(itr != collecs_.end())
     _return.names.push_back( *itr++ );
 _return.responseCode = ResponseCode::Success;
}

void EJDBServerHandler::GenerateId(std::string& _return, const std::string& /*colName*/)
{
    bson_oid_t oid;
    bson_oid_gen( &oid );
    char bytes[25];
    bson_oid_to_string( &oid, bytes );
    _return = string(bytes);
}


void EJDBServerHandler::ReadRecord(ValueResponse& _return,
       const std::string& colName, const std::string& key)
{
  cout << "ReadRecord " << colName << " Key: " << key << endl;
  // boost::shared_lock< boost::shared_mutex> readLock(mutex_);; must lock mutex for current collection
  /** auto itr = collecs_.find(colName);
   if (itr == collecs_.end())
   {
       _return.responseCode = ResponseCode::NameNotFound;
       return;
   }*/

   bson *bsrec=0;
   try{
        // Get oid of record
        bson_oid_t oid;
        getBsonOid( &oid, key );
        // Get current collection
        EJCOLL *coll = getCollection( colName, false );
        // Read bson structure from collection by index oid
        bsrec = ejdbloadbson(coll, &oid);
        if( !bsrec )
        {
            string errejdb  = "Error Loading record ";
                   errejdb += key +" from Data Base";
           _return.responseCode = ResponseCode::RecordNotFound;
           _return.errormsg = errejdb;
        }
        else
        {
            // bson to json string
            jsonFromBson( bsrec->data, _return.value );
            _return.responseCode = ResponseCode::Success;
        }
    }
   catch(bsonio_exeption& e)
   {
       _return.responseCode = ResponseCode::Error;
       _return.errormsg = e.what();
   }
   catch( ... )
    {
       _return.responseCode = ResponseCode::Error;
       _return.errormsg = strerror(errno);
    }

   // free memory
   bson_del( bsrec );
}

void EJDBServerHandler::CreateRecord(KeyResponse& _return,
          const std::string& colName, const std::string& value)
{
  cout << "CreateRecord " << colName <<  endl;
  // boost::shared_lock< boost::shared_mutex> readLock(mutex_);; must lock mutex for current collection
  /** auto itr = collecs_.find(colName);
  if (itr == collecs_.end())
  {   _return.responseCode = ResponseCode::NameNotFound;
      return;
  }*/
  bson bsrec;
  try{
      // convert value to bson
      jsonToBson( &bsrec, "", value );

      bson_oid_t oid;
      // Get current collection
      EJCOLL *coll = getCollection( colName, true );
      // Persist BSON object in the collection
      bool retSave = ejdbsavebson( coll, &bsrec, &oid );
      if( !retSave )
      {
         _return.responseCode = ResponseCode::Error;
         _return.errormsg = bson_first_errormsg(&bsrec);
      }
      else
      {
        setBsonOid(&oid, _return.key );
        _return.responseCode = ResponseCode::Success;
      }
   }
   catch(bsonio_exeption& e)
   {
       _return.responseCode = ResponseCode::Error;
       _return.errormsg = e.what();
   }
   catch( ... )
    {
       _return.responseCode = ResponseCode::Error;
       _return.errormsg = strerror(errno);
    }
  bson_destroy(&bsrec);
  cout << "CreateRecord Key " << _return.key <<  endl;
}

ResponseCode::type EJDBServerHandler::UpdateRecord(const std::string& colName,
         const std::string& key, const std::string& value)
{
  cout << "UpdateRecord " << colName << " Key: " << key << endl;
  ResponseCode::type _return;
  // boost::shared_lock< boost::shared_mutex> readLock(mutex_);; must lock mutex for current collection
  /** auto itr = collecs_.find(colName);
  if (itr == collecs_.end())
       return ResponseCode::NameNotFound;
  */
  bson bsrec;
  try{
      // convert value to bson
      jsonToBson( &bsrec, key, value );

      bson_oid_t oid;
      // Get current collection
      EJCOLL *coll = getCollection( colName, true );
      // Persist BSON object in the collection
      bool retSave = ejdbsavebson( coll, &bsrec, &oid );
      if( !retSave )
      {
         _return = ResponseCode::Error;
         //_return.errormsg = bson_first_errormsg(bsrec);
      }
      else
      {
        _return = ResponseCode::Success;
      }
   }
   catch(bsonio_exeption& )
   {
       _return = ResponseCode::Error;
       //_return.errormsg = e.what();
   }
   catch(...)
    {
       _return = ResponseCode::Error;
    }
  bson_destroy(&bsrec);
  return _return;
}

ResponseCode::type EJDBServerHandler::DeleteRecord(const std::string& colName, const std::string& key)
{
  cout << "DeleteRecord " << colName << " Key: " << key << endl;
  // boost::shared_lock< boost::shared_mutex> readLock(mutex_);; must lock mutex for current collection
  /** auto itr = collecs_.find(colName);
  if (itr == collecs_.end())
      return ResponseCode::NameNotFound;
  */
  try{
      // Get oid of record
      bson_oid_t oid;
      getBsonOid( &oid, key );
      // Get current collection file
      EJCOLL *coll = getCollection( colName, false );
      // Remove BSON object from collection.
      if( ejdbrmbson( coll,  &oid ) )
        return  ResponseCode::Success;
      else
        return  ResponseCode::RecordNotFound;
     }
  catch(...)
   {
       return ResponseCode::Error;
   }
}


void EJDBServerHandler::SearchRecords(DBRecordListResponse& _return, const std::string& colName,
                                    const std::set<std::string>& fields, const std::string& recordQuery)
{
   cout << "SearchRecords " << colName << " Query: " << recordQuery << endl;
  // boost::shared_lock< boost::shared_mutex> readLock(mutex_);; must lock mutex for current collection
  /** auto itr = collecs_.find(colName);
   if (itr == collecs_.end())
   {   _return.responseCode = ResponseCode::NameNotFound;
        return;
   }*/

   bson bshits1;
   bson bsq1;
   bson_reset_data(&bshits1);
   bson_reset_data(&bsq1);
   EJQ *q1 = 0;
   TCXSTR *log = 0;
   TCLIST *q1res = 0;

   try{

       bson_init_as_query(&bshits1);
       bson_append_start_object(&bshits1, "$fields");
       bson_append_int(&bshits1, JDBIDKEYNAME, 1);
       auto it = fields.begin();
       while( it!= fields.end())
           bson_append_int(&bshits1, (it++)->c_str(), 1);
       bson_append_finish_object(&bshits1);
       bson_finish(&bshits1);

       // select all records or by recordQuery
       string _queryJson = replace_all( recordQuery, "\'", "\"");
       bson_init_as_query(&bsq1);
       if( !_queryJson.empty() )
       { ParserJson pars;
         pars.setJsonText( _queryJson.substr( _queryJson.find_first_of('{')+1 ) );
         pars.parseObject( &bsq1 );
       }
       bson_finish(&bsq1);

       q1 = ejdbcreatequery( curentEJDB.ejDB, &bsq1, NULL, 0, /*NULL*/ &bshits1 );
       // bshits1 query to order or fields to be loaded
       bsonioErrIf( !q1, "SEJDB0003", "Error in query (test)" );

       // Get current collection file
       EJCOLL *coll = getCollection( colName, false );
       if( coll )
       {
           uint32_t count = 0;
           log = tcxstrnew();
           q1res = ejdbqryexecute(coll, q1, &count, 0, log);
           //fprintf(stderr, "%s", TCXSTRPTR(log));

           cout << count << " Records in collection " << endl;
           DBRecord rec;
           for (int i = 0; i < TCLISTNUM(q1res); ++i)
           {
            void *bsdata = TCLISTVALPTR(q1res, i);
            // make oid
            bson_iterator it;
            bson_find_from_buffer(&it, (char *)bsdata, JDBIDKEYNAME );
            setBsonOid(bson_iterator_oid(&it), rec.key );
            jsonFromBson( (char *)bsdata, rec.value );
            //cout << rec.key << "  " << rec.value << endl;
            _return.records.push_back( rec );
         }
       }
       _return.responseCode = ResponseCode::Success;
   }
   catch(bsonio_exeption& e)
   {
       if( e.title() == string("SEJDB0002") ) // empty record
           _return.responseCode = ResponseCode::Success;
       else
           _return.responseCode = ResponseCode::Error;
       _return.errormsg = e.what();
   }
   catch( ...)
   {
     _return.responseCode = ResponseCode::Error;
     _return.errormsg = strerror(errno);
   }

   bson_destroy(&bshits1);
   bson_destroy(&bsq1);
   if(q1res) tclistdel(q1res);
   if(log) tcxstrdel(log);
   if(q1) ejdbquerydel(q1);
}

/*
 * Provides 'distinct' operation over query
 *
 * @param colName       collection name
 * @param fpath Field path to collect distinct values from.
 * @param recordQuery   json query string for collection
 * @return RecordListResponse
 *             responseCode - Success
 *                          - NameNotFound collection doesn't exist.
 *                          - Error on any other errors
 *             records - list of distinct values filed fpath.
 *
 * @param colName
 * @param fpath
 * @param recordQuery
 */
void EJDBServerHandler::SearchValues(StringListResponse& _return, const std::string& colName,
                                     const std::string& fpath, const std::string& recordQuery)
{
   cout << "SearchValues " << colName << " Query: " << recordQuery << endl;
  // boost::shared_lock< boost::shared_mutex> readLock(mutex_);; must lock mutex for current collection
  /** auto itr = collecs_.find(colName);
   if (itr == collecs_.end())
   {   _return.responseCode = ResponseCode::NameNotFound;
        return;
   }*/

   bson bsq1;
   bson_reset_data(&bsq1);
   TCXSTR *log = 0;
   bson *q1res =0;

   try{

       // select all records or by recordQuery
       string _queryJson = replace_all( recordQuery, "\'", "\"");
       bson_init_as_query(&bsq1);
       if( !_queryJson.empty() )
       { ParserJson pars;
         pars.setJsonText( _queryJson.substr( _queryJson.find_first_of('{')+1 ) );
         pars.parseObject( &bsq1 );
       }
       bson_finish(&bsq1);

       EJCOLL *coll = getCollection( colName, false );
       if( coll )
       {
           uint32_t count = 0;
           log = tcxstrnew();
           q1res = ejdbqrydistinct(coll, fpath.c_str(), &bsq1, NULL, 0, &count, log);
           bsonioErrIf( !q1res, "TGRDB0015", "Error in query (test)" );

           //
           _return.names.clear();
            string value;
            bson_iterator i;
            bson_iterator_from_buffer(&i, q1res->data);
            while (bson_iterator_next(&i))
            {
               const char* key = bson_iterator_key(&i);
               if( !bson_to_string( q1res->data, key, value ) )
                   value = "";
               _return.names.push_back(value);
            }
        }
       _return.responseCode = ResponseCode::Success;
   }
   catch(bsonio_exeption& e)
   {
       if( e.title() == string("SEJDB0002") ) // empty record
           _return.responseCode = ResponseCode::Success;
       else
           _return.responseCode = ResponseCode::Error;
       _return.errormsg = e.what();
   }
   catch( ...)
   {
     _return.responseCode = ResponseCode::Error;
     _return.errormsg = strerror(errno);
   }

   bson_destroy(&bsq1);
   if( q1res) bson_del(q1res);
   if(log) tcxstrdel(log);
}


/*
 * Delete list of records in a collection.
 *
 * @param colName       collection name
 * @param recordQuery   json query string for delete
 * @returns Success
 *          NameNotFound collection doesn't exist.
 *          Error
 *
 * @param colName
 * @param deleteQuery
 */
ResponseCode::type EJDBServerHandler::DeleteRecordsQuery(const std::string& colName, const std::string& deleteQuery)
{
  cout << "DeleteRecordsQuery " << colName << " Query: " << deleteQuery << endl;
  // boost::shared_lock< boost::shared_mutex> readLock(mutex_);; must lock mutex for current collection
  /** auto itr = collecs_.find(colName);
   if (itr == collecs_.end())
   {   _return.responseCode = ResponseCode::NameNotFound;
        return;
   }*/

   ResponseCode::type _return;
   bson bshits1;
   bson bsq1;
   bson_reset_data(&bshits1);
   bson_reset_data(&bsq1);
   EJQ *q1 = 0;
   TCXSTR *log = 0;
   TCLIST *q1res = 0;

   try{

       bson_init_as_query(&bshits1);
       bson_append_start_object(&bshits1, "$fields");
       bson_append_int(&bshits1, JDBIDKEYNAME, 1);
       bson_append_finish_object(&bshits1);
       bson_finish(&bshits1);

       // select all records or by recordQuery
       string _queryJson = replace_all( deleteQuery, "\'", "\"");
       bson_init_as_query(&bsq1);
       if( !_queryJson.empty() )
       { ParserJson pars;
         pars.setJsonText( _queryJson.substr( _queryJson.find_first_of('{')+1 ) );
         pars.parseObject( &bsq1 );
       }
       bson_finish(&bsq1);

       q1 = ejdbcreatequery( curentEJDB.ejDB, &bsq1, NULL, 0, /*NULL*/ &bshits1 );
       // bshits1 query to order or fields to be loaded
       bsonioErrIf( !q1, "SEJDB0004", "Error in delete query (test)" );

       // Get current collection file
       EJCOLL *coll = getCollection( colName, false );
       if( coll )
       {
           uint32_t count = 0;
           log = tcxstrnew();
           q1res = ejdbqryexecute(coll, q1, &count, 0, log);
           cout << count << " Delete records into collection " << endl;
       }
      _return = ResponseCode::Success;

   }
   catch(bsonio_exeption& e)
   {
       if( e.title() == string("SEJDB0002") ) // empty record
           _return = ResponseCode::Success;
       else
           _return = ResponseCode::Error;
   }
   catch( ...)
   {
     _return = ResponseCode::Error;
   }

   bson_destroy(&bshits1);
   bson_destroy(&bsq1);
   if(q1res) tclistdel(q1res);
   if(log) tcxstrdel(log);
   if(q1) ejdbquerydel(q1);
   return _return;
}

// internal functions -----------------------------------------------------

// convert current json string  to bson structure
void EJDBServerHandler::jsonToBson( bson *obj, const string& key, const string& recJson )
{
    bson_init( obj );
    if( !recJson.empty() )
    { ParserJson pars;
      pars.setJsonText( recJson.substr( recJson.find_first_of('{')+1 ) );
      pars.parseObject(  obj );
    }

    // add oid
    bson_oid_t oid;
    if( !key.empty() )
    {
        bson_type idtype = BSON_EOO;
        if( recJson.find( JDBIDKEYNAME ) != string::npos)
           idtype = _bsonoidkey(obj, &oid);
        if( idtype ==  BSON_OID )
        {
          char bytes[25];
          bson_oid_to_string( &oid, bytes );
          if( string(bytes) != key )
            bsonioErr("SEJDB0005", recJson+" Changed record oid" );
        } else // add oid if not here
          {
            getBsonOid( &oid, key );
            bson_append_oid( obj, JDBIDKEYNAME, &oid);
          }
    }
    bson_finish( obj );
}


EJCOLL *EJDBServerHandler::getCollection(
        const string& collName,  bool createifempty )
{
    // cureentEJDB.Open();
    // Test connection to opened db
    if( !curentEJDB.ejDB )
    {
        string err= "Undefined EJDB data base server";
        bsonioErr("SEJDB0001", err );
    }
    EJCOLL *coll;
    if( createifempty )
    {  // Get or create collection 'module name'
       coll = ejdbcreatecoll(curentEJDB.ejDB, collName.c_str(), NULL );
    }
    else
       coll = ejdbgetcoll(curentEJDB.ejDB, collName.c_str());

    if( !coll )
    {
      string err= "Cannot open/create into ";
      err += curentEJDB.Name();
      err += " EJDB collection ";
      err += collName;
      bsonioErr("SEJDB0002", err );
    }
    return coll;
}

void EJDBServerHandler::getBsonOid( bson_oid_t *oid, const string& pars )
{
   bson_oid_from_string( oid, pars.c_str() );
}

void EJDBServerHandler::setBsonOid( bson_oid_t *oid, string& oidstr )
{
  char bytes[25];
  bson_oid_to_string( oid, bytes );
  oidstr = bytes;
}
