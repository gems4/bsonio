//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file thrift_import.h
/// Declarations of classes AbstractIEFile, StructDataIEFile, KeyValueIEFile -
/// implementation import of foreign format files
/// Prepared an impex.thrift file describing 4 file formats for import/export
/// of data to internal DOM based on our JSON schemas.
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef THRIFT_IMPORT_H
#define THRIFT_IMPORT_H

#include "thrift_node.h"
#include "lua-run.h"
#include "impex_types.h"

using namespace  ::impex;

namespace bsonio {

enum RUNMODE_ {
     modeImport = 0, modeExport = 1
   };

class AbstractIEFile
{

protected:

    int _mode;
    const ThriftSchema* _schema;
    LuaRun _lua;

    /** Label of data type (vertex type), e.g. "datasource", "element" ... */
    string label;
    /** Export: the whole comment text; Import: the comment begin markup string (to skip until endl) */
    string comment;
    /** File name or "console" for export */
    string file_name;
    /** number of data block in file >=1, 0 if unknown */
    int Nblocks;
    /** number of text lines in file (>=1), 0 if unknown */
    int Nlines;

    /** Text block format in file corresponding to one database document (record) */
    FormatBlock block;

    // internal data

    /** DOM of label */
    SchemaNode *structNode;
    /** Key, Value pairs readed from the block of data in file*/
    map<string,vector<string>> externData;

    // internal functions

    /// Read data from one block in file
    virtual bool readBlockFromFile() =0;

    /// Save readed data to DOM
    virtual void setReadedData();

    /// Change number value, using script
    void run_lua_script( const string& script, vector<string>& values, bool isArray );
    /// Change string value, using script
    void run_lua_script_string( const string& script, vector<string>& values, bool isArray );

    /// Change value, using convert
    void run_convert( const std::map<std::string, std::string>&  convert, vector<string>& values );

    /// Run script for operation on data values in block
    void run_block_script( const string& script, bson *obj );

    /// Get Organization of object from the imported or exported file
    string impexFieldOrganization( const string& impexFieldKey );
    /// Get Type of object from the imported or exported file
    string impexFieldType( const string& impexFieldKey );

    /// Inser key value paies to extern data list
    void addMapKeyValue( const string& _key, const vector<string>& vec );

    vector<string> extractValue( const string& key, const map<string,vector<string>>& writeData )
    {
       auto itval = writeData.find(key);
       if( itval != writeData.end() && itval->second.size() > 0 )
         return itval->second;
       else
         return {};
    }

    /// Asseble parts of group to one vector
    /// Extract data from different parts of thrift record
    void assembleGroups( map<string,vector<string>>& writeData  );


public:

    enum RUNMODE {
         modeImport = 0, modeExport = 1
       };

    AbstractIEFile( int amode, const ThriftSchema* aschema, const string& alualibPath):
      _mode(amode), _schema(aschema), _lua(alualibPath), structNode(0)
    {}

    virtual ~AbstractIEFile()
    {
        closeFile();
        delete structNode;
    }

    /// Name of data type
    const string&  getDataName() const
    { return  label; }


    /// Open file to import
    virtual void openFile( const string& /*inputFname*/ ) {}
    /// Close internal file
    virtual void closeFile() {}

    /// Read one block to bson data
    bool readBlock( bson *bsobj)
    {
     // read data from block to readedData
     externData.clear();
     if( !readBlockFromFile() )
       return false;

     // reset default structure
     structNode->clearNode();
     // write data to structNode
     setReadedData();

     // structNode to bson
     bson_destroy( bsobj );
     structNode->getStructBson(bsobj);
     // Run script for operation on data values in block
     run_block_script(block.script, bsobj);

     // Set data according schema (+4 to 4)
     structNode->setStructBson(bsobj->data);
     bson_destroy( bsobj );
     structNode->getStructBson(bsobj);

     return true;
    }

    // export part -----------------------------------------------------------------

    /// write data from one block to file
    virtual bool writeBlockToFile( const map<string,vector<string>>& /*writeData*/ ) =0 ;

    /// Extract data from bson
    virtual void extractDataToWrite( bson *bsobj, map<string,vector<string>>& writeData );

    /// Write one block to extern format file
    bool writeBlock( bson *bsobj)
    {
     externData.clear();
     extractDataToWrite( bsobj, externData );
     writeBlockToFile( externData );
     return true;
    }

};

//--------------------------------------------------------------------------
// Export/import of data records to/from text file in legacy JSON/YAML/XML (foreign keywords)
// Maybe using a generated JSON schema of that input file format?

/// Definition of foreign structured data JSON/YAML/XML text file
class  StructDataIEFile: public AbstractIEFile
{
   fstream _stream;      ///< Stream to input/output
   /** Rendering syntax for the foreign file "JSON" | "YAML" | "XML" | ... */
   string renderer = "JSON";

  // internal values
  char _ftype;    ///< Input data type "JSON" | "YAML" | "XML" ...

  // read bson records array from xml file
   bson xmlobj;
   bson_iterator ixml;

  // internal functions

   /// Reading object from YAML text file
   bool next_from_yaml_file( bson *obj );
   /// Reading object from json text file
   bool next_from_json_file( bson *obj );
   /// Reading object from xml text file
   bool next_from_xml_file( bson *obj );
   // Read next bson object
   //bool next_from_file( bson *obj );

  /// Read data from one block in file
  bool readBlockFromFile();
  /// Read data one block from bson data
  bool readBlockFromBson( bson *bobj, string& key, int ndx=-1 );

 public:

   StructDataIEFile( int amode, const ThriftSchema* aschema, const string& alualibPath,
                     const FormatStructDataFile& defData );

   ~StructDataIEFile()
   {
     //bson_destroy(&xmlobj);
   }

   /// Open file to import
   void openFile( const string& inputFname );
   /// Close internal file
   void closeFile();

   /// Read next bson object
   bool nextFromFile( bson *obj );

   /// Convert free format bson to schema defined
   bool readBlock(  bson *inobj, bson *bsobj, string& key, int ndx=-1 );

   // export part -----------------------------------------------------------------

   /// write data from one block to file
   virtual bool writeBlockToFile( const map<string,vector<string>>& /*writeData*/ )
   { return true; }

};


// http://eax.me/cpp-regex/
// https://regex101.com/

// bib
// "head_regexp" :   "^\\s*@article\\s*\\{\\s*(\\w+)\\s*,",
// "key_regexp" :   "^\\s*(\\w+)\\s*=\\s*",
// "value_regexp" :   "^\"([^\"]*)\"\\s*,{0,1}",
// "end_regexp" :   "^\\s*\\}",

// ris
// "head_regexp" :   "",
// "end_regexp" :   "^\\s*ER\\s*-\\s*",
// "key_regexp" :   "^\\s*([A-Z0-9]{2})\\s*-\\s*",
// "value_regexp" :   "",
// "value_next" :   "\r\n",

// Match a "quoted string"  : ^"(?:[^\\"]|\\.)*"$


//--------------------------------------------------------------------------
// Definition of text file with key-value pair data
class  KeyValueIEFile: public AbstractIEFile
{

   fstream _stream;      ///< Stream to input/output

   /** Definition of key-value pair (line) in file */
   //FormatKeyValuePair lineFormat;
   /** Definition of key-value pair (line) in file */
   FormatKeyValue format;
   /** Rendering syntax for the foreign key-value file "GEMS3K" | "BIB" | "RIS" | ... */
   string renderer;
   /** Obsolute Definition of value, line, row, block, comment, end-of-data separators */
   Separators separs;

  // internal data
  string str_buf;  ///< Internal data to match regular expr
  string::const_iterator test_start;
  string::const_iterator test_end;

  // internal functions

  bool readBlockFromFile();
  bool readNextBlockfromFile();
  bool readRegexp( const string& re_str, vector<string>& mach_str );
  bool testRegexp( const string& re_str );
  bool readStartBlock();
  bool isEndBlock();
  string readAllValue();
  bool readKeyValuePair();
  void addKeyValue( const string& _key, const string& _value );
  void insertKeyValue( const string& _key, const vector<string>& vec );
  void skipComments();

 public:

   KeyValueIEFile( int amode, const ThriftSchema* aschema, const string& alualibPath,
                   const FormatKeyValueFile& defData );

   ~KeyValueIEFile()
   {}

   /// Open file to import
   void openFile( const string& inputFname );
   /// Close internal file
   void closeFile();

   /// Convert readed key-value data to schema defined bson
   bool initBlock(   bson *bsobj, const map<string,vector<string>>& areadedData );
   const map<string,vector<string>>& getReadedData() const
   {
     return externData;
   }


   // export part -----------------------------------------------------------------

   /// write data from one block to file
   virtual bool writeBlockToFile( const map<string,vector<string>>& writeData );

};

//--------------------------------------------------------------------------
// Definition of text file with table data
class  TableIEFile: public AbstractIEFile
{

   fstream _stream;      ///< Stream to input/output

   /** Definition of key-value pair (line) in file */
   FormatTable format;
   /** Rendering syntax for the foreign key-value file "GEMS3K" | "BIB" | "RIS" | ... */
   string renderer;
   /** Obsolute Definition of value, line, row, block, comment, end-of-data separators */
   Separators separs;

  // internal data
  string str_buf;  ///< Internal data to match regular expr
  string::const_iterator test_start;
  string::const_iterator test_end;

  // internal functions

  bool readBlockFromFile();
  bool readHeaderFromFile();
  void skipComments( string& row );
  string readNextRow();
  void addValue( uint col, const string& _value, bool toheader);
  void splitFixedSize( const string& row, bool toheader=false);
  void splitRegExpr( const string& row, bool toheader=false);
  uint extractQuoted( const string& row, string& value );
  void splitDelimiter( const string& row, bool toheader=false);

 public:

   TableIEFile( int amode, const ThriftSchema* aschema, const string& alualibPath,
                   const FormatTableFile& defData );

   ~TableIEFile()
   {}

   /// Open file to import
   void openFile( const string& inputFname );
   /// Close internal file
   void closeFile();

   // export part -----------------------------------------------------------------

   /// write data from one block to file
   bool writeBlockToFile( const map<string,vector<string>>& writeData );


};


string parseCode(int etype);

} // namespace bsonio

#endif // THRIFT_IMPORT_H
