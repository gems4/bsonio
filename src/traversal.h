//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file traversal.h
/// Declarations of class GraphTraversal - Graph Traversal for Database.
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef GRAPH_PARSER_H
#define GRAPH_PARSER_H

#include <iostream>
#include <set>
#include "dbedgedoc.h"
#include "thrift_schema.h"

namespace bsonio {

/// The function is executed for all vertexes and edges
using  GraphElementFunction = std::function<void( bool isVertex, bson *data )>;

/// Graph Traversal for Database
class GraphTraversal
{
    int travType = trAll;
    set<string> _vertexList;
    set<string> _edgeList;
    shared_ptr<bsonio::TDBVertexDocument> _vertexdoc;
    shared_ptr<bsonio::TDBEdgeDocument> _edgedoc;

    /// Visit Vertex and all linked Edges
    bool parseVertex( const string& idVertex, GraphElementFunction afunc );

    /// Visit Edge and all linked Vertexes
    bool parseEdge( const string& idEdge, GraphElementFunction afunc  );

public:

    /// Types of Matrix table mode
    enum TRAVERCE_TYPES {
        trNo  = 0x0000,      ///< No edges
        trIn  = 0x0001,      ///< Containing all vertexes connected by incoming edges
        trOut = 0x0002,      ///< Containing all vertexes connected by outgoing edges
        trAll = trIn|trOut   ///< Containing all vertexes
       };

    /// Constructor
    /// Params are needed to define DataBase
    GraphTraversal( const bsonio::TDataBase* dbconnect );

    /// Graph Traversal started from one vertex or edge
    void Traversal( bool startFromVertex, const string& id, GraphElementFunction afunc, int atravType = trAll )
    {
      travType  =atravType;
      _vertexList.clear();
      _edgeList.clear();

      if( startFromVertex )
        parseVertex( id, afunc );
      else
        parseEdge( id, afunc );
    }

    ///  Graph Traversal started from list vertexes or edges
    void Traversal( bool startFromVertex, const vector<string>& ids, GraphElementFunction afunc, int atravType = trAll )
    {
      travType  =atravType;
      _vertexList.clear();
      _edgeList.clear();

      if( startFromVertex )
      {  for(auto const &id : ids)
          parseVertex( id, afunc );
      }
      else
      {  for(auto const &id : ids)
           parseEdge( id, afunc );
      }
      cout << "vertexList " << _vertexList.size() << " edgeList " << _edgeList.size() << endl;
    }

    /// Read graph from JSON file
    void RestoreGraphFromFile( const string& filePath, bool overwrite );

};

} // namespace bsonio

#endif // GRAPH_PARSER_H
