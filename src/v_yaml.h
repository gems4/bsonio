//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file v_yaml.h
/// Declarations of functions for data exchange to/from YAML format
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef V_YAML_H
#define V_YAML_H

#include <string>
#include <stack>
#include "ejdb/ejdb.h"
#include "yaml-cpp/yaml.h"
#include "yaml-cpp/eventhandler.h"
using namespace std;

namespace bsonio {

void addScalar(const char* key, const string& value, bson* brec );

namespace ParserYAML
{
    /// Print bson structure to YAML string
    void printBsonObjectToYAML( string& resStr, const char *b);

    /// Print bson structure to YAML format file
    void printBsonObjectToYAML(fstream& fout, const char *b);

    /// Print bson array to YAML string
    void printBsonArrayToYAML( string& resStr, const char *b);

    /// Print bson array to YAML format file
    void printBsonArrayToYAML(fstream& fout, const char *b);


    /// Parse one YAML object from string to bson structure
    bool parseYAMLToBson( const string& currentYAML, bson *obj );

    /// Read one YAML object from text file and parse to bson structure
    bool parseYAMLToBson( fstream& fin, bson *brec );

    /// Parse json string to YAML string
    string Json2YAML( const string& jsonData );

    /// Parse YAML string to json string
    string YAML2Json( const string& currentYAML );

    /// Print bson structure to YAML Emitter class
    void bson_emitter( YAML::Emitter& out, const char *data, int datatype );

/// The BsonHandler class provides support for dynamically generating
/// bson data from yaml parser
class BsonHandler: public YAML::EventHandler
{

 public:

  string to_string();

  BsonHandler(bson* bobj);

  virtual void OnDocumentStart(const YAML::Mark& mark);
  virtual void OnDocumentEnd();

  virtual void OnNull(const YAML::Mark& mark, YAML::anchor_t anchor);
  virtual void OnAlias(const YAML::Mark& mark, YAML::anchor_t anchor);
  virtual void OnScalar(const YAML::Mark& mark, const std::string& tag,
                        YAML::anchor_t anchor, const std::string& value);

  virtual void OnSequenceStart(const YAML::Mark& mark, const std::string& tag,
                               YAML::anchor_t anchor, YAML::EmitterStyle::value style);
  virtual void OnSequenceEnd();

  virtual void OnMapStart(const YAML::Mark& mark, const std::string& tag,
                          YAML::anchor_t anchor, YAML::EmitterStyle::value style);
  virtual void OnMapEnd();

 private:

  void BeginNode();
  //void EmitProps(const std::string& tag, YAML::anchor_t anchor);

 private:
  bson* m_bson;

  enum   STATE_ { WaitingForSequenceEntry, WaitingForKey, WaitingForValue, };

  struct State {
    string key;
    int    ndx;
    STATE_ state;

    State( STATE_ st):
        key(""),ndx(0),state(st)
    {}
  };

  std::stack<State> m_stateStack;
};

/// The BsonHandler class provides support for dynamically generating
/// json data from yaml parser
class JsonHandler: public YAML::EventHandler
{

 void addHead(const string& key );
 void addScalar(const string&  key, const string& value );
 void shift()
 {
   for(int temp = 0; temp < m_depth; temp++)
        m_os << "     ";
 }

 public:

  string to_string();

  JsonHandler(stringstream& os_);

  virtual void OnDocumentStart(const YAML::Mark& mark);
  virtual void OnDocumentEnd();

  virtual void OnNull(const YAML::Mark& mark, YAML::anchor_t anchor);
  virtual void OnAlias(const YAML::Mark& mark, YAML::anchor_t anchor);
  virtual void OnScalar(const YAML::Mark& mark, const std::string& tag,
                        YAML::anchor_t anchor, const std::string& value);

  virtual void OnSequenceStart(const YAML::Mark& mark, const std::string& tag,
                               YAML::anchor_t anchor, YAML::EmitterStyle::value style);
  virtual void OnSequenceEnd();

  virtual void OnMapStart(const YAML::Mark& mark, const std::string& tag,
                          YAML::anchor_t anchor, YAML::EmitterStyle::value style);
  virtual void OnMapEnd();

 private:

  void BeginNode();
  //void EmitProps(const std::string& tag, YAML::anchor_t anchor);

 private:
  stringstream& m_os;
  int m_depth =0;
  bool m_first= true;

  enum   STATE_ { WaitingForSequenceEntry, WaitingForKey, WaitingForValue, };

  struct State {
    string key;
    int    ndx;
    STATE_ state;

    State( STATE_ st):
        key(""),ndx(0),state(st)
    {}
  };

  std::stack<State> m_stateStack;
};

}

} // namespace bsonio

#endif // V_YAML_H
