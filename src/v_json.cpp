//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file v_json.cpp
/// Implementation API to work with bson data
/// Implementation of functions for data exchange to/from json format
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <cstring>
#include <iomanip>
#include <iostream>

#include "v_json.h"
#include "ar2base.h"
#include "gdatastream.h"

namespace bsonio {

// convert current json string  to bson structure
void jsonToBson( bson *obj, const string& recJson )
{
    bson_init( obj );
    if( !recJson.empty() )
    { ParserJson pars;
      pars.setJsonText( recJson.substr( recJson.find_first_of('{')+1 ) );
      pars.parseObject(  obj );
    }
    bson_finish( obj );
}

// convert current json string  to bson array
void jsonToBsonArray( bson *obj, const string& recJson )
{
    bson_init( obj );
    if( !recJson.empty() )
    { ParserJson pars;
      pars.setJsonText( recJson.substr( recJson.find_first_of('[')+1 ) );
      pars.parseArray(  obj );
    }
    bson_finish( obj );
}


// Convert data from bson structure to json string
void jsonFromBson( const char *objdata, string& recJson )
{
    ParserJson pars;
    pars.printBsonObjectToJson( recJson, objdata );
}

// Convert data from bson structure to json string
void jsonArrayFromBson( const char *objdata, string& recJson )
{
    ParserJson pars;
    pars.printBsonArrayToJson( recJson, objdata );
}

void bson_reset_data(bson *b)
{
    b->cur = b->data = 0;
    b->dataSize = 0;
    b->finished = 0;
    b->stackPos = 0;
    b->err = 0;
    b->errstr = NULL;
    b->flags = 0;
}


/*
 * Get OID value from the '_id' field of specified bson object.
 * @param bson[in] BSON object
 * @param oid[out] Pointer to OID type
 * @return True if OID value is found int _id field of bson object otherwise False.
 */
bson_type _bsonoidkey(bson *bs, bson_oid_t *oid) {
    bson_iterator it;
    //bson_type bt = bson_find(&it, bs, JDBIDKEYNAME);
    bson_type bt =  bson_find_from_buffer(&it, bs->data, JDBIDKEYNAME );
    if (bt == BSON_OID) {
        *oid = *bson_iterator_oid(&it);
    }
    return bt;
}

template <>
bool bson_read_map_path( const char *obj, const char *namepath, map<string,string>& newmap )
{
    newmap.clear();

    bson_iterator it;
    bson_type type;
    bson_iterator_from_buffer(&it, obj );
    type =  bson_find_fieldpath_value( namepath, &it );
    if( type != BSON_OBJECT && type != BSON_NULL  )
        return false;

    string value;
    if( type == BSON_OBJECT  )
    {
      bson_iterator itobj;
      bson_iterator_from_buffer(&itobj, bson_iterator_value(&it));
      while( bson_iterator_next(&itobj) )
      {
          const char *key = bson_iterator_key(&itobj);
          if( bson_to_string( bson_iterator_value(&it), key, value ) )
            newmap[key] = value;
      }
    }
    return true;
 }


bool bson_find_string( const char *obj, const char *name, string& str )
{
    bson_iterator it;
    bson_type type;
    type =  bson_find_from_buffer(&it, obj, name );

    switch( type )
    {
    case BSON_NULL:
        str = S_EMPTY;
        break;
    case BSON_STRING:
        str = bson_iterator_string(&it);
         break;
    default: // error
         // bson_errprintf("can't print type : %d\n", type);
        str = S_EMPTY;
        return false;
   }
   return true;
}

bool bson_to_string( const char *obj, const char *name, string& str )
{
    bson_iterator it;
    bson_type type;
    type =  bson_find_from_buffer(&it, obj, name );
    return bson_to_string( type, &it, str );
}

bool bson_to_string( bson_type type,  bson_iterator *it, string& str )
{
    switch( type )
    {
    // impotant datatypes
    case BSON_NULL:
        str =  "null";
        break;
    case BSON_BOOL:
         str = ( bson_iterator_bool(it) ? "true" : "false");
         break;
    case BSON_INT:
         str = to_string( bson_iterator_int(it));
         break;
    case BSON_LONG:
         str = to_string(  (uint64_t) bson_iterator_long(it));
         break;
    case BSON_DOUBLE:
         str = TArray2Base::value2string( bson_iterator_double(it));
         break;
    case BSON_STRING:
         str = bson_iterator_string(it);
         break;
    case BSON_OID:
        {  char oidhex[25];
           bson_oid_to_string(bson_iterator_oid(it), oidhex);
           str = oidhex;
        }
        break;

    // main constructions
    case BSON_OBJECT:
    {  ParserJson parser;
       parser.printBsonObjectToJson(str,bson_iterator_value(it));
    }
    break;
    case BSON_ARRAY:
        {  ParserJson parser;
           parser.printBsonArrayToJson(str,bson_iterator_value(it));
        }
        break;
    default: // error
         // bson_errprintf("can't print type : %d\n", type);
        str = "";
        return false;
   }
    return true;
}

bool bson_to_key( const char *obj, string name, string& str )
{
    bool ret = true;
    bson_iterator it;
    bson_type type;

    auto pos = name.find(".");
    string cur_name = name.substr( 0, pos );
    string new_name = "";
    if( pos != string::npos  )
        new_name = name.substr( pos+1, string::npos );

    type =  bson_find_from_buffer(&it, obj, cur_name.c_str() );
    switch( type )
    {
    // impotant datatypes
    case BSON_NULL:
        str =  "null";
        break;
    case BSON_BOOL:
         str = ( bson_iterator_bool(&it) ? "true" : "false");
         break;
    case BSON_INT:
         str = to_string( bson_iterator_int(&it));
         break;
    case BSON_LONG:
         str = to_string(  (uint64_t) bson_iterator_long(&it));
         break;
    case BSON_DOUBLE:
         str = TArray2Base::value2string( bson_iterator_double(&it));
         break;
    case BSON_STRING:
         str = bson_iterator_string(&it);
         break;
    case BSON_OID:
        {  char oidhex[25];
           bson_oid_to_string(bson_iterator_oid(&it), oidhex);
           str = oidhex;
        }
        break;
    // main constructions
    case BSON_OBJECT:
        if( new_name.empty() )
         ret = "struct";
        else
         ret = bson_to_key( bson_iterator_value(&it), new_name, str );
        break;
    case BSON_ARRAY:
        if( new_name.empty() )
         ret = "array";
        else
         ret = bson_to_key( bson_iterator_value(&it), new_name, str );
        // May be error str = "array";
        break;
    default: // error
         // bson_errprintf("can't print type : %d\n", type);
        str = "*";
        return false;
   }
   return ret;
}

const char* bson_find_array( const char *obj, const char *name )
{
    bson_iterator it;
    bson_type type;
    type =  bson_find_from_buffer(&it, obj, name );
    bsonioErrIf( type != BSON_ARRAY, "E005BSon: ", "Must be array.", name );
    return bson_iterator_value(&it);
}


void bson_read_array( const char *obj, const char *name, vector<string>& lst )
{
    lst.clear();

    bson_iterator it;
    bson_type type;
    type =  bson_find_from_buffer(&it, obj, name );
    if( type == BSON_NULL || type ==BSON_EOO )
      return;
    bsonioErrIf( type != BSON_ARRAY, //&& type != BSON_OBJECT,
                 "E015BSon: ", "Must be array", name );

    string value;
    bson_iterator i;
    bson_iterator_from_buffer(&i, bson_iterator_value(&it));
    while (bson_iterator_next(&i))
    {
        //bson_type t = bson_iterator_type(&i);
        const char* key = bson_iterator_key(&i);
        //const char* data = bson_iterator_value(&i);
        if( !bson_to_string( bson_iterator_value(&it), key, value ) )
          value = "";
        lst.push_back(value);
    }
 }


bool bson_read_array_path( const char *obj, const char *namepath, vector<string>& lst )
{
    lst.clear();

    bson_iterator it;
    bson_type type;
    bson_iterator_from_buffer(&it, obj );
    type =  bson_find_fieldpath_value( namepath, &it );
    if( type == BSON_EOO ) // empty
      return false;
    string value;

//cout << " bson_read_array_path ";
    if( type == BSON_ARRAY  )
    { bson_iterator itobj;
      bson_iterator_from_buffer(&itobj, bson_iterator_value(&it));
      while( bson_iterator_next(&itobj) )
      {
        bson_type t = bson_iterator_type(&itobj);
        //const char* key = bson_iterator_key(&i);
        //const char* data = bson_iterator_value(&i);
        if( !bson_to_string( t, &itobj, value ) )
          value = "";
//cout<< " " << value.c_str();
        lst.push_back(value);
     }
    }
    else if( type != BSON_NULL  )
      {
      if( !bson_to_string( type, &it, value ) )
        value = "";
//cout<< " " << value.c_str();
      lst.push_back(value);
      }
//cout << " end" << endl;
    return true;
 }

const char* bson_find_object( const char *obj, const char *name )
{
    bson_iterator it;
    bson_type type;
    type =  bson_find_from_buffer(&it, obj, name );
    bsonioErrIf( type != BSON_OBJECT, "E006_BSon: ", "Must be object.", name );
    return bson_iterator_value(&it);
}


const char* bson_find_array_null( const char *obj, const char *name )
{
    bson_iterator it;
    bson_type type;
    type =  bson_find_from_buffer(&it, obj, name );
    if( type == BSON_NULL || type ==BSON_EOO )
      return 0;
    bsonioErrIf( type != BSON_ARRAY, "E005BSon: ", "Must be array.", name);
    return bson_iterator_value(&it);
}

const char* bson_find_object_null( const char *obj, const char *name )
{
    bson_iterator it;
    bson_type type;
    type =  bson_find_from_buffer(&it, obj, name );
    if( type == BSON_NULL )
      return 0;
    if( type != BSON_OBJECT )
    bsonioErrIf( type != BSON_OBJECT, "E006BSon: ", "Must be object.", name );
    return bson_iterator_value(&it);
}


time_t bson_find_time_t( const char *obj, const char *name )
{
    time_t ctm;
    bson_iterator it;
    bson_type type;
    type =  bson_find_from_buffer(&it, obj, name );
    switch( type )
    {
    case BSON_STRING:
         { string stime = bson_iterator_string(&it);
           ctm =  tcstrmktime(stime.c_str());
         }
         break;
    case BSON_DATE:
         ctm = bson_iterator_time_t(&it);
         break;
    case BSON_NULL:
    default:
            ctm = time(NULL);
            break;
   }
   return ctm;
}

void bson_print_raw_txt_(FILE *f, const char *data, int depth, int datatype );

void bson_print_raw_txt_(FILE *f, const char *data, int depth, int datatype )
{
    bson_iterator i;
    const char *key;
    int temp;
    bson_timestamp_t ts;
    char oidhex[25];
    bson scope;
    bool first = true;

    bson_iterator_from_buffer(&i, data);
    while (bson_iterator_next(&i))
    {
        if(!first )
         fprintf(f, ",\n");
        else
         first = false;

        bson_type t = bson_iterator_type(&i);
        if (t == 0)
          break;

        key = bson_iterator_key(&i);

        // before print
        switch( datatype )
        {
         case BSON_OBJECT:
           for (temp = 0; temp <= depth; temp++)
             fprintf(f, "\t");
           fprintf(f, "\"%s\": ", key );
           break;
         case BSON_ARRAY:
            for (temp = 0; temp <= depth; temp++)
              fprintf(f, "\t");
            break;
         default:
            break;
        }

        switch (t)
        {
          // impotant datatypes
          case BSON_NULL:
              fprintf(f, "null");
              break;
          case BSON_BOOL:
               fprintf(f, "%s", bson_iterator_bool(&i) ? "true" : "false");
               break;
          case BSON_INT:
               fprintf(f, "%d", bson_iterator_int(&i));
               break;
          case BSON_LONG:
               fprintf(f, "%ld", (uint64_t) bson_iterator_long(&i));
               break;
          case BSON_DOUBLE:
               fprintf(f, "%.15lg", bson_iterator_double(&i));
               break;
          case BSON_STRING:
              { // decode string '\\' to '\\\\', '\n' to '\\n'
               string str= bson_iterator_string(&i);
               convertStringToWrite( str );
               fprintf(f, "\"%s\"", str.c_str());
               }
              break;
          // main constructions
          case BSON_OBJECT:
             fprintf(f, "{\n");
             bson_print_raw_txt_( f, bson_iterator_value(&i), depth + 1, BSON_OBJECT);
             for (temp = 0; temp <= depth; temp++)
               fprintf(f, "\t");
             fprintf(f, "}");
             break;
          case BSON_ARRAY:
               fprintf(f, "[\n");
               bson_print_raw_txt_(f, bson_iterator_value(&i), depth + 1, BSON_ARRAY );
               for (temp = 0; temp <= depth; temp++)
                 fprintf(f, "\t");
               fprintf(f, "]");
               break;

           // not used in GEMS data types
              case BSON_SYMBOL:
                     fprintf(f, "SYMBOL: %s", bson_iterator_string(&i));
                     break;
              case BSON_OID:
                     bson_oid_to_string(bson_iterator_oid(&i), oidhex);
                     fprintf(f, "%s", oidhex);
                     break;
              case BSON_DATE:
                     char buf[100];
                     tcdatestrhttp(bson_iterator_time_t(&i), INT_MAX, buf);
                     fprintf(f, "\"%s\"", buf);
            //fprintf(f, "%ld", (long int) bson_iterator_date(&i));
                     break;
              case BSON_BINDATA:
                     fprintf(f, "BSON_BINDATA");
                     break;
              case BSON_UNDEFINED:
                     fprintf(f, "BSON_UNDEFINED");
                     break;
              case BSON_REGEX:
                     fprintf(f, "BSON_REGEX: %s", bson_iterator_regex(&i));
                     break;
              case BSON_CODE:
                     fprintf(f, "BSON_CODE: %s", bson_iterator_code(&i));
                     break;
              case BSON_CODEWSCOPE:
                     fprintf(f, "BSON_CODE_W_SCOPE: %s", bson_iterator_code(&i));
                     /* bson_init( &scope ); */ /* review - stepped on by bson_iterator_code_scope? */
                     bson_iterator_code_scope(&i, &scope);
                     fprintf(f, "\n\t SCOPE: ");
                     bson_print_raw_txt_(f, (const char*) &scope, 0, BSON_CODEWSCOPE);
                     //bson_print(f, &scope);
                     /* bson_destroy( &scope ); */ /* review - causes free error */
                     break;
               case BSON_TIMESTAMP:
                     ts = bson_iterator_timestamp(&i);
                     fprintf(f, "i: %d, t: %d", ts.i, ts.t);
                     break;
               default:
                      fprintf(f,"can't print type : %d\n", t);
        }
    }
    fprintf(f, "\n");

}

void print_bson_to_json(FILE *f, const bson *b)
{
    bson_print_raw_txt_(f, b->data, 0, BSON_OBJECT);
}

void print_bson_object_to_json(FILE *f, const bson *b)
{
    fprintf(f, "{\n");
    bson_print_raw_txt_(f, b->data, 0, BSON_OBJECT);
    fprintf(f, "}");
}


// Read one Json Object from text file to string
string  ParserJson::readObjectText( fstream& ff )
{
  jsontext = "";
  //jsontext += jsBeginObject;
  int cntBeginObject = 1;
  char input;
  bool intoString = false;

  do{
       ff.get( input );
       if( input ==  jsBeginObject && !intoString )
          cntBeginObject++;
       else if( input ==  jsEndObject && !intoString )
             cntBeginObject--;
       // test into string
       if( input == jsQuote &&  jsontext.back() != '\\' )
          intoString = !intoString;
       jsontext+= input;
     } while( cntBeginObject > 0 && !ff.eof());

  curjson = jsontext.c_str();
  end = curjson + jsontext.length();
  return jsontext;
}

// Load Json text
void  ParserJson::setJsonText( const string& json )
{
  jsontext = json;
  curjson = jsontext.c_str();
  end = curjson + jsontext.length();
}

bool ParserJson::xblanc()
{
    curjson += strspn( curjson, " \r\n\t" );
    while( *curjson == '#' )
    {
      curjson = strchr( curjson, '\n' );
      if( !curjson )
        return ( false );
      curjson += strspn( curjson, " \r\n\t" );
    }
    return (curjson < end);
}

// Get "<string>" data
void ParserJson::getString( string& str )
{
    //curjson++;
    const char * posQuote = strchr( curjson, jsQuote );
    while( posQuote && *(posQuote-1) == '\\' )
      posQuote = strchr( posQuote+1, jsQuote );

    bsonioErrIf( !posQuote ,"E01JSon: ",
             "Missing \" - end of string constant.", curjson );

    str = string(curjson, 0, posQuote-curjson);
     // conwert all pair ('\\''\n') to one simbol '\n' and other
    convertReadedString( str );
    curjson = ++posQuote;
    bsonioErrIf( curjson >= end ,"E02JSon: ",
             "Termination by String.");
}

// Get <double/integer> data (convert nan to 0)
void ParserJson::getNumber( double& value, int& type )
{
    const char *start = curjson;
    bool isInt = true;

    // minus
    if (curjson < end &&  ( *curjson == '-' || *curjson == '+' ) )
        ++curjson;

    // add new to test nan
    if( (end - curjson) > 3 && (*curjson++ == 'n' &&
        *curjson++ == 'a' && *curjson++ == 'n') )
    {
        type =  BSON_DOUBLE;
        value = 0.;
        return;
    }

    // int = zero / ( digit1-9 *DIGIT )
    while (curjson < end && isdigit(*curjson) )
            ++curjson;

    // frac = decimal-point 1*DIGIT
    if (curjson < end && *curjson == '.')
    {
        isInt = false;
        ++curjson;
        while (curjson < end && isdigit(*curjson) )
            ++curjson;
    }

    // exp = e [ minus / plus ] 1*DIGIT
    if (curjson < end && (*curjson == 'e' || *curjson == 'E'))
    {
        isInt = false;
        ++curjson;
        if (curjson < end && (*curjson == '-' || *curjson == '+'))
            ++curjson;
        while (curjson < end && isdigit(*curjson) )
            ++curjson;
    }

    bsonioErrIf( curjson >= end ,"E03JSon: ",
             "Termination by Number.");
    sscanf( start, "%lg", &value );
    if( isInt && ( value < SHORT_ANY && value > SHORT_EMPTY ) )
      type = BSON_INT;
    else
      type =  BSON_DOUBLE;       \
}


// value = false / null / true / object / array / number / string
void ParserJson::parseValue( const char *name, bson *brec )
{
    int type = BSON_UNDEFINED;
    bsonioErrIf( !xblanc() ,"E04JSon: ", "Must be value.");

    switch (*curjson++)
    {
    case 'n':
        if( (end - curjson) > 4 && (*curjson++ == 'u' &&
            *curjson++ == 'l' && *curjson++ == 'l') )
        {
            type = BSON_NULL;
            bson_append_null(brec, name );
            break;
        }
        bsonioErrIf( "E05JSon: ", "Illegal Value.", curjson);
    case 't':
        if( (end - curjson) > 4 && (*curjson++ == 'r' &&
            *curjson++ == 'u' && *curjson++ == 'e') )
        {
            type = BSON_BOOL;
            bson_append_bool(brec, name, true );
            break;
        }
        bsonioErrIf( "E05JSon: ", "Illegal Value.", curjson);
    case 'f':
        if( (end - curjson) > 5 && (*curjson++ == 'a' &&
            *curjson++ == 'l'  && *curjson++ == 's' && *curjson++ == 'e' ) )
        {
            type = BSON_BOOL;
            bson_append_bool(brec, name, false );
            break;
        }
        bsonioErrIf( "E05JSon: ", "Illegal Value.", curjson);
    case jsQuote:
       {
        string str = "";
        type = BSON_STRING;
        getString( str );
        if( strcmp(name,JDBIDKEYNAME) == 0  )
        {  // oid
            if( !str.empty()&& str != emptiness )
            {
              bson_oid_t oid;
              bson_oid_from_string( &oid, str.c_str() );
              bson_append_oid( brec, JDBIDKEYNAME, &oid);
            }
        }
        else
          bson_append_string( brec, name, str.c_str() );
       }
       break;

    case jsBeginArray:
        type = BSON_ARRAY;
        bson_append_start_array( brec, name);
        parseArray( brec );
        bson_append_finish_array(brec);
        break;

    case jsBeginObject:
        type = BSON_OBJECT;
        bson_append_start_object( brec, name);
        parseObject( brec );
        bson_append_finish_object(brec);
        break;

    case jsEndArray:
         --curjson;
        break;  // empty array
    case jsEndObject:
         --curjson;
        break;  // empty object

    default:  // number
      { --curjson;
        if( isdigit(*curjson) || *curjson == '+' ||
            *curjson == '-' || *curjson == '.' ||
            *curjson == 'e' || *curjson == 'E'    )
        {
            double value = DOUBLE_EMPTY;
            getNumber( value, type );
            if( type == BSON_INT )
              bson_append_int( brec, name, (int)value );
            else
              bson_append_double( brec, name, value );
            break;
        }
        else
            bsonioErrIf( "E05JSon: ", "Illegal Value.", curjson );
      }
      break;
    }
}

//    array = [ <value1>, ... <valueN> ]
void ParserJson::parseArray( bson *brec )
{
    char name[40];
    int ndx = 0;

    while( *curjson != jsEndArray )
    {
      sprintf( name, "%d", ndx);
      parseValue( name, brec );
      bsonioErrIf( !xblanc() ,"E06JSon: ", "Unterminated Array." );
      if( *curjson == jsValueSeparator  )
        curjson++;
      else
        bsonioErrIf( *curjson != jsEndArray ,"E07JSon: ",
                 "Missing Value Separator.", curjson );
      ndx++;
    }
    curjson++;
}

//  object = { "<key1>" : <value1>, ... , "<keyN>" : <valueN> }
void ParserJson::parseObject( bson *brec )
{
    string name;

    // read key
    bsonioErrIf( !xblanc() ,"E08JSon: ", "Unterminated Object.");

    while( *curjson != jsEndObject )
    {
      // read key
      bsonioErrIf( !xblanc() ,"E08JSon: ", "Unterminated Object.");

      if( *curjson++== jsQuote  )
      {
          getString( name );
       }
      else
        bsonioErrIf( "E10JSon: ", "Missing Key of Object.", curjson);

      bsonioErrIf( !xblanc() ,"E08JSon: ", "Unterminated Object.");
      if( *curjson == jsNameSeparator  )
          curjson++;
      else
        bsonioErr( "E09JSon: ", "Missing Name Separator.", curjson );
      // read value
      parseValue( name.c_str(), brec );
      bsonioErrIf( !xblanc() ,"E08JSon: ", "Unterminated Object.");
      if( *curjson == jsValueSeparator  )
        curjson++;
      else
        bsonioErrIf( *curjson != jsEndObject ,"E07JSon: ",
                 "Missing Value Separator.", curjson );
    }
    curjson++;
}

void ParserJson::bson_print_raw_txt( iostream& os, const char *data, int depth, int datatype )
{
    bson_iterator i;
    const char *key;
    int temp;
    //bson_timestamp_t ts;
    //char oidhex[25];
    //bson scope;
    bool first = true;

    bson_iterator_from_buffer(&i, data);
    while (bson_iterator_next(&i))
    {
        bson_type t = bson_iterator_type(&i);
        if (t == 0)
          break;
        ///if( t == BSON_OID )
          ///continue;

        if(!first )
         os <<  ",\n";
        else
         first = false;

        key = bson_iterator_key(&i);

         // before print
        switch( datatype )
        {
         case BSON_OBJECT:
           for (temp = 0; temp <= depth; temp++)
             os <<  "     ";
           os << "\"" << key << "\" :   ";
           break;
         case BSON_ARRAY:
            for (temp = 0; temp <= depth; temp++)
              os << "     ";
            break;
         default:
            break;
        }

        switch (t)
        {
          // impotant datatypes
          case BSON_NULL:
               os << "null";
              break;
          case BSON_BOOL:
               os << ( bson_iterator_bool(&i) ?  "true": "false");
               break;
          case BSON_INT:
               os << bson_iterator_int(&i);
               break;
          case BSON_LONG:
               os << bson_iterator_long(&i);
               break;
          case BSON_DOUBLE:
               os << setprecision(15) << bson_iterator_double(&i);
               break;
          case BSON_STRING:
             { // decode string '\\' to '\\\\', '\n' to '\\n'
               string str= bson_iterator_string(&i);
               convertStringToWrite( str );
               os << "\"" << str << "\"";
             }
             break;

          // main constructions
          case BSON_OBJECT:
             os << "{\n";
             bson_print_raw_txt( os, bson_iterator_value(&i), depth + 1, BSON_OBJECT);
             for (temp = 0; temp <= depth; temp++)
               os << "     ";
             os << "}";
             break;
          case BSON_ARRAY:
              os << "[\n";
              bson_print_raw_txt(os, bson_iterator_value(&i), depth + 1, BSON_ARRAY );
               for (temp = 0; temp <= depth; temp++)
                 os << "     ";
               os << "]";
               break;

           // not used in GEMS data types
              case BSON_SYMBOL:
              //       os<<  "SYMBOL: " << bson_iterator_string(&i);
                     break;
              case BSON_OID:
                  {  char oidhex[25];
                     bson_oid_to_string(bson_iterator_oid(&i), oidhex);
                     os << "\"" << oidhex << "\"";
                  }
                  break;
              case BSON_DATE:
              //       char buf[100];
              //       tcdatestrhttp(bson_iterator_time_t(&i), INT_MAX, buf);
              //       os << "\"" << buf <<"\"";
                     break;
              case BSON_BINDATA:
              //       os << "BSON_BINDATA";
                     break;
              case BSON_UNDEFINED:
              //      os << "BSON_UNDEFINED";
                     break;
              case BSON_REGEX:
              //       os << "BSON_REGEX: " << bson_iterator_regex(&i);
                     break;
              case BSON_CODE:
              //       os << "BSON_CODE: " << bson_iterator_code(&i);
                     break;
              case BSON_CODEWSCOPE:
              //       os << "BSON_CODE_W_SCOPE: " << bson_iterator_code(&i);
              //       bson_iterator_code_scope(&i, &scope);
              //       os << "\n      SCOPE: ";
              //       bson_print_raw_txt( os, (const char*) &scope, 0, BSON_CODEWSCOPE);
                     break;
               case BSON_TIMESTAMP:
              //       ts = bson_iterator_timestamp(&i);
              //       os <<  "i: " << ts.i << ", t: " << ts.t;
                     break;
               default:
                     os  << "can't print type : " << t;
        }
    }
    os << "\n";
}

void ParserJson::printBsonObjectToJson( string& resStr, const char *b)
{
    stringstream os;
    os << "{\n";
    bson_print_raw_txt( os, b, 0, BSON_OBJECT);
    os << "}";
    resStr = os.str();
}

void ParserJson::printBsonArrayToJson( string& resStr, const char *b)
{
    stringstream os;
    os << "[\n";
    bson_print_raw_txt( os, b, 0, BSON_ARRAY);
    os << "]";
    resStr = os.str();
}

// Print bson object to binary file
void print_bson_object_to_bin( GemDataStream& ff, const bson *b)
{
   ff << b->dataSize;
   ff.writeArray(b->data, b->dataSize );
}

// Load bson object from binary file
char *read_bson_object_from_bin( GemDataStream& ff )
{
    int dataSize = 0;
    ff >> dataSize;

    char *data = new char[ dataSize];
    if( data )
     ff.readArray( data,  dataSize );

    return data;
   // bson_init_finished_data(b, data);
   //  b->dataSize = dataSize;
}

} // namespace bsonio
