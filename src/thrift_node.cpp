//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file thrift_node.cpp
/// Implementation of  SchemaNode class an API for direct
/// code access to internal DOM based on our JSON schemas.
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <ctime>
#include "thrift_node.h"
#include "io_settings.h"
#include <thrift/protocol/TVirtualProtocol.h>
using namespace ::apache::thrift::protocol;

namespace bsonio {

ThriftFieldDef SchemaNode::topfield = { -1, "", {T_STRUCT}, 0, "", "", "", "", DOUBLE_EMPTY, DOUBLE_EMPTY };

/// Create new Node for struct name
SchemaNode* SchemaNode::newSchemaStruct( const string& className, const char *bsondata )
{
  ThriftStructDef* strDef = ioSettings().Schema()->getStruct( className );
  if( strDef == nullptr )
    return nullptr;
  else
   {
     topfield.className = className;
     SchemaNode *newStruct = new SchemaNode( strDef , bsondata );
     return newStruct;
  }
}

// --------------------- init part

/// Constructor for struct (up level )
SchemaNode::SchemaNode( ThriftStructDef* aStrDef, const char *bsondata ):
  _strDef(aStrDef),  _fldDef( &topfield ), _isSetUp( false ),
  _fldKey(aStrDef->getName()), _parent(nullptr),  _level(0),  _ndx(0)
{
    _fldValue = "";

    //if( bsondata )   // find value for field
       _isSetUp = true;

    // add levels for objects and arrays
    addChildren( bsondata );
}


/// Constructor for field
SchemaNode::SchemaNode( SchemaNode* aparent, ThriftFieldDef* afldDef, const char *bsondata ):
  _strDef(aparent->_strDef),  _fldDef( afldDef), _isSetUp(afldDef->fRequired == fld_required ),
  _fldKey(afldDef->fName), _parent(aparent),  _level(0)
{
   _ndx = _parent->_children.size();

    bson_iterator it;
    bson_type type_ = BSON_EOO;
    if( bsondata ) // find value for field
        type_ =  bson_find_from_buffer(&it, bsondata, _fldDef->fName.c_str() );

    if(type_ != BSON_EOO )
    {
      valueFromBson(type_, &it, _fldValue );

      if( !_isSetUp )
       _isSetUp = _parent->_isSetUp;

      // add levels for objects and arrays
      if( type_ != BSON_NULL)
        addChildren( bson_iterator_value(&it) );
      else
        {
          switch( _fldDef->fTypeId[_level] )
          {
            case T_MAP:
            case T_SET:
            case T_LIST: _fldValue = "";
            default:     break;

          }
        }
    }
    else
    {        // set default value
        int deflevel = 0;
        if(!bsondata ) // switch off
         deflevel = 2;
        setDefault("", deflevel);
        if(_fldDef->className == "TimeStamp" || deflevel == 2 )
          setCurrentTime();
    }
}


/// Constructor for array fields
SchemaNode::SchemaNode( SchemaNode* aparent, bson_iterator* it, int alevel ):
  _strDef( aparent->_strDef),  _fldDef( aparent->_fldDef),
  _isSetUp( aparent->_isSetUp ), _parent(aparent),  _level(alevel)
{
  _ndx = _parent->_children.size();

  if( it )
  {  bson_type type_ = bson_iterator_type(it);
    _fldKey = bson_iterator_key(it);
     // read value from bson
     valueFromBson(type_, it, _fldValue );
     // add levels for objects and arrays
     if( type_ != BSON_NULL)
       addChildren( bson_iterator_value(it) );
     else
       {
         switch( _fldDef->fTypeId[_level] )
         {
           case T_MAP:
           case T_SET:
           case T_LIST: _fldValue = "";
           default:     break;

         }
       }
     //addChildren( bson_iterator_value(it) );
  }
    else // empty item
    {
      _fldValue = getDefValue( _fldDef->fTypeId[_level], "" );
      _fldKey = to_string( _parent->_children.size() );
      addChildren( 0 ); // add struct
    }
}

/// Copy Constructor
SchemaNode::SchemaNode( const SchemaNode* data ):
  _strDef( data->_strDef),  _fldDef( data->_fldDef), _isSetUp( data->_isSetUp ),
  _fldValue(data->_fldValue ),  _parent(data->_parent), _level(data->_level),
  _noUsedFields(data->_noUsedFields)
{
    _fldKey = data->_fldKey; //to_string( _parent->_children.size() );
    _ndx = _parent->_children.size();

    // copy childrens
    for(uint ii=0; ii<data->_children.size(); ii++ )
    {
        SchemaNode *line = new SchemaNode( data->_children[ii].get() );
        _children.push_back( unique_ptr<SchemaNode>(line) );
    }
}


void SchemaNode::addChildren( const char* bson_data )
{
    // add levels for objects and arrays
    switch ( _fldDef->fTypeId[_level] )
    {
       // main constructions
       case T_STRUCT:
             {
               // structure
                ThriftStructDef* strDef2;
                  if( isTop() )
                     strDef2  = _strDef;
                   else
                     strDef2  = ioSettings().Schema()->getStruct( _fldDef->className );
                if( strDef2 == nullptr )
                     bsonioErr( _fldDef->className , "Undefined struct definition" );
                struct2model( strDef2, bson_data );
             }
             break;
       case T_MAP:
             list2model( _level+2, bson_data );
            break;
      case T_SET:
      case T_LIST: // !! next level
             list2model( _level+1, bson_data);
           break;
       default:     break;
    }
}

void SchemaNode::setDefault( const string& newdef, int deflevel )
{
    string def = newdef;
    if( !_level && def.empty() )  // define only for fields
       def = _fldDef->fDefault;

    _fldValue = getDefValue( _fldDef->fTypeId[_level], def );

    switch( deflevel )
    {
      case 0: _isSetUp = _fldDef->fRequired == fld_required;
              break;
      case 1: _isSetUp = _fldDef->fRequired <= fld_default;
              break;
      default: _isSetUp = (_parent != nullptr ? _parent->_isSetUp :true );
    }
    const char *dfdata = 0;
    bson bsrec;
    if(!def.empty() )
    {
        if( _fldDef->fTypeId[_level] == T_STRUCT || _fldDef->fTypeId[_level] == T_MAP )
            jsonToBson( &bsrec, def );
        else
            if( isArray() )
                jsonToBsonArray( &bsrec, def );
       dfdata =  bsrec.data;
    }

    // add levels for objects and arrays
    addChildren( dfdata );
}

void SchemaNode::valueFromBson(int type, bson_iterator *it, string& str )
{
   switch (type)
   {
    // impotant datatypes
    case BSON_NULL:
       str =  "null";
       break;
    case BSON_BOOL:
        str = ( bson_iterator_bool(it) ? "true" : "false");
        break;
    case BSON_INT:
        str = to_string( bson_iterator_int(it));
        break;
    case BSON_LONG:
        str = to_string(  (uint64_t) bson_iterator_long(it));
        break;
    case BSON_DOUBLE:
        str = TArray2Base::value2string( bson_iterator_double(it));
        break;
    case BSON_STRING:
        str = bson_iterator_string(it);
        break;
    case BSON_OID:
       {  char oidhex[25];
          bson_oid_to_string(bson_iterator_oid(it), oidhex);
          str = oidhex;
       }
       break;
       // main constructions
     case BSON_OBJECT:
     case BSON_ARRAY:
             str = "";
             break;
      default:
            bsonioErr("bson_to_list" ,"can't print type " );
   }
}


// Define Struct
void SchemaNode::struct2model( ThriftStructDef* strDef, const char *bson_data )
{
    _strDef = strDef;
    _children.clear();
    auto it = strDef->fields.begin();
    while( it != strDef->fields.end() )
    {
       SchemaNode *newfld = new SchemaNode( this,  &(*it), bson_data );
       _children.push_back(unique_ptr<SchemaNode>(newfld));
       if( !newfld->_isSetUp )
            _noUsedFields.push_back(it->fName.c_str());
       it++;
    }
}


// Define Array ( LIST, SET, MAP )
void SchemaNode::list2model( int level, const char *bson_data )
{
    bson_iterator it;

    _children.clear();
    if( !bson_data )
      return;

    bson_iterator_from_buffer(&it, bson_data);
    while (bson_iterator_next(&it))
    {
       bson_type t = bson_iterator_type(&it);
       if (t == 0)
          break;

       SchemaNode *newfld = new SchemaNode( this,  &it, level );
       _children.push_back(unique_ptr<SchemaNode>(newfld));
    }
}

//---------------------- work with structure -------------------------------

// Set bson object to structure
void SchemaNode::setStructBson( const char *bsondata )
{
  if( !isStruct() )
    bsonioErr("setStruct" , _fldKey+" is not a structure" );
  if( bsondata )
  _isSetUp = true;
  struct2model( _strDef, bsondata );
}

// Get bson object from structure
void SchemaNode::getStructBson(  bson* bsonobj )
{
    if( !isStruct() )
      bsonioErr("setStruct" , _fldKey+" is not a structure" );

    bson_init( bsonobj );
    for(uint ii=0; ii< _children.size(); ii++ )
      _children[ii]->list2bson( bsonobj );
    bson_finish( bsonobj );
}

// Get bson object from field
void SchemaNode::getFieldBson(  bson* bsonobj )
{
    if( !( isStruct() || isArray()) )
      bsonioErr("getField" , _fldKey+" is not a structure" );

    bson_init( bsonobj );
    for(uint ii=0; ii< _children.size(); ii++ )
      _children[ii]->list2bson( bsonobj );
    bson_finish( bsonobj );
}


// Get json string from structure
string SchemaNode::getFieldJSON()
{
    bson bsonobj;
    string sJson = _fldValue;
    if( isStruct() || isMap() )
    {
       getFieldBson( &bsonobj );
       jsonFromBson( bsonobj.data, sJson );
       bson_destroy( &bsonobj );
   }
   else if( isArray() )
   {
      getFieldBson( &bsonobj );
      jsonArrayFromBson( bsonobj.data, sJson );
      bson_destroy( &bsonobj );
  }
  return  sJson;
}

void SchemaNode::list2bson( bson* bsonobj )
{
    uint ii;
    if( !_isSetUp )
      return;

    int bstype = getBsonType( _fldDef->fTypeId[_level] );
    switch (bstype)
    {
      // impotant datatypes
      case BSON_NULL:
           bson_append_null(bsonobj, _fldKey.c_str() );
           break;
      case BSON_BOOL:
           bson_append_bool(bsonobj, _fldKey.c_str(),
              ( _fldValue == "true"? true : false));
           break;
      case BSON_INT:
           { int val;
             getValue( val );
             bson_append_int(bsonobj, _fldKey.c_str(), val );
           }
           break;
      case BSON_LONG:
           { long val;
             getValue( val );
             bson_append_long(bsonobj, _fldKey.c_str(), val );
           }
           break;
      case BSON_DOUBLE:
           { double val;
             getValue( val );
             bson_append_double(bsonobj, _fldKey.c_str(), val );
           }
           break;
      case BSON_OID:
      case BSON_STRING:
               if( _fldKey == JDBIDKEYNAME )
               {  // oid
                  if( _fldValue.empty() || _fldValue == emptiness )
                    break;
                  bson_oid_t oid;
                  bson_oid_from_string( &oid, _fldValue.c_str() );
                  bson_append_oid( bsonobj, JDBIDKEYNAME, &oid);
               }
               else
                   bson_append_string(bsonobj, _fldKey.c_str(), _fldValue.c_str() );
               break;
      // main constructions
      case BSON_OBJECT:
           bson_append_start_object(bsonobj, _fldKey.c_str());
           for( ii=0; ii< _children.size(); ii++ )
             _children[ii]->list2bson( bsonobj );
           bson_append_finish_object( bsonobj );
           break;
     case BSON_ARRAY:
         bson_append_start_array(bsonobj, _fldKey.c_str() );
         for( ii=0; ii< _children.size(); ii++ )
           _children[ii]->list2bson(  bsonobj );
         bson_append_finish_array( bsonobj );
         break;
     default:
        break;
      //     bsonioErr("list_to_bson" ,"can't print type " );
    }
}

// Get field by fieldpath ("name1.name2.name3")
SchemaNode *SchemaNode::field( const string& fieldpath)
{
  auto pos = fieldpath.find_first_not_of("0123456789.");
  if( pos != string::npos || isArray() ) // names
  {
      queue<string> names = split(fieldpath, ".");
      return field(names);

  } else   // ids
    {
        queue<int> ids = string2intarray( fieldpath, ".");
        return field(ids);
    }
}

// Get field by fieldpath
SchemaNode *SchemaNode::field( queue<string> names )
{
  if( names.empty() )
    return this;

  string fname = names.front();
  names.pop();

  for(uint ii=0; ii< _children.size(); ii++ )
   if( _children[ii]->_fldKey == fname )
       return _children[ii]->field( names );

  return nullptr;  // not found
}

// Get field by idspath
SchemaNode *SchemaNode::field( queue<int> ids )
{
    if( ids.empty() )
      return this;

    int id_ = ids.front();
    ids.pop();

    for(uint ii=0; ii< _children.size(); ii++ )
     if( _children[ii]->_fldDef->fId == id_ )
         return _children[ii]->field( ids );

    return nullptr;  // not found
}

string SchemaNode::getFullDescription() const
{
    // get doc from thrift schema
    if( _level == 0 )
      return  _fldDef->fDoc;

    string desc = _parent->getDescription();
    desc += " ";
    desc += _fldKey;
    return desc;
}


//-----------------------------Arrays

// get table sizes
vector<int> SchemaNode::getArraySizes()
{
   if( !isArray() )
        bsonioErr("getArraySize" , _fldKey+" is not a array" );

   vector<int>  sizes;

   SchemaNode *itline = this;
   auto it = _fldDef->fTypeId.begin();
   while(  (*it == T_SET || *it == T_LIST || *it == T_MAP ) &&
            it< _fldDef->fTypeId.end() )
   {
      if( itline )
      {  uint sizef = itline->_children.size();
         sizes.push_back(sizef);

         if(sizef>0)
           itline = itline->_children[0].get();
         else
           itline = 0;
      }
      else
         sizes.push_back(0);
      it++;
   }
   return sizes;
}

void SchemaNode::resizeArray( uint  size, const string& defval  )
{
    if( !isArray() )
        bsonioErr("resizeArray" , _fldKey+" is not a array" );

    // resizeline item
    if( size == _children.size() )
       ;
    else // delete if smaler
      if( size < _children.size() )
       _children.resize(size);
      else
      {
          int level = _level;
          if( _fldDef->fTypeId[_level] == T_MAP )
            level++;
          level++;

         // add new elements
          SchemaNode *linechild;
          for( uint ii=_children.size(); ii<size; ii++)
          {
             if( !ii )  // empty
                linechild = new SchemaNode( this,  0, level );
             else  // copy
                linechild = new SchemaNode( _children.back().get() );
             linechild->_fldValue = getDefValue( _fldDef->fTypeId[level], defval );
             linechild->_fldKey = to_string(ii);
             linechild->_ndx = _children.size();
             _children.push_back(unique_ptr<SchemaNode>(linechild));
         }
      }
}


void SchemaNode::resizeArray(uint level, const vector<int>& sizes, const string& defval  )
{
    if( sizes.size() <= level )
      return;
    uint size = sizes[level];

    // resize current level
    resizeArray( size, defval  );

    // try to resize next level
    for( uint ii=0; ii< _children.size(); ii++)
      _children[ii]->resizeArray( level+1, sizes, defval );
}

// Set up current time as default value
// only for fild
void SchemaNode::setCurrentTime()
{
    if(_fldDef->className != "TimeStamp")
     return;

    struct tm *time_now;
    time_t secs_now;

    tzset();
    time(&secs_now);
    time_now = localtime(&secs_now);

    SchemaNode *fld = field(  "year" );
    fld->setValue(1900+time_now->tm_year);
    fld = field(  "month" );
    fld->setValue(time_now->tm_mon+1);
    fld = field(  "day" );
    fld->setValue(time_now->tm_mday);
    fld = field(  "hour" );
    fld->setValue(time_now->tm_hour);
    fld = field(  "minute" );
    fld->setValue(time_now->tm_min);
    fld = field(  "second" );
    fld->setValue(time_now->tm_sec);
}

/// Set json Value to Node
template<> void SchemaNode::setValue( const string& value  )
{
    if( isArray() || isStruct()  )
        setComplexValue( value );
    else
       _fldValue = getDefValue( _fldDef->fTypeId[_level], value );
    addField();
}

} // namespace bsonio
