
SOURCES += \
    $$PWD/nservice.cpp \
    $$PWD/ar2base.cpp \
    $$PWD/ar2bson.cpp \
    $$PWD/ar2format.cpp \
    $$PWD/valsimpl.cpp \
    $$PWD/n_object.cpp \
    $$PWD/io2format.cpp \
    $$PWD/v_json.cpp \
    $$PWD/v_yaml.cpp \
    $$PWD/gdatastream.cpp \
    $$PWD/json2cfg.cpp \
    $$PWD/dbbase.cpp \
    $$PWD/nejdb.cpp \
    $$PWD/pugixml.cpp \
    $$PWD/ar2xml.cpp \
    $$PWD/valsstl.cpp
#    $$PWD/dbgems.cpp

HEADERS  += \
    $$PWD/ar2format.h \
    $$PWD/valsbase.h \
    $$PWD/valsimpl.h \
    $$PWD/v_json.h \
    $$PWD/v_yaml.h \
    $$PWD/ar2base.h \
    $$PWD/ar2bson.h \
    $$PWD/n_object.h \
    $$PWD/io2format.h \
    $$PWD/gdatastream.h \
    $$PWD/nservice.h \
    $$PWD/json2cfg.h \
    $$PWD/dbbase.h \
    $$PWD/nejdb.h \
    $$PWD/pugixml.hpp \
    $$PWD/pugiconfig.hpp \
    $$PWD/ar2xml.h \
    $$PWD/valsstl.h
#    $$PWD/dbgems.h




