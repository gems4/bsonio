//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file thrift_schema.cpp
/// Implementation of classes and functions to work with Thrift Schemas
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include "ar2base.h"
#include "TBSONSerializer.h"

#include <thrift/protocol/TVirtualProtocol.h>
using namespace ::apache::thrift::protocol;

namespace bsonio {

ThriftFieldDef ThriftStructDef::emptyfield = { -1, "", {}, 2, "", "", "", "", DOUBLE_EMPTY, DOUBLE_EMPTY };
int ThriftEnumDef::emptyenum = SHORT_EMPTY;

const char * key_name = "name";
const char * key_doc = "doc";
const char * key_fields ="fields";
const char * key_isException ="isException";
const char * key_isUnion ="isUnion";
const char * key_toselect ="to_select";
const char * key_unique_fields ="to_unique";

const char * key_key ="key";
const char * key_typeId = "typeId";
const char * key_type = "type";
const char * key_elemTypeId = "elemTypeId";
const char * key_elemType = "elemType";
const char * key_keyTypeId ="keyTypeId";
const char * key_keyType ="keyType";
const char * key_valueTypeId ="valueTypeId";
const char * key_valueType ="valueType";
const char * key_default ="default";
const char * key_minval ="minval";
const char * key_maxval ="maxval";
const char * key_class ="class";
const char * key_required ="required";
const char * key_req_out ="req_out";
const char * key_optional ="optional";
const char * key_undefined ="undefined";

const char * key_members ="members";
const char * key_value ="value";
const char * key_structs ="structs";
const char * key_enums ="enums";

void writeDataToBsonSchema( const ThriftSchema* schema, bson* obj,
           const string& structName, ::apache::thrift::TBase* data )
{
    //Set the Serializator
    ::apache::thrift::stdcxx::shared_ptr<TBSONSerializer> proto;
    proto.reset(new TBSONSerializer(schema));
    proto->setBson( obj, structName );
    // Get bson object from structure
    /*int  i = */data->write(proto.get());
    //std::cout << "Wrote " << i << " bytes" << std::endl;
}

void readDataFromBsonSchema( const ThriftSchema* schema, bson* obj,
    const string& structName,  ::apache::thrift::TBase* data )
{
    //Set the Serializator
    ::apache::thrift::stdcxx::shared_ptr<TBSONSerializer> proto;
    proto.reset(new TBSONSerializer(schema));
    proto->setBson( obj, structName );
   // Get bson object from structure
   /*int  i = */data->read(proto.get());
   //std::cout << "Read " << i << " bytes" << std::endl;
}

void ThriftSchema::setTypeMap()
{
  name_to_thrift_types.insert(pair<string, int>("stop", T_STOP));
  name_to_thrift_types.insert(pair<string, int>("void", T_VOID));
  name_to_thrift_types.insert(pair<string, int>("bool", T_BOOL));
  name_to_thrift_types.insert(pair<string, int>("byte", T_BYTE));
  name_to_thrift_types.insert(pair<string, int>("i8", T_I08));
  name_to_thrift_types.insert(pair<string, int>("i16", T_I16));
  name_to_thrift_types.insert(pair<string, int>("i32", T_I32));
  name_to_thrift_types.insert(pair<string, int>("u64", T_U64));
  name_to_thrift_types.insert(pair<string, int>("i64", T_I64 ));
  name_to_thrift_types.insert(pair<string, int>("double", T_DOUBLE));
  name_to_thrift_types.insert(pair<string, int>("string", T_STRING));
  name_to_thrift_types.insert(pair<string, int>("utf7", T_UTF7));
  name_to_thrift_types.insert(pair<string, int>("struct", T_STRUCT));
  name_to_thrift_types.insert(pair<string, int>("union", T_STRUCT));
  name_to_thrift_types.insert(pair<string, int>("exception", T_STRUCT));
  name_to_thrift_types.insert(pair<string, int>("map", T_MAP));
  name_to_thrift_types.insert(pair<string, int>("set", T_SET));
  name_to_thrift_types.insert(pair<string, int>("list", T_LIST));
  name_to_thrift_types.insert(pair<string, int>("utf8", T_UTF8));
  name_to_thrift_types.insert(pair<string, int>("utf16", T_UTF16));
}


/// Read json schema for one thrift structure from bsondata
void ThriftStructDef::readSchema( const char *bsondata )
{
    // read information about structure
    if(!bson_find_string( bsondata, key_name, name ) )
        name = key_undefined;  // May be exeption
    if(!bson_find_value( bsondata, key_isException, isException ) )
        isException=false;
    if(!bson_find_value( bsondata, key_isUnion, isUnion ) )
        isUnion=false;
    if(!bson_find_string( bsondata, key_doc, sDoc ) )
        sDoc="";
   replaceall(sDoc, "\t\r\n", ' ' );
   // read to_select
   bson_read_array( bsondata, key_toselect, keyIdSelect );
   // read unique_fields
   bson_read_array( bsondata, key_unique_fields, uniqueList );

    // read arrays of fields
    const char *fieldsarr  = bson_find_array(  bsondata, key_fields );
    bson_iterator itobj;
    //const char* key;
    int ii=0;
    bson_iterator_from_buffer(&itobj, fieldsarr);
    while( bson_iterator_next(&itobj) )
    {
        ThriftFieldDef value;
        //key = bson_iterator_key(&itobj);
        const char* fieldsobject = bson_iterator_value(&itobj);

        if(!bson_find_value( fieldsobject, key_key, value.fId ) )
            value.fId=-1;
        if(!bson_find_string( fieldsobject, key_name, value.fName ) )
            value.fName=key_undefined;

        string requred;
        if(!bson_find_string( fieldsobject, key_required, requred ) )
            requred=key_req_out;
        if(requred == key_required )
             value.fRequired = fld_required;
            else if( requred == key_optional )
                   value.fRequired = fld_optional;
                 else
                    value.fRequired = fld_default;

        // vector<int> fTypeId; // "typeId"+"type"+"elemTypeId"+"elemType"  - all levels
        if(!bson_find_string( fieldsobject, key_class, value.className ) )
           value.className = "";   // "class": enum name

        string  typeId;
        if(!bson_find_string( fieldsobject, key_typeId, typeId ) )
            value.fTypeId.push_back(T_VOID);
        else
            read_type_spec(fieldsobject, value, key_type, typeId);

        // default value we get to string for different types
        bson_to_string( fieldsobject, key_default, value.fDefault );
        value.insertedDefault = "";

        if(!bson_find_string( fieldsobject, key_doc, value.fDoc ) )
            value.fDoc="";
        replaceall(value.fDoc, "\t\r\n", ' ' );
        if(!bson_find_value( fieldsobject, key_minval, value.minval ) )
            value.minval=DOUBLE_EMPTY;
        if(!bson_find_value( fieldsobject, key_maxval, value.maxval ) )
            value.maxval=DOUBLE_EMPTY;

        fields.push_back( value );
        id2index.insert( pair<int, int>(value.fId, ii));
        name2index.insert( pair<string, int>(value.fName, ii++) );
    }

}

/// Read json schema for thrift structure from bsondata
void ThriftStructDef::read_type_spec( const char *bsondata,
        ThriftFieldDef& value, const char* keyspec, const string& typeID)
{
   auto it = ThriftSchema::name_to_thrift_types.find(typeID);
   if( it == ThriftSchema::name_to_thrift_types.end() )
    bsonioErr("Undefined type", typeID );

   TType type = (TType)it->second;
   value.fTypeId.push_back(type);

  if( type == T_STRUCT )
  {
    const char * nextobj = bson_find_object( bsondata, keyspec );
    if(!bson_find_string( nextobj, key_class, value.className ) )
        bsonioErr("Undefined class name", typeID );
    size_t pos = value.className.find_last_of('.');
    if( pos != std::string::npos)
       value.className.erase(0, pos+1);
  } else if ( type == T_MAP)
   {
     const char * nextobj = bson_find_object( bsondata, keyspec );
     string  typeId;
     if(!bson_find_string( nextobj, key_keyTypeId, typeId ) )
         value.fTypeId.push_back(T_VOID);
     else
         read_type_spec(nextobj, value, key_keyType, typeId);
     if(!bson_find_string( nextobj, key_valueTypeId, typeId ) )
         value.fTypeId.push_back(T_VOID);
     else
         read_type_spec(nextobj, value, key_valueType, typeId);
  }
    else
      if (type == T_LIST || type == T_SET)
      {
        const char * nextobj = bson_find_object( bsondata, keyspec );
        string  typeId;
        if(!bson_find_string( nextobj, key_elemTypeId, typeId ) )
              value.fTypeId.push_back(T_VOID);
        else
              read_type_spec(nextobj, value, key_elemType, typeId);
      }
}

//---------------------------------------------------------------------------

/// Read json schema for one thrift enum from bsondata
void ThriftEnumDef::readEnum( const char *bsondata )
{
    // read information about enum
    if(!bson_find_string( bsondata, key_name, name ) )
        name = key_undefined;  // May be exeption
    if(!bson_find_string( bsondata, key_doc, sDoc ) )
        sDoc="";
    replaceall(sDoc, "\t\r\n", ' ' );

    // read arrays of fields
    const char *fieldsarr  = bson_find_array(  bsondata, key_members );
    bson_iterator itobj;
    //const char* key;
    string ename;
    int eId;
    string edoc;
    bson_iterator_from_buffer(&itobj, fieldsarr);
    while( bson_iterator_next(&itobj) )
    {
        //key = bson_iterator_key(&itobj);
        const char* fieldsobject = bson_iterator_value(&itobj);

        if(!bson_find_string( fieldsobject, key_name, ename ) )
           ename=key_undefined;
        if(!bson_find_string( fieldsobject, key_doc, edoc ) )
            edoc="";
        replaceall(edoc, "\t\r\n", ' ' );
        if(!bson_find_value( fieldsobject, key_value, eId ) )
            eId=emptyenum;

        name2index.insert( pair<string, int>(ename, eId) );
        name2doc.insert( pair<string, string>(ename, edoc) );
    }

}

//-----------------------------------------------------------

map<string, int> ThriftSchema::name_to_thrift_types;

// Read thrift schema from json file fileName
void ThriftSchema::addSchemaFile( const char *fileName )
{
    bson obj;
    char b;

    // Reading work structure from json text file
    fstream f(fileName, ios::in);
    bsonioErrIf( !f.good() , fileName, "Fileread error...");

    ParserJson parserJson;
    string objStr;
    while( !f.eof() )
       {
          f.get(b);
          if( b == jsBeginObject )
          {
            b= ' ';
            objStr =  parserJson.readObjectText(f);
            //std::cout << objStr.c_str() << endl;
            bson_init(&obj);
            parserJson.parseObject( &obj );
            bson_finish(&obj);
            break;
           }
        }

    // copy schema to internal structures
    readSchema( obj.data );
    bson_destroy(&obj);

}

/// Read bson data with thrift schema
void ThriftSchema::readSchema( const char *bsondata )
{
    string fname, fdoc;
    bson_iterator itobj;
    //const char* key;

    // read information about enum
    if(!bson_find_string( bsondata, key_name, fname ) )
        fname = key_undefined;  // May be exeption
    if(!bson_find_string( bsondata, key_doc, fdoc ) )
        fdoc="";
    files.insert( pair<string, string>(fname, fdoc) );

    // read enum array
    const char *enumsarr  = bson_find_array(  bsondata, key_enums );
    bson_iterator_from_buffer(&itobj, enumsarr);
    while( bson_iterator_next(&itobj) )
    {
        //key = bson_iterator_key(&itobj);
        const char* enumobject = bson_iterator_value(&itobj);

        ThriftEnumDef* enumdata = new ThriftEnumDef(enumobject);
        enums.insert( pair<string, unique_ptr<ThriftEnumDef>>
                    (enumdata->getName(), unique_ptr<ThriftEnumDef>(enumdata )) );
    }

    // read structs array
    const char *structsarr  = bson_find_array(  bsondata, key_structs );
    bson_iterator_from_buffer(&itobj, structsarr);
    while( bson_iterator_next(&itobj) )
    {
        //key = bson_iterator_key(&itobj);
        const char* structobject = bson_iterator_value(&itobj);

        ThriftStructDef* structdata = new ThriftStructDef(structobject);
        structs.insert( pair<string, unique_ptr<ThriftStructDef>>
                    (structdata->getName(), unique_ptr<ThriftStructDef>(structdata )) );

        const ThriftFieldDef* typeFld = structdata->getFieldDef( "_type" );
        const ThriftFieldDef* labelFld = structdata->getFieldDef( "_label" );
        if( typeFld != nullptr && labelFld != nullptr )
        {
            if( typeFld->fDefault == "vertex")
              vertexes[labelFld->fDefault ] = structdata->getName();
            if( typeFld->fDefault == "edge")
              edges[labelFld->fDefault ] = structdata->getName();
        }
    }
}

/// Get structs names list
vector<string> ThriftSchema::getStructsList( bool withDoc ) const
{
  vector<string> members;
  auto it = structs.begin();
  while( it != structs.end() )
  {
     string vasl = it->first;
     if( withDoc  )
     {
        vasl+= ',';
        vasl+= it->second->getComment();
     }
     members.push_back(vasl);
     it++;
  }
  return members;
}


// Convert to_select Ids to Names
void ThriftSchema::getSelectData( const string& to_select_fld,  ThriftStructDef* strDef,
                                  string& to_select_name, string& to_select_doc ) const
{
   to_select_name ="";
   to_select_doc = "";
   queue<int> idLst = string2intarray( to_select_fld, "." );
   getSelectLevel( idLst, strDef,to_select_name, to_select_doc );
}

/// Recursion to convert Id key to Name key
void ThriftSchema::getSelectLevel( queue<int>& idLst,
           ThriftStructDef* strDef, string& to_select_name, string& to_select_doc ) const
{
  const ThriftFieldDef*  fldDef = strDef->getFieldDef( idLst.front() );
  if( fldDef == nullptr )
     bsonioErr("Undefined search field", to_select_name+to_string(idLst.front()) );
  if( !to_select_name.empty() )
      to_select_name += ".";
  to_select_name += fldDef->fName;
  idLst.pop();

  // skip array indexes
  if( idLst.size() > 0 )
  {
    uint level = 0;
    while( level<fldDef->fTypeId.size() &&
           ( fldDef->fTypeId[level] == T_MAP ||
             fldDef->fTypeId[level] == T_SET ||
             fldDef->fTypeId[level] == T_LIST ) )
    {
        to_select_name += "."+to_string(idLst.front());
        idLst.pop();
        if( fldDef->fTypeId[level] == T_MAP )
            level++;
        level++;
        if( idLst.size() < 1 )
         break;
    }
   }

   if( idLst.size() > 0 )
   {
    string strname = fldDef->className;
    if( strname.empty() )
       bsonioErr("Must be structure", to_select_name );
    ThriftStructDef* strDef2 =  getStruct(strname);
    if( strDef2 == nullptr )
       bsonioErr("Undefined search struct", strname );
    getSelectLevel( idLst, strDef2, to_select_name, to_select_doc );
   } else
     to_select_doc = fldDef->fDoc;
}


//---------------------------------------------------------------
// internal convertion

int getBsonType( int thriftType )
{
    int btype = BSON_EOO;
    switch( thriftType )
    {
      case T_BOOL:  btype = BSON_BOOL;
                    break;
      //case T_BYTE:
      case T_I08:
      case T_I16:
      case T_I32:  btype = BSON_INT;
                   break;
      case T_U64:
      case T_I64:  btype = BSON_LONG;
                   break;
      case T_DOUBLE:  btype = BSON_DOUBLE;
                      break;
      case T_STRING:
      //case T_UTF7:
      case T_UTF8:   btype = BSON_STRING;
                     break;
      case T_MAP:
      case T_STRUCT:  btype = BSON_OBJECT;
                   break;
      case T_SET:
      case T_LIST:  btype = BSON_ARRAY;
                   break;
      default:
      case T_UTF16:
      case T_STOP:
      case T_VOID: // may be error
                  btype =  BSON_EOO;
    }
    return btype;
}

string getBsonTypeName( int bsonType )
{
    string btype = "Null";
    switch ( bsonType )
        {
          // impotant datatypes
          case BSON_NULL:  btype = "Null";
                           break;
          case BSON_BOOL:  btype = "Bool";
                           break;
          case BSON_INT:   btype = "Int";
                           break;
          case BSON_LONG:  btype = "Long";
                           break;
          case BSON_DOUBLE: btype = "Double";
                            break;
    case BSON_OID: btype = "OID";
                      break;
         case BSON_STRING: btype = "String";
                      break;
          // main constructions
          case BSON_OBJECT: btype = "Object";
                            break;
          case BSON_ARRAY:  btype = "Array";
                            break;
          default:          break;
        }
        return btype;
}

string getDefValue( int thriftType, const string& defval )
{
    string value = defval;
    switch( thriftType )
    {
      case T_BOOL:
                    if( value != "true" )
                       value = "false";
                    break;
      //case T_BYTE:
      case T_I08:
      case T_I16:
      case T_I32:
      case T_I64:
                   if( value.empty() )
                     value = "0";
                    else
                    { int ival;
                      if( is<int>( ival, value.c_str()) )
                           value = to_string( ival );
                      else
                          value = "0";
                    }
                    break;
       case T_U64:
                  if( value.empty() )
                    value = "0";
                  //value = to_string( stoul( value ));
                  break;
      case T_DOUBLE:
                  if( value.empty() || value == "null" )
                        value = "0.";
                  else
                  { double dval;
                    if( is<double>( dval, value.c_str()) )
                        value = to_string( dval );
                    else
                       value = "0.";
                  }
                  break;
      case T_STRING:
      //case T_UTF7:
      case T_UTF8:   if( value.empty() )
                       value = "";//emptiness;
                     break;
      case T_MAP:
      case T_STRUCT: value = "";
                      break;
      case T_SET:
      case T_LIST:   value = "";
                   break;
      default:
      case T_UTF16:
      case T_STOP:
      case T_VOID: // may be error
                  value = "";
                  break;
    }
    return value;
}

string getDefValueBson( int bsonType, const string& defval )
{
    string value = defval;
    switch ( bsonType )
    {
      // impotant datatypes
      case BSON_NULL:  value = "null";
                       break;
      case BSON_BOOL:  if( value.empty() )
                          value = "false";
                       break;
      case BSON_INT:
      case BSON_LONG:  if( value.empty() )
                         value = "0";
                       break;
      case BSON_DOUBLE: if( value.empty() )
                          value = "0.0";
                        break;
      case BSON_STRING: if( value.empty() )
                          value = "";//emptiness;
                        break;
      // main constructions
      case BSON_OBJECT:
      case BSON_ARRAY:
      default:
        value = "";
        break;
    }
    return value;
}

} // namespace bsonio
