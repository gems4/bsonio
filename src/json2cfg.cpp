//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file json2cfg.cpp
/// Implementation of TFileBase, FJson, ConfigJson,
/// ConfigArray - classes for file manipulation
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include <cstring>
#include "json2cfg.h"
#include "v_yaml.h"
#include "v_xml.h"

namespace bsonio {

extern const char* S_EMPTY;
char FJson::defType = FileTypes::Json_;

// File class -----------------------------------------

TFileBase::TFileBase(const string& fName, const string& fExt, const string& fDir):
    isopened_( false ), dir_(fDir), name_(fName), ext_(fExt)
{
    makePath();
    makeKeyword();
}

TFileBase::TFileBase(const string& path):
        isopened_( false ), path_(path)
{
    u_splitpath(path_, dir_, name_, ext_);
    makeKeyword();
}

// Ctor by readind from configurator file
TFileBase::TFileBase(const char* bsobj):
        isopened_( false )
{
    fromBson(bsobj);
}

// Writes DOD data to ostream file
void TFileBase::toBson( bson *obj ) const
{
    bson_append_string( obj, "Path",  path_.c_str() );
    bson_append_string( obj, "Keywd", Keywd_ );
    bson_append_string( obj, "Type",  to_string(type_).c_str() );
}

// Reads DOD data from istream file
void TFileBase::fromBson( const char* bsobj )
{
    if(!bson_find_string( bsobj, "Path", path_ ) )
        bsonioErr( "E011BSon: ", "Undefined File Path.");
    u_splitpath(path_, dir_, name_, ext_);
    string sbuf;
    if(!bson_find_string( bsobj, "Keywd", sbuf ) )
       makeKeyword();
    else
       strncpy( Keywd_, sbuf.c_str(), MAX_FILENAME_LEN);
    if(!bson_find_string( bsobj, "Type", sbuf ) )
        sbuf="t";
    type_=sbuf[0];
}

// For all files first simbols for name
void TFileBase::makeKeyword()
{
    if( name_.empty() )
        return;
    strncpy( Keywd_, name_.c_str(), MAX_FILENAME_LEN);
    Keywd_[MAX_FILENAME_LEN]='\0';
}

/// Check for existence of file
bool TFileBase::Exist()
{
    if( testOpen() )
        return true;  // file alredy open
    if( access( path_.c_str(), 0 ) )
        return false;
    else  return true;
}

// FJson --------------------------------------------------------

/// Load data from json file to bson object
void FJson::loadJson( bson *bobj )
{
    fstream fin(path_, ios::in );
    bsonioErrIf( !fin.good() , path_, "Fileopen error...");
    // read one bson record from file
    ParserJson parserJson;
    char b;
    string objStr;

    while( !fin.eof() )
    {
      fin.get(b);
      if( b == jsBeginObject )
      {
        objStr =  parserJson.readObjectText( fin );
        //cout << objStr.c_str() << endl;
        bson_init( bobj );
        parserJson.parseObject( bobj );
        bson_finish( bobj );
        break;
      }
    }
}

/// Save data from bson object to  json file
void FJson::saveJson( const char *bobj )
{
    fstream fout(path_, ios::out);
    bsonioErrIf( !fout.good() , path_, "Fileopen error...");
    // record to json string
    ParserJson pars;
    string jsonstr;
    pars.printBsonObjectToJson( jsonstr, bobj );
    fout <<  jsonstr.c_str() << endl;
}

/// Load data from yaml file to bson object
void FJson::loadYaml( bson *bobj )
{
    fstream fin(path_, ios::in );
    bsonioErrIf( !fin.good() , path_, "Fileopen error...");
    bson_init(bobj);
    ParserYAML::parseYAMLToBson( fin, bobj );
    bson_finish(bobj);
}

/// Save data from bson object to yaml file
void FJson::saveYaml( const char *bobj )
{
    fstream fout(path_, ios::out);
    bsonioErrIf( !fout.good() , path_, "Fileopen error...");
    ParserYAML::printBsonObjectToYAML( fout, bobj );
}

/// Load data from xml file to bson object
void FJson::loadXml( bson *bobj )
{
    bson_init(bobj);
    ParserXML::parseXMLToBson( path_.c_str(), bobj );
    bson_finish(bobj);
}

/// Save data from bson object to xml file
void FJson::saveXml( const char *bobj )
{
    ParserXML::printBsonObjectToXml( path_.c_str(), bobj);
}

void FJson::LoadBson( bson *bobj )
{
    switch( type_ )
    {
      case FileTypes::Json_:
          loadJson( bobj );
           break;
      case FileTypes::Yaml_:
           loadYaml( bobj );
           break;
      case FileTypes::XML_:
           loadXml( bobj );
           break;
    }
}

void FJson::SaveBson( const char *bobj )
{
    switch( type_ )
    {
      case FileTypes::Json_:
          saveJson(  bobj );
           break;
      case FileTypes::Yaml_:
           saveYaml(  bobj );
           break;
      case FileTypes::XML_:
           saveXml(  bobj );
           break;
    }
}

//---------------------------------------------------------------

// Save configuration data to file
void ConfigJson::saveCfg()
{
    // get data from map
    bson obj;
    bson_init( &obj );
    for(map<string,CfgVal>::iterator itr = cfgData.begin();
        itr != cfgData.end(); ++itr)
    {
      switch( itr->second.type() )
      {
        case cfg_string:
           bson_append_string( &obj, itr->first.c_str(),  itr->second.toString().c_str() );
           break;
        case cfg_int:
          bson_append_int( &obj, itr->first.c_str(),  itr->second.toInt() );
          break;
        case cfg_double:
          bson_append_double( &obj, itr->first.c_str(),  itr->second.toDouble() );
          break;
        case cgf_bool:
          bson_append_bool( &obj, itr->first.c_str(),  itr->second.toBool() );
        break;
      }
    }
    bson_finish( &obj );

    // save data
    SaveBson( obj.data );
    bson_destroy(&obj);
}

// Load configuration data from file
void ConfigJson::loadCfg()
{
  // get data from file
  bson obj;
  LoadBson( &obj );

  // load data to map
  const char *key;
  bson_iterator itobj;
  bson_iterator_from_buffer(&itobj, obj.data);
  while (bson_iterator_next(&itobj))
  {
    key = bson_iterator_key(&itobj);
    bson_type bstype = bson_iterator_type(&itobj);
    switch (bstype)
    {
     // impotant datatypes
     case BSON_BOOL:
         cfgData[key] = (bool)bson_iterator_bool(&itobj);
         break;
     case BSON_INT:
         cfgData[key] = bson_iterator_int(&itobj);
         break;
     case BSON_LONG:
         cfgData[key] =  (int)bson_iterator_long(&itobj);
         break;
     case BSON_DOUBLE:
         cfgData[key] = bson_iterator_double(&itobj);
         break;
     case BSON_NULL:
         cfgData[key] = string();
         break;
     case BSON_STRING:
         cfgData[key] = string(bson_iterator_string(&itobj));
      default:  break;
     }
  }

  bson_destroy(&obj);

}


// Open file to import
void FJsonArray::Open(int amode )
{
  mode_ = amode;

  if( mode_ == OpenModeTypes::ReadOnly )
  {
    if( type_ == FileTypes::XML_ )
    {
      bson_init(&xmlobj);
      if( !ParserXML::parseXMLToBson( path_.c_str(), &xmlobj ))
             return;
      bson_finish(&xmlobj);
      const char *arr  = bson_find_array(  xmlobj.data, "node" );
      bson_iterator_from_buffer(&ixml,arr);
      //bson_iterator_from_buffer(&ixml, xmlobj.data);
    } else
      {
        // open  file for other types
         if (_stream.is_open())
           _stream.close();
         _stream.open(path_,  std::fstream::in );
          bsonioErrIf( !_stream.good() , path_, "Fileopen error...");
        if( type_ == FileTypes::Yaml_ )
        {
         bson_init(&xmlobj);
         if( !ParserYAML::parseYAMLToBson( _stream, &xmlobj ))
             return;
         bson_finish(&xmlobj);
         //const char *arr  = bson_find_array(  xmlobj.data, "node" );
         //bson_iterator_from_buffer(&ixml,arr);
         bson_iterator_from_buffer(&ixml, xmlobj.data);
        }
     }
  }
  else if( mode_ == OpenModeTypes::WriteOnly )
  {
    if( type_ == FileTypes::XML_ || type_ == FileTypes::Yaml_ )
    {
      bson_init(&xmlobj);
    }
    if( type_ == FileTypes::Json_ || type_ == FileTypes::Yaml_ )
    { // open  file for other types
      if (_stream.is_open())
        _stream.close();
      _stream.open(path_,  std::fstream::out );
      bsonioErrIf( !_stream.good() , path_, "Fileopen error...");
      if(  type_ == FileTypes::Json_ )
          _stream << "[" << endl;
    }
  } else
       bsonioErr( path_, "Illegal file mode...");

  isopened_ = true;
}

// Close internal file
void FJsonArray::Close()
{

    if( mode_ == OpenModeTypes::WriteOnly )
    {
      if( type_ == FileTypes::XML_ )
      {
          bson_finish(&xmlobj);
          ParserXML::printBsonArrayToXml( path_.c_str(), xmlobj.data );
      }
      else
          if( type_ == FileTypes::Yaml_ )
          {
              bson_finish(&xmlobj);
              ParserYAML::printBsonArrayToYAML( _stream, xmlobj.data );
          } else
            if(  type_ == FileTypes::Json_ )
              _stream << "]" << endl;
    }

    // close file
    if (_stream.is_open())
    _stream.close();
    isopened_ = false;
}

// Reading work structure from YAML text file
bool FJsonArray::next_from_yaml_file( bson *obj )
{
  return next_from_xml_file( obj );
  /*
  bson_destroy( obj );
  bson_init(obj);
  if( !ParserYAML::parseYAMLToBson( _stream, obj ) )
      return false;
  bson_finish(obj);
  // test
  ParserJson pars;
  string jsonstr;
  pars.printBsonObjectToJson( jsonstr, obj->data );
  cout << jsonstr << endl << "!!!!" << endl;
  return true;*/
}

// Reading work structure from json text file
bool FJsonArray::next_from_json_file( bson *obj )
{
    // read bson records array from file
    ParserJson parserJson;
    char b;
    string objStr;

    while( !_stream.eof() )
    {
     _stream.get(b);
      if( b == jsBeginObject ) // read one object
      {
           b= ' ';
           objStr =  parserJson.readObjectText( _stream );
           bson_destroy( obj );
           bson_init(obj);
           parserJson.parseObject( obj );
           bson_finish(obj);
           return true;  // next record
       }
    }
    return false;
}

// Reading work structure from xml text file
bool FJsonArray::next_from_xml_file( bson *obj )
{
  if (bson_iterator_next(&ixml))
  {
      bson_iterator sit;
      bson_iterator_from_buffer( &sit, bson_iterator_value(&ixml) );
      bson_destroy( obj );
      bson_init(obj);
      while (bson_iterator_next(&sit) != BSON_EOO) {
          bson_append_field_from_iterator(&sit, obj);
      }
      bson_finish(obj);
      return true;
  }
  return false;
}

// Read next bson object
bool FJsonArray::LoadNext( bson *obj )
{
  switch( type_ )
  {
   case  FileTypes::Json_: return next_from_json_file( obj );
   case  FileTypes::Yaml_: return next_from_yaml_file( obj );
   case  FileTypes::XML_:  return next_from_xml_file( obj );
   default: return false;
  }
}

// Saving object to YAML text file
void FJsonArray::next_to_yaml_file( const bson *bobj )
{
    next_to_xml_file( bobj );

    /*ParserYAML::printBsonObjectToYAML( _stream, bobj->data );
    _stream << "\n---\n";
    ndx_++;*/
}

// Saving object to json text file
void FJsonArray::next_to_json_file( const bson *bobj )
{
    if( ndx_ > 0   )
      _stream << ",\n";   // next node
    ParserJson pars;
    string jsonstr;
    pars.printBsonObjectToJson( jsonstr, bobj->data );
    _stream <<  jsonstr.c_str() << endl;
    ndx_++;
}

// Saving object to xml text file
void FJsonArray::next_to_xml_file( const bson *bobj )
{
    bson_append_bson( &xmlobj, to_string(ndx_).c_str(), bobj );
    ndx_++;
}

// Read next bson object
void FJsonArray::SaveNext( const bson *obj )
{
  switch( type_ )
  {
   case  FileTypes::Json_: next_to_json_file( obj ); break;
   case  FileTypes::Yaml_: next_to_yaml_file( obj ); break;
   case  FileTypes::XML_:  next_to_xml_file( obj ); break;
   default: break;
  }
}


} // namespace bsonio

//-----------------------------------------------------

