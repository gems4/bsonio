//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbbase.cpp
/// Implementation of class TDBAbstract - base interface of abstract DB chain
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include <cstring>
#include <regex>
#include "dbdocument.h"
#include "dbconnect.h"
#include "v_yaml.h"

namespace bsonio {

//-------------------------------------------------------------
// TDBAbstract - This class contains the structure of Abstract Data Base
//-------------------------------------------------------------


// Default configuration of the Data Base
TDBDocumentBase::TDBDocumentBase( const TDataBase* dbconnect,const string& coltype,
                                  const string& colname, const vector<KeyFldsData>& keyFldsInf  ):
    _colltype(coltype), _defqueryJson(""), _queryResult(DBQueryDef(_defqueryFields))
{
   _collection = dbconnect->getCollection( coltype, colname, keyFldsInf  );
   _collection->addDocument(this);
}


// Default configuration of the Data Base
TDBDocumentBase::TDBDocumentBase( TDBCollection* collection  ):
    _collection( collection ), _colltype(collection->type() ), _defqueryJson(""), _queryResult(DBQueryDef(_defqueryFields))
{
   _collection->addDocument(this);
}


// Configurator by readind from bson object
TDBDocumentBase::TDBDocumentBase( TDBCollection* collection, const char* bsobj):
   _collection( collection ), _colltype(collection->type() ), _queryResult(DBQueryDef(_defqueryFields))
{
    fromBson(bsobj);
    _collection->addDocument(this);
}


// Writes data to bson
void TDBDocumentBase::toBson( bson *obj ) const
{
    bson_append_string( obj, "Type", _colltype.c_str() );
    bson_append_string( obj, "QueryJson", _defqueryJson.c_str() );
    bson_append_start_array(obj, "QueryFields");
    for(uint ii=0; ii<_defqueryFields.size(); ii++)
       bson_append_string( obj, to_string(ii).c_str(), _defqueryFields[ii].c_str() );
    bson_append_finish_array(obj);
}

// Reads data from bson
void TDBDocumentBase::fromBson( const char* bsobj )
{
    if(!bson_find_string( bsobj, "Type", _colltype ) )
        _colltype="undef";
    if(!bson_find_string( bsobj, "QueryJson", _defqueryJson ) )
        _defqueryJson="";
    _defqueryFields.clear();
    const char *arr  = bson_find_array(  bsobj, "QueryFields"  );
    bson_iterator i;
    bson_iterator_from_buffer(&i, arr);
    while (bson_iterator_next(&i))
    {
        bson_type t = bson_iterator_type(&i);
        bsonioErrIf( t != BSON_STRING, "fromBson", "Error reading cfg file..." );
        const char* data = bson_iterator_string(&i);
        _defqueryFields.push_back(data);
    }
}

// Set json format string to curent record
void TDBDocumentBase::SetDefaultQueryJson( const string& qrjson)
{
    _defqueryJson = /*queryJson =*/ replace_all( qrjson, "\'", "\"");
}

// extern functions ---------------------------------------

// Extract the string value by key from query
int extractIntField( const string& key, const string& jsondata )
{
  string token;
  string query2 =  replace_all( jsondata, "\'", "\"");
  string regstr =  string(".*\"")+key+"\"\\s*:\\s*([+-]?[1-9]\\d*|0).*";
  std::regex re( regstr );
  std::smatch match;

  if( std::regex_search( query2, match, re ))
  {
     if (match.ready())
        token = match[1];
  }
  //cout << key << "  token " << token  << endl;
  return stoi(token);
}

// Extract the string value by key from query
string extractStringField( const string& key, const string& jsondata )
{
  string token;
  string query2 =  replace_all( jsondata, "\'", "\"");
  string regstr =  string(".*\"")+key+"\"\\s*:\\s*\"([^\"]*)\".*";
  std::regex re( regstr );
  std::smatch match;

  if( std::regex_search( query2, match, re ))
  {
     if (match.ready())
        token = match[1];
  }
  //cout << key << "  token " << token  << endl;
  return token;
}

FieldSetMap extractFields( const vector<string> queryFields, bson *bobj )
{
    string valbuf;
    FieldSetMap res;
    for( auto& ent: queryFields )
    {
        bson_to_key( bobj->data, ent, valbuf );
        res[ ent] = valbuf;
    }
    return res;
}


FieldSetMap extractFields( const vector<string> queryFields, const string& jsondata )
{
    bson bobj;
    jsonToBson(&bobj, jsondata);
    FieldSetMap res = extractFields( queryFields, &bobj );
    bson_destroy(&bobj);
    return res;
}

void addFieldsToQuery( string& query, const FieldSetMap& fldvalues )
{
   cout << query << endl;

   bson oldobj;
   jsonToBson(&oldobj, query);

   bson addobj;
   bson_init_as_query(&addobj);
   for(auto& ent : fldvalues)
    addScalar( ent.first.c_str(), ent.second, &addobj );
   bson_finish(&addobj);

   // add two bson
   bson fullobj;
   bson_init_as_query(&fullobj);
   bson_merge(&oldobj, &addobj, false, &fullobj);
//   bson_check_duplicate_keys(&fullobj);
   bson_finish( &fullobj);

   bson_destroy(&oldobj);
   bson_destroy(&addobj);

   jsonFromBson( fullobj.data, query );
   cout << " Result: " <<  query << endl;
   bson_destroy(&fullobj);

}

} // namespace bsonio
