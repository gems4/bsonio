//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbdocument.h
/// Declarations of class TDBDocumentBase - base interface of abstract DB document
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TDBDOCUMENT_H
#define TDBDOCUMENT_H

//#include <set>
//#include <map>
#include <memory>
#include "dbquerydef.h"
#include "dbcollection.h"

namespace bsonio {

class TDataBase;
using FieldSetMap = map<string, string >;   ///< map fieldpath<->value

// some extern functions
int extractIntField( const string& key, const string& query );
string extractStringField( const string& key, const string& query );
FieldSetMap extractFields( const vector<string> queryFields, bson *bobj );
FieldSetMap extractFields( const vector<string> queryFields, const string& jsondata );
void addFieldsToQuery( string& query, const FieldSetMap& fldvalues );


/// Definition of Abstract DB document
class TDBDocumentBase: public IsUnloadToBson
{
    /// Do it after read record from DB to bson
    virtual void afterLoad( const char* ) {}
    /// Do it before remove current object from collection.
    virtual void beforeRm( const char* ) {}
    /// Do it after remove current object from collection.
    virtual void afterRm( const char* ) {}
    /// Do it before write record to DB
    virtual void beforeSaveUpdate( const char* ) {}
    /// Do it after write record to DB
    virtual void afterSaveUpdate( const char* ) {}

 protected:

    TDBCollection* _collection; ///< Access to the database collection
    string _colltype = "undef";     ///< Collectuion Type

    //--- Manipulation list of records (tables) query
    string         _defqueryJson;     ///< Default query for select record json
    vector<string> _defqueryFields;   ///< Default list of fields to query
    DBQueryResult  _queryResult;      ///< Last query result

    /// Run current query, rebuild internal table of values
    virtual void executeQuery() =0;
    /// Run query
    ///  \param _queryJson - json string with where condition
    ///  \param  _queryFields - list of fields into result json ( if empty only _id )
    ///  \return _resultData - list of json strings with query result
    virtual void executeQuery( const string&  _queryJson, const vector<string>& _queryFields,
                vector<string>& _resultData ) = 0;

    ///  Provides 'distinct' operation over query
    ///  \param fpath Field path to collect distinct values from.
    ///  \param queryJson - json string with where condition
    ///  \return Unique values by specified fpath and query
    virtual vector<string> distinctQuery( const string& fpath, const string&  queryJson="" ) = 0;

 public:

    ///  Constructor
    TDBDocumentBase( const TDataBase* dbconnect, const string& coltype,
                     const string& colname, const vector<KeyFldsData>& keyFldsInf  );
    ///  Constructor
    TDBDocumentBase( TDBCollection* collection  );
    ///  Destructor
    virtual ~TDBDocumentBase()
    {
      _collection->removeDocument(this);
    }

    /// Constructor from bson data
    TDBDocumentBase( TDBCollection* collection, const char* bsobj );

    virtual void toBson( bson *obj ) const;
    virtual void fromBson( const char *obj );

    string type() const
    {
      return _colltype;
    }
    ///< Access to the database
    const TDataBase* database() const
    {
        return _collection->_dbconnect;
    }
    /// Save current record to bson structure
    virtual void recToBson( bson *obj, time_t crtt, char* oid ) = 0;
    /// Load data from bson structure (return read record key)
    virtual string recFromBson( bson *obj ) = 0;

    /// Gen new oid or other pointer of location
    virtual string genOid()
    {
        return _collection->genOid();
    }

    /// Return curent record in json format string
    virtual string GetJson() = 0;
    /// Set json format string to curent record
    virtual void SetJsonYaml( const string& sjson, bool is_json = true) = 0;
    /// Return curent record in YAML format string
    virtual string GetYAML() = 0;

    /// Get record key from bson structure
    virtual string getKeyFromBson( const char* bsdata )
    {
      return _collection->getKeyFromBson(bsdata);
    }

    //--- Manipulation records

    /// Find record with key into internal record keys list
    bool Find( const char *key )
    {
      return _collection->Find( key );
    }

    /// Retrive one record from the collection
    void GetRecord( const char *key )
    {
      _collection->GetRecord( this, key );
      afterLoad( key );
    }

    /// Removes record from the collection
    void DelRecord( const char *key )
    {
      beforeRm( key );
      _collection->DelRecord(key);
      afterRm( key );
    }

    /// Save/update record in the collection
    void SaveRecord( const char* key  )
    {
      beforeSaveUpdate( key);
      _collection->SaveRecord( this, key );
      afterSaveUpdate( key);
    }

    /// Save new record in the collection
    void InsertRecord( const char *key=0 )
    {
      beforeSaveUpdate( key );
      _collection->InsertRecord( this, key );
      afterSaveUpdate( key );
    }

    //--- Manipulation list of records (tables)

    /// Set default json query string for collection
    virtual void SetDefaultQueryJson( const string& qrjson);
    /// Set default query fields to table
    void SetDefaultQueryFields( const vector<string>&  fieldsList )
    {
       _defqueryFields = fieldsList;
    }

    const DBQueryDef& getQuery()
    {
       return _queryResult.getQuery();
    }
    void setQuery( const DBQueryDef& qrdef )
    {
       _queryResult.setQuery(qrdef);
    }

    /// Set query json string for collection
    virtual void SetQueryJson( const string& qrjson)
    {
       _queryResult.setEJDBQuery(qrjson);
    }
    /// Return curent query json string
    string GetLastQuery()
    {
        return  _queryResult.getQuery().getEJDBQuery();
    }
    /// Set query fields to table
    void SetQueryFields( const vector<string>&  fieldsList )
    {
       _queryResult.setFieldsCollect( fieldsList );
    }

    /// Run current query, rebuild internal table of values
    void runQuery()
    {
      executeQuery();
    }
    /// Run current query, rebuild internal table of values
    void runQuery( const DBQueryDef&  query  )
    {
       _queryResult.setQuery(query);
       executeQuery();
    }
    /// Run query
    ///  \param _queryJson - json string with where condition
    ///  \param  _queryFields - list of fields into result json ( if empty only _id )
    ///  \return _resultData - list of json strings with query result
    void runQuery( const string&  queryJson, const vector<string>& queryFields,
                vector<string>& resultData )
    {
        executeQuery( queryJson, queryFields, resultData );
    }
    /// Run query
    ///  \param _query - selection query
    ///  \return _resultData - list of json strings with query result
    void runQuery( const DBQueryDef&  query, vector<string>& resultData )
    {
       executeQuery( query.getEJDBQuery(), query.getFieldsCollect(), resultData );
    }

    /// Run query  \return _resultData - list of json strings with query result
    void runQuery( vector<string>& resultData )
    {
       runQuery( _queryResult.getQuery(), resultData );
    }

    ///  Provides 'distinct' operation over query
    ///  \param fpath Field path to collect distinct values from.
    ///  \param queryJson - json string with where condition
    ///  \return Unique values by specified fpath and query
    vector<string> fieldQuery( const string& fpath, const string&  queryJson="" )
    {
       return  distinctQuery( fpath, queryJson );
    }

    /// Get query result table
    const TableOfValues& GetQueryResult()
    {
       return  _queryResult.getQueryResult();
    }
    /// Get all keys list for current query
    int GetKeyValueList( vector<string>& aKeyList,
           vector<vector<string>>& aValList )
    {
      return  _queryResult.getKeyValueList(aKeyList,aValList);
    }
    /// Get query fields list
    const vector<string>& GetQueryFields()
    {
       return _queryResult.getQuery().getFieldsCollect();
    }

    /// Get keys list for current query according pattern
    int GetKeyValueList( const char* keypat,  vector<string>& aKeyList,
           vector<vector<string>>& aValList )
    {
       CompareTemplateFunction compareFunc = [=]( const char* keypat, const char* akey)
       {
           return _collection->CompareTemplate( keypat, akey);
       };
       return  _queryResult.getKeyValueList(aKeyList,aValList, keypat, compareFunc );
    }

    /// Find key from data
    string GetKeyFromValue( const char *bsdata )
    {
       return  _queryResult.getKeyFromValue(bsdata);
    }

    /// Add line to view table
    void addLine( const string& keyStr, const char *bsdata, bool isupdate )
    {
       _queryResult.addLine( keyStr, bsdata, isupdate );
    }

    /// Update line into view table
    void updateLine( const string& keyStr, const char *bsdata )
    {
      _queryResult.updateLine( keyStr, bsdata );
    }

    /// Delete line from view table
    void deleteLine( const string& keyStr )
    {
       _queryResult.deleteLine( keyStr );
    }
};

} // namespace bsonio

#endif // TDBDOCUMENT_H
