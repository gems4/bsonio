//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file io_settings.cpp
/// Implementation of BsonioSettings object for storing preferences to BSONIO
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//


#include <iostream>
#include <cstring>
#include "boost/filesystem.hpp"
#include "io_settings.h"
#include "dbdriverejdb.h"
#include "dbclient.h"
using namespace boost::filesystem;

namespace bsonio {

string BsonioSettings::settingsFileName = "bsonui.json";

/** TDBSchema* newDBImpexClient(  const string& queryString  )
{
    vector<KeyFldsData> _keyFldsInf = { KeyFldsData( "name", "name", "Only name" )};
    return ioSettings().newDBSchemaClient(
             "impexdefs", "ImpexFormat", _keyFldsInf, queryString );
}

TDBSchema* newDBQueryClient(  const string& queryString  )
{
    vector<KeyFldsData> _keyFldsInf = { KeyFldsData( "name", "name", "Query name" )};
    return ioSettings().newDBSchemaClient(
             "queries", "Query", _keyFldsInf, queryString );
}
*/

BsonioSettings& ioSettings()
{
  static  BsonioSettings data( BsonioSettings::settingsFileName );
  return  data;
}

//------------------------------------------------------------

BsonioSettings::BsonioSettings( const string& inifile ): mainSettings( inifile )
{
    getDataFromPreferences();
}

void BsonioSettings::getDataFromPreferences()
{
   UserDir = mainSettings.getString("UserFolderPath", ".");

   SchemDir =  "";
   updateSchemaDir();
   TEJDBDriveOne::flEJPath = "";
   updateLocalDB();

   // server db defaults
   // The host that the socket is connected to
   TDBClientOne::theHost =  mainSettings.getString("DBSocketHost",
         TDBClientOne::theHost );
   // The port that the socket is connected to
   TDBClientOne::thePort = mainSettings.getValue("DBSocketPort",
         TDBClientOne::thePort );

}

void BsonioSettings::setUserDir( const string& dirPath)
{
  if( dirPath.empty() )
          return;
   mainSettings.setValue("UserFolderPath",   dirPath );
   mainSettings.saveCfg();
   UserDir = mainSettings.getString("UserFolderPath", ".");
}

/**
TDBGraph* BsonioSettings::newDBGraphClient(const string& schemaName, const string& queryString ) const
{
  if( schemaName.empty() )
      return nullptr;
  string collcName = schemaName.c_str();
  collcName = mainSettings.getString("LocalDBCollection", "data");
  TDBGraph* newClient =TDBGraph::newDBClient( &schema,
          dbDrive( isLocalDB() ), collcName.c_str(), schemaName, queryString  );
  return newClient;
}

TDBSchema* BsonioSettings::newDBSchemaClient(  const string& collcName, const string& schemaName,
   const vector<KeyFldsData>& keyFldsInf, const string& queryString  ) const
{
    if( schemaName.empty() )
        return nullptr;
    TDBSchema* newClient =  TDBSchema::newDBClient( &schema, dbDrive( isLocalDB() ),
                        collcName, schemaName, keyFldsInf, queryString );
    return newClient;
}
*/

//-----------------------------------------------------

/// Graph database collection name (by default "data")
string BsonioSettings::localDBPath() const
{
    string LocalDBDir = mainSettings.getString("LocalDBDirectory", "./EJDB");
    string LocalDBName = mainSettings.getString("LocalDBName", "localdb");
    string path(LocalDBDir);
    if( path != "" && path.back() != '\\' && path.back() != '/' )
           path += "/";
    path += LocalDBName;
    return path;
}


// change local db settings
bool BsonioSettings::updateLocalDB()
{
  string path = localDBPath();
  if( !path.empty() && path != TEJDBDriveOne::flEJPath )
  {
    TEJDBDriveOne::flEJPath = path;
    ejdbDriveOne().resetEJDB(TEJDBDriveOne::flEJPath);
    return true;
  }
  return false;
}

// change server settingth
bool BsonioSettings::updateClientDB()
{
    // The host that the socket is connected to
    string newHost =  mainSettings.getString("DBSocketHost",
          TDBClientOne::theHost);
    // The port that the socket is connected to
    int newPort = mainSettings.getValue("DBSocketPort",
          TDBClientOne::thePort );

    if( TDBClientOne::theHost!= newHost || TDBClientOne::thePort!= newPort )
    {
      TDBClientOne::theHost = newHost;
      TDBClientOne::thePort = newPort;
      dbClientOne().resetDBServerClient(TDBClientOne::theHost,TDBClientOne::thePort);
      return true;
    }
    return false;
}

// Read all thrift schemas from Dir
void BsonioSettings::readSchemaDir( const string& dirPath )
{
   if( dirPath.empty() )
          return;

   path p (dirPath);
   directory_iterator end_itr;
   vector<std::string> fileNames;

    for(directory_iterator itr(p); itr != end_itr; ++itr)
    {
      if (is_regular_file(itr->path()))
      {
        string file = itr->path().string();
        //cout << "file = " << file << endl;
        if ( file.find(".schema.json") != std::string::npos)
            fileNames.push_back(file);
      }
    }

   schema.clearAll();
   for (auto file: fileNames)
      schema.addSchemaFile( file.c_str() );
}

} // namespace bsonio
