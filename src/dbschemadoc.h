//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbschemadoc.h
/// Declarations of class TDBSchema - working with database
/// Used  SchemaNode class - API for direct code access to internal
/// DOM based on our JSON schemas.
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TDBSCHEMADOC_H
#define TDBSCHEMADOC_H

#include "dbdocument.h"

namespace bsonio {

extern vector<KeyFldsData> defSchemaKeyFldsInf;

/// Definition of schema based database document
class TDBSchemaDocument : public TDBDocumentBase
{
    vector<string> _internalData;
    friend class GraphTraversal;

 protected:

    // Internal data
    const ThriftSchema *_schema;
    string _schemaName = "";                    ///<
    unique_ptr<SchemaNode> _currentData;        ///< Last read/save record

    // internal functions
    set<string> listQueryFields( bool addkey, const vector<string>& fields);

    /// Run current query, rebuild internal table of values
    void executeQuery();
    /// Run query
    ///  \param _queryJson - json string with where condition
    ///  \param  _queryFields - list of fields into result json ( if empty only _id )
    ///  \return _resultData - list of json strings with query result
    void executeQuery( const string&  _queryJson, const vector<string>& _queryFields,
                vector<string>& _resultData );

    ///  Provides 'distinct' operation over query
    ///  \param fpath Field path to collect distinct values from.
    ///  \param queryJson - json string with where condition
    ///  \return Unique values by specified fpath and query
    vector<string> distinctQuery( const string& fpath, const string&  queryJson="" );

    /// Extract key from internal data
    string getKeyFromCurrent();

    TDBSchemaDocument( const string& schemaName, const TDataBase* dbconnect,
                       const string& coltype, const string& colname,
                       const vector<KeyFldsData>& keyFldsInf );

 public:

   static TDBSchemaDocument* newDBSchemaDocument( const TDataBase* dbconnect,
                             const string& schemaName, const string& collcName,
                             const vector<KeyFldsData>& keyFldsInf = defSchemaKeyFldsInf,
                             const string& queryString = ""  );
   ///  Constructor
   TDBSchemaDocument( const string& aschemaName,
                      const TDataBase* dbconnect, const string& colname,
                      const vector<KeyFldsData>& keyFldsInf = defSchemaKeyFldsInf ):
       TDBSchemaDocument( aschemaName, dbconnect, "schema", colname, keyFldsInf )
   {
       resetSchema( aschemaName, true );
       // set query data
   }

   ///  Constructor
   TDBSchemaDocument( const string& schemaName, TDBCollection* collection  );
   /// Constructor from bson data
   TDBSchemaDocument( TDBCollection* collection, const char* bsobj );

    ///  Destructor
    virtual ~TDBSchemaDocument(){}

    /// Write to bson config file
    void toBson( bson *obj ) const;
    /// Read from bson config file
    void fromBson( const char *obj );

    const string& GetSchemaName() const
    {
      return _schemaName;
    }
    /// Change current schema
    virtual void resetSchema( const string& aschemaName, bool change_queries );

    /// Save current record to bson structure
    void recToBson( bson *obj, time_t crtt, char* oid );
    /// Load data from bson structure (return read record key)
    string recFromBson( bson *obj );


    /// Read current record to bson structure
    void GetBson( bson *obj )
    {
        _currentData.get()->getStructBson( obj );
    }
    /// Load data from bson structure to current record
    ///  \return new record key
    string SetBson( bson *obj )
    {
      return recFromBson( obj );
    }

    /// Save/update bson data as record
    /// \param obj - data to save
    /// \param  overwrite - can overwrite old records
    /// \param  testValues - compare full query line, not only key
    /// (if the same values into internal table and undefined key we do not add new record )
    void SaveBson( bson* obj, bool overwrite, bool testValues );
    /// Insert bson data as record
    /// \param obj - data to save
    /// \param  testValues - compare full query line, not only key
    /// (if the same values into internal table and undefined key we do not add new record )
    /// \return new _id
    string InsertBson(  bson* obj, bool testValues )
    {
      SetBson(obj);
      return InsertCurrent(testValues);
    }

    /// Return curent record in json format string
    string GetJson();
    /// Set json format string to current record
    void SetJsonYaml( const string& sjson, bool is_json = true);
    /// Return curent record in YAML format string
    string GetYAML();

    /// Build default query fields list
    virtual vector<string>  makeDefaultQueryFields(const string& schName);

    // working with internal data

    /// Get Value from Node
    /// If field is not type T, the false will be returned.
    template <class T>
    bool getValue( const string& fldpath, T& value  )
    {
        SchemaNode *data = _currentData->field(  fldpath );
        if( data == nullptr)
          return false;
        else
          return  data->getValue( value );
    }

    /// Set Value to Node
    template <class T>
    void setValue( const string& fldpath, const T& value  )
    {
        SchemaNode *data = _currentData->field(  fldpath );
        if( data != nullptr)
          data->setValue(value);
    }

    /// Find key from data
    string GetKeyFromValueNode( SchemaNode *node )
    {
       return  _queryResult.getKeyFromValue(node);
    }

    /// Insert currentData to database
    /// return new _id
    string InsertCurrent( bool testValues );

    /// Save currentData to database
    void SaveCurrent( bool overwrite, bool testValues );

};

} // namespace bsonio

#endif // TDBSCHEMADOC_H
