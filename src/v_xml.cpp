//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file v_xml.cpp
/// Implementation of functions for data exchange to/from XML format
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <time.h>
#include <sstream>
#include <iostream>
#include <algorithm>
#include "v_xml.h"
#include "v_yaml.h"

namespace bsonio {


namespace ParserXML
{

 void printBsonObjectToXml(const char* file_name, const char *aobj)
 {
      pugi::xml_document doc;
      pugi::xml_node decl = doc.append_child(node_declaration);
      decl.append_attribute("version") = "1.0";
      decl.append_attribute("encoding") = "UTF-16";
      decl.append_attribute("standalone") = "yes";
      // define root node
      pugi::xml_node fld = doc.append_child("node");
      bson_emitter( fld, aobj, BSON_OBJECT);
      doc.save_file(file_name);
  }

 void printBsonArrayToXml(const char* file_name, const char *aobj)
 {
      pugi::xml_document doc;
      pugi::xml_node decl = doc.append_child(node_declaration);
      decl.append_attribute("version") = "1.0";
      decl.append_attribute("encoding") = "UTF-16";
      decl.append_attribute("standalone") = "yes";
      // define root node
      pugi::xml_node fld = doc.append_child("node");
      fld.append_attribute("isarray") = true;
      bson_emitter( fld, aobj, BSON_ARRAY );
      doc.save_file(file_name);
  }

  void bson_emitter( xml_node& out, const char *data, int datatype )
  {
      bson_iterator i;
      const char *key;
      //bson_timestamp_t ts;
      xml_node ch1=out;

      bson_iterator_from_buffer(&i, data);
      while (bson_iterator_next(&i))
      {
          bson_type t = bson_iterator_type(&i);
          if (t == 0)
            break;
          //if( t == BSON_OID )
          //  continue;
          key = bson_iterator_key(&i);

          // before print
          switch( datatype )
          {
           case BSON_OBJECT:
             ch1 = out.append_child(key);
             break;
           case BSON_ARRAY:
             ch1 = out.append_child("element");
              break;
           default:
              break;
          }

          switch (t)
          {
            // impotant datatypes
            case BSON_NULL:
                 ch1.append_child(node_null);
                break;
            case BSON_BOOL:
                 ch1.text().set((bson_iterator_bool(&i) ? "true" : "false"));
                 break;
            case BSON_INT:
                 ch1.text().set(TArray2Base::value2string( bson_iterator_int(&i) ).c_str() );
                 break;
            case BSON_LONG:
                 ch1.text().set(TArray2Base::value2string( bson_iterator_long(&i) ).c_str() );
                 break;
            case BSON_DOUBLE:
                 ch1.text().set(TArray2Base::value2string( bson_iterator_double(&i) ).c_str() );
                 break;
            case BSON_STRING: // ?? " bson_iterator_string(&i) "
                 ch1.text().set(bson_iterator_string(&i) );
                 break;
            // main constructions
            case BSON_OBJECT:
                 bson_emitter( ch1, bson_iterator_value(&i), BSON_OBJECT);
               break;
            case BSON_ARRAY:
                  ch1.append_attribute("isarray") = true;
                  bson_emitter(ch1, bson_iterator_value(&i), BSON_ARRAY );
               break;

             // not used in GEMS data types
                case BSON_SYMBOL:
                  ch1.text().set(bson_iterator_string(&i) );
                       break;
                case BSON_OID:
                   { char oidhex[25];
                     bson_oid_to_string(bson_iterator_oid(&i), oidhex);
                     ch1.text().set( oidhex );
                   }
                   break;
                case BSON_DATE:
                     {
                       time_t time = bson_iterator_time_t(&i);
                       ch1.text().set( ctime( &time ) );
                       break;
                     }
                case BSON_BINDATA:
                //       out << "BSON_BINDATA";
                       break;
                case BSON_UNDEFINED:
                //      os << "BSON_UNDEFINED";
                       break;
                case BSON_REGEX:
                //       os << "BSON_REGEX: " << bson_iterator_regex(&i);
                       break;
                case BSON_CODE:
                //       os << "BSON_CODE: " << bson_iterator_code(&i);
                       break;
                case BSON_CODEWSCOPE:
                //       os << "BSON_CODE_W_SCOPE: " << bson_iterator_code(&i);
                //       bson_iterator_code_scope(&i, &scope);
                //       os << "\n      SCOPE: ";
                //       bson_print_raw_txt( os, (const char*) &scope, 0, BSON_CODEWSCOPE);
                       break;
                 case BSON_TIMESTAMP:
                //       ts = bson_iterator_timestamp(&i);
                //       os <<  "i: " << ts.i << ", t: " << ts.t;
                       break;
                 default:
                    ch1.text().set( "can't print type : " );

      }
     }
  }

  void parse( const pugi::xml_node& node, bson *brec  );


  // Parse one XML object from string to bson structure
  bool parseXMLToBson( const string& /*currentXML*/, bson */*obj*/ )
  {
    return false;
  }

  // Read one XML object from text file and parse to bson structure
  bool parseXMLToBson( const char *fname, bson *brec )
  {
      // read and parse xml document
      pugi::xml_document doc;
      pugi::xml_parse_result result = doc.load_file(fname);
      if (!result)
      {
          stringstream str;
          str << "XML [" << fname << "] parsed with errors " << endl;
          str << "Error description: " << result.description() << "\n";
          str << "Error offset: " << result.offset << " (error at [..." << (result.offset) << "]\n\n";
          bsonioErr( "XML parser", str.str() );
      }
      // parse xml to bson data
      auto node = doc.child("node");
      bool isarray = false;
      if( node.attribute("isarray").as_bool() == true )
          isarray = true;
      if( isarray )
          bson_append_start_array( brec,  node.name() );
      int ii=0;
      for (pugi::xml_node_iterator it = node.begin(); it != node.end(); ++it, ++ii )
      {
        if( it->type() != node_element )
           continue;
        if( isarray )
          it->set_name(to_string(ii).c_str());
        cout << it->name() << endl;
        parse( *it, brec  );
      }
      if( isarray )
          bson_append_finish_array(brec);
      return true;
  }

  // add ',' separated array
  void addArray(const char* key, const string& value, bson* brec )
  {
      string val = "",str = value;
      size_t pos = str.find( ',' );
      int ii=0;

      bson_append_start_array( brec, key);
      while( pos != string::npos )
      {
          val = str.substr(0, pos);
          str = str.substr( pos+1 );
          pos = str.find( ',' );
          strip_all(val, " \n\t\r\'\"");
          addScalar( to_string(ii++).c_str(), val, brec );
      }
      strip_all(str, " \n\t\r\'\"");
      addScalar( to_string(ii).c_str(), str, brec );
      bson_append_finish_array(brec);
  }


  void parse( const pugi::xml_node& node, bson *brec  )
  {
      if( !node.first_child() )
      {
           bson_append_null(brec, node.name() );
           return;
      }

      vector<string> keys;
      bool isobject = true;  // object list (all node_pcdata will be ignored )
      bool isarray = false;  // isobject ==  true and all names is same or attribut isarray == true
      for (pugi::xml_node_iterator it = node.begin(); it != node.end(); ++it)
      {
          if( it->type() == node_element )
            keys.push_back( it->name() );
      }

      if( keys.empty() )
          isobject = false;
      else
          if( node.attribute("isarray").as_bool() == true )
          {    isarray = true;
               isobject = false;
          }
          else  if( keys.size()>1 )
              {   auto pred = [&](const string& str ){ return str==keys[0]; };
                  if ( std::all_of(keys.begin()+1, keys.end(),  pred ) )
                  {    isarray = true;
                       isobject = false;
                  }
             }

      if( !isarray && !isobject ) // only internal text
      {
          string value = node.text().as_string();
          //if( !value.empty() )
          { if( node.attribute("isarray").as_bool() == true )
              addArray( node.name(), value, brec );
            else
            {     strip_all(value, " \n\t\r\'\"");
                  addScalar( node.name(), value, brec );
            }
          }
      }
      else if( isobject )
         {
            bson_append_start_object( brec,  node.name() );
            for (pugi::xml_node_iterator it = node.begin(); it != node.end(); ++it)
            {
              if( it->type() != node_element )
                 continue;
              parse( *it, brec  );
            }
            bson_append_finish_object(brec);
         }
         else if( isarray )
             {
                bson_append_start_array( brec,  node.name() );
                int ii=0;
                for (pugi::xml_node_iterator it = node.begin(); it != node.end(); ++it, ++ii)
                {
                  if( it->type() != node_element )
                    continue;
                  it->set_name(to_string(ii).c_str());
                  parse( *it, brec  );
                }
                bson_append_finish_array(brec);
              }
  }

 } // ParserXML

} // namespace bsonio
