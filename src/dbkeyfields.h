//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbkeyfields.h
/// Declarations of class TEJDBKey - description the key of record
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef DBKEYFIELDS_H
#define DBKEYFIELDS_H

#include <vector>
#include "nservice.h"
#include "v_json.h"

namespace bsonio {

extern const char* S_ANY;

/// Element in sequence of record keys
class IndexEntry
{
   vector<string> keyFlds;      ///< Record key fields

 public:

   IndexEntry( vector<string> akeyFlds): keyFlds(akeyFlds)
   { }

   IndexEntry( const IndexEntry& ndxE ): keyFlds(ndxE.keyFlds)
   { }

   const string& getKeyField( int ii ) const
   { return keyFlds[ii];}

   friend bool operator <( const IndexEntry&,  const IndexEntry& );
   friend bool operator >( const IndexEntry&,  const IndexEntry& );
   friend bool operator==( const IndexEntry&,  const IndexEntry& );
   friend bool operator!=( const IndexEntry&,  const IndexEntry& );

   friend class TEJDBKey;
};

/// This is struct contened definition of key field
struct KeyFldsData
{
    string name;       ///< Key fields names
    string shortName;  ///< Short key fields names
    string descr;      ///< Key field description
    int size;          ///< Key field size (-1 if any )

    KeyFldsData( const string& nameKey, const string& shortKey,
             const string& descrKey ="", int sizeKey = -1 ):
        name(nameKey), shortName(shortKey), descr(descrKey), size(sizeKey)
    { }

    KeyFldsData( const KeyFldsData& data ):  name(data.name),
        shortName(data.shortName), descr(data.descr), size(data.size)
    { }

};

/// This is struct contened the key of record
class TEJDBKey
{

protected:

    vector<KeyFldsData> rkDesc;    ///< Key fields description
    vector<string> rkFlds;         ///< Key fields current record
    string pKey;                   ///< Current key in packed form ( external )
    string uKey;                   ///< Current key in unpacked form ( external )
    bool usesize_ = false;

    /// Return record key in packed form
    const char *pack( const vector<string>& akeyFlds );
    /// Return record key in unpacked form
    const char *unpack( const vector<string>& akeyFlds );

    void setUseSize()
    {
      usesize_ = true;
      for( uint ii=0; ii<rkDesc.size(); ii++)
           if( rkDesc[ii].size <= 0 )
            {
              usesize_ = false;
              break;
           }
    }

public:

    /// Default configuration
    TEJDBKey( const vector<KeyFldsData>& keyFldsInf );
    /// Copy struct
    TEJDBKey( const TEJDBKey& dbKey );
    /// Constructor from bson data
    TEJDBKey( const char* bsobj )
    {
        fromBson( bsobj );
        setUseSize();
    }
    ~TEJDBKey()
    {}

    virtual void toBson( bson *obj ) const;
    virtual void fromBson( const char *obj );

    /// Return current record key in packed form
    const char *packKey()
    { return pack( rkFlds ); }
    /// Return current record key in unpacked form
    const char *unpackKey()
    { return unpack( rkFlds ); }

    /// Return number of record key fields
    uint keyNumFlds() const
    {  return rkDesc.size();  }

    /// Return record key field i
    const char* keyFld(int i) const
    {   return rkFlds[i].c_str(); }

    /// Return record key field name
    const char* keyFldName(int i) const
    {   return rkDesc[i].name.c_str(); }
    /// Return short record key field name
    const char* keyFldShortName(int i) const
    {   return rkDesc[i].shortName.c_str(); }
    /// Return record key field description
    const char* keyFldDesc(int i) const
    {   return rkDesc[i].descr.c_str(); }
    /// Return record key field size
    uint keyFldSize(int i) const
    {   return rkDesc[i].size; }
    /// Return full record key size
    int keySize() const
    {
      if( !usesize_ )
          return -1;
      int size = 0;
      for( uint ii=0; ii<rkDesc.size(); ii++)
           size+= rkDesc[ii].size;
      return size;
    }

    //--- Manipulation keys

    /// Putting DB record key pkey to internal structure
    void setKey( const char *pkey );

    /// Change i-th field of TEJDBKey to fld
    void setFldKey( uint i, const char *fld );

    IndexEntry retIndex()
    { return IndexEntry(rkFlds); }

    /// Return index key in pack form
    const char *packKey( const IndexEntry& ndx )
    {  return pack( ndx.keyFlds ); }
    /// Return current record key in unpacked form
    const char *unpackKey(const IndexEntry& ndx )
    { return unpack( ndx.keyFlds ); }

    bool compareTemplate( const IndexEntry& elm );

    // Test work DB record key

    /// Check if pattern in record key
    bool isPattern();
    /// Check if setted S_ANY
    bool isAll();
 };

} // namespace bsonio

#endif // DBKEYFIELDS_H
