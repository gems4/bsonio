//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbcollection.cpp
/// Implementation of class TDBAbstract - base interface of abstract DB chain
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include <cstring>
#include "dbdocument.h"
#include "dbconnect.h"

namespace bsonio {

//-------------------------------------------------------------
// TDBCollection - This class contains the structure of Data Base Collection
//-------------------------------------------------------------


// Default configuration of the Data Base
TDBCollection::TDBCollection(  const TDataBase* adbconnect,
       const char* name, const vector<KeyFldsData>& keyFldsInf  ):
    _name( name ), _key( keyFldsInf ),
    _dbconnect(adbconnect), _dbdriver(adbconnect->newDrive())
{ }


// Configurator by readind from bson object
TDBCollection::TDBCollection( const TDataBase* adbconnect, const char* bsobj):
   _key( bsobj ), _dbconnect(adbconnect), _dbdriver(adbconnect->newDrive())
{
    fromBson(bsobj);
}

// Open collection file and build linked record list
void TDBCollection::Open()
{
    _recList.clear();
    _itrL= _recList.end();
    loadCollection();
}

// Close file, clear key list
void TDBCollection::Close()
{
    closeCollection();
    _recList.clear();
    _itrL= _recList.end();
}

// Writes data to bson
void TDBCollection::toBson( bson *obj ) const
{
    _key.toBson(obj);
    bson_append_string( obj, "CollectionName", _name.c_str() );
}

// Reads data from bson
void TDBCollection::fromBson( const char* bsobj )
{
    if(!bson_find_string( bsobj, "CollectionName", _name ) )
        bsonioErr( "E010BSon: ", "Undefined Collection Name.");
}

// Reconnect DataBase (collection)
void TDBCollection::changeDriver( TAbstractDBDriver* adriver )
{
  Close();
  _dbdriver = adriver;
  Open();
  for( auto doc: _documents )
    doc->runQuery(); // Run current query, rebuild internal table of values
}

// Reconnect DataBase (collection)
void TDBCollection::reloadCollection()
{
  Close();
  Open();
  for( auto doc: _documents )
    doc->runQuery(); // Run current query, rebuild internal table of values
}


// Add current record key to bson structure
void TDBCollection::addKeyToBson( bson *obj )
{
    for( uint ii=0; ii < keyNumFlds(); ii++)
       bson_append_string( obj, keyFldShortName(ii), keyFld(ii) );
}

// Get current record key from bson structure
string TDBCollection::getKeyFromBson( const char* bsdata )
{
    //get key from object
    string keyStr = "", kbuf;
    for(uint ii=0; ii<keyNumFlds(); ii++ )
    {
        if( !bson_to_key( bsdata, keyFldShortName(ii), kbuf ) )
            if( !bson_to_key( bsdata, keyFldName(ii), kbuf ) )
                 kbuf = "*";
        strip( kbuf );
        keyStr += kbuf;
        keyStr += ":";
    }
    return keyStr;
}

// Seach record index with key pkey.
bool TDBCollection::Find( const char *pkey )
{
    if(_recList.empty() )
      return false;
    TEJDBKey wkey(_key);
    wkey.setKey( pkey );
    _itrL = _recList.find( wkey.retIndex() );

    if( _itrL == _recList.end() )
        return  false;
    else
        return true;
}

// Retrive one record from the collection
void TDBCollection::GetRecord( TDBDocumentBase* document, const char *pkey )
{
    _key.setKey( pkey );
    if( _key.isPattern() )
      bsonioErr("TEJDB0010", "Cannot get under record key template" );

    _itrL = _recList.find( _key.retIndex() );
    if( _itrL == _recList.end() )
    {
       string erstr = pkey;
              erstr += "\nrecord to retrive does not exist!";
       bsonioErr("TEJDB0001", erstr );
    }

    // Read record from DB to bson
    bson *bsrec = _dbdriver->loadbson(getName(), _itrL);
     if( !bsrec )
     {  string errejdb = "Error Loading record ";
               errejdb+= pkey;
               errejdb+= " from Data Base";
        bsonioErr( "TEJDB0025",  errejdb );
     }

     // Save bson structure to internal arrays
     document->recFromBson( bsrec );
     bson_del( bsrec );
}

// Removes record from the collection
void TDBCollection::DelRecord( const char *pkey )
{
    _key.setKey( pkey );
    if( _key.isPattern() )
      bsonioErr("TEJDB0010", "Cannot delete under record key template" );

    _itrL = _recList.find( _key.retIndex() );
    if( _itrL == _recList.end() )
    {
       string erstr = pkey;
              erstr+= "\nrecord to delete does not exist!";
       bsonioErr("TEJDB0002", erstr );
    }

    // Remove BSON object from collection.
    bool iRet = _dbdriver->rmbson(getName(), _itrL);
    if( !iRet )
    {  string errejdb = "Error deleting of record ";
              errejdb+= pkey;
              errejdb+= " from Data Base ";
       bsonioErr( "TEJDB0024",  errejdb );
    }

    // Set up internal data
    _recList.erase(_itrL);
    _itrL = _recList.end();
    for( auto itDoc:  _documents)
       itDoc->deleteLine( _key.packKey() );
}

// Save new record in the collection
void TDBCollection::InsertRecord( TDBDocumentBase* document, const char *pkey )
{
    // Try to insert new record to list
    if( pkey )
      _key.setKey( pkey );

    bson bsrec;
    // Get bson structure from internal data
    document->recToBson( &bsrec, time(NULL), nullptr );

    //if( !pkey ) protect different data into bsrec and key
    _key.setKey( getKeyFromBson( bsrec.data ).c_str() );
    if( _key.isPattern() )
      bsonioErr("TEJDB0010", "Cannot save under record key template" );

    pair<KeysSet::iterator,bool> ret;
    ret = _recList.insert( pair<IndexEntry,unique_ptr<char>>(_key.retIndex(), nullptr ) );
    _itrL = ret.first;
    // Test unique keys name before add the record(s)
    if( ret.second == false)
    {
        string erstr = "Cannot insert record:\n";
               erstr += _key.packKey();
               erstr += ".\nTwo records with the same key!";
        bsonioErr("TEJDB0004", erstr );
    }

   try{
       // save record to data base
       string errejdb;
       bool retSave = _dbdriver->savebson( getName(), _itrL, &bsrec, errejdb );
       if( !retSave )
        bsonioErr( "TEJDB0021",  errejdb );
    }
    catch(...)
    {
       bson_destroy(&bsrec);
       _recList.erase(_itrL);
       _itrL = _recList.end();
       throw;
    }

    // Set up internal data - only delete all, not add to selection
    string keyStr = getKeyFromBson( bsrec.data );
    // for( auto itDoc:  _documents)
    document->addLine( keyStr ,bsrec.data, false );
    bson_destroy(&bsrec);
}

// Save/update record in the collection
void TDBCollection::SaveRecord( TDBDocumentBase* document, const char* pkey )
{
    // Try to insert new record to list
    _key.setKey( pkey );
    if( _key.isPattern() )
      bsonioErr("TEJDB0011", "Cannot save under record key template" );

   pair<KeysSet::iterator,bool> ret;
   ret = _recList.insert( pair<IndexEntry,unique_ptr<char>>(_key.retIndex(), nullptr ) );
   _itrL = ret.first;

   bson bsrec;
   try{
       // Get bson structure from internal data
       document->recToBson( &bsrec, time(NULL), _itrL->second.get() );

      // save record to data base
      string errejdb;
      bool retSave = _dbdriver->savebson( getName(), _itrL, &bsrec, errejdb );
      if( !retSave )
       bsonioErr( "TEJDB0022",  errejdb );
   }
   catch(...)
   {
      bson_destroy(&bsrec);
      if( ret.second == true )
      {
          _recList.erase(_itrL);
          _itrL = _recList.end();
      }
      throw;
   }

   // Set up internal data ( only for current document )
   string keyStr = getKeyFromBson( bsrec.data );
   for( auto itDoc:  _documents)
     if( itDoc != document )
       itDoc->updateLine( keyStr ,bsrec.data );
   document->addLine( keyStr ,bsrec.data, true );
   bson_destroy(&bsrec);
}


//Test state of record with key key_ as template.
// in field field setted any(*) data
bool TDBCollection::FindPart( const char *pkey, int field )
{
    TEJDBKey wkey(_key);
    wkey.setKey( pkey );
    wkey.setFldKey(field,"*");
    RecStatus iRet = Rtest( wkey.packKey() );
    return ( iRet==MANY_ || iRet==ONEF_ );
}

// Test state of record with key pkey.
// returns state of record
TDBCollection::RecStatus TDBCollection::Rtest( const char *key )
{
    int iRec;
    const char *pkey;
    bool OneRec = true;

    if( key != 0 && *key )
        pkey = key;
    else
        pkey = packKey();

    if( strpbrk( pkey, "*?" ))
        OneRec = false; // pattern

     if( getRecCount() <= 0 )
        return EMPC_;

    if( OneRec )
    {
       if( !Find( pkey )  )
            return NONE_;
       return ONEF_;
    }
    else // template
    {   vector<string> aKeyList;
        iRec = GetKeyList( pkey, aKeyList, false );
        if( iRec == 0 )
            return NONE_;
        if( iRec == 1 )
        {
            return ONEF_;
        }
        return MANY_;
    }
}

int TDBCollection::GetKeyList( const char *keypat,
        vector<string>& aKeyList, bool retUnpackform )
{
    // Set key template
    TEJDBKey wkey(_key);
    wkey.setKey( keypat );
    aKeyList.clear();

    bool OneRec = !wkey.isPattern(),
         AllRecs = wkey.isAll();

    KeysSet::iterator it;
    it = _recList.begin();
    while( it != _recList.end() )
    {
        if( !AllRecs )
            if( !wkey.compareTemplate( it->first ) )
              { it++;
                continue;
              }
        if( retUnpackform )
           aKeyList.push_back( wkey.unpackKey( it->first ) );
        else
           aKeyList.push_back( wkey.packKey( it->first ) );
        if( OneRec )
         break;

        it++;
    }
    return aKeyList.size();
}

//-----------------------------------------------------------------

set<string> TDBCollection::listQueryFields()
{
   set<string> queryFields;
   for(uint ii=0; ii<keyNumFlds(); ii++ )
   {
         queryFields.insert( _key.keyFldName(ii) );
         queryFields.insert( _key.keyFldShortName(ii) );
   }
   return queryFields;
}


// Working with collections
void TDBCollection::loadCollectionFile(  const set<string>& queryFields )
{
    SetReadedFunctionKey setfnc = [=]( const char* bsdata, char*keydata )
    {
        listKeyFromBson( bsdata, keydata );
    };
    _dbdriver->allQuery( getName(), queryFields, setfnc );
}

// Load records key from bson structure
void TDBCollection::listKeyFromBson( const char* bsdata, char* keydata )
{
    // Get key of record
    string keyStr = getKeyFromBson( bsdata );
    _key.setKey( keyStr.c_str() );
    if( _key.isPattern() )
      bsonioErr("TGRDB0005", "Cannot save under record key template" );

    // Try add key to list
    pair<KeysSet::iterator,bool> ret;
    ret = _recList.insert( pair<IndexEntry,unique_ptr<char> >( _key.retIndex(), unique_ptr<char>(keydata) ) );
    _itrL = ret.first;
    // Test unique keys name before add the record(s)
    if( ret.second == false)
    {
        string erstr = "Cannot add new record:\n";
               erstr += keyStr;
               erstr += ".\nTwo records with the same key!";
        bsonioErr("TGRDB0006", erstr );
    }
}

bool TDBCollection::CompareTemplate( const char* keypat, const char* akey)
 {
     TEJDBKey currentKey(_key);
     currentKey.setKey( akey);
     TEJDBKey wkey(_key);
     wkey.setKey( keypat );
     return wkey.compareTemplate( currentKey.retIndex() );
 }

} // namespace bsonio
