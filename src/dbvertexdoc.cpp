//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbgraph.cpp
/// Implementation of classes TGraphAbstract - working with graph databases (OLTP)
/// TGraphEJDB - working with internal EJDB data base in graph mode
/// Used  SchemaNode class - API for direct code access to internal
/// DOM based on our JSON schemas.
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include <regex>
#include "dbvertexdoc.h"
#include "io_settings.h"

namespace bsonio {

// TGraphAbstract ---------------------------------------------

vector<KeyFldsData> graphKeyFldsInf = { KeyFldsData( "_id", "_id", "Only id" )};

string collectionNameFromSchema( const string& schemaName )
{
   return ioSettings().collection();
}

TDBVertexDocument* TDBVertexDocument::newDBVertexDocument( const TDataBase* dbconnect,
              const string& schemaName, const string& queryString  )
{
   if( schemaName.empty()  )
          return nullptr;
   TDBVertexDocument* newDov = new TDBVertexDocument( schemaName, dbconnect );
   if( !queryString.empty() )
            newDov->SetQueryJson(queryString);
   newDov->runQuery();
   return newDov;
}


TDBVertexDocument::TDBVertexDocument( const string& aschemaName, TDBCollection* collection  ):
  TDBSchemaDocument( aschemaName, collection ),
  _changeSchemaMode(false)
{
   resetSchema( aschemaName, true );
   // set query data
}

TDBVertexDocument::TDBVertexDocument( const string& aschemaName, const TDataBase* dbconnect ):
    TDBVertexDocument( aschemaName,  dbconnect, "vertex",
               collectionNameFromSchema( aschemaName ).c_str(), graphKeyFldsInf )
{
   resetSchema( aschemaName, true );
   // set query data
}

/// Constructor from bson data
TDBVertexDocument::TDBVertexDocument( TDBCollection* collection, const char* bsobj ):
    TDBSchemaDocument( collection, bsobj ), _changeSchemaMode(false)
{
    resetSchema( _schemaName, true );
    // set query data
    SetDefaultQueryFields(makeDefaultQueryFields(_schemaName));
    SetDefaultQueryJson(makeDefaultQuery());
}

string TDBVertexDocument::makeDefaultQuery()
{
   string queryJson =  "{'_label': '";
    queryJson += _label;
    queryJson += "' }";

   return queryJson;
}

void TDBVertexDocument::resetSchema( const string& aschemaName, bool change_queries )
{
    //if( schemaName != aschemaName )
    {
      _schemaName = aschemaName;
      _currentData.reset( SchemaNode::newSchemaStruct( _schemaName ) );
      SchemaNode* type_node = _currentData->field("_type");
      _type = "";
      if( type_node != nullptr )
        type_node->getValue(_type);
      bsonioErrIf( _type != type(), "TGRDB0004", "Illegal record type: " + _type );

      type_node = _currentData->field("_label");
      _label = "";
      if( type_node != nullptr )
         type_node->getValue(_label);
    }
    if( change_queries )
    {
        SetDefaultQueryFields(makeDefaultQueryFields(_schemaName));
        SetDefaultQueryJson(makeDefaultQuery());
        _queryResult.setFieldsCollect(_defqueryFields);
        _queryResult.setEJDBQuery(_defqueryJson);
        _queryResult.setToSchema(_schemaName);
    }

    // load unique map
    loadUniqueFields();
}

// Test true type and label for schema
void TDBVertexDocument::testSchema( const char *bsobj )
{
    string newtype, newlabel;

    if(!bson_find_string( bsobj, "_type", newtype ) )
        newtype = "";
    if(!bson_find_string( bsobj, "_label", newlabel ) )
        newlabel = "";

    if( _changeSchemaMode )
    {
      string newSchema;
      if( newtype == "edge")
       newSchema = _schema->getEdgeName(newlabel);
      else
        if( newtype == "vertex")
         newSchema = _schema->getVertexName(newlabel);
      bsonioErrIf(newSchema.empty(),  "TGRDB0007",
        "Undefined record: " + newtype + " type " + newlabel + " label" );
      resetSchema( newSchema, false );
    } else
      {
        if( newtype != _type || newlabel != _label )
          bsonioErr("TGRDB0004", "Illegal record type: " + newtype + " or label: " + newlabel );
      }
}


void TDBVertexDocument::loadUniqueFields()
{
    uniqueFieldsNames.clear();
    uniqueFieldsValues.clear();

    ThriftStructDef* strDef = _schema->getStruct(_schemaName);
    if( strDef == nullptr )
        return;

    uniqueFieldsNames = strDef->getUniqueList();
    if( uniqueFieldsNames.empty() )
       return;

    uniqueFieldsNames.push_back("_id");
    ValuesTable uniqueVal = loadRecords( makeDefaultQuery(), uniqueFieldsNames );

    for( auto row: uniqueVal)
    {
      auto idkey = row.back();
      row.pop_back();

      if( !uniqueFieldsValues.insert( std::pair< vector<string>, string>(row, idkey)).second )
      {
          cout << "Not unique values: " << string_from_vector( row ) << endl;
      }
   }
}

void TDBVertexDocument::recToBson( bson *obj, time_t /*crtt*/, char* pars )
{
     // test oid
    SchemaNode* oidNode = _currentData.get()->field("_id");
    bsonioErrIf(oidNode==nullptr, "TGRDB0002", "No oid in record" );
    string oidval;
    oidNode->getValue( oidval );

    if( pars )
    {
        if( !oidval.empty() )
        {
          if( oidval != string((char *)pars) )
            bsonioErr("TGRDB0003", "Changed record oid " + string((char *)pars) );
        } else // add oid
          {
            oidNode->setValue( string((char *)pars) );
          }
     }
     else
         if( oidval.empty() )
         {
             oidNode->setValue( genOid() ); // new oid
         }

    // added Modify time ??? Special strucrure use
    // bson_append_time_t( obj , "mtime", crtt );

    // make bson from current data
    _currentData.get()->getStructBson( obj );
}

// Load data from bson structure (return read record key)
string TDBVertexDocument::recFromBson( bson *obj )
{
    testSchema( obj->data );
    _currentData.get()->setStructBson( obj->data );
    // Get key of record
    string keyStr = getKeyFromBson( obj->data );
    return keyStr;
}


/// Delete all edges by vertex id
void TDBVertexDocument::beforeRm( const char *key )
{
    string  _vertexid = key;
    strip_all( _vertexid, ":" ); // get id from key
    //_vertexid =_vertexid.substr(0, _vertexid.size()-1); // delete ":"

    // init query to fields
    //bson bshits1;
    //bson_init_as_query(&bshits1);
    //bson_append_start_object(&bshits1, "$fields");
    //bson_append_int(&bshits1, JDBIDKEYNAME, 1);
    //bson_append_finish_object(&bshits1);
    //bson_finish(&bshits1);

    // delete all edges query
    bson bsq1;
    bson_init_as_query(&bsq1);
    bson_append_string(&bsq1, "_type", "edge");
    bson_append_start_array(&bsq1, "$or");
    bson_append_start_object(&bsq1, "0" );
    bson_append_string(&bsq1, "_inV", _vertexid.c_str() );
    bson_append_finish_object( &bsq1 );
    bson_append_start_object(&bsq1, "1" );
    bson_append_string(&bsq1, "_outV", _vertexid.c_str() );
    bson_append_finish_object( &bsq1 );
    bson_append_finish_array(&bsq1);
    bson_append_bool(&bsq1, "$dropall", true);
    bson_finish(&bsq1);

    _collection->delQuery( &bsq1 );

    //bson_destroy(&bshits1);
    bson_destroy(&bsq1);
}

void TDBVertexDocument::afterRm( const char *key )
{

    // delete from unique map
    if( !uniqueFieldsNames.empty() )
    {
      string  _vertexid = key;
      strip_all( _vertexid, ":" ); // get id from key
      //_vertexid =_vertexid.substr(0, _vertexid.size()-1); // delete ":"
      auto itrow = uniqueLinebyId( _vertexid );
      if( itrow != uniqueFieldsValues.end() )
         uniqueFieldsValues.erase(itrow);
    }
}

UniqueFieldsMap::iterator TDBVertexDocument::uniqueLinebyId( const string& idschem )
{
    UniqueFieldsMap::iterator itrow = uniqueFieldsValues.begin();
    while( itrow != uniqueFieldsValues.end() )
    {
      if( itrow->second == idschem )
          break;
      itrow++;
    }
    return itrow;
}

void TDBVertexDocument::beforeSaveUpdate( const char * )
{
  if( !uniqueFieldsNames.empty() )
  {
      bson bsrec;
      GetBson( &bsrec );
      FieldSetMap uniqfields = extractFields( uniqueFieldsNames, bsrec.data );
      bson_destroy(&bsrec);

     vector<string> uniqValues;
     for( uint ii=0; ii<uniqueFieldsNames.size()-1; ii++ )
       uniqValues.push_back( uniqfields[uniqueFieldsNames[ii]] );

     auto itfind = uniqueFieldsValues.find(uniqValues);
     if( itfind != uniqueFieldsValues.end() )
     {
       if( itfind->second != uniqfields["_id"] )
         bsonioErr("TGRDB0009", "Not unique values: " + string_from_vector( uniqValues ) );
     }
  }
}

void TDBVertexDocument::afterSaveUpdate( const char * )
{
  if( !uniqueFieldsNames.empty() )
  {
      bson bsrec;
      GetBson( &bsrec );
      FieldSetMap uniqfields = extractFields( uniqueFieldsNames, bsrec.data );
      bson_destroy(&bsrec);

      // delete old
      auto itrow = uniqueLinebyId( uniqfields["_id"] );
      if( itrow != uniqueFieldsValues.end() )
         uniqueFieldsValues.erase(itrow);

      // insert new
      vector<string> uniqValues;
      for( uint ii=0; ii<uniqueFieldsNames.size()-1; ii++ )
         uniqValues.push_back( uniqfields[uniqueFieldsNames[ii]] );
      if( !uniqueFieldsValues.insert( std::pair< vector<string>, string>(uniqValues, uniqfields["_id"])).second )
      {
          cout << "Not unique values: " << string_from_vector( uniqValues ) << endl;
      }
  }
}

// Test existence records
bool TDBVertexDocument::existKeysByQuery( const string& query )
{
    vector<string> _queryFields = { "_id"};
    vector<string> _resultData;
    runQuery( query,  _queryFields, _resultData );
    return !_resultData.empty();
}

// Build keys list by query
vector<string> TDBVertexDocument::getKeysByQuery( const string& query )
{
    vector<string> keys;
    vector<string> queryFields = { "_id"};
    vector<string> resultData;
    runQuery( query,  queryFields, resultData );

    std::regex re("\\{\\s*\"_id\"\\s*:\\s*\"([^\"]*)\"\\s*\\}");
    std::string replacement = "$1:";
    for( uint ii=0; ii<resultData.size(); ii++)
    {
      string akey = std::regex_replace( resultData[ii], re, replacement);
      //cout << _resultData[ii] << "      " << akey << endl;
      keys.push_back(akey);
    }
    return keys;
}

// Extract label by id
string TDBVertexDocument::extractLabelById( const string& id )
{
  string token;
  string query =  idQuery( id );
  vector<string> queryFields = { "_label"};
  vector<string> resultData;
  runQuery( query,  queryFields, resultData );
  if( resultData.size()>0  )
    token = extractStringField( "_label", resultData[0] );
  return token;
}

string TDBVertexDocument::getSchemaFromId( const string& id  )
{
    string schemaName, label = extractLabelById( id );
    if( !( label.empty() ) )
       schemaName = _schema->getVertexName(label);
    return schemaName;
}

// build functions ------------------------------------------------------------

// Define new Vertex
void TDBVertexDocument::setVertex( const string& aschemaName, const FieldSetMap& fldvalues )
{
    // check schema
    if( _schemaName != aschemaName )
    {
      if( _changeSchemaMode )
          resetSchema( aschemaName, false );
      else
        bsonioErr("TGRDB0007", "Illegal record schame: " + aschemaName + " current schema: " + _schemaName );
    }
   _currentData->clearNode(); // set default values

   for(auto const &ent : fldvalues)
     setValue( ent.first, ent.second  );
}

// Update current schema data
void TDBVertexDocument::updateVertex( const string& aschemaName, const FieldSetMap& fldvalues )
{
   // check schema
   if( _schemaName != aschemaName )
        bsonioErr("TGRDB0008", "Illegal record schame: " + aschemaName + " current schema: " + _schemaName );
   for(auto const &ent : fldvalues)
     setValue( ent.first, ent.second  );
}


ValuesTable TDBVertexDocument::loadRecords( const string& query, const vector<string>& queryFields )
{
    ValuesTable recordsValues;
    FieldSetMap  fldsValues;

    vector<string> resultData, rowData;
    runQuery( query,  queryFields, resultData );

    for( const auto& subitem: resultData )
    {
      fldsValues = extractFields( queryFields, subitem );

      rowData.clear();
      for( const auto& fld: queryFields)
        rowData.push_back( fldsValues[fld] );
      recordsValues.push_back(rowData);
    }

    return recordsValues;
}

ValuesTable TDBVertexDocument::loadRecords( const vector<string>& ids, const vector<string>& queryFields )
{
    ValuesTable recordsValues;
    FieldSetMap  fldsValues;
    vector<string> rowData;

    for( const auto& id_: ids )
    {
      bson bobj;
      GetRecord( (id_+":").c_str() );
      GetBson( &bobj );
      fldsValues = extractFields( queryFields, &bobj );

      rowData.clear();
      for( const auto& fld: queryFields)
        rowData.push_back( fldsValues[fld] );
      recordsValues.push_back(rowData);
      bson_destroy(&bobj);
    }

    return recordsValues;
}

FieldSetMap TDBVertexDocument::loadRecordFields( const string& id, const vector<string>& queryFields )
{
    bson bobj;
    GetRecord( (id+":").c_str() );
    GetBson( &bobj );
    FieldSetMap fldsValues = extractFields( queryFields, &bobj );
    bson_destroy(&bobj);
    return fldsValues;
}


} // namespace bsonio
