//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbserver.h
/// Declarations of class DBServerHandler - to define a service interface
/// for RPC client and server to communicate over.
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef DBSERVERHANDLER_H
#define DBSERVERHANDLER_H

#include "DBServer.h"
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

#include <set>
#include "dbdriverejdb.h"
using namespace bsonio;
using namespace  ::dbserver;

class EJDBServerHandler : virtual public DBServerIf
{

    // will be mutex for collecs_ amd map of mutexes for all collections

    /// Directory to store db files.
    std::string dbName_;
    /// Description of current EJDB * .
    TEJDBFile curentEJDB;
    /// map of collections
    std::set<std::string> collecs_;

    EJCOLL *getCollection( const string& collName, bool createifempty );
    void getBsonOid( bson_oid_t *oid, const string& pars );
    void setBsonOid( bson_oid_t *oid, string& oidstr );
    /// Convert record key and json string value  to bson structure
    void jsonToBson( bson *obj, const string& key, const string& recJson );

public:

  explicit EJDBServerHandler( const char* pathname ):
        dbName_(pathname), curentEJDB(string(pathname))
  {
     setDB( pathname );
  }

  /// Open/Create EJDB for path
  void setDB(const char* pathname  )
  {
     curentEJDB.reOpen( pathname );
  }

  /**
   * Pings DBServer.
   *
   * @return Success - if ping was successful.
   *         Error -   if ping failed.
   */
  ResponseCode::type ping();

  /**
   * Stop DBServer.
   *
   */
  void stop();

  /**
   * Add a new collection of records.
   *
   * A record is a string key / json string value pair.
   * A key uniquely identifies a record in a collection.
   *
   * @param  colName collection name
   *         keynames list of names used as key fields
   * @return Success - on success.
   *         NameExists - collection already exists.
   *         Error - on any other errors.
   */
  ResponseCode::type addCollection(const std::string& colName);

  /**
   * Drops a collection of records.
   *
   * @param colName collection name
   * @return Ok - on success.
   *         NameNotFound - map doesn't exist.
   *         Error - on any other errors.
   */
  ResponseCode::type dropCollection(const std::string& colName);

  /**
   * List all the collections.
   *
   * @returns StringListResponse
   *              responseCode Success  or
   *                           Error - on error.
   *              values - list of collection names.
   */
  void listCollections(StringListResponse& _return);

  /**
   * Add new record into the collection
   *
   * @param colName       collection name
   * @param value         record value to save
   * @returns
   *          responseCode - Success
   *                         NameNotFound collection doesn't exist.
   *                         Error
   *              value -   oid  of the record.
   */
  void CreateRecord( KeyResponse& _return,
                     const std::string& colName, const std::string& value);

  /**
   * Retrive one record from the collection
   *
   * @param colName       collection name
   * @param key           records key to retrive.
   * @returns BinaryResponse
   *              responseCode - Success
   *                             NameNotFound collection doesn't exist.
   *                             RecordNotFound record doesn't exist.
   *                             Error on any other errors.
   *              value - value of the record.
   */
  void ReadRecord( ValueResponse& _return,
                  const std::string& colName, const std::string& key);

  /**
   * Update record in the collection
   *
   * @param colName       collection name
   * @param key           records key to update.
   * @param value         record value to update
   * @returns Success
   *          NameNotFound collection doesn't exist.
   *          RecordNotFound
   *          Error
   */
  ResponseCode::type UpdateRecord(const std::string& colName,
                      const std::string& key, const std::string& value);

  /**
   * Removes record from the collection
   *
   * @param colName       collection name
   * @param key           records key to delete.
   * @returns Success
   *          NameNotFound collection doesn't exist.
   *          RecordNotFound
   *          Error
   */
  ResponseCode::type DeleteRecord(const std::string& colName, const std::string& key);

  /**
   * Returns list of records in a collection.
   *
   * @param colName       collection name
   * @param recordQuery   json query string for collection
   * @param fieldQuery    json query string for fields list or order
   * @return RecordListResponse
   *             responseCode - Success
   *                          - NameNotFound collection doesn't exist.
   *                          - Error on any other errors
   *             records - list of records.
   */
  virtual void SearchRecords(DBRecordListResponse& _return, const std::string& colName,
          const std::set<std::string>& fields, const std::string& recordQuery);


  /**
   * Delete list of records in a collection.
   *
   * @param colName       collection name
   * @param recordQuery   json query string for delete
   * @returns Success
   *          NameNotFound collection doesn't exist.
   *          Error
   *
   * @param colName
   * @param deleteQuery
   */
  ResponseCode::type DeleteRecordsQuery(const std::string& colName, const std::string& deleteQuery);

  /**
   * Provides 'distinct' operation over query
   *
   * @param colName       collection name
   * @param fpath Field path to collect distinct values from.
   * @param recordQuery   json query string for collection
   * @return StringListResponse
   *             responseCode - Success
   *                          - NameNotFound collection doesn't exist.
   *                          - Error on any other errors
   *             records - list of distinct values filed fpath.
   *
   * @param colName
   * @param fpath
   * @param recordQuery
   */
  void SearchValues(StringListResponse& _return, const std::string& colName,
                    const std::string& fpath, const std::string& recordQuery);

  /**
   *  Generate new unique oid or other key string
   *
   * @param colName       collection name
   * @returns new key string
   *
   * @param colName
   */
  void GenerateId(std::string& _return, const std::string& colName);

};


#endif // DBSERVERHANDLER_H
