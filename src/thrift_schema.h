//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file thrift_schema.h
/// Declarations of classes and functions to work with Thrift Schemas
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef THRIFTSCHEMA_H
#define THRIFTSCHEMA_H

#include <memory>
#include <thrift/protocol/TVirtualProtocol.h>
#include <thrift/TBase.h>
#include "v_json.h"
using namespace std;

namespace bsonio {

/// A requiredness attribute which defines how
/// Apache Thrift reads and writes the field
enum  REQURED_FIELD
{
     fld_required = 0,
     fld_default = 1,
     fld_optional = 2
};

class ThriftSchema;

/// Write data from thrift generated structure to bson object (using schema )
void writeDataToBsonSchema( const ThriftSchema* schema, bson* obj,
           const string& structName, ::apache::thrift::TBase* data );
/// Read data to thrift generated structure from bson object (using schema )
void readDataFromBsonSchema( const ThriftSchema* schema, bson* obj,
    const string& structName,  ::apache::thrift::TBase* data );


/// Determine the bson type corresponding to the thrift internal type
int getBsonType( int thriftType );
/// Get string name of bson type
string getBsonTypeName( int bsonType );

/// Determine the default value corresponding to the thrift internal type
string getDefValue( int thriftType, const string& defval );
/// Determine the default value corresponding to the bson internal type
string getDefValueBson( int bsonType, const string& defval );

/// Definition of one field in thrift structure
/// [id:] [requiredness] type FieldName [=default] [,|;]
struct ThriftFieldDef
{
    int fId;             ///< Key id
    string fName;        ///< Field Name
    vector<int> fTypeId; ///< Type or all "typeId"+"type"+"elemTypeId"+"elemTypeId"  - all levels
    int fRequired;       ///< A requiredness attribute (REQURED_FIELD)
    string  fDefault;    ///< Default value
    string  insertedDefault;    ///< Default value from editor
    string  className;   ///< "class": Struct or enum name for corresponding field type
    string  fDoc;        ///< "doc" - Comment string
    double minval;
    double maxval;
};

/// Thrift structs definition
class ThriftStructDef
{
    string name;      ///< Name of strucure
    bool isException; ///< True if exeption structure
    bool isUnion;     ///< True if union
    string  sDoc;     ///< Comment
//    vector<ThriftFieldDef> fields;   ///< List of fields

    map<int, int> id2index;
    map<string, int> name2index;

    vector<string>  keyIdSelect;
    vector<string>  uniqueList;

    void readSchema( const char *bsondata );
    void read_type_spec( const char *bsondata,
       ThriftFieldDef& value, const char* keyspec, const string& typeID);

public:

    vector<ThriftFieldDef> fields;   ///< List of fields
    static  ThriftFieldDef emptyfield;

    /// Constructor - read information from json/bson schema
    ThriftStructDef( const char *bsondata )
    {
       readSchema( bsondata );
    }

    /// Get name of structure
    const char* getName() const
    { return name.c_str();}

    /// Get description of structure
    const char* getComment() const
    { return sDoc.c_str();}

    /// Test structure is Union
    bool testUnion() const
    { return isUnion;}

    /// Get number of base select fields
    uint getSelectSize() const
    { return keyIdSelect.size(); }


    /// Return short record key field name
    const char* keyFldIdName(uint i) const
    {
        if( i < keyIdSelect.size() )
          return keyIdSelect[i].c_str();
        else
          return 0;
    }

    /// Get unique fields list
    const vector<string>& getUniqueList()
    {
      return uniqueList;
    }


    /// Get field definition by index
    ThriftFieldDef& getSchema( int fId )
    {
        auto it = id2index.find( fId);
        if( it == id2index.end() )
         return emptyfield;
        else
         return fields[it->second];
    }

    /// Get field definition by name
    ThriftFieldDef& getSchema( const string& fName )
    {
        auto it = name2index.find( fName );
        if( it == name2index.end() )
         return emptyfield;
        else
         return fields[it->second];
    }


    /// Get field definition by name
    ThriftFieldDef* getFieldDef( const string& fName )
    {
        auto it = name2index.find( fName );
        if( it == name2index.end() )
         return nullptr;
        else
         return &fields[it->second];
    }

    /// Get field definition by index
    ThriftFieldDef* getFieldDef( int fId )
    {
        auto it = id2index.find( fId);
        if( it == id2index.end() )
         return nullptr;
        else
         return &fields[it->second];
    }

};

/// Thrift enums definition
class ThriftEnumDef
{
    string name;      ///< Name of enum
    string  sDoc;     ///< Comment
    map<string, int> name2index; ///< Members of enum
    map<string, string> name2doc; ///< Comments of enum

    void readEnum( const char *bsondata );

public:

    static  int emptyenum;

    /// ThriftEnumDef - read information from json/bson schema
    ThriftEnumDef( const char *bsondata )
    {
       readEnum( bsondata );
    }

    /// Get name of enum
    const char* getName()
    { return name.c_str();}

    /// Get description of structure
    const char* getComment()
    { return sDoc.c_str();}

    /// Get enum value by name
    int& getId( const string& lName )
    {
        auto it = name2index.find( lName );
        if( it == name2index.end() )
         return emptyenum;
        else
         return it->second;
    }

    /// Get enum comment by name
    string getDoc( const string& lName )
    {
        auto it = name2doc.find( lName );
        if( it == name2doc.end() )
         return "";
        else
         return it->second;
    }


    /// Get enum names list
    vector<string> getNamesList()
    {
      vector<string> members;
      auto it = name2index.begin();
      while( it != name2index.end() )
        members.push_back(it++->first);
      return members;
    }

    /// Get enum name by value
    string getNamebyId( int id )
    {
      auto it = name2index.begin();
      while( it != name2index.end() )
      {
         if( it->second == id )
          return it->first;
         it++;
      }
      return "";
    }

};

/// Thrift schema definition
class ThriftSchema
{
    map<string, string> files; ///< List of readed files (name, doc)

    map<string, unique_ptr<ThriftStructDef>> structs; ///< List of all structures
    map<string, unique_ptr<ThriftEnumDef>> enums;     ///< List of all enums

    map<string, string> vertexes; ///< Map of readed vertexes (_label, schema name)
    map<string, string> edges;   ///< Map of readed edges (_label, schema name)

    void readSchema( const char *bsondata );
    void setTypeMap();
    void getSelectLevel( queue<int>& idLst,
     ThriftStructDef* strDef, string& to_select_name, string& to_select_doc ) const;

public:

static map<string, int> name_to_thrift_types;

    /// ThriftSchema - constructor
    ThriftSchema(  )
    {
       setTypeMap();
    }

    /// Read thrift schema from json file fileName
    void addSchemaFile( const char *fileName );


    /// Get description of structure
    ThriftStructDef* getStruct(const string& structName) const
    {
        auto it = structs.find( structName );
        if( it == structs.end() )
         return nullptr;
        else
         return it->second.get();
    }

    /// Get description of enum
    ThriftEnumDef* getEnum(const string& enumName) const
    {
        auto it = enums.find( enumName );
        if( it == enums.end() )
         return nullptr;
        else
         return it->second.get();
    }

    /// Get structs names list
    vector<string> getStructsList( bool withDoc ) const;


    /// Get enums names list
    vector<string> getEnumsList() const
    {
      vector<string> members;
      auto it = enums.begin();
      while( it != enums.end() )
        members.push_back(it++->first);
      return members;
    }

    /// Get vertex names list
    vector<string> getVertexesList() const
    {
      vector<string> members;
      auto it = vertexes.begin();
      while( it != vertexes.end() )
        members.push_back(it++->second);
      return members;
    }

    /// Get vertex schema name from label data
    string getVertexName(const string& _label) const
    {
        auto it = vertexes.find( _label );
        if( it == vertexes.end() )
         return "";
        else
         return it->second;
    }

    /// Get edge names list
    vector<string> getEdgesList() const
    {
      vector<string> members;
      auto it = edges.begin();
      while( it != edges.end() )
        members.push_back(it++->second);
      return members;
    }

    /// Get edge schema name from label data
    string getEdgeName(const string& _label) const
    {
        auto it = edges.find( _label );
        if( it == edges.end() )
         return "";
        else
         return it->second;
    }

    /// Clear all internal data
    void clearAll()
    {
      files.clear();
      structs.clear();
      enums.clear();
      vertexes.clear();
      edges.clear();
    }

    /// Convert to_select Ids to Names
    void getSelectData( const string& to_select_fld,  ThriftStructDef* strDef,
         string& to_select_name, string& to_select_doc )  const;

};

} // namespace bsonio

#endif // THRIFTSCHEMA_H
