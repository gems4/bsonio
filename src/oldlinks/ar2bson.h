//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file ar2bson.h
/// Declarations of classes for read/write scalar/array from/to
/// bson, json and YAML formats
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef AR2BSON_H
#define AR2BSON_H

#include <vector>
#include "ar2base.h"
#include "v_json.h"
#include "v_yaml.h"
#include "v_xml.h"
//using namespace pugi;

namespace bsonio {

/// Print scalar/array to bson data
class TArray2Bson: public  TArray2Base
{
    bson *obj;

public:

    /// Constructor
    TArray2Bson( bson *aobj ): obj(aobj)
    { }

    /// Add new object to bson format
    void addStartObj( const char* name )
    {
       bson_append_start_object( obj, name );
    }

    /// Add end of new object to bson format
    void addEndObj()
    {
        bson_append_finish_object( obj );
    }

    /// Add comment to bson format
    void addComment( const char*  )
    { }

    /// Write value to bson format
    template <class T>
      void value_to( const char* name, const T& value )
      {
         if( isEMPTY(value) )
             bson_append_null( obj, name); // null
         else
             bson_append_double( obj, name, value );
      }

    /// Add bson to bson format
    void bson_to( const char* name, bson* objdata )
     {
          bson_append_bson( obj, name, objdata  );
     }

    /// Write string to bson format
    void string_to( const char* name, const string& value )
        {
           if( value.empty() )
               bson_append_null( obj, name); // null
           else
               bson_append_string( obj, name, value.c_str() );
        }

    /// Write null to bson format
    void writeNull( const char* name, const string& /*_comment*/ = "" )
        {
               bson_append_null( obj, name); // null
        }


    /// Write array to bson format
    template <class T>
       void array_to( const char* name, const T* parr, long int sizeN, long int sizeM  )
       {
           curArray = name;
           if( !parr )
               bson_append_null( obj, name ); // null
            else
            {
               int ndx_;
               string key;
               bson_append_start_array(obj, name );
               for(int i=0; i < sizeN; i++ )
                for(int j=0; j < sizeM; j++ )
                 {
                    ndx_ = i * sizeM + j;
                    key = std::to_string(ndx_);
                    //value_to_bson( obj, key.c_str(), parr[ndx_] );
                    value_to( key.c_str(), parr[ndx_] );
                }
               bson_append_finish_array(obj);
            }
         }

       /// Write array to bson format
       template <class T>
          void array_to( const char* name, const vector<T>& parr  )
          {
              curArray = name;
              if( parr.empty() )
                  bson_append_null( obj, name ); // null
               else
               {
                  string key;
                  bson_append_start_array(obj, name );
                  for(int i=0; i < parr.size(); i++ )
                    {
                       key = std::to_string(i);
                       value_to( key.c_str(), parr[i] );
                   }
                  bson_append_finish_array(obj);
               }
            }
};

/// Read fields from bson data to structure outField
 class TBson2Array : public  TArray2Base
 {
     const char *obj;

     /// Read value from bson format
     template <class T>
      void value_from_( const char *obj_, const char *name, T& value, const T& defValue )
      {
         if( !bson_find_value( obj_, name, value ) )
              value = defValue;  //getEMPTY(value);
      }

 public:

    /// Constructor
    TBson2Array( const char *aobj ): obj( aobj )
    { }

    const char * getStartObj( const char* name )
    {
       return bson_find_object_null( obj, name );
    }

    /// Read value from bson format
    template <class T>
     void value_from( const char *name, T& value, const T& defValue )
     {
        value_from_( obj, name, value, defValue );
     }

     /// Read bson from bson format
      void bson_from( const char *name, bson* newbs  )
      {
          const char *objdata = bson_find_object_null( obj, name );
          if( objdata  )
           bson_create_from_buffer2( newbs, objdata, bson_size2(objdata) );
      }

     /// Read string from bson format
      void string_from( const char *name, string& value )
      {
         if( !bson_find_string( obj, name, value ) )
              setEMPTY(value);
      }

    /// Read array from bson format
    template <class T>
      void array_from( const char *name, T* parr, long int sizeN, long int sizeM, const T& defValue  )
      {
        curArray = name;
        int ndx_;
        string key;
        const char *arrobj  = bson_find_array_null(  obj, name );
        if( !arrobj )  // null object
           return;
        for(int i=0; i < sizeN; i++ )
            for(int j=0; j < sizeM; j++ )
            {
                ndx_ = i * sizeM + j;
                key = std::to_string(ndx_);
                value_from_( arrobj,  key.c_str(), parr[ndx_], defValue );
                //value_from( arrobj, key.c_str(), parr[ndx_], defValue );
             }
      }

      /// Read array from bson format
      template <class T>
        void array_from( const char *name, vector<T>& parr, const T& defValue  )
        {
          curArray = name;
          parr.clear();
          const char* key;
          const char *arrobj  = bson_find_array_null(  obj, name );
          if( !arrobj )  // null object
             return;

          bson_iterator itobj;
          bson_iterator_from_buffer(&itobj, arrobj);
          while( bson_iterator_next(&itobj) )
          {
              T value;
              key = bson_iterator_key(&itobj);
              value_from_( arrobj,  key, value, defValue );
              parr.push_back( value);
          }
        }

 };

 template<> void TArray2Bson::value_to( const char* name, const char& value );
 template<> void TArray2Bson::value_to( const char* name, const bool& value );
 template <> void TArray2Bson::value_to( const char* name,const string& value );

 template<> void TArray2Bson::array_to( const char* name, const char* parr, long int sizeN, long int sizeM);
 template<> void TBson2Array::value_from_( const char *obj_,const char* name, char& value, const char& defValue );
 template<> void TBson2Array::value_from_( const char *obj_,const char* name, string& value, const string& defValue );
 template<> void TBson2Array::array_from( const char* name, char* parr, long int sizeN, long int sizeM, const char& );

 //---------------------------------------------------------------------------------

 /// Print scalar/array to json data
 class TArray2Json: public  TArray2Base
 {
     string& ff;
     string depth = "";//"\t";
     vector<size_t> posSt;

     /// Write value to json format
     template <class T>
        void _value_to_json( string& outff, const T& value )
        {
           if( isEMPTY(value) )
               outff += "null, ";
           else
               outff += to_string(value) + ", ";
        }

     /// Delete last character ch in string from the end
     void deleteLast( string& strg, char ch, size_t start    );

  public:

     /// Constructor
     TArray2Json( string& aff ): ff( aff )
     { }

     void addDepth()
     {  depth.push_back('\t');
        posSt.push_back( ff.length() );
     }

     void delDepth()
     {
         posSt.pop_back();
         depth.pop_back();
     }

     /// Add new object to json format
     void addStartObj( const char* name )
     {
         ff += depth;
         if(name)
              ff +=  "\"" + string( name ) + "\": {\n";
         else
              ff +=  "{";
         addDepth();
     }

     /// Add end of new object to json format
     void addEndObj( bool last=false )
     {
         deleteLast( ff, ',', *(posSt.end()-1)  ); //??
         delDepth();
         if(*(ff.end()-1) != '\n' ) ff += '\n';
         ff += depth + "}";
         if( !last )
           ff += ",\n";
     }

     /// Add comment to json format
     void addComment( const char* _comment )
     {
         ff += depth +  "#" + _comment + "\n";
     }

     /// Writes value as field to a json text file.
     /// "name" : value, # _comment
     template <class T>
     void writeField( const char *name, const T& value, const string& _comment = "" )
     {
       curArray = name;
       // write name
         ff += depth;
         ff +=  "\"" + string( name ) + "\": ";
       // write value
         _value_to_json( ff, value );
       // write comment
          if( !_comment.empty() )
              ff +=  "#  " + _comment;
          ff += "\n";
      }

     /// Writes bson to json format.
     void writeBson( const char *name, const char *aobj, const string& _comment="" );

     /// Writes string to json format.
     void writeString( const char *name, const string& value, const string& _comment = "" );

     /// Writes string to json format.
     void writeNull( const char *name, const string& _comment = "" );

      /// Writes array as field to a text file.
      /// # _comment
      /// "name" : [ arr[0], ..., arr[sizeN*sizeM-1]]
      /// \param sizeM - Setup number of elements in line
      template <class T>
       void writeArray( const char *name, const T* arr,
                        long int sizeN, long int sizeM, const string& _comment="" )
         {
            curArray = name;
            if( !_comment.empty() )
             ff += depth + "# "+_comment + "\n";

            if( !arr )
            {
              ff += depth + "\"" + name + "\": null,\n";
            }
            else
            {
               // write name
               ff += depth +  "\"" + name + "\": [\n";
               addDepth();

               for(int i=0; i < sizeN; i++ )
               {
                  ff += depth;
                  for(long int j=0; j < sizeM; j++ )
                     _value_to_json( ff, arr[ i * sizeM + j] );
                  ff += "\n";
                }
               deleteLast( ff, ',', *(posSt.end()-1) );

               delDepth();
               ff += depth + "],\n";
            }
       }

       /// Writes array as field to a text file.
       /// # _comment
       /// "name" : [ arr[0], ..., arr[size]]
       template <class T>
        void writeArray( const char *name, const vector<T>& arr, const string& _comment="" )
          {
             curArray = name;
             if( !_comment.empty() )
              ff += depth + "# "+_comment + "\n";

             if( arr.empty() )
             {
               ff += depth + "\"" + name + "\": null,\n";
             }
             else
             {
                // write name
                ff += depth +  "\"" + name + "\": [\n";
                addDepth();

                ff += depth;
                for(int i=0; i < arr.size(); i++ )
                      _value_to_json( ff, arr[i] );
                ff += "\n";
                deleteLast( ff, ',', *(posSt.end()-1) );
                delDepth();
                ff += depth + "],\n";
             }
        }
 };

 template<> void TArray2Json::_value_to_json( string& outff, const float& value );
 template<> void TArray2Json::_value_to_json( string& outff, const double& value );
 template<> void TArray2Json::_value_to_json( string& outff, const char& val );
 template<> void TArray2Json::_value_to_json( string& outff, const bool& val );
 template<> void TArray2Json::_value_to_json( string& outff, const string& val );
 template<> void TArray2Json::writeArray(  const char *name,
        const char* arr, long int sizeN, long int sizeM, const string& _comment );

 //------------------------------------------------------------

 /// Print fields to YAML format data
 class TArray2YAML: public  TArray2Base
 {
    YAML::Emitter& out;

    /// Write value to yaml format
    template <class T>
      void _value_to_yaml( const T& value )
             {
                if( isEMPTY(value) )
                    out << YAML::_Null();
                else
                    out << value;
             }

 public:

     /// Constructor
     TArray2YAML( YAML::Emitter& aout ): out( aout )
     { }

     /// Add new object to YAML format
     void addStartObj( const char* name )
     {
         if( name )
         {
             out << YAML::Key << name;
             out << YAML::Value;
         }
         out << YAML::BeginMap;
     }

     /// Add end of new object to YAML format
     void addEndObj()
     {
          out << YAML::EndMap;
     }

     /// Add comment to YAML format
     void addComment( const char* _comment )
     {
         out << YAML::Comment( _comment );
     }

     /// Writes value  to yaml format.
     /// name :  value  # _comment
     template <class T>
       void writeField( const char *name,const T& value, const string& _comment =  "" )
       {
           curArray = name;
            out << YAML::Key << name << YAML::Value;
            _value_to_yaml(  value );
            if( !_comment.empty() )
              out << YAML::Comment( _comment );
       }

    /// Writes bson to yaml format.
    void writeBson( const char *name, const char *aobj, const string& _comment="" );

    /// Writes string to yaml format.
    void writeString( const char *name, const string& value, const string& _comment = "" );

    /// Writes string to yaml format.
    void writeNull( const char *name, const string& _comment = "" );

    /// Writes array to yaml format.
    template <class T>
      void writeArray( const char *name, const T* arr,
         long int sizeN, long int sizeM, const string& _comment="" )
        {
          curArray = name;
          out << YAML::Key << name;
          if( !_comment.empty() )
              out << YAML::Comment( string(_comment) );
          out<< YAML::Value;

          if( !arr )
          {
             out << YAML::_Null();
           }
           else
            {
              out << YAML::BeginSeq;
              for(int i=0; i < sizeN; i++ )
               for(int j=0; j < sizeM; j++ )
                {
                   _value_to_yaml(  arr[i * sizeM + j] );
                }
              out << YAML::EndSeq;
            }
         }
      /// Writes array to yaml format.
      template <class T>
        void writeArray( const char *name, const vector<T>& arr, const string& _comment="" )
          {
            curArray = name;
            out << YAML::Key << name;
            if( !_comment.empty() )
                out << YAML::Comment( string(_comment) );
            out<< YAML::Value;

            if( arr.empty() )
            {
               out << YAML::_Null();
             }
             else
              {
                out << YAML::BeginSeq;
                for(int i=0; i < arr.size(); i++ )
                  {
                     _value_to_yaml(  arr[i] );
                  }
                out << YAML::EndSeq;
              }
           }

 };

 template<> void  TArray2YAML::_value_to_yaml(  const bool& value );
 template<> void TArray2YAML::writeArray( const char *name, const char* arr,
       long int sizeN, long int sizeM, const string& _commen );

 /// Print scalar/array to xml data
 class TArray2XML: public  TArray2Base
 {
     pugi::xml_document& xmldoc_;
     pugi::xml_node xmlnode_;

  public:

     /// Constructor
     TArray2XML( pugi::xml_document& doc ): xmldoc_( doc )
     {
        xml_node decl = xmldoc_.append_child(node_declaration);
        decl.append_attribute("version") = "1.0";
        decl.append_attribute("encoding") = "UTF-16";
        decl.append_attribute("standalone") = "yes";

        // define root node
        xmlnode_ = xmldoc_.append_child("node");
     }

     void addNextNode()
     {
         xmlnode_ = xmldoc_.child("node").append_child("element");
         //xmlnode_.append_child("element");
     }

     /// Add new object to xml format
     void addStartObj( const char* name )
     {
         xmlnode_ = xmlnode_.append_child(name);
     }

     /// Add end of new object to xml format
     void addEndObj()
     {
         xmlnode_ = xmlnode_.parent();
     }

     /// Add comment to xml format
     void addComment( const char* _comment )
     {
         xml_node comm = xmlnode_.append_child(node_comment);
         comm.set_value(_comment);
     }

     /// Writes value as field to a xml file.
     template <class T>
     void writeField( const char *name, const T& value, const string& _comment = "" )
     {
       curArray = name;
       // write comment
       if( !_comment.empty() )
           addComment( _comment.c_str() );
       // write name
       xml_node fld = xmlnode_.append_child(name);
       // write value
       string val = value2string( value );
       xml_node child = fld.append_child(node_pcdata);
       child.set_value(val.c_str());
      }

     /// Writes bson to json format.
     void writeBson( const char *name, const char *aobj, const string& _comment="" );

     /// Writes string to json format.
     void writeString( const char *name, const string& value, const string& _comment = "" );

     /// Writes string to json format.
     void writeNull( const char *name, const string& _comment = "" );

      /// Writes array as field to a text file.
      /// # _comment
      /// "name" : [ arr[0], ..., arr[sizeN*sizeM-1]]
      /// \param sizeM - Setup number of elements in line
      template <class T>
       void writeArray( const char *name, const T* arr,
                        long int sizeN, long int sizeM, const string& _comment="" )
         {
           curArray = name;
           // write comment
           if( !_comment.empty() )
               addComment( _comment.c_str() );
           // write name
           xml_node fld = xmlnode_.append_child(name);
           fld.append_attribute("isarray") = true;
           if( !arr )
            {
              fld.append_child(node_null);
            }
            else
            {
               // write value
               string val = "";
               for(int i=0; i < sizeN; i++ )
               {
                  for(long int j=0; j < sizeM; j++ )
                    val += value2string( arr[ i * sizeM + j] )+", ";
                  val += "\n";
                }

               size_t foundCh = val.find_last_of(',');
               if( foundCh != string::npos )
                 val.erase(foundCh, 1 );
               xml_node child = fld.append_child(node_pcdata);
               child.set_value(val.c_str());
            }
       }

       /// Writes array as field to a text file.
       /// # _comment
       /// "name" : [ arr[0], ..., arr[N]]
       template <class T>
        void writeArray( const char *name, const vector<T> arr, const string& _comment="" )
          {
            curArray = name;
            // write comment
            if( !_comment.empty() )
                addComment( _comment.c_str() );
            // write name
            xml_node fld = xmlnode_.append_child(name);
            fld.append_attribute("isarray") = true;
            if( arr.empty() )
             {
               fld.append_child(node_null);
             }
             else
             {
                // write value
                string val = "";
                for(int i=0; i < arr.size(); i++ )
                   val += value2string( arr[ i ] )+", ";
                val += "\n";

                size_t foundCh = val.find_last_of(',');
                if( foundCh != string::npos )
                  val.erase(foundCh, 1 );
                xml_node child = fld.append_child(node_pcdata);
                child.set_value(val.c_str());
             }
        }

 };

 template<> void TArray2XML::writeArray(  const char *name,
        const char* arr, long int sizeN, long int sizeM, const string& _comment );

} // namespace bsonio

#endif // AR2BSON_H
