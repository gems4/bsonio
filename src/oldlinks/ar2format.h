//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file ar2format.h
/// Declarations of classes for read/write scalar/array from/to txt format
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef AR2ARRAYS_H
#define AR2ARRAYS_H

#include <fstream>
#include "ar2base.h"
#include "v_json.h"

namespace bsonio {

/// Print scalar/array to txt format
class TArray2Txt: public  TArray2Base
 {
     fstream& ff;

     /// Writes value to a text file.
     template <class T>
       void writeValue( const T& val)
       {
           if( isEMPTY( val ) )
               ff << '`' << " ";
           else
               ff << val << " ";
       }

     /// Writes bson value to a text file.
     void writeValue( bson_type type, const bson_iterator *it, int l_size, bool isformat );

 public:

     /// Constructor
     TArray2Txt( fstream& fout ): ff( fout )
     { }

     /// Added comment line
     void addComment( const char* _comment )
     {
         ff <<  "\n# " << _comment;
     }

     /// Added any/free line
     void addLine( const char* _comment )
     {
         ff << "\n" << _comment;
     }

   void writeString( const string& name, const string& value, const string& _comment = "" )
   {
      curArray = name;
      if( !_comment.empty() )
            ff << "\n# " << _comment;
       ff << endl << "<" << name << ">  ";
       ff << "\"" << value  << "\"";
   }

     /// Writes value  to text format.
     ///# _comment
     /// <name>  value
     template <class T>
       void writeField( const string& name, const T& value, const string& _comment = "" )
     {
        curArray = name;
        // write comment
           if( !_comment.empty() )
              ff << "\n# " << _comment;
         // write name
           ff << endl << "<" << name << ">  ";
         // write value
           writeValue( value );
     }

    /// Writes array as field to a text file.
    /// # _comment
    /// <name> arr[0] ... arr[size-1]
    /// \param l_size - Setup number of elements in line
    template <class T, class S>
      void writeArray( const string& name, const T* arr,
         S size, S l_size = -1, const string& _comment = "" )
       {
          curArray = name;
          S sz = (l_size >= 0? l_size : 40);
          if( !_comment.empty() )
             ff <<  "\n# " << _comment;
          // write name
          ff << endl << "<" << name << ">" << endl;
          // write values (sz on line )
          for( S ii=0, jj=0; ii<size; ii++, jj++  )
             {
                if(jj == sz)
                { jj=0;  ff << endl;}
                writeValue(arr[ii]);
             }
       }

      /// Writes array as field to a text file.
      /// # _comment
      /// <name> arr[0] ... arr[size-1]
      /// \param l_size - Setup number of elements in line
      template <class T>
        void writeArray( const string& name, const vector<T>& arr,
           const string& _comment = "" )
         {
            curArray = name;
            if( !_comment.empty() )
               ff <<  "\n# " << _comment;
            // write name
            ff << endl << "<" << name << ">" << endl;
            // write values (sz on line )
            for( int ii=0; ii<arr.size(); ii++  )
                  writeValue(arr[ii]);
         }


      /// Writes selected elements from array to a text file.
     template <class T, class S>
      void writeArray( const char *name, const T*  arr, S size, long int* selAr,
             S nColumns=1L, S l_size=-1L )
      {
        if( !arr )
             return;

        curArray = name;
        S sz = (l_size >= 0? l_size : 40);

        // write name
        ff << endl << "<" << name << ">" << endl;
        // write values
        for( S ii=0, jj=0; ii<size; ii++  )
        {
            for(S cc=0; cc<nColumns; cc++ )
            {
                if(jj == sz)
                { jj=0;  ff << endl;}
                writeValue(arr[selAr[ii]*nColumns+cc]);
                jj++;
           }
       }
    }

   /// Write object from bson structure with name <name> to a text file
   /// \param l_size - Setup number of elements in line
      void writeArrayFromBson( const char *name,  bson* obj,
                               const string& _comment = "", int l_size=0, bool isformat = false );
 };

 /// Read scalar/array from txt format
  class TTxt2Array: public  TArray2Base
  {
      fstream& ff;

  protected:

     /// Reads value from a text file.
     template <class T>
       inline bool readValue(T& val)
       {
         char input;
         skipSpace();

         if( ff.eof() )
           return false;

         ff.get( input );
         if( input ==  '<' )  // next field
         {
            ff.putback(input);
            return false;
         }

         if( input ==  '`' )
             setEMPTY(val);
         else
            {
              ff.putback(input);
              ff >> val;
            }
         return true;
       }

       void addValueToBson( const char* name, bson* obj,
               long int type, double val, const string& format );
       void  skipSpace();

  public:

     /// Constructor
     TTxt2Array(  fstream& fin ): ff( fin )
     { }

     /// Read next <name> from txt file
     string findNext();
     /// Read next <name> from txt file and compare to name
     void readNext(const string&);

     /// Read String
     string readString();

     /// Read value from txt format.
     template <class T>
       void readField( const string& name,T& value )
       {
           //curArray = name; //setCurrentArray( name, size);
           readValue(value);
       }

     /// Reads array from a text file.
     template <class T>
       void readArray( const string& name, T* arr, long int size )
       {
        //curArray = name;
        for( long int ii=0; ii<size; ii++  )
            readValue(arr[ii]);
       }

       /// Reads array from a text file.
       template <class T>
         void readArray( const string& name, vector<T>& arr )
         {
          //curArray = name;
          T value;
          arr.clear();
          while( readValue(value) )
            arr.push_back(value);
         }

     /// Reads string array from a text file.
     void readArray( const string& name, char* arr, long int size, long int el_size );

     /// Read and add object with name to bson structure
     void readFormatArrayToBson( const char* name, bson* obj );

  };

  template<> void TArray2Txt::writeValue(const float& val);
  template<> void TArray2Txt::writeValue(const double& val);
  template<> void TArray2Txt::writeValue(const bool& val);
  template<> void TArray2Txt::writeValue( const char& val );
  template<> void TArray2Txt::writeValue( const string& val );
  template<>  void TArray2Txt::writeArray( const string& name, const char* arr,
           long size, long arr_siz, const string& _comment );

  template <> bool TTxt2Array::readValue( char& val);
  template <> bool TTxt2Array::readValue( bool& val);
  template <> bool TTxt2Array::readValue( string& val);
  template <> void TTxt2Array::readArray( const string& name, char* arr, long int size );


} // namespace bsonio

#endif // AR2ARRAYS_H
