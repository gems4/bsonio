//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file valsimpl.cpp
/// Implementation of classes TScalarVals, TArrVals, TFixStringVals,
/// TStringVals, TConstString - for linking user structures to internal service
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include "valsimpl.h"
#include "v_json.h"

namespace bsonio {

template<> void TArrVals<char>::from_text_file( TTxt2Array& fTxt, const char* name )
{
  fTxt.readArray( name,  parr, sizeN*sizeM );
}

// Write array to formated text file
template<> void TArrVals<char>::to_text_file( TArray2Txt& fTxt, const char* name,const string& _comments )
{
   fTxt.writeArray( name,  parr, sizeN*sizeM, 1L, _comments );
}

bool TStringVals::SetString(const string& s, size_t, size_t )
{
    long int l = s.length();
    if( (l > size || !parr ) && dynamic )
    {
      size = l;
      alloc();
    }
    if(!parr)
      return false;

    strncpy(parr, s.c_str(), size);
    if(dynamic)
       parr[size] = '\0';
    return true;
}


} // namespace bsonio
