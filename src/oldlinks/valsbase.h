//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file valsbase.h
/// Declarations of TValsBase interface for manipulation of
/// internal values
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TVALSBASE_H
#define TVALSBASE_H

#include <string>
using namespace std;

namespace bsonio {

class GemDataStream;
class TArray2Bson;
class TBson2Array;
class TArray2XML;
class TArray2YAML;
class TArray2Json;
class TArray2Txt;
class TTxt2Array;

class TValsBase
{
  public:

    TValsBase()
    {}

    virtual ~TValsBase()
    {}

    virtual long int cSize() const = 0;

    /// Alloc memory for array and set default values
    virtual void alloc() {}
    /// Free memory
    virtual void free() {}

    virtual long int GetN() const
    {  return 1;  }
    virtual long int GetM() const
    {  return 1;  }
    virtual const void * GetPtr() const = 0;

    virtual double Get(size_t n = 0, size_t m = 0) const = 0;
    virtual void Put(double v, size_t n = 0, size_t m = 0 ) = 0;

    virtual bool IsAny( size_t n = 0, size_t m = 0 ) const = 0;
    virtual bool IsEmpty( size_t n = 0, size_t m = 0 ) const = 0;

    virtual string GetString( size_t n = 0, size_t m = 0 ) const = 0;
    virtual bool SetString(const string& s, size_t n = 0, size_t m = 0 ) = 0;

    /// Write array to binary file
    virtual void to_file( GemDataStream& ff ) = 0;
    /// Read array from binary file
    virtual void from_file( GemDataStream& ff ) = 0;

    /// Write array to bson format
    virtual void to_bson( TArray2Bson& fbson, const char* name, const string& _comments  ) = 0;
    /// Read array from bson format
    virtual void from_bson( TBson2Array& fbson, const char* name ) = 0;

    /// Write array to json format
    virtual void to_bson( TArray2Json& fjson, const char* name, const string& _comments ) = 0;
    /// Write array to yaml format
    virtual void to_bson( TArray2YAML& fyaml, const char* name, const string& _comments ) = 0;
    /// Write array to XML format
    virtual void to_bson( TArray2XML& fyaml, const char* name, const string& _comments ) = 0;

    /// Write array to formated text file
    virtual void to_text_file( TArray2Txt& fTxt, const char* name, const string& _comments ) = 0;
    /// Read array from formated text file
    virtual void from_text_file( TTxt2Array& fTxt, const char* name ) = 0;


private:

// forbidding copying and assigning - it's dangerous!
    TValsBase (const TValsBase&);
    const TValsBase& operator= (const TValsBase&);

};

} // namespace bsonio

#endif // TVALSBASE_H
