//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file ar2bson.cpp
/// Implementation of classes for read/write scalar/array from/to
/// bson, json and YAML formats
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <cstring>
#include <iostream>
#include "ar2bson.h"

namespace bsonio {

// Write char value to bson format
template <>
void TArray2Bson::value_to( const char* name, const char& value )
{
   if( value == isEMPTY(value) )
       bson_append_null( obj, name); // null
   else
       bson_append_string( obj, name, string(1, value).c_str() );
}

// Write char value to bson format
template <>
void TArray2Bson::value_to( const char* name,const bool& value )
{
   bson_append_bool( obj, name, value );
}

// Write string value to bson format
template <>
void TArray2Bson::value_to( const char* name,const string& value )
{
   bson_append_string( obj, name, value.c_str() );
}

// Read char value from bson format
template <>
void TBson2Array::value_from_( const char *obj_,const char *name, char& value, const char& defValue )
{
    string str;
    if( !bson_find_string( obj_, name, str ) )
         value = defValue;  //getEMPTY(value);
    else
         value = str[0];
}

// Read string value from bson format
template <>
void TBson2Array::value_from_( const char *obj_,const char *name,
                              string& value, const string& defValue )
{
    if( !bson_find_string( obj_, name, value ) )
         value = defValue;  //getEMPTY(value);
}


//Write char array to bson format
template<>
void TArray2Bson::array_to( const char* name, const char* parr, long int sizeN, long int sizeM)
{
    curArray = name;
    if( !parr || *parr == '\0' )
       bson_append_null( obj, name); // null
    else
    {
       bson_append_start_array(obj, name );
       for(int i=0; i < sizeN; i++ )
         {
            string key = std::to_string(i);
            string str = string( parr +(i*sizeM), 0, sizeM );
            int ret =  bson_append_string( obj, key.c_str(),  str.c_str());
            if( ret == BSON_ERROR )
                cout<< name << "___" << str.c_str() << endl;
        }
      bson_append_finish_array(obj);
   }
}

// Read char array from bson format
template<> void TBson2Array::array_from( const char *name,
        char* parr, long int sizeN, long int sizeM, const char& )
{
    curArray = name;
    const char *arr  = bson_find_array_null(  obj, name );
    if( !arr || !parr )  // null object
       return;
    for(int i=0; i < sizeN; i++ )
        {
          string key = std::to_string(i);
          string str;
          bson_find_string( arr, key.c_str(), str );
          fillValue( parr+(i*sizeM), ' ', sizeM );
          strncpy( parr+(i*sizeM), str.c_str(), sizeM );
         }
}

//----------------------------------------------------------

  // Delete last character in string from the end
  void TArray2Json::deleteLast( string& strg, char ch, size_t start  )
  {
   if( strg.empty() )
     return;
   size_t foundCh = strg.find_last_of(ch);

   size_t foundComment = strg.find_last_of('#');
   size_t lastComment = strg.find_first_of('\n', foundComment);

   while( foundCh > foundComment && foundCh < lastComment && foundCh != string::npos )
   {   foundCh = strg.find_last_of(ch, foundComment );
       foundComment = strg.find_last_of('#', foundComment );
       lastComment = strg.find_first_of('\n', foundComment);
   }

   if( foundCh != string::npos && foundCh > start )
     strg.erase(foundCh, 1 );

  }

// Write value to json format
   template <> void TArray2Json::_value_to_json( string& outff, const float& value )
   {
      if( isEMPTY(value) )
          outff += "null, ";
      else
      {   char vbuf[50];
          sprintf(vbuf, "%.*lg" , 7, value );
          outff +=  vbuf;
          //std::ostringstream sstream;
          //sstream << std::scientific <<  setprecision(6) << value;
          //outff += sstream.str();
          //outff += to_string(value);
          outff += ", ";
      }
   }

   template <> void TArray2Json::_value_to_json(string& outff, const double& value )
     {
           if( isEMPTY(value) )
               outff += "null, ";
           else
           {   char vbuf[50];
               sprintf(vbuf, "%.*lg" , 15, value );
               outff +=  vbuf;
               //std::ostringstream sstream;
               //sstream << std::scientific << setprecision(14) << value;
               //outff += sstream.str();
               outff+= ", ";
           }
     }

   // Write char value to file
  template<> void TArray2Json::_value_to_json( string& outff, const char& val )
  {
        outff += "\"";
        outff += val;
        outff += "\",";
  }

  // Write char value to file
 template<> void TArray2Json::_value_to_json( string& outff, const string& val )
 {
       outff += "\"";
       outff += val;
       outff += "\",";
 }

  // Write bool value to file
  template<> void TArray2Json::_value_to_json( string& outff, const bool& val )
  {
           if( val)
             outff += "true, ";
           else
             outff += "false, ";
  }

  template<> void TArray2Json::writeArray( const char *name,
    const char* arr, long int sizeN, long int sizeM, const string& _comment )
       {
            curArray = name;
            if( !_comment.empty() )
                ff += depth + "# "+_comment + "\n";

            if( !arr )
            {
              ff += depth + "\"" + name + "\": null,\n";
              return;
            }

            // write name
            ff +=  depth + "\"" + string(name) + "\": [\n";

            addDepth();
            for(int i=0; i < sizeN; i++ )
            {
               ff +=  depth;
               string str = string( arr+(i*sizeM), 0, sizeM );
               strip(str);
               ff  += "\"" + str + "\"";
               if( i< sizeN-1)
                  ff  += ",";
                ff += "\n";
            }
            delDepth();
            ff +=  depth + "], \n";
        }

  // Writes bson to json format.
  void TArray2Json::writeBson( const char *name, const char *aobj, const string& _comment )
  {
      curArray = name;

      if( !_comment.empty() )
         ff += depth + "# "+_comment + "\n";
      // write name
      ff += depth +  "\"" + name + "\": {\n";
      addDepth();
      stringstream os;
      ParserJson::bson_print_raw_txt( os, aobj, depth.length(), BSON_OBJECT);
      ff+=os.str();
      delDepth();
      ff += depth + "},\n";
  }

  // Write string to json format
  void TArray2Json::writeString( const char *name, const string& value, const string& _comment )
  {
    curArray = name;

    if( !_comment.empty() )
     ff += depth + "# " +_comment + "\n";

    if( value.empty() )
    {
      ff += depth + "\"" + name + "\": null,\n";
    }
    else
    {
       // write name
       ff += depth +  "\"" + name + "\": ";
       string str = value;
       strip(str);
       ff  += "\"" + str + "\"" + ",\n";
    }
 }

  // Write string to json format
  void TArray2Json::writeNull( const char *name, const string& _comment )
  {
    curArray = name;

    if( !_comment.empty() )
     ff += depth +"# "+ _comment + "\n";
    ff += depth + "\"" + name + "\": null,\n";
 }

//-----------------------------------------------

 template <>
   void  TArray2YAML::_value_to_yaml(  const bool& value )
   {
      out <<  (value ? "true" : "false");
   }

 template<>
   void TArray2YAML::writeArray( const char *name, const char* arr,
       long int sizeN, long int sizeM, const string& _comment )
   {
      curArray = name;
      // write name
      out << YAML::Key << name;
      if( !_comment.empty() )
        out << YAML::Comment(_comment );
      out << YAML::Value;

      if( !arr )
       {
         out << YAML::_Null();
         return;
       }
      out << YAML::BeginSeq;
      for(int i=0; i < sizeN; i++ )
      {
         string str = string( arr+(i*sizeM), 0, sizeM );
         strip(str);
         out << str;
      }
      out << YAML::EndSeq;
   }

   // Writes bson to yaml format.
   void TArray2YAML::writeBson( const char *name, const char *aobj, const string& _comment )
   {
       curArray = name;
       out << YAML::Key << name;
       if( !_comment.empty() )
           out << YAML::Comment( string(_comment) );
       out<< YAML::Value;
       out << YAML::BeginMap;
       ParserYAML::bson_emitter( out, aobj, BSON_OBJECT);
       out << YAML::EndMap;
   }

   // Writes string to yaml format.
   void TArray2YAML:: writeString( const char *name, const string& value, const string& _comment  )
    {
        curArray = name;
        out << YAML::Key << name;
        if( !_comment.empty() )
            out << YAML::Comment( string(_comment) );
        out<< YAML::Value;
        if( value.empty() )
        {
           out << YAML::_Null();
         }
         else
          {
            string str = value;
            strip(str);
            out << str;
          }
    }

   // Writes string to yaml format.
   void TArray2YAML:: writeNull( const char *name,  const string& _comment  )
    {
        curArray = name;
        out << YAML::Key << name;
        if( !_comment.empty() )
            out << YAML::Comment( string(_comment) );
        out<< YAML::Value << YAML::_Null();
    }


//--------------------------------------------------------

template<> void TArray2XML::writeArray( const char *name,
  const char* arr, long int sizeN, long int sizeM, const string& _comment )
 {
  curArray = name;
  // write comment
  if( !_comment.empty() )
      addComment( _comment.c_str() );
  // write name
  xml_node fld = xmlnode_.append_child(name);
  fld.append_attribute("isarray") = true;
  if( !arr )
   {
     fld.append_child(node_null);
   }
   else
   {
      // write value
      string val = "";
      for(int i=0; i < sizeN; i++ )
      {
         string str = string( arr+(i*sizeM), 0, sizeM );
         strip(str);
         //val  += "\"" + str + "\"";
         val  += str;
         if( i < sizeN-1)
            val  += ", ";
         // val += "\n";
      }
      xml_node child = fld.append_child(node_pcdata);
      child.set_value(val.c_str());
   }
}

// Writes bson to json format.
void TArray2XML::writeBson( const char *name, const char *aobj, const string& _comment )
{
    curArray = name;
    // write comment
    if( !_comment.empty() )
        addComment( _comment.c_str() );
    // write name
    xml_node fld = xmlnode_.append_child(name);
    ParserXML::bson_emitter( fld, aobj, BSON_OBJECT);
}

// Write string to json format
void TArray2XML::writeString( const char *name, const string& value, const string& _comment )
{
    curArray = name;
    // write comment
    if( !_comment.empty() )
        addComment( _comment.c_str() );
    // write name
    xml_node fld = xmlnode_.append_child(name);
    if( value.empty() )
    {
      fld.append_child(node_null);
    }
    else
    {
      string str = value;
      strip(str);
      //str  = "\"" + str + "\"";
      xml_node child = fld.append_child(node_pcdata);
      child.set_value(str.c_str());
   }
}

// Write string to json format
void TArray2XML::writeNull( const char *name, const string& _comment )
{
    curArray = name;
    // write comment
    if( !_comment.empty() )
        addComment( _comment.c_str() );
    // write name
    xml_node fld = xmlnode_.append_child(name);
    fld.append_child(node_null);
}

} // namespace bsonio

//-----------------------------------------------
