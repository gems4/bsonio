//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file n_object.h
/// Declarations of class TObject provides access to application data
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef N_OBJECT_H
#define N_OBJECT_H

#include <memory>
#include "valsimpl.h"
#include "valsstl.h"
#include "v_json.h"

namespace bsonio {

enum ObjType_
{ S_ = 0, I_ = -1, U_ = -2, L_ = -3, X_ = -4, F_ = -5, D_ = -6,
  C_ = -7, N_ = -8, A_ = -9, B_ = -10, H_ = -11, J_ = -12, N_TYPE_ = -13,
  Str_ = -14, Class_ =-15, Vector_ = -16
};

using  conditF = std::function<bool()>;

/**
    TObject provides GUI and DBMS access to application data
    and/or used for internal descriptions of output/input formats
    It can be any type, and in general is N*M matrix
*/
class TObject: public IsUnloadToBson
{
    static int useShortKeywds; ///< Internal flag for using short names (not 0 if short )

    string KeywordShort;   ///< Short name of field in structure
    string KeywordLong;    ///< Long name of field in structure
    short int Type;
    short int IndexationCode;
    string Descr;          ///< Description/comment of object

    TValsBase* pV;
    //unique_ptr<TValsBase> pV;

    // from struct outField - Internal descriptions of fields
    short int alws;        ///< 1 - Must be read, 0 - default values can be used
    short int readed;      ///< 0; set to 1 after reading the field from input file
    //short int indexation;  ///< 1 - static object; 0 - undefined; <0 type of indexation, >1 number of elements in array

    // internal data
    bool specialField = false;  ///< This field must be outputted separately
    conditF cndFunction;        ///< This field must be allocated if condition returrn true

//    TObject (const TObject& );	                    // forbidden
    const TObject& operator = (const TObject& );	// ----""---

protected:

    void check() const
    {
        bsonioErrIf( IsNull(), getKeywordShort(), "Access to null object");
    }
    void check_dim(int n, int m) const
    {
        check();
        bsonioErrIf( n >= getN() || m >= getM(), getKeywordShort(),
            "Cell index beyond object dimension");
    }
    void check_type(ObjType_ typ) const
    {
        bsonioErrIf(typ > 126 || typ < N_TYPE_, getKeywordShort(), "Invalid object type");
    }

    // Determines size of one cell ( using only from GemDataStream )
    size_t GetCellSize() const
    {
        if( IsNull() )
            return 0;
        return pV->cSize();
    }

public:

    TObject( const string& shname, const string& lname,
             char indexCode, const string& descr, long int aalws = 0 );
    /// Constructor from bson data
    TObject( const char* bsobj );
    virtual ~TObject ();
    bool operator == (const TObject& o) const
    {  return pV == o.pV;  }

    void toBson( bson *obj ) const;
    void fromBson( const char *obj );

    // New staff --------------------------------------------------

    /// Link array to object
    template <class T>
     void linkVal(  T* &ptr, long int& N, long int& M, bool adynamic = true,
                    T defV = static_cast<T>(0), long int v2line = -1L )
    {
      if( pV )
       delete pV;
      pV = new TArrVals<T>( ptr, N, M, adynamic, defV, v2line );

      Type = D_;
      // may be function to find old num tipes
      // I_ = -1, U_ = -2, L_ = -3, X_ = -4, F_ = -5, D_ = -6,
      // C_ = -7, N_ = -8, A_ = -9, B_ = -10, H_ = -11,
      // but impotant only for old DB
    }

     /// Link char array to object ( fixed strings array)
    void linkValChar( char* &ptr, long int& N, long int& M, long int aType,
               bool adynamic = true, const string& defV = " " )
    {
        if( pV )
         delete pV;
        pV = new TFixStringVals( ptr, N, M, aType, adynamic, defV );
        Type =  aType;
    }

    /// Link char array for text fields (S_)
    void linkValString( char* &ptr, long int& M, bool adynamic = true )
    {
        if( pV )
         delete pV;
        pV = new TStringVals( ptr, M, adynamic );
        Type = S_;
    }

    /// Link char array for text fields (S_)
    void linkConstString( char* ptr, long int M )
    {
        if( pV )
         delete pV;
        pV = new TConstString( ptr, M );
        Type = S_;
    }

    /// Link scalar value
    template <class T>
     void linkVal( T& value2ptr, T defV = static_cast<T>(0)  )
    {
      if( pV )
       delete pV;
      pV = new TScalarVals<T>( value2ptr, defV );
      Type = D_;
    }

     /// Link string value
     void linkVal( string& value2ptr  )
     {
       if( pV )
        delete pV;
       pV = new TStringStl( value2ptr );
       Type = Str_;
     }

     /// Link user class value
     template <class T>
      void linkClassVal( T& value2ptr  )
     {
       if( pV )
        delete pV;
       pV = new TClassVals<T>( value2ptr );
       Type = Class_;
     }

      /// Link user class value
      template <class T>
       void linkVal( vector<T>& value2ptr, T defV = static_cast<T>(0)   )
      {
        if( pV )
         delete pV;
        pV = new TVectorVals<T>( value2ptr, defV );
        Type = Vector_;
      }


    /// Link bson value
    void linkBsonVal()
     {
       if( pV )
        delete pV;
       pV = new TBsonVal;
       Type = J_;
     }

    /// Realloc memory
    bool alloc()
     {
       if( !pV )
          return false;
       pV->alloc();
       return true;
     }

     /// Free memory
     void free()
     {
       if( pV )
        pV->free();
     }

    //--- Selectors

    const char *getKeywordShort() const
    {
        return KeywordShort.c_str();
    }

    const char *getKeywordLong() const
    {
        return KeywordLong.c_str();
    }

    const char *getKeyword() const
    {
        if( useShortKeywds )
         return KeywordShort.c_str();
        else
         return KeywordLong.c_str();
    }

    long int getType() const
    {
        return Type;
    }
    char getIndexationCode() const
    {
        return IndexationCode;
    }

    long int getN() const
    {
        if( IsNull() )
          return 0;
        else
          return pV->GetN();
    }
    long int getM() const
    {
        if( IsNull() )
          return 0;
        else
          return pV->GetM();
    }
    bool IsNull() const
    {
        return ( pV == nullptr || !pV->GetPtr() );
    }

    /// Get object description for current line/column
    string getDescription(int n, int m);
    /// Get object name with indexes "Keywd[n][m]"
    string getFullName(int n, int m);
    /// Get object help link  with index "Keywd_line/column"
    string getHelpLink(int line, int column);

    //--- Value manipulation

    bool IsAny(int n, int m) const
    {
      check_dim( n, m);
      return pV->IsAny( n, m );
    }
    bool IsEmpty(int n, int m) const
    {
      check_dim( n,  m);
      return pV->IsEmpty( n, m );
    }

    /// Get cell of data object as double
    double Get(int n, int m)
    {
      check_dim(n,m);
      return pV->Get(n,m);
    }
    /// Get cell of data object as double
    double GetEmpty(int n, int m)
    {
      check_dim(n,m);
      if( pV->IsEmpty( n, m ))
            return DOUBLE_EMPTY;
        return pV->Get(n,m);
    }
    /// Put value to cell of data object as double
    void Put(double Value, int n, int m)
    {                    // truncated if overflowed !!!
       check_dim(n,m);
       pV->Put(Value, n, m );
    }

    /// Put cell of object to string.
    string GetString(int n = 0, int m = 0) const
    {
        check_dim(n, m);
        return pV->GetString( n, m);
    }
    /// Put cell of object to string (Empty string if null object).
    string GetStringEmpty( int n = 0, int m = 0) const
    {
        if( IsNull() )
          return string(S_EMPTY);
        check_dim(n, m);
        return pV->GetString( n, m);
    }
    /// Puts string into data object cell
    bool SetString(const char *vbuf, int n, int m)
    {
        check_dim( n, m);
        bsonioErrIf( !vbuf, getKeywordShort(),"TObject:W06 Cannot set empty string to object");
        if( !pV->SetString(vbuf, n, m ) )
            return false;
        return true;
    }

    // old DB only into inheritor for GEMS
    //size_t lenDB() const;
    //size_t toDB(GemDataStream& f);
    //size_t ofDB(GemDataStream& f);// moved into void checkUtf8(); // for old records

    // EJDB left old format into inheritor for GEMS
    virtual void toBsonObject( bson *obj );
    virtual void fromBsonObject( const char *obj );

    // New - output to formats

    /// Write array to formated text file
     void to_text_file( TArray2Txt& fTxt,  bool with_comments, bool brief_mode )
     {
         string _comment="";
         if( pV  && (!brief_mode || alws == 1) )
         {
             if( with_comments )
                _comment = Descr;
            pV->to_text_file( fTxt, getKeyword(),  _comment );
         }
     }
   /// Read array from formated text file
     void from_text_file( TTxt2Array& fTxt )
     {
       if( pV )
         pV->from_text_file( fTxt, getKeyword() );
       readed = 1;
     }

   /// Write array to binary file
    void to_file( GemDataStream& ff )
    {
      if( pV )
        pV->to_file(ff);
    }
    /// Read array from binary file
    void from_file( GemDataStream& ff )
    {
      if( pV )
          pV->from_file(ff);
      readed = 1;
    }

    template< class T>
    void to_bson( T& fbson, bool with_comments, bool brief_mode  )
    {
       string _comment="";
       if( pV && ( !brief_mode || alws == 1 ) )
       {
           if( with_comments )
              _comment = Descr;
           pV->to_bson( fbson, getKeyword(), _comment );
       }
    }

    /// Read array from bson format
    void from_bson( TBson2Array& fbson, const char* key )
    {
        if( pV )
           pV->from_bson( fbson, key );
        readed = 1;
    }

 // special flags

    /// Mark this field to output separately
    void setSpecialField()
    {
       specialField = true;
    }
    /// This field must be outputed separately
    bool isSpecialField() const
    {
       return specialField;
    }

    /// Set condition of allocation field
    void setCondition(conditF acond )
    {
       cndFunction = acond;
    }
    /// Condition of allocation field
    bool testCondition() const
    {
      return  cndFunction();
    }

    /// Clear flag "readed"
    void resetRead()
    {
      readed = 0;
    }
    /// Test do not read always field
    bool testRead() const
    {
     return( alws==1 && readed != 1 );
    }
    /// Set field to must/mustn`t be read
    void setAlways( bool isAlws )
    {
       alws = isAlws;
    }
};

} // namespace bsonio

#endif // N_OBJECT_H
