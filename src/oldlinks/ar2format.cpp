//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file ar2format.cpp
/// Implementation of classes for read/write scalar/array from/to txt format
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <cstring>
#include <iomanip>
#include "ar2format.h"

namespace bsonio {

// Write float value to file
template<> void TArray2Txt::writeValue(const float& val)
       {
         if( isEMPTY(val)/*IsFloatEmpty( val )*/)
           ff << CHAR_EMPTY << " ";
         else
          ff << setprecision(7) << val << " ";
       }

// commented out for gemsfitt windows build
// Write double value to file
template<> void TArray2Txt::writeValue(const double& val)
       {
         if( isEMPTY(val) /*IsDoubleEmpty( val )*/)
           ff << CHAR_EMPTY << " ";
         else
           ff << setprecision(15) << val << " ";
       }

template<> void TArray2Txt::writeValue(const bool& val)
       {
         if( val)
           ff << "true" << " ";
         else
           ff << "false" << " ";
       }


 // Write char value to file
 template<> void TArray2Txt::writeValue( const char& val )
    {
           ff << "\'" << val << "\'" << " ";
     }

// Write char value to file
template<> void TArray2Txt::writeValue( const string& val )
   {
          ff << "\'" << val.c_str() << "\'" << " ";
    }

   // If the first parameter is given as NULL then the char array
   // will be printed as a comment
 template<>  void TArray2Txt::writeArray( const string& name, const char* arr,
            long size, long arr_siz, const string& _comment )
   {
    bool isComment = false;
    curArray = name;

    if( !_comment.empty() )
       ff <<  "\n# " << _comment;

    if( !name.empty() )
        ff << endl << "<" << name << ">" << endl;
    else
    { ff << endl << "#  ";
      isComment = true;
    }

    for( long int ii=0, jj=0; ii<size; ii++, jj++  )
    {
       if(jj == 40 )
       { jj=0;  ff << endl;
         if(isComment)
             ff << "#  ";
       }
       string str = string( arr +(ii*arr_siz), 0, arr_siz );

       strip(str);
       ff  << "\'" << str.c_str() << "\'" << " ";
    }
   }


  void TArray2Txt::writeValue( bson_type type, const bson_iterator *it, int l_size, bool isformat )
  {
    switch(type)
    {
          // impotant datatypes
          case BSON_NULL:
               ff << "null";
              break;
          case BSON_BOOL:
               ff << ( bson_iterator_bool(it) ?  "true": "false");
               break;
          case BSON_INT:
               ff << bson_iterator_int(it);
               break;
          case BSON_LONG:
               ff << bson_iterator_long(it);
               break;
          case BSON_DOUBLE:
               ff << setprecision(15) << bson_iterator_double(it);
               break;
          case BSON_STRING:
               { string val = bson_iterator_string(it);
                 if( isformat && (val[0] == 'F' || val[0] == 'L' || val[0] == 'R')   )
                      ff << val;
                 else
                    ff << "\'" << val << "\'";
                 break;
                }
          // main constructions
          case BSON_OBJECT:
          {
            // find ptype
            bson_iterator itt;
            bson_type typet;
            typet =  bson_find_from_buffer(&itt, bson_iterator_value(it), "ptype" );
            if( typet == BSON_STRING )
            {  ff << bson_iterator_string(&itt);
               typet =  bson_find_from_buffer(&itt, bson_iterator_value(it), "spec" );
               if( typet == BSON_DOUBLE )
                 ff << setprecision(15) << bson_iterator_double(&itt);
               else if( typet == BSON_INT )
                   ff << bson_iterator_int(&itt);
                 else
               {
                   // record to json string
                   ParserJson pars;
                   string jsonstr;
                   pars.printBsonObjectToJson( jsonstr, bson_iterator_value(&itt) );
                   ff <<  jsonstr.c_str();
               }
             }
            else
            {   ff << "\n \'";
                // record to json string
                ParserJson pars;
                string jsonstr;
                pars.printBsonObjectToJson( jsonstr, bson_iterator_value(it) );
               ff <<  jsonstr.c_str() << "\'";
            }
             break;
          }
         case BSON_ARRAY:
         {   bson_iterator iter;
            //const char *key;
             int ii=0;

            ff << "\n";
            bson_iterator_from_buffer(&iter, bson_iterator_value(it));
            while (bson_iterator_next(&iter))
            {
                bson_type tp = bson_iterator_type(&iter);
                if (tp == 0)
                  break;

                if( ii == l_size )
                { ii = 0;  ff << endl; }
                    ii++;
                writeValue( tp, &iter, l_size,  isformat ); // BSON_ARRAY );
                ff << " ";
            }
          }
            break;

          // not used in GEMS data types
          default:
                     break;
     }
}


 void TArray2Txt::writeArrayFromBson( const char *name,  bson* obj,
                                      const string& _comment, int l_size, bool isformat )
 {
    if( !_comment.empty() )
        ff <<  endl << _comment;

    // find bson field
    bson_iterator it;
    bson_type type;
    type =  bson_find_from_buffer(&it, obj->data, name );
    if( type == BSON_EOO )
      return;

    int sz = (l_size > 0? l_size : 40);
    ff << endl << "<" << name << ">" << " ";
    writeValue( type, &it, sz, isformat);
}


//=============================================================
   template <>
      bool TTxt2Array::readValue( bool& val)
   {
     char input;
     skipSpace();

     if( ff.eof() )
       return false;

     ff.get( input );
     if( input ==  '<' )  // next field
     {
        ff.putback(input);
        return false;
     }
     if( input ==  't' )
     {
         val = true;
         ff.get( input ); //r
         ff.get( input ); //u
         ff.get( input ); //e
     }
     else
        {
          val = false;
          ff.get( input ); //a
          ff.get( input ); //l
          ff.get( input ); //s
          ff.get( input ); //e
        }
     return true;
    }

   template <>
         bool TTxt2Array::readValue( char& val)
      {
        char input;
        skipSpace();

        if( ff.eof() )
          return false;

        ff.get( input );     // skip first '\''
        if( input ==  '<' )  // next field
        {
           ff.putback(input);
           return false;
        }
        ff.get( val );
        ff.get( input ); // skip last '\''
        return true;
      }

   template <>
       bool TTxt2Array::readValue( string& val)
  {
    char input;
    skipSpace();

    if( ff.eof() )
      return false;

    ff.get( input );    // skip first '\"'
    if( input ==  '<' )  // next field
    {
       ff.putback(input);
       return false;
    }
    std::getline( ff, val, '\"');
    return true;
  }

         // Reads array from a text file.
    template <>
        void TTxt2Array::readArray( const string& name, char* arr, long int size )
        {
           readArray( name, arr, size, 1 );
        }

      // skip  ' ',  '\n', '\t' and comments (from '#' to end of line)
    void  TTxt2Array::skipSpace()
      {
        char input;
        if( ff.eof() )
           return;
        ff.get( input );
        while( input == '#' || input == ' ' ||
              input == '\n' || input == '\t')
       {
         if( input == '#' )
          do{
               ff.get( input );
            }while( input != '\n' && input != '\0' && !ff.eof());
         if( input == '\0' || ff.eof() )
           return;
         ff.get( input );
        }
       ff.putback(input);
      }

  void TTxt2Array::readArray( const string& /*name*/, char* arr, long int size, long int el_size )
    {
     char ch, buf[300];
     //curArray = name; //setCurrentArray( name, size);

      for( long int ii=0; ii<size; ii++  )
      {
       skipSpace();
       ff.get(ch);  //  skip first '\''
    //   while( ff.good() && ch != '\'' )
    //       ff.get(ch);
       ff.getline( buf, min(el_size+1, 300L), '\'');
       copyValues( arr +(ii*el_size), buf, el_size );
      }

    }

  // Read String
  string TTxt2Array::readString()
  {
      char ch, buf[500];
      skipSpace();
      ff.get(ch);  //  skip first '\"'
      ff.getline( buf, 500, '\"');
      return string(buf);
  }

  string TTxt2Array::findNext()
  {
     skipSpace();
     if( ff.eof() )
       return "";

     char buf[101];
     ff.getline( buf, 100, '>');
     // ErrorIf( *buf != '<' )
     curArray = buf+1;
     return curArray.c_str();
  }

  void TTxt2Array::readNext(const string& name)
  {
    findNext();
    if( curArray !=  name )
    { string errmsg = name+ " - no data where expected.";
      bsonioErr( "Txt format file read error", errmsg, curArray );
    }
  }

} // namespace bsonio
