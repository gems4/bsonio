//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file valsimpl.h
/// Declarations&Implementation of classes TScalarVals, TArrVals, TFixStringVals,
/// TStringVals, TConstString - for linking user structures to internal service
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef VALSIMPL_H
#define VALSIMPL_H

//   Templates is defining implementation
// of TValBase interface for manipulation of
// calculation values

#include <cstring>
#include "valsbase.h"
#include "gdatastream.h"
#include "ar2format.h"
#include "ar2bson.h"

namespace bsonio {

/// Class for Internal descriptions of scalars
/// double, float, int, long, char, ...
template <class T>
class TScalarVals: public TValsBase
{
  T& value;
  T defValue;            ///< Default value after alloc

  public:

  /// Constructor
    TScalarVals( T& value2ptr, T defV = static_cast<T>(0)  ):
        value(value2ptr), defValue(defV)
    { }

    ~TScalarVals()
    {
      free();
    }

    long int cSize() const
    {
        return sizeof(T);
    }
    const void * GetPtr() const
    { return &value; }

    double Get(size_t n = 0, size_t m = 0) const
    {
        return TArray2Base::value2double(value);
    }
    void Put(double v, size_t n = 0, size_t m = 0 )
    {
      TArray2Base::double2value( v, value );
      // old?   parr[n*sizeM+m] = ( fabs(v) <= ANY() ) ? T(v/*+.5*/) : EMPTY();
    }

    bool IsAny( size_t n = 0, size_t m = 0 ) const
    {
        return TArray2Base::isANY( value );
    }
    bool IsEmpty( size_t n = 0, size_t m = 0 ) const
    {
        return TArray2Base::isEMPTY( value );
    }

    string GetString( size_t n = 0, size_t m = 0 ) const
    {
       return TArray2Base::value2string( value );
    }
    bool SetString(const string& s, size_t n = 0, size_t m = 0 )
    {
      return TArray2Base::string2value( s, value );
    }

    /// Write array to formated text file
    void to_text_file( TArray2Txt& fTxt, const char* name,const string& _comments )
    {
       fTxt.writeField( name,  value, _comments );
    }
    /// Read array from formated text file
    void from_text_file( TTxt2Array& fTxt, const char* name )
    {
      fTxt.readField( name,  value );
    }

    /// Write value to binary file
     void to_file( GemDataStream& ff )
     {
       ff << value;
     }
   /// Read value from binary file
     void from_file( GemDataStream& ff )
     {
       ff >> value;
     }

   /// Write array to bson format
   void to_bson( TArray2Bson& fbson, const char* name, const string&  )
   {
      fbson.value_to( name,  value );
   }
   /// Read array from bson format
   void from_bson( TBson2Array& fbson, const char* name )
   {
       fbson.value_from( name, value, defValue);
   }

   /// Write array to json format
   void to_bson( TArray2Json& fjson, const char* name,const string& _comments )
   {
      fjson.writeField( name,  value, _comments );
   }
   /// Write array to yaml format
   void to_bson(  TArray2YAML& fyaml, const char* name,const string& _comments  )
   {
      fyaml.writeField( name,  value, _comments );
   }
   /// Write array to xml format
   void to_bson(  TArray2XML& fyaml, const char* name,const string& _comments  )
   {
      fyaml.writeField( name,  value, _comments );
   }

};

/// Class for Internal descriptions of array
/// double, float, int, long, char, ...
template <class T>
class TArrVals : public TValsBase
{
  T* & parr;
  long int& sizeN;
  long int& sizeM;

  bool dynamic;          ///< This is dynamic array
  long int values2line;  ///< Number of values on one line - set up to sizeM
  T defValue;            ///< Default value after alloc
  long int allocN;       ///< internal allocated memory N
  long int allocM;       ///< internal allocated memory M

  public:

    /// Constructor
    TArrVals( T* &ptr, long int& N, long int& M,
              bool adynamic = true, T defV = static_cast<T>(0),
              long int v2line = -1L ):
      parr(ptr), sizeN(N), sizeM( M ), dynamic(adynamic),
      values2line(v2line), defValue( defV ), allocN(0), allocM(0)
    { }

    ~TArrVals()
    {
      free();
    }

    long int cSize() const
    {
        return sizeof(T);
    }

    long int GetN() const
    {  return sizeN;  }
    long int GetM() const
    {  return sizeM;  }
    const void * GetPtr() const
    { return parr; }

    double Get(size_t n = 0, size_t m = 0) const
    {
        return TArray2Base::value2double(parr[n*sizeM+m]);
    }
    void Put(double v, size_t n = 0, size_t m = 0 )
    {
      TArray2Base::double2value( v, parr[n*sizeM+m] );
      //  parr[n*sizeM+m] = static_cast<T>(v);
      // old?   parr[n*sizeM+m] = ( fabs(v) <= ANY() ) ? T(v/*+.5*/) : EMPTY();
    }

    bool IsAny( size_t n = 0, size_t m = 0 ) const
    {
        return TArray2Base::isANY( parr[n*sizeM+m] );
    }
    bool IsEmpty( size_t n = 0, size_t m = 0 ) const
    {
        return TArray2Base::isEMPTY( parr[n*sizeM+m] );
    }

    string GetString( size_t n = 0, size_t m = 0 ) const
    {
       return TArray2Base::value2string( parr[n*sizeM+m] );
    }
    bool SetString(const string& s, size_t n = 0, size_t m = 0 )
    {
      return TArray2Base::string2value( s, parr[n*sizeM+m] );
    }

    /// Write array to formated text file
    void to_text_file( TArray2Txt& fTxt, const char* name,const string& _comments )
    {
       if( values2line >= 0 )
           values2line = sizeM;
       fTxt.writeArray( name,  parr, sizeN*sizeM, values2line, _comments );
    }
    /// Read array from formated text file
    void from_text_file( TTxt2Array& fTxt, const char* name )
    {
      fTxt.readArray( name,  parr, sizeN*sizeM );
    }

   /// Write array to binary file
   void to_file( GemDataStream& ff )
   {
     ff.writeArray( parr, sizeN*sizeM );
   }
   /// Read array from binary file
   void from_file( GemDataStream& ff )
   {
     ff.readArray( parr, sizeN*sizeM );
   }

   /// Write array to bson format
   void to_bson( TArray2Bson& fbson, const char* name, const string&   )
   {
      fbson.array_to( name,  parr, sizeN, sizeM );
   }
   /// Read array from bson format
   void from_bson( TBson2Array& fbson, const char* name )
   {
       fbson.array_from( name, parr, sizeN, sizeM, defValue);
   }

   /// Write array to json format
   void to_bson( TArray2Json& fjson, const char* name,const string& _comments )
   {
      fjson.writeArray( name,  parr, sizeN, sizeM, _comments );
   }
   /// Write array to yaml format
   void to_bson(  TArray2YAML& fyaml, const char* name,const string& _comments  )
   {
      fyaml.writeArray( name,  parr, sizeN, sizeM, _comments );
   }
   /// Write array to xml format
   void to_bson(  TArray2XML& fyaml, const char* name,const string& _comments  )
   {
      fyaml.writeArray( name,  parr, sizeN, sizeM, _comments );
   }

   /// Alloc memory for array and set default values
   void alloc()
   {
       if( dynamic )
       {
         if( sizeN && sizeM )
         {
           T* oldArr =  parr;
           parr = new T[sizeN*sizeM ];

           for( int i=0; i<sizeN; i++ )
               for( int j=0; j<sizeM; j++ )
                  if( oldArr && i < allocN && j < allocM )
                   parr[i*sizeM+j] = oldArr[i*allocM+j];
                  else
                   parr[i*sizeM+j] = defValue;

           allocN = sizeN;
           allocM = sizeM;
           if(oldArr) delete[] oldArr;
         }
         else
          free();
       }
   }

   /// Free memory
   void free()
   {
       if( dynamic && parr )
       {
           delete[]   parr;
           parr = 0;
           allocN = allocM  = 0;
       }
   }

};

template<> void TArrVals<char>::from_text_file( TTxt2Array& fTxt, const char* name );
template<> void TArrVals<char>::to_text_file( TArray2Txt& fTxt, const char* name,const string& _comments );

/// Type-specific implemetations of the functions and classes
/// case when 0 < Type < 127
class TFixStringVals : public TValsBase
{
          char* & parr;
          long int& sizeN;
          long int& sizeM;
          long int len;          ///< Type or length of strings into array

          bool dynamic;          ///< This is dynamic array
          //long int values2line;  ///< Number of values on one line - set up to sizeM
          string defValue;            ///< Default value after alloc
          long int allocN;       ///< internal allocated memory N
          long int allocM;       ///< internal allocated memory M

          string valstr( size_t n, size_t m ) const
          {
              return string( parr+((n*sizeM+m)*len), 0, len);
          }

          public:

            /// Constructor
            TFixStringVals( char* &ptr, long int& N, long int& M, long int aType,
                      bool adynamic = true, const string& defV = " " ):
              parr(ptr), sizeN(N), sizeM( M ), len(aType), dynamic(adynamic),
              defValue( defV ), allocN(0), allocM(0)
            { }

            ~TFixStringVals()
            {
              free();
            }

            long int cSize() const
            {
                return len;
            }

            long int GetN() const
            {  return sizeN;  }
            long int GetM() const
            {  return sizeM;  }
            const void * GetPtr() const
            { return parr; }

            double Get(size_t , size_t ) const
            {
                return 0.;
            }
            void Put(double , size_t , size_t  )
            { }

            bool IsAny( size_t n = 0, size_t m = 0 ) const
            {
                return TArray2Base::isANY( valstr( n, m ) );
            }
            bool IsEmpty( size_t n = 0, size_t m = 0 ) const
            {
                return TArray2Base::isEMPTY( valstr( n, m ) );
            }

            string GetString( size_t n = 0, size_t m = 0 ) const
            {
               return valstr( n, m );
            }
            bool SetString(const string& s, size_t n = 0, size_t m = 0 )
            {
              strncpy(parr+((n*sizeM+m)*len), s.c_str(), len);
              return true;
            }

            /// Write array to formated text file
            void to_text_file( TArray2Txt& fTxt, const char* name,const string& _comments )
            {
              fTxt.writeArray( name,  parr, sizeN*sizeM, len, _comments );
            }
            /// Read array from formated text file
            void from_text_file( TTxt2Array& fTxt, const char* name )
            {
              fTxt.readArray( name,  parr, sizeN*sizeM, len );
            }

           /// Write array to binary file
           void to_file( GemDataStream& ff )
           {
             ff.writeArray( parr, sizeN*sizeM*len );
           }
           /// Read array from binary file
           void from_file( GemDataStream& ff )
           {
             ff.readArray( parr, sizeN*sizeM*len );
           }

           /// Write array to bson format
           void to_bson( TArray2Bson& fbson, const char* name, const string&  )
           {
              fbson.array_to( name,  parr, sizeN*sizeM, len );
           }
           /// Read array from bson format
           void from_bson( TBson2Array& fbson, const char* name )
           {
               fbson.array_from( name, parr, sizeN*sizeM, len, defValue[0]);
           }

           /// Write array to json format
           void to_bson( TArray2Json& fjson, const char* name, const string& _comments )
           {
              fjson.writeArray( name,  parr, sizeN*sizeM, len, _comments );
           }
           /// Write array to yaml format
           void to_bson(  TArray2YAML& fyaml, const char* name,const string& _comments  )
           {
              fyaml.writeArray( name,  parr, sizeN*sizeM, len, _comments );
           }
           /// Write array to yaml format
           void to_bson(  TArray2XML& fyaml, const char* name,const string& _comments  )
           {
              fyaml.writeArray( name,  parr, sizeN*sizeM, len, _comments );
           }

           /// Alloc memory for array and set default values
           void alloc()
           {
               if( dynamic )
               {
                 if( sizeN && sizeM )
                 {
                     char* oldArr =  parr;
                     parr = new char[sizeN*sizeM*len ];

                     for( int i=0; i<sizeN; i++ )
                         for( int j=0; j<sizeM; j++ )
                            if( oldArr &&  i < allocN && j < allocM )
                             strncpy(parr+((i*sizeM+j)*len),oldArr+(i*allocM+j),len);
                            else
                             strncpy(parr+((i*sizeM+j)*len), defValue.c_str(), len);

                     allocN = sizeN;
                     allocM = sizeM;
                     if(oldArr) delete[] oldArr;
                 }
                 else
                   free();
               }
           }

           /// Free memory
           void free()
           {
               if( dynamic && parr )
               {
                   delete[]   parr;
                   parr = 0;
                   allocN = allocM = 0;
               }
           }
};


/// Type-specific implemetations of the functions and classes
/// case Type == S_
class TStringVals : public TValsBase
{
          char* & parr;
          long int& size;
          bool dynamic;          ///< This is dynamic array
          //string defValue;            ///< Default value after alloc
          long int allocSize;       ///< internal allocated memory N

          public:

            /// Constructor
          TStringVals( char* &ptr, long int& M, bool adynamic = true ):
              parr(ptr), size( M ), dynamic(adynamic), allocSize(0)
            { }

            ~TStringVals()
            {
              free();
            }

            long int cSize() const
            {
                return size;
            }

            long int GetN() const
            {  return 1;  }
            long int GetM() const
            {  return size;  }
            const void * GetPtr() const
            { return parr; }

            double Get(size_t , size_t ) const
            {
                return 0.;
            }
            void Put(double , size_t , size_t  )
            { }

            bool IsAny( size_t , size_t ) const
            {
                return TArray2Base::isANY( string(parr) );
            }
            bool IsEmpty( size_t, size_t ) const
            {
                return TArray2Base::isEMPTY( string(parr) );
            }

            string GetString( size_t, size_t ) const
            {
               return(!*parr) ? S_EMPTY: string(parr, 0, size);
            }

            bool SetString(const string& s, size_t n=0, size_t m=0);

            /// Write array to formated text file
            void to_text_file( TArray2Txt& fTxt, const char* name,const string& _comments )
            {
              // ??? may be more than one line
              fTxt.writeArray( name,  parr, 1L, size, _comments );
            }
            /// Read array from formated text file
            void from_text_file( TTxt2Array& fTxt, const char* name )
            {
              // ??? may be more than one line and internal "\""
              fTxt.readArray( name,  parr, 1L, size ); // ???
            }

           /// Write array to binary file
           void to_file( GemDataStream& ff )
           {
             ff.writeArray( parr, size );
           }
           /// Read array from binary file
           void from_file( GemDataStream& ff )
           {
             ff.readArray( parr, size );
           }

           /// Write array to bson format
           void to_bson( TArray2Bson& fbson, const char* name, const string&   )
           {
              fbson.string_to( name,  parr );
           }
           /// Read array from bson format
           void from_bson( TBson2Array& fbson, const char* name )
           {
               string sval;
               fbson.string_from( name, sval);
               SetString( sval );
           }

           /// Write array to json format
           void to_bson( TArray2Json& fjson, const char* name,const string& _comments )
           {
               //fjson.writeString( name,  string((char*)parr, 0, size), _comments );
               fjson.writeString( name,  string((char*)parr), _comments );
           }
           /// Write array to yaml format
           void to_bson(  TArray2YAML& fyaml, const char* name,const string& _comments  )
           {
              fyaml.writeString( name,  string(parr, 0, size), _comments );
           }
           /// Write array to xml format
           void to_bson(  TArray2XML& fyaml, const char* name,const string& _comments  )
           {
              fyaml.writeString( name,  string(parr, 0, size), _comments );
           }

           /// Alloc memory for array and set default values
           void alloc()
           {
               if( dynamic )
               {
                 if( size )
                 {
                   char *oldArr = parr;
                   parr = new char[size+1];
                   if( oldArr )
                     strncpy(parr, oldArr, min(size, allocSize));
                   else
                     strncpy(parr, S_EMPTY, size);
                   allocSize = size;
                   delete oldArr;
                 }
                 else
                   free();
               }
           }

           /// Free memory
           void free()
           {
               if( dynamic && parr )
               {
                   delete[]   parr;
                   parr = 0;
                   allocSize = 0;
               }
           }

};

/// Type-specific implemetations of the functions and classes
/// case Type == S_ (fixed strings )
class TConstString : public TValsBase
{
          char* parr;
          long int size;

          public:

            /// Constructor
          TConstString( char* ptr, long int M ):
              parr(ptr), size( M )
            {}

            ~TConstString()
            {
              free();
            }

            long int cSize() const
            {
                return size;
            }

            long int GetN() const
            {  return 1;  }
            long int GetM() const
            {  return size;  }
            const void * GetPtr() const
            { return parr; }

            double Get(size_t , size_t ) const
            {
                return 0.;
            }
            void Put(double , size_t , size_t  )
            { }

            bool IsAny( size_t , size_t ) const
            {
                return TArray2Base::isANY( string(parr) );
            }
            bool IsEmpty( size_t, size_t ) const
            {
                return TArray2Base::isEMPTY( string(parr) );
            }

            string GetString( size_t, size_t ) const
            {
               return(!*parr) ? S_EMPTY: string(parr, 0, size);
            }

            bool SetString(const string& s, size_t =0, size_t =0)
            {
                if(!parr)
                  return false;
                strncpy(parr, s.c_str(), size);
                return true;
            }

            /// Write array to formated text file
            void to_text_file( TArray2Txt& fTxt, const char* name,const string& _comments )
            {
              // ??? may be more than one line
              fTxt.writeArray( name,  parr, 1L, size, _comments );
            }
            /// Read array from formated text file
            void from_text_file( TTxt2Array& fTxt, const char* name )
            {
              // ??? may be more than one line and internal "\""
              fTxt.readArray( name,  parr, 1L, size ); // ???
            }

           /// Write array to binary file
           void to_file( GemDataStream& ff )
           {
             ff.writeArray( parr, size );
           }
           /// Read array from binary file
           void from_file( GemDataStream& ff )
           {
             ff.readArray( parr, size );
           }

           /// Write array to bson format
           void to_bson( TArray2Bson& fbson, const char* name, const string&   )
           {
              fbson.string_to( name,  parr );
           }
           /// Read array from bson format
           void from_bson( TBson2Array& fbson, const char* name )
           {
               string sval;
               fbson.string_from( name, sval);
               SetString( sval );
           }

           /// Write array to json format
           void to_bson( TArray2Json& fjson, const char* name,const string& _comments )
           {
               //fjson.writeString( name,  string((char*)parr, 0, size), _comments );
               fjson.writeString( name,  string((char*)parr), _comments );
           }
           /// Write array to yaml format
           void to_bson(  TArray2YAML& fyaml, const char* name,const string& _comments  )
           {
              fyaml.writeString( name,  string(parr, 0, size), _comments );
           }
           /// Write array to xml format
           void to_bson(  TArray2XML& fyaml, const char* name,const string& _comments  )
           {
              fyaml.writeString( name,  string(parr, 0, size), _comments );
           }

           /// Alloc memory for array and set default values
           void alloc()
           { }

           /// Free memory
           void free()
           { }

};


} // namespace bsonio

#endif // VALSIMPL_H
