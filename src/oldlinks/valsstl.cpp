//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file valsstl.cpp
/// Implementation of class TBsonVal, TStringStl, TClassVals,
/// TVectorVals - for linking user structures to internal service
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include "valsstl.h"
#include "v_json.h"

namespace bsonio {


string TBsonVal::GetString( size_t , size_t  ) const
{
    ParserJson pars;
    string jsonstr;
    pars.printBsonObjectToJson( jsonstr, bcobj.data );
    return jsonstr;
}

bool TBsonVal::SetString(const string& s, size_t , size_t  )
{
    string objStr = s;
    ParserJson parserJson;
    parserJson.setJsonText( objStr.substr( objStr.find_first_of('{')+1 ));
    bson_destroy(&bcobj);
    bson_init( &bcobj );
    parserJson.parseObject( &bcobj );
    bson_finish( &bcobj );
    return true;
}

void TBsonVal::to_file(GemDataStream& /*s*/)
{
  bsonioErr( "Bson object", "Invalid object type for old DataBase");
  //s.writeArray(bcobj.data, size );
}

void TBsonVal::from_file(GemDataStream& /*s*/ )
{
   bsonioErr( "Bson object", "Invalid object type for old DataBase");
   //if( bcobj.data )
   //  s.readArray( bcobj.data, size );
}

} // namespace bsonio

