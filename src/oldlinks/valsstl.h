//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file valsstl.h
/// Declarations&Implementation of classes TBsonVal, TStringStl, TClassVals,
/// TVectorVals - for linking user structures to internal service
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef VALSSTL_H
#define VALSSTL_H

//   Templates is defining implementation
// of TValBase interface for manipulation of
// calculation values

#include <cstring>
#include "valsbase.h"
#include "gdatastream.h"
#include "ar2format.h"
#include "ar2bson.h"

namespace bsonio {


/// Type-specific implemetations of the functions and classes
/// string type
class TStringStl : public TValsBase
{
  string& parr;

  public:

  /// Constructor
  TStringStl( string& ptr ):parr(ptr) { }
  ~TStringStl() { }

  long int cSize() const
  { return parr.size(); }
  long int GetN() const
  {  return 1;  }
  long int GetM() const
  {  return parr.size(); }
  const void * GetPtr() const
  { return parr.c_str(); }

   double Get(size_t , size_t ) const
   {   return TArray2Base::value2double( parr );  }
   void Put(double value, size_t , size_t  )
   { parr = TArray2Base::value2string( value ); }

   bool IsAny( size_t , size_t ) const
   {  return TArray2Base::isANY( parr );  }
   bool IsEmpty( size_t, size_t ) const
   {  return TArray2Base::isEMPTY( parr ); }

   string GetString( size_t, size_t ) const
   {  return parr;  }
   bool SetString(const string& s, size_t =0, size_t =0)
   { parr = s;  return true; }

   /// Write array to formated text file
   void to_text_file( TArray2Txt& fTxt, const char* name,const string& _comments )
   { fTxt.writeString( name,  parr, _comments );  }
   /// Read array from formated text file
   void from_text_file( TTxt2Array& fTxt, const char* /*name*/ )
   {  parr = fTxt.readString(); }

   /// Write array to binary file
   void to_file( GemDataStream& ff )
   { ff << parr;  }
   /// Read array from binary file
   void from_file( GemDataStream& ff )
   { ff >> parr;  }

   /// Write array to bson format
   void to_bson( TArray2Bson& fbson, const char* name, const string&   )
   { fbson.string_to( name,  parr );  }
   /// Read array from bson format
   void from_bson( TBson2Array& fbson, const char* name )
   { fbson.string_from( name, parr);  }

   /// Write array to json format
   void to_bson( TArray2Json& fjson, const char* name,const string& _comments )
   { fjson.writeString( name,  parr, _comments );  }
   /// Write array to yaml format
   void to_bson(  TArray2YAML& fyaml, const char* name,const string& _comments  )
   { fyaml.writeString( name,  parr, _comments );  }
   /// Write array to xml format
   void to_bson(  TArray2XML& fxml, const char* name,const string& _comments  )
   {  fxml.writeString( name,  parr, _comments );  }

   /// Alloc memory for array and set default values
   void alloc() {}

    /// Free memory
    void free() {}
};

/// Class for Internal descriptions of other class
template <class T>
class TClassVals: public TValsBase
{
  T& value;
  public:

  /// Constructor
  TClassVals( T& value2ptr): value(value2ptr)
  {}

  ~TClassVals() { }

    long int cSize() const
    { return sizeof(T);  }
    const void * GetPtr() const
    { return &value; }
    double Get(size_t n = 0, size_t m = 0) const
    {
     return value.Get();
    }
    void Put(double v, size_t n = 0, size_t m = 0 )
    {
      value.Put(v);
    }
    bool IsAny( size_t n = 0, size_t m = 0 ) const
    {
        return value.isANY();
    }
    bool IsEmpty( size_t n = 0, size_t m = 0 ) const
    {
        return value.isEMPTY();
    }

    string GetString( size_t n = 0, size_t m = 0 ) const
    {
      return value.GetString();
    }
    bool SetString(const string& s, size_t n = 0, size_t m = 0 )
    {
      return value.SetString( s );
    }

    /// Write array to formated text file
    void to_text_file( TArray2Txt& fTxt, const char* name,const string& _comments )
    {
       value.to_text_file( fTxt, name, _comments );
    }
    /// Read array from formated text file
    void from_text_file( TTxt2Array& fTxt, const char* name )
    {
      value.from_text_file( fTxt, name );
    }

    /// Write value to binary file
    void to_file( GemDataStream& ff )
    {
      value.to_file( ff );
    }
   /// Read value from binary file
   void from_file( GemDataStream& ff )
   {
     value.from_file( ff );
   }

   /// Write array to bson format
   void to_bson( TArray2Bson& fbson, const char* name, const string& _comments )
   {
      value.to_bson( fbson, name, _comments );
   }
   /// Read array from bson format
   void from_bson( TBson2Array& fbson, const char* name )
   {
     value.from_bson( fbson, name );
   }

   /// Write array to json format
   void to_bson( TArray2Json& fjson, const char* name,const string& _comments )
   {
     value.to_bson( fjson, name, _comments );
   }
   /// Write array to yaml format
   void to_bson(  TArray2YAML& fyaml, const char* name,const string& _comments  )
   {
      value.to_bson( fyaml, name, _comments );
   }
   /// Write array to xml format
   void to_bson(  TArray2XML& fxml, const char* name,const string& _comments  )
   {
      value.to_bson( fxml, name, _comments );
   }

};


/// Class for Internal descriptions of bson data
class TBsonVal : public TValsBase
{
    bson bcobj;

public:

    TBsonVal()
    {
      bcobj.data = 0;
      bcobj.errstr = 0;
    }

    ~TBsonVal()
    {
       bson_destroy(&bcobj);
    }

    long int cSize() const
    {
        bsonioErr( "Bson object", "Invalid object type for old DataBase");
        return bson_size(&bcobj);
    }

    const void * GetPtr() const
    { return &bcobj; }

    double Get(size_t  = 0, size_t = 0) const
    { return 0.;  }
    void Put(double , size_t  = 0, size_t  = 0 )
    { }

    bool IsAny( size_t  = 0, size_t  = 0 ) const
    {  return false;     }
    bool IsEmpty( size_t  = 0, size_t  = 0 ) const
    {  return (bcobj.data==0); }

    // in json format
    string GetString( size_t n = 0, size_t m = 0 ) const ;
    bool SetString(const string& s, size_t n = 0, size_t m = 0 );

    /// Write array to formated text file
    void to_text_file( TArray2Txt& fTxt, const char* name, const string& _comments )
    {
      bsonioErr( "Bson object", "Invalid object type to write object to formated text file");
      fTxt.writeArrayFromBson( name,  &bcobj, _comments );
    }
    /// Read array from formated text file
    void from_text_file( TTxt2Array& /*fTxt*/, const char* /*name*/ )
    {
      bsonioErr( "Bson object", "Invalid object type to read object from formated text file");
    }

   /// Write array to binary file
   void to_file( GemDataStream& ff );
   /// Read array from binary file
   void from_file( GemDataStream& ff );

   /// Write array to bson format
   void to_bson( TArray2Bson& fbson, const char* name, const string&   )
   {
     fbson.bson_to( name,  &bcobj );
   }
   /// Read array from bson format
   void from_bson( TBson2Array& fbson, const char* name )
   {
      bson_destroy(&bcobj);
      fbson.bson_from( name, &bcobj );
   }

   /// Write array to json format
   void to_bson( TArray2Json& fjson, const char* name,const string& _comments )
   {
      fjson.writeBson( name,  bcobj.data, _comments );
   }
   /// Write array to yaml format
   void to_bson(  TArray2YAML& fyaml, const char* name,const string& _comments  )
   {
      fyaml.writeBson( name,  bcobj.data, _comments );
   }
   /// Write array to xml format
   void to_bson(  TArray2XML& fyaml, const char* name,const string& _comments  )
   {
      fyaml.writeBson( name,  bcobj.data, _comments );
   }

 };

/// Class for Internal descriptions of vector
/// double, float, int, long, char, string ...
template <class T>
class TVectorVals : public TValsBase
{
  vector<T>& parr;
  T defValue;            ///< Default value after alloc

  public:

    /// Constructor
    TVectorVals( vector<T>&ptr, T defV = static_cast<T>(0)):
      parr(ptr), defValue( defV )  { }

    ~TVectorVals()
    { free(); }

    long int cSize() const
    {  return sizeof(T);  }

    long int GetN() const
    {  return parr.size();  }
    long int GetM() const
    {  return 1;  }
    const void * GetPtr() const
    { return 0; } //???

    double Get(size_t n = 0, size_t m = 0) const
    {
      return TArray2Base::value2double(parr[n]);
    }
    void Put(double v, size_t n = 0, size_t m = 0 )
    {
       TArray2Base::double2value( v,  parr[n] );
    }

    bool IsAny( size_t n = 0, size_t m = 0 ) const
    {
        return TArray2Base::isANY( parr[n] );
    }
    bool IsEmpty( size_t n = 0, size_t m = 0 ) const
    {
        return TArray2Base::isEMPTY( parr[n] );
    }

    string GetString( size_t n = 0, size_t m = 0 ) const
    {
       return TArray2Base::value2string( parr[n] );
    }
    bool SetString(const string& s, size_t n = 0, size_t m = 0 )
    {
      return TArray2Base::string2value( s, parr[n] );
    }

    /// Write array to formated text file
    void to_text_file( TArray2Txt& fTxt, const char* name,const string& _comments )
    {
       fTxt.writeArray( name,  parr, _comments );
    }
    /// Read array from formated text file
    void from_text_file( TTxt2Array& fTxt, const char* name )
    {
      fTxt.readArray( name,  parr  );
    }

   /// Write array to binary file
   void to_file( GemDataStream& ff )
   {
     ff.writeArray( parr );
   }
   /// Read array from binary file
   void from_file( GemDataStream& ff )
   {
     ff.readArray( parr );
   }

   /// Write array to bson format
   void to_bson( TArray2Bson& fbson, const char* name, const string&   )
   {
      fbson.array_to( name,  parr );
   }
   /// Read array from bson format
   void from_bson( TBson2Array& fbson, const char* name )
   {
       fbson.array_from( name, parr, defValue);
   }

   /// Write array to json format
   void to_bson( TArray2Json& fjson, const char* name,const string& _comments )
   {
      fjson.writeArray( name,  parr, _comments );
   }
   /// Write array to yaml format
   void to_bson(  TArray2YAML& fyaml, const char* name,const string& _comments  )
   {
      fyaml.writeArray( name,  parr, _comments );
   }
   /// Write array to xml format
   void to_bson(  TArray2XML& fyaml, const char* name,const string& _comments  )
   {
      fyaml.writeArray( name,  parr, _comments );
   }

   /// Alloc memory for array and set default values
   void alloc() { }

   /// Free memory
   void free()
   {
     parr.clear();
   }

};


} // namespace bsonio

#endif // VALSSTL_H
