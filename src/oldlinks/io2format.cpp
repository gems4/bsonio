//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file io2format.cpp
/// Implementation of IO2FormatBase - Base class for read/write
/// internal structure to files
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include "io2format.h"

namespace bsonio {

//  internal values
long int const1 = 1L;
long int const8 = 8L;
long int const3 = 3L;
long int const2 = 2L;
long int const16 = 16L;
long int const4 = 4L;
long int const6 = 6L;

// Define internal search map
void IO2FormatBase::setupMap()
{
    key2IndexMap.clear();
    int ii=0;
    for( auto itr = startObj;
        itr != endObj; ++itr, ++ii )
    {
       key2IndexMap.insert( pair< string, int>( itr->getKeywordShort(), ii));
       key2IndexMap.insert( pair< string, int>( itr->getKeywordLong(), ii));
    }
}


// Alloc memory for arrays and set default values
void IO2FormatBase::reallocStruct()
{
    setConstValues();
    for( auto itr = startObj; itr != endObj; ++itr )
    {
       if( itr->testCondition() )
          itr->alloc();
       else
          itr->free();
    }
}

// Alloc memory for arrays and set default values
void IO2FormatBase::freeStruct()
{
    for( auto itr = startObj; itr != endObj; ++itr )
     itr->free();
}

// Reset readed flag
void IO2FormatBase::resetRead()
{
    for( auto itr = startObj; itr != endObj; ++itr )
     itr->resetRead();
}

// Test fields must be read from strucrure
void IO2FormatBase::testRead(objIter last )
{
  string ret = "";
  for( auto itr = startObj; itr != last; ++itr )
  {
   if( itr->testRead() )
   {
      if( !ret.empty() )
              ret += ", ";
      ret += itr->getKeyword();
   }
  }
  if( !ret.empty() )
  {   ret += " - fields must be read from ";
     ret += structureName;
     ret += " structure";
     bsonioErr( structureName, ret);
  }
}

//--------------------------------------------------

void IO2FormatBase::FromFormatFile( const char* fname, long int  type_f )
{
    switch( type_f )
    {
     case ft_binary:
        {  GemDataStream f_ch(fname, ios::in|ios::binary);
           from_file(f_ch);
           break;
         }
     case ft_text:
        {  from_text_file(fname);
           break;
        }
     case ft_json:
        from_json_file( fname );
        break;
     case ft_yaml:
        from_YAML_file( fname );
        break;
     case ft_xml:
        from_XML_file( fname );
        break;
     default:  bsonioErr( fname, "Undefined file format." );
   }
}

void  IO2FormatBase::ToFormatFile( const char* fname, long int type_f,
                                     bool with_comments, bool brief_mode )
{
    switch( type_f )
    {
       case ft_binary:
           {
              GemDataStream out_br(fname, ios::out|ios::binary);
              to_file(out_br);
              break;
           }
       case ft_text:
            to_text_file(fname, with_comments, brief_mode );
             break;
       case ft_json:
             to_json_file(fname, with_comments, brief_mode);
             break;
       case ft_yaml:
          to_YAML_file(fname, with_comments, brief_mode);
          break;
       case ft_xml:
          to_XML_file(fname, with_comments, brief_mode);
          break;
    default:  bsonioErr( fname, "Undefined file format." );
  }
}


void IO2FormatBase::FromFormatFile( const char* fname, long int  type_f, LoadUnloadF savef )
{
    switch( type_f )
    {
     case ft_binary:
        {
           bsonioErr( fname, "Unreleased for binary file format." );
           break;
         }
     case ft_text:
         from_text_file(fname, savef );
         break;
     case ft_json:
        from_json_file( fname, savef );
        break;
     case ft_yaml:
        from_YAML_file( fname, savef );
        break;
     case ft_xml:
        from_XML_file( fname, savef );
        break;
     default:  bsonioErr( fname, "Undefined file format." );
   }
}

void  IO2FormatBase::ToFormatFile( const char* fname, long int type_f,
       bool with_comments, bool brief_mode, LoadUnloadF readf  )
{
    switch( type_f )
    {
       case ft_binary:
           {
              bsonioErr( fname, "Unreleased for binary file format." );
              break;
           }
       case ft_text:
            to_text_file(fname, with_comments, brief_mode, readf );
             break;
       case ft_json:
             to_json_file(fname, with_comments, brief_mode, readf);
             break;
       case ft_yaml:
          to_YAML_file(fname, with_comments, brief_mode, readf);
          break;
       case ft_xml:
          to_XML_file(fname, with_comments, brief_mode, readf);
          break;
    default:  bsonioErr( fname, "Undefined file format." );
  }
}


//------------------------------------------------------

// Reading one section from bson format
void IO2FormatBase::section_from_bson(  const char *sectionName, TBson2Array&  rdar )
{
    // read objects
    const char *key;
    const char *arrdod =  rdar.getStartObj( sectionName );
    if(!arrdod )
     return;
    TBson2Array secread(arrdod);
    bson_iterator itobj;
    bson_iterator_from_buffer(&itobj, arrdod);
    while( bson_iterator_next(&itobj) )
    {
        key = bson_iterator_key(&itobj);

        long int nfild = ndxFromName( key );
        bsonioErrIf( nfild<0, "Internal error", "Undefined field key");
        //if( nfild >=0 )
        {
            auto itw = startObj + nfild;
            if( itw->isSpecialField() )
                      ;//  readSpecialFields( itrm, rdar  );
             else
                itw->from_bson( secread/*rdar*/, key );
        }
    }
}

// Read  Head data Fields from bson format
void IO2FormatBase::get_head_from_bson( TBson2Array& rdar )
{
    string str;
    rdar.string_from("Type", str );
    if( str != structureName )
    {  str += "  - Illegal structure!\n Fields must be read from ";
       str += structureName;
       str += " structure";
       bsonioErr( "E100BSon: ", str);
   }
}

// Reading work structure from bson format
void IO2FormatBase::from_bson( TBson2Array& rdar )
{
    // setup default constants
    setStaticDefault();
    resetRead();
    get_head_from_bson( rdar );

    for( uint ii=1; ii< outSection.size(); ii++ )
    {
       section_from_bson( outSection[ii].c_str(), rdar );
       if( endSizeSection == ii )
       {
           // sizes readed -finished static fields
           testRead( (startObj+numStaticFields) );
           setConstValues(); // reset calculated sizes
           reallocStruct();
           resetAlwsReadSizes();
       }
    }

    resetAlwsRead();
     // testing read
    testRead( endObj );
    get_special_from_bson( rdar );
}

// -----------------------------------------------------------

// Writing structure to json text file
void IO2FormatBase::to_json_file( const char *file_name, bool with_comments, bool brief_mode )
{
    fstream f(file_name, ios::out);
    bsonioErrIf( !f.good() , file_name, "Filewrite error...");
    string strData = "";
    TArray2Json  prar( strData );
    try{
         prar.addStartObj(0);
         to_bson( prar,  with_comments, brief_mode );
         prar.addEndObj(true);
         f << strData;
       }
    catch(bsonio_exeption& e)
      {
        e.field_ = TArray2Base::curArray;
        throw;
      }
    catch(std::exception& e)
     {
         bsonioErr( file_name , e.what(),TArray2Base::curArray );
     }
}

// Writing structure to json text file
void IO2FormatBase::to_json_file( const char *file_name,
         bool with_comments, bool brief_mode, LoadUnloadF readf  )
{
    fstream f(file_name, ios::out);
    bsonioErrIf( !f.good() , file_name, "Filewrite error...");
    string strData = "";
    TArray2Json  prar( strData );
    int ii=0;
    try{
         f << "[" << endl;
         while( readf(ii++))
         {
           strData = "";
           if( ii>1 ) f << ",\n"; // next node
           prar.addStartObj(0);
           to_bson( prar,  with_comments, brief_mode );
           prar.addEndObj(true);
           f << strData;
         }
         f << "\n]" << endl;
       }
    catch(bsonio_exeption& e)
      {
        e.field_ = TArray2Base::curArray;
        throw;
      }
    catch(std::exception& e)
     {
         bsonioErr( file_name , e.what(),TArray2Base::curArray );
     }
}

// Reading work structure from json text file
void IO2FormatBase::from_json_file( const char *file_name )
{
    fstream f(file_name, ios::in);
    bsonioErrIf( !f.good() , file_name, "Fileread error...");

    // read bson records array from file
    ParserJson parserJson;
    try {
         char b;
         string objStr;
         while( !f.eof() )
         {
          f.get(b);
          if( b == jsBeginObject )
          {
           b= ' ';
           objStr =  parserJson.readObjectText(f);
           bson obj;
           bson_init(&obj);
           parserJson.parseObject( &obj );
           bson_finish(&obj);
           // read structure from bson
           TBson2Array rdar(obj.data);
           from_bson(rdar);
           bson_destroy(&obj);
         }
       }
    }
    catch(bsonio_exeption& e)
    {
      if( e.field_.empty() )
        e.field_ = TArray2Base::curArray;
     throw;
    }
   catch(std::exception& e)
   {
      bsonioErr( file_name , e.what(), TArray2Base::curArray );
   }
}

// Reading work structure from json text file
void IO2FormatBase::from_json_file( const char *file_name, LoadUnloadF savef )
{
    fstream f(file_name, ios::in);
    bsonioErrIf( !f.good() , file_name, "Fileread error...");

    // read bson records array from file
    ParserJson parserJson;
    int ii=0;
    try {
         char b;
         string objStr;
         while( !f.eof() )
         {
          f.get(b);
          if( b == jsBeginObject ) // read one object
          {
           b= ' ';
           objStr =  parserJson.readObjectText(f);
           bson obj;
           bson_init(&obj);
           parserJson.parseObject( &obj );
           bson_finish(&obj);
           // read structure from bson
           TBson2Array rdar(obj.data);
           from_bson(rdar);
           bson_destroy(&obj);
           if( savef( ii++ ))
               break;
           }
         }
    }
    catch(bsonio_exeption& e)
    {
      if( e.field_.empty() )
        e.field_ = TArray2Base::curArray;
     throw;
    }
   catch(std::exception& e)
   {
      bsonioErr( file_name , e.what(), TArray2Base::curArray );
   }
}

// Writing structure to YAML text file
void IO2FormatBase::to_YAML_file( const char *file_name, bool with_comments, bool brief_mode )
{
    fstream fout(file_name, ios::out);
    bsonioErrIf( !fout.good() , file_name, "Fileread error...");
    YAML::Emitter out;
    out.SetDoublePrecision(15);
    out.SetPostCommentIndent(0);
    TArray2YAML  prar(out);
    try{
         prar.addStartObj( 0 );
         to_bson( prar, with_comments, brief_mode );
         prar.addEndObj();
         fout << out.c_str();
    }
   catch(bsonio_exeption& e)
   {
     e.field_ = TArray2Base::curArray;
     throw;
   }
   catch(std::exception& e)
   {
      bsonioErr( file_name , e.what(), TArray2Base::curArray );
   }
}

// Writing structure to YAML text file
void IO2FormatBase::to_YAML_file( const char *file_name,
           bool with_comments, bool brief_mode, LoadUnloadF readf  )
{
    fstream fout(file_name, ios::out);
    bsonioErrIf( !fout.good() , file_name, "Fileread error...");
    YAML::Emitter out;
    out.SetDoublePrecision(15);
    out.SetPostCommentIndent(0);
    TArray2YAML  prar(out);
    int ii=0;
    try{
         //out << YAML::BeginSeq;
         while( readf(ii++) )
         {
           prar.addStartObj(0);
           to_bson( prar,  with_comments, brief_mode );
           prar.addEndObj();
         }
        //out << YAML::EndSeq;
        fout << out.c_str();
       }
   catch(bsonio_exeption& e)
   {
     e.field_ = TArray2Base::curArray;
     throw;
   }
   catch(std::exception& e)
   {
      bsonioErr( file_name , e.what(), TArray2Base::curArray );
   }
}

// Reading work structure from YAML text file
void IO2FormatBase::from_YAML_file( const char *file_name )
{
    fstream fin(file_name, ios::in);
    bsonioErrIf( !fin.good() , file_name, "Fileread error...");

    try {
        // read bson records array from file
        bson obj;
        bson_init(&obj);
        ParserYAML::parseYAMLToBson( fin, &obj );
        bson_finish(&obj);
        // read structure from bson
        TBson2Array rdar(obj.data);
        from_bson(rdar);
        bson_destroy(&obj);
     }
   catch(bsonio_exeption& e)
   {
     e.field_ = TArray2Base::curArray;
     throw;
   }
   catch(std::exception& e)
   {
      bsonioErr( file_name , e.what(), TArray2Base::curArray );
   }
}

// Reading work structure from YAML text file
void IO2FormatBase::from_YAML_file( const char *file_name, LoadUnloadF savef )
{
    fstream fin(file_name, ios::in);
    bsonioErrIf( !fin.good() , file_name, "Fileread error...");
    int ii=0;
    try {
        // read bson records array from file
        do{
            bson obj;
            bson_init(&obj);
            if( !ParserYAML::parseYAMLToBson( fin, &obj ) )
               break;
            bson_finish(&obj);
            // read structure from bson
            TBson2Array rdar(obj.data);
            from_bson(rdar);
            bson_destroy(&obj);
          } while( savef(ii++));
     }
   catch(bsonio_exeption& e)
   {
     e.field_ = TArray2Base::curArray;
     throw;
   }
   catch(std::exception& e)
   {
      bsonioErr( file_name , e.what(), TArray2Base::curArray );
   }
}

// Writing structure to xml text file
void IO2FormatBase::to_XML_file( const char *file_name, bool with_comments, bool brief_mode )
{
    //fstream fout(file_name, ios::out);
    //bsonioErrIf( !fout.good() , file_name, "Fileread error...");

    pugi::xml_document doc;
    TArray2XML  prar(doc);
    try{
         //prar.addStartObj( 0 );
         to_bson( prar, with_comments, brief_mode );
         //prar.addEndObj();
         doc.save_file(file_name);
    }
   catch(bsonio_exeption& e)
   {
     e.field_ = TArray2Base::curArray;
     throw;
   }
   catch(std::exception& e)
   {
      bsonioErr( file_name , e.what(), TArray2Base::curArray );
   }
}

// Writing structure to xml text file
void IO2FormatBase::to_XML_file( const char *file_name,
      bool with_comments, bool brief_mode, LoadUnloadF readf )
{
    //fstream fout(file_name, ios::out);
    //bsonioErrIf( !fout.good() , file_name, "Fileread error...");

    pugi::xml_document doc;
    TArray2XML  prar(doc);
    int ii=0;
    try{
         while( readf(ii++) )
         {
             prar.addNextNode();
             to_bson( prar, with_comments, brief_mode );
         }
         doc.save_file(file_name);
       }
   catch(bsonio_exeption& e)
   {
     e.field_ = TArray2Base::curArray;
     throw;
   }
   catch(std::exception& e)
   {
      bsonioErr( file_name , e.what(), TArray2Base::curArray );
   }
}

// Reading work structure from xml text file
void IO2FormatBase::from_XML_file( const char *file_name )
{
    try {

        // read bson records array from file
        bson obj;
        bson_init(&obj);
        ParserXML::parseXMLToBson( file_name, &obj );
        bson_finish(&obj);

        // read structure from bson
        TBson2Array rdar(obj.data);
        from_bson(rdar);
        bson_destroy(&obj);
     }
   catch(bsonio_exeption& e)
   {
     e.field_ = TArray2Base::curArray;
     throw;
   }
   catch(std::exception& e)
   {
      bsonioErr( file_name , e.what(), TArray2Base::curArray );
   }
}

// Reading work structure from xml text file
void IO2FormatBase::from_XML_file( const char *file_name, LoadUnloadF savef )
{
    try {
          int ii=0;
          // read bson records array from file
          bson obj;
          bson_init(&obj);
          if( !ParserXML::parseXMLToBson( file_name, &obj ))
                 return;
          bson_finish(&obj);
           // read structure from bson
          bson_iterator i;
          bson_iterator_from_buffer(&i, obj.data);
          while (bson_iterator_next(&i))
          {
             TBson2Array rdar(bson_iterator_value(&i));
             from_bson(rdar);
             if( !savef(ii++))
               break;
          }
          bson_destroy(&obj);
     }
   catch(bsonio_exeption& e)
   {
     e.field_ = TArray2Base::curArray;
     throw;
   }
   catch(std::exception& e)
   {
      bsonioErr( file_name , e.what(), TArray2Base::curArray );
   }
}

//---------------------------------------------------------------------

// Writing structure to binary file
void IO2FormatBase::to_file( GemDataStream& ff )
{
    bson obj;
    bson_init( &obj );
    TArray2Bson  prar( &obj );
    to_bson( prar, false, false );
    bson_finish( &obj );
    print_bson_object_to_bin( ff, &obj );
    bson_destroy(&obj);
}

// Reading work structure from binary file
void IO2FormatBase::from_file( GemDataStream& ff )
{
    char *data =0;
    //bson obj;
    data = read_bson_object_from_bin( ff  );
    // read structure from bson
    if( data )
    { TBson2Array rdar(data);
      from_bson( rdar );
    }
    delete data;
    //bson_destroy(&obj);
}

void IO2FormatBase::to_text( TArray2Txt& prar, bool with_comments, bool brief_mode )
{
    setConstValues();
    resetAlwsPrint();
    add_head_to_txt( prar, with_comments, brief_mode );

    // main part
    for(auto itrl = jsonList.begin();
        itrl != jsonList.end(); ++itrl)
    {
       if( !itrl->testCondition() )
           continue;

        if( itrl->comment == "<END_DIM>" )
            prar.addLine( "<END_DIM>\n" );
        else
          if( with_comments && !itrl->comment.empty() )
                prar.addComment( itrl->comment.c_str() );

        if( itrl->fieldName != ""  /*&& itrl->comment.empty()*/ )
        {
           long int ndx = ndxFromName( itrl->fieldName );
           if(ndx >= 0 )
           { auto itw = startObj + ndx;
             if( itw->isSpecialField() )
              printSpecialFields( itw, prar,  with_comments, brief_mode );
            else
              itw->to_text_file( prar, with_comments, brief_mode );
           }
        }
        else
          if( with_comments && itrl->comment.empty() )  // new empty string
                prar.addLine("");
    }

   add_special_to_txt( prar, with_comments, brief_mode );
   prar.addLine("");
}

// Reading work structure from text file
void IO2FormatBase::from_text( TTxt2Array& rdar )
{
  if( numStaticFields == 0 ) // for DataBr part
     reallocStruct();

  // setup default constants
  setStaticDefault();
  resetRead();
  get_head_from_txt( rdar );

  string key = rdar.findNext();
  while( !key.empty() )
  {
      long int nfild = ndxFromName( key );
      bsonioErrIf( nfild<0, "Internal error", "Undefined field key");
      //if( nfild >=0 )
      {
          auto itw = startObj + nfild;
          if( itw->isSpecialField() )
                readSpecialFields( itw, rdar  );
           else
              itw->from_text_file( rdar );
      }

      // try read next field
      key = rdar.findNext();
      if( key == "END_DIM" )
      {
        testRead( (startObj+numStaticFields) );
        setConstValues(); // reset calculated sizes
        reallocStruct();
        resetAlwsReadSizes();
        key = rdar.findNext();
      }
      if( key == "END_ELEMENT" )
      {
        break;
      }
  }

  resetAlwsRead();
   // testing read
  testRead( endObj );
  get_special_from_txt( rdar );
}

// Produces a formatted text file with detailed contents (scalars and arrays) of the structure.
void IO2FormatBase::to_text_file( const char *file_name, bool with_comments, bool brief_mode )
{
  fstream out_br(file_name, ios::out );
  bsonioErrIf( !out_br.good() , file_name, "Format text open error");
  TArray2Txt prar(out_br);
  try {
        to_text( prar, with_comments, brief_mode );
  }
 catch(bsonio_exeption& e)
 {
   e.field_ = TArray2Base::curArray;
   throw;
 }
 catch(std::exception& e)
 {
    bsonioErr( "file_name" , e.what(),TArray2Base::curArray );
 }
}

// Produces a formatted text file with detailed contents (scalars and arrays) of the structure.
void IO2FormatBase::to_text_file( const char *file_name,
          bool with_comments, bool brief_mode, LoadUnloadF readf )
{
  fstream out_br(file_name, ios::out );
  bsonioErrIf( !out_br.good() , file_name, "Format text open error");
  TArray2Txt prar(out_br);
  int ii=0;
  try{
       while( readf(ii++) )
       {
           if( ii>1 ) prar.addLine( "<END_ELEMENT>\n" );
           to_text( prar, with_comments, brief_mode );
       }
     }
 catch(bsonio_exeption& e)
 {
   e.field_ = TArray2Base::curArray;
   throw;
 }
 catch(std::exception& e)
 {
    bsonioErr( "file_name" , e.what(),TArray2Base::curArray );
 }
}

// Readed a formatted text file with detailed contents (scalars and arrays) of the structure.
void IO2FormatBase::from_text_file( const char *file_name )
{
  fstream f_ch(file_name, ios::in );
  bsonioErrIf( !f_ch.good() , file_name, "Fileopen error");
  TTxt2Array rdar(f_ch);
  try {
       from_text( rdar );
      }
 catch(bsonio_exeption& e)
 {
   e.field_ = TArray2Base::curArray;
   throw;
 }
 catch(std::exception& e)
 {
    bsonioErr( file_name , e.what(), TArray2Base::curArray );
 }
}


// Readed a formatted text file with detailed contents (scalars and arrays) of the structure.
void IO2FormatBase::from_text_file( const char *file_name, LoadUnloadF savef )
{
  fstream f_ch(file_name, ios::in );
  bsonioErrIf( !f_ch.good() , file_name, "Fileopen error");
  TTxt2Array rdar(f_ch);
  int ii=0;
  try {
        do{
            from_text( rdar );
          }while( savef(ii++));
      }
 catch(bsonio_exeption& e)
 {
   e.field_ = TArray2Base::curArray;
   throw;
 }
 catch(std::exception& e)
 {
    bsonioErr( file_name , e.what(), TArray2Base::curArray );
 }
}

//----------------------------------------------------------
// Writing one section to bson format
void IO2FormatBase::SectionToBson( bson *obj, int nsection )
{
    bson_destroy(obj);
    bson_init( obj );
    TArray2Bson  prar( obj );

    prar.addStartObj(  outSection[nsection].c_str() );
    for(auto itrl = jsonList.begin();
        itrl != jsonList.end(); ++itrl)
    {
        if( itrl->sectionIndex > nsection )
          break;
        if( itrl->sectionIndex < nsection )
          continue;

       if( itrl->fieldName != ""  /*&& itrl->comment.empty()*/ )
       {
           long int ndx = ndxFromName( itrl->fieldName );
           if(ndx >= 0 )
           { auto itw = startObj + ndx;
             if( itw->isSpecialField() )
              ;
            else
              itw->to_bson( prar, false, false );
           }
        }
      }
    prar.addEndObj();
    bson_finish( obj );
}

// Reading one section from bson format
void IO2FormatBase::SectionFromBson( const char *obj, int nsection )
{
    TBson2Array rdar(obj);
    section_from_bson(  outSection[nsection].c_str(), rdar );
}

string IO2FormatBase::getExt( long int type_f  )
{
  string ext = "";
  switch( type_f )
  {
     case ft_binary:
            ext = "bin";
            break;
     case ft_text:
           ext = "dat";
           break;
     case ft_json:
           ext = "json";
           break;
     case ft_yaml:
           ext = "yaml";
           break;
      case ft_xml:
        ext = "xml";
        break;
     default: break;
  }
  return ext;
}


} // namespace bsonio
