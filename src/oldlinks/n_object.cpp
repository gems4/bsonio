//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file n_object.cpp
/// Implementation of class TObject provides access to application data
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include "n_object.h"
#include "v_json.h"
#include "ar2bson.h"

namespace bsonio {

int TObject::useShortKeywds = 1;

// DOD list element explicit ctor
TObject::TObject( const string& shname, const string& lname,
     char indexCode, const string& descr, long int aalws ):
        KeywordShort( shname ),
        KeywordLong( lname ),
        Type(N_TYPE_),        // Data type code
        IndexationCode(indexCode),
        Descr(descr),         // Description (tooltip) line
        pV(nullptr),          // Pointer to data
        alws(aalws), readed(0)
{
    cndFunction = [](){ return true; };
}

// Ctor by readind from configurator file
TObject::TObject(const char* bsobj):
        pV(nullptr), readed(0)
{
    cndFunction = [](){ return true; };
    fromBson(bsobj);
}

TObject::~TObject()
{ }

// Writes DOD data to ostream file
void TObject::toBson( bson *obj ) const
{
    bson_append_string( obj, "KwdShort", KeywordShort.c_str() );
    bson_append_string( obj, "KwdLong", KeywordLong.c_str() );
    bson_append_int( obj, "alws", alws );
    bson_append_int( obj, "IndexCode", IndexationCode );
    bson_append_string( obj, "Descr",  Descr.c_str() );
}

// Reads DOD data from istream file
void TObject::fromBson( const char* bsobj )
{
    if(!bson_find_string( bsobj, "KwdShort", KeywordShort ) )
        bsonioErr( "E010BSon: ", "Undefined KeywordShort.");
    if(!bson_find_string( bsobj, "KwdLong", KeywordLong ) )
        bsonioErr( "E011BSon: ", "Undefined KeywordLong.");
    if(!bson_find_value( bsobj, "alws", alws ) )
        alws = 0;
    if(!bson_find_value( bsobj, "IndexCode", IndexationCode ) )
        IndexationCode = 'N';
    if(!bson_find_string( bsobj, "Descr", Descr ) )
        Descr="";
}

string TObject::getFullName(int n, int m)
{
  string item = KeywordShort;
  if( getN() > 1 )
    item += "["+to_string(n)+"]";
  if( getM() > 1 && ( Type != S_ && Type != J_) )
    item += "["+to_string(m)+"]";
  return item;
}

string TObject::getHelpLink(int n, int m)
{
  string item = KeywordShort;
  if( !(getN()<=1 || Descr[0] == '|') )
    item += "_"+to_string(n);
  else
    if( getM() > 1 && Type != S_ && Type != J_)
        item += "_"+to_string(m);
  return item;
}

// Gets description line from n line of DOD list
//    e.g., for displaying a tooltip
string TObject::getDescription(int n, int m)
{
    size_t prev_pos = 0;
    size_t pos = Descr.find('\n');
    if( pos == string::npos )
        return Descr;

    int Nn = n;   // Description by lines
    if( getN() <= 1 || Descr[0] == '|' )     // Decriptions by colums
           Nn = m;

    for(int ii=0;  ii < Nn; ii++ )
    {
        prev_pos = pos + 1;
        if( prev_pos >= Descr.length() )
            return string("TObject:E01 -bad description line in DOD-");
        pos = Descr.find("\n", prev_pos);
        if( pos == string::npos )
            return Descr.substr(prev_pos, string::npos);
    }
    return Descr.substr(prev_pos, pos-prev_pos-1);
}

// ----------------- EJDB


/// Puts data object to bson format
void TObject::toBsonObject( bson *obj )
{
  bson_append_string( obj,"label", getKeywordShort() );
  bson_append_int( obj, "dot", Type );
  if( IsNull() && getM() != 0)
    bson_append_int( obj, "dN", 0 );
  else
    bson_append_int( obj, "dN", getN() );
   bson_append_int( obj, "dM", getM() );

  if( IsNull() )
      bson_append_null(obj, "val");
  else
  {
      TArray2Bson fbson( obj );
      pV->to_bson( fbson, "val", "" );
  }
}

/// read object from bson structure
/// "label" and "id" must be test before
void TObject::fromBsonObject( const char *obj )
{
    signed char Otype;
    int Odim_N;
    unsigned int Odim_M;

    // get definition of object
    if(!bson_find_value( obj, "dot", Otype ) )
        bsonioErr( "E001BSon: ", "Undefined dot.");
    if(!bson_find_value( obj, "dN", Odim_N ) )
        bsonioErr( "E002BSon: ", "Undefined dN.");
    if(!bson_find_value( obj, "dM", Odim_M ) )
        bsonioErr( "E003BSon: ", "Undefined dM.");

    // Test sizes
    if( Otype != Type || Odim_N != getN() || Odim_M != getM() )
        bsonioErr( getKeywordShort(), "TObject:E07 Invalid type/size on getting data object");

    if( !IsNull() )
    {
        TBson2Array fbson(obj);
        pV->from_bson( fbson, "val" );
     }
 }

} // namespace bsonio
