//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file io2format.h
/// Declarations of IO2FormatBase - Base class for read/write
/// internal structure to files
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef IO2FORMAT_H
#define IO2FORMAT_H

#include <vector>
#include <map>
#include "n_object.h"


namespace bsonio {

//  internal values
extern long int const1;
extern long int const8;
extern long int const3;
extern long int const2;
extern long int const16;
extern long int const4;
extern long int const6;

/// Flag defining whether the i/o file type
typedef enum {
  ft_undef =  0,
  ft_text =   1,    // the file is in text format
  ft_binary = 2,    // the file is in binary format
  ft_json   = 3,    // the file is in json format
  ft_yaml   = 4,    // the file is in YAML format
  ft_xml    = 5     // the file is in XML format
} FILE_IO_FORMATS;


/// Internal descriptions of objects in text format file
struct outObject
{
   string fieldName;         ///< Field name into TObject list structure ( empty no exist )
   long int sectionIndex;    ///< Logical section index
   string comment;           ///< Comment line
   conditF testCondition;    ///< This field must be outputted if condition is true
   //long int conditionIndex;  ///< This field must be outputted if condition by index is true

   /// Add comment line to format
   outObject( long int sect, char , const char *cmnt ):
       fieldName(""), sectionIndex(sect),  comment(cmnt)
   {
     testCondition = [](){ return true; };
   }

   /// Add array/scalar to format
   outObject( long int sect, const char * fld, const char *cmnt=""  ):
       fieldName(fld), sectionIndex(sect), comment(cmnt)
   {
     testCondition = [](){ return true; };
   }
};

using objIter = vector<TObject>::iterator;
using  LoadUnloadF = std::function<bool(int)>;

/// Base class for read/write internal structure to files
class IO2FormatBase
{

protected:

  string structureName;
  vector<outObject> jsonList;  ///< List of ordered output object and comments to json format file
  vector<string> outSection;          ///< Logical sections of content
  long int endSizeSection;            ///< Finishing sizes blocks index

  // for internal formats
  long int numStaticFields;        ///< Number of static fields
  objIter startObj;
  objIter endObj;

  // internal data
  map< string, int> key2IndexMap;  ///< Dictionary of all objects to output
  double tmpVal;                   ///< internal value

  // Set up internal structures

  /// Define output format list
   virtual void setupJsonList() =0;
  /// Define internal search map
   void setupMap();
  /// Find nabe from internal search map
   long int ndxFromName( const string& name )
   {
       auto it = key2IndexMap.find(name);
       if (it != key2IndexMap.end())
           return it->second;
       return -1;
   }

  // Internal service

   /// Reset readed flag
   void resetRead();
   /// Test fields must be read from strucrure
   void testRead(objIter last );

   // may be lambda Expressions
   /// Calculate or setup Internal values
   virtual void setConstValues() {}
   /// Setup default constants before reading data
   virtual void setStaticDefault() {}
   /// Define always data to Print
   virtual void resetAlwsPrint() {}
   /// Define always sizes data to Read and
   /// Setup default arrays data before reading
   virtual void resetAlwsReadSizes() {}
   /// Define always other data to Read
   virtual void resetAlwsRead() {}

   // main read/write functions

   /// Added  Specific data Fields to bson format
   virtual void add_special_to_bson( TArray2Bson&, bool, bool  )  { }
   /// Added  Head data Fields to bson format
   virtual void add_head_to_bson( TArray2Bson& prar, bool, bool )
   {
       prar.string_to( "Type",  structureName );
   }

   /// Added  Specific data Fields to json with comments format
   virtual void add_special_to_bson( TArray2Json&, bool, bool  ) { }
   /// Added  Head data Fields to json with comments format
   virtual void add_head_to_bson( TArray2Json& prar, bool, bool )
   {
       prar.writeString("Type",  structureName );
   }

   /// Added  Specific data Fields to yaml with comments format
   virtual void add_special_to_bson( TArray2YAML& , bool, bool ) { }
   /// Added  Head data Fields to json with comments format
   virtual void add_head_to_bson( TArray2YAML& prar, bool, bool  )
   {
       prar.writeString("Type",  structureName );
   }

   /// Added  Specific data Fields to xml with comments format
   virtual void add_special_to_bson( TArray2XML& , bool, bool ) { }
   /// Added  Head data Fields to xml with comments format
   virtual void add_head_to_bson( TArray2XML& prar, bool, bool  )
   {
       prar.writeString("Type",  structureName );
   }

   /// Read  Specific data Fields from bson format
   virtual void get_special_from_bson( TBson2Array& /*rdar*/ ){ }
   /// Read  Head data Fields from bson format
   virtual void get_head_from_bson( TBson2Array& rdar );
   /// Reading one section from bson format
   void section_from_bson( const char *sectionName, TBson2Array&  rdar );

   /// Added  Specific data Fields to txt format
   virtual void add_special_to_txt( TArray2Txt& prar, bool with_comments, bool )
   {
       if( with_comments )
       {
           prar.addLine("");
           prar.addComment( "End of file");
       }
   }
   /// Added  Head data Fields to txt format
   virtual void add_head_to_txt( TArray2Txt& prar, bool with_comments, bool /*brief_mode*/ )
   {
     if( with_comments )
      prar.addComment( "# Comments can be marked with # $ ; as the first character in the line");
   }
   virtual void printSpecialFields( objIter& /*itrl*/,
                TArray2Txt&  /*prar*/,  bool /*with_comments*/, bool /*brief_mode*/) { }
   /// Read  Specific data Fields from txt format
   virtual void get_special_from_txt( TTxt2Array& /*rdar*/ ){}
   /// Read  Head data Fields from txt format
   virtual void get_head_from_txt( TTxt2Array& /*rdar*/ ) {}
   virtual void readSpecialFields(objIter& /*itrm*/, TTxt2Array&  /*rdar*/  ) { }

public:

   /// Constructor
   IO2FormatBase( long int aNumStatic, objIter astartObj, objIter aendObj ):
       structureName("Base"), numStaticFields(aNumStatic)
    {
       startObj = astartObj;
       endObj = aendObj;
       setupMap();
    }

   /// Destructor
   virtual ~IO2FormatBase()
   { }

   /// Set up field by name as always or not
   void setAlws( const char *name, bool isAlws )
   {
      int ndx = ndxFromName( name );
      if( ndx >= 0 )
      {
         auto it =startObj+ndx;
         it->setAlways( isAlws );
      }
   }

   /// Build special file Name
   virtual string buildFileName( const char *name, const char *ext )
   {  return name+string(".")+ext;  }
   /// Build special file Name by type
   virtual string buildFileName( const char *path_name, long int /*type_f*/, long int /*ndx*/  )
   {   return path_name;   }
   /// Get extension from file type
   string getExt( long int type_f  );

   /// Alloc memory for arrays and set default values for json format
   virtual void freeStruct();
   /// Alloc memory for arrays and set default values
   virtual void reallocStruct();

   /// Produces a formatted file with detailed contents (scalars and arrays) of the structure.
   void FromFormatFile( const char* fname, long int  type_f );
   /// Readed a formatted file with detailed contents (scalars and arrays) of the structure.
   void ToFormatFile( const char* fname, long int type_f, bool with_comments, bool brief_mode );

   /// Produces a formatted file with detailed contents (scalars and arrays) of the structure.
   /// Produces array of structures
   void FromFormatFile( const char* fname, long int  type_f, LoadUnloadF savef );
   /// Readed a formatted file with detailed contents (scalars and arrays) of the structure.
   /// Readed array of structures
   void ToFormatFile( const char* fname, long int type_f,
                      bool with_comments, bool brief_mode, LoadUnloadF readf );


   /// Writing structure to T (bson, json, yaml, xml) format
   template < class T>
   void to_bson( T&  prar, bool with_comments, bool brief_mode )
   {
       setConstValues();
       resetAlwsPrint();

       //prar.addStartObj(0);
       add_head_to_bson( prar, with_comments, brief_mode );

       // main part
       int nsection = 1;
       prar.addStartObj( outSection[nsection].c_str() );

       for(auto itrl = jsonList.begin();
           itrl != jsonList.end(); ++itrl)
       {
          if( itrl->sectionIndex > nsection ) // finished static data
          {
             // finished section
             prar.addEndObj();
             nsection =  itrl->sectionIndex;
             // start new section
             prar.addStartObj( outSection[nsection].c_str() );
             //continue;
          }

          if( !itrl->testCondition() )
              continue;

          if( with_comments && !itrl->comment.empty() )
             prar.addComment( itrl->comment.c_str() );

          if( itrl->fieldName != ""  /*&& itrl->comment.empty()*/ )
          {
              long int ndx = ndxFromName( itrl->fieldName );
              if(ndx >= 0 )
              { auto itw = startObj + ndx;
                if( itw->isSpecialField() )
                 ;
               else
                 itw->to_bson( prar, with_comments, brief_mode );
              }
           }
       }
      prar.addEndObj();
      add_special_to_bson( prar, with_comments, brief_mode );
   }

   /// Reading structure from bson data
   void from_bson( TBson2Array& rdar );

   /// Produces a formatted text file with detailed contents (scalars and arrays) of the structure.
   void to_text( TArray2Txt& prar, bool with_comments, bool brief_mode );
   /// Readed a formatted text file with detailed contents (scalars and arrays) of the structure.
   void from_text( TTxt2Array& rdar );

   /// Writing structure to binary file
   void to_file( GemDataStream& ff );
   /// Reading structure from binary file
   void from_file( GemDataStream& ff );

   //----------------------------------------------------------------------------------------

   /// Produces a json text file with detailed contents (scalars and arrays) of the structure.
   void to_json_file( const char *file_name, bool with_comments, bool brief_mode  );
   /// Readed a json text file with detailed contents (scalars and arrays) of the structure.
   void from_json_file( const char *file_name );
   /// Writing structure to YAML text file
   void to_YAML_file( const char *file_name, bool with_comments, bool brief_mode );
   /// Reading work structure from YAML text file
   void from_YAML_file( const char *file_name );
   /// Writing structure to xml file
   void to_XML_file( const char *file_name, bool with_comments, bool brief_mode );
   /// Reading work structure from xml file
   void from_XML_file( const char *file_name );
   /// Produces a formatted text file with detailed contents (scalars and arrays) of the structure.
   void to_text_file( const char *file_name, bool with_comments, bool brief_mode );
   /// Readed a formatted text file with detailed contents (scalars and arrays) of the structure.
   void from_text_file( const char *file_name );

   /// Produces a json text file with detailed contents (scalars and arrays) of the structure.
   /// Produces array of structures
   void to_json_file( const char *file_name, bool with_comments, bool brief_mode, LoadUnloadF readf  );
   /// Readed a json text file with detailed contents (scalars and arrays) of the structure.
   /// Readed array of structures
   void from_json_file( const char *file_name, LoadUnloadF savef );
   /// Writing structure to YAML text file
   /// Produces array of structures
   void to_YAML_file( const char *file_name, bool with_comments, bool brief_mode, LoadUnloadF readf );
   /// Reading work structure from YAML text file
   /// Readed array of structures
   void from_YAML_file( const char *file_name, LoadUnloadF savef );
   /// Writing structure to xml file
   /// Produces array of structures
   void to_XML_file( const char *file_name, bool with_comments, bool brief_mode, LoadUnloadF readf );
   /// Reading work structure from xml file
   /// Readed array of structures
   void from_XML_file( const char *file_name, LoadUnloadF savef );
   /// Produces a formatted text file with detailed contents (scalars and arrays) of the structure.
   /// Produces array of structures
   void to_text_file( const char *file_name, bool with_comments, bool brief_mode, LoadUnloadF readf );
   /// Readed a formatted text file with detailed contents (scalars and arrays) of the structure.
   /// Readed array of structures
   void from_text_file( const char *file_name, LoadUnloadF savef );

   /// Writing one section to bson format
   void SectionToBson( bson *obj, int nsection );
   /// Reading one section from bson format
   void SectionFromBson( const char *obj, int nsection );

 };

} // namespace bsonio

#endif // IO2FORMAT_H
