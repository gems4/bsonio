//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file thrift_import.cpp
/// Implementation of classes AbstractIEFile, StructDataIEFile, KeyValueIEFile -
/// import of foreign format files
/// Prepared an impex.thrift file describing 4 file formats for import/export
/// of data to internal DOM based on our JSON schemas.
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include <boost/regex.hpp>
#include "thrift_import.h"
#include "json2cfg.h"
#include "v_yaml.h"
#include "v_xml.h"
using namespace boost;

namespace bsonio {

void AbstractIEFile::setReadedData()
{
   SchemaNode* fld;
  // set up predefined data
   auto itr = block.defaults.begin();
   while ( itr != block.defaults.end() )
   {
       fld = structNode->field(itr->first);
       if( fld == nullptr)
       {
           cout << "Not found " <<  itr->first << endl;
           itr++;
           continue;
       }
       if( fld->isArray() || fld->isStruct() )
       {
         fld->setComplexValue( itr->second );
       }
       else
         fld->setValue(itr->second);
       itr++;
   }

  // set up readed data
  string impkey;
  vector<string> impvalue;
  auto it = externData.begin();
  while ( it != externData.end() )
  {
     impkey = it->first;
     impvalue = it->second;
     it++;

     auto idom = block.matches.find( impkey );
     if( idom == block.matches.end() ) // not converted
       continue;
     string domkey = idom->second.field; //idom->second.fieldkey;
     if( idom->second.ignore || domkey.empty() )  // skip ignored
         continue;

     fld = structNode->field(domkey);
     if( !fld )
     {
       auto pos = domkey.find_last_of(".");
       if(pos != string::npos)
       {
         string key = string(domkey,  pos+1 );
         domkey = string(domkey, 0, pos);
         fld = structNode->field(domkey);
         if( fld && fld->isMap() )
         {
           run_convert( idom->second.convert, impvalue );
           run_lua_script_string( idom->second.script, impvalue, false );
           fld->addMapKeyValue( key, impvalue[0]  );
         }
        }
        continue;
      }

      run_convert( idom->second.convert, impvalue );
      if( fld->elemTypeId() == T_STRING )
       run_lua_script_string( idom->second.script, impvalue, fld->isArray() );
      else
        run_lua_script( idom->second.script, impvalue, fld->isArray() );

      if( fld->isArray() )
      {
        fld->resetArray( impvalue );
      }
      else
       if(impvalue.size()>0)
          fld->setValue(impvalue[0]);
//     it++;
  }
}

/// change value, using convert
void AbstractIEFile::run_convert( const map<string,string>&  convert, vector<string>& values )
{
    if( convert.empty() )
      return;
    for(uint ii=0; ii< values.size(); ii++ )
    {
        auto itmap = convert.find( values[ii] );
        if( itmap != convert.end() )
         values[ii] = itmap->second;
    }
}

// change all values use script
void AbstractIEFile::run_lua_script( const string& luascript,
            vector<string>& values, bool isArray )
{
  if(luascript.empty()  || values.size() < 1 )
     return;

  double dv;
  vector<double> dvals;

  // convert to numbers
  auto it = values.begin();
  while( it != values.end() )
  {
    bsonio::TArray2Base::string2value( *it++, dv );
    dvals.push_back(dv);
  }

  // run script
  bool ret;
  if( isArray )
      ret = _lua.runFunc(luascript, dvals);
  else
      ret = _lua.runFunc(luascript, dvals[0]);

  if( ret)   // script run ok
  {
      // convert to strings
      values.clear();
      auto itd = dvals.begin();
      while( itd != dvals.end() )
        values.push_back(bsonio::TArray2Base::value2string( *itd++ ));
  }
}

// change all values use script
void AbstractIEFile::run_lua_script_string( const string& luascript,
            vector<string>& values, bool isArray )
{
  if(luascript.empty()  || values.size() < 1 )
     return;

  // run script
  if( isArray )
     _lua.runFunc(luascript, values);
  else
     _lua.runFunc(luascript, values[0]);
}

// Run script for operation on data values in block
void AbstractIEFile::run_block_script( const string& luascript, bson *obj )
{
    if(luascript.empty() )
       return;

    // bson to json
    string recBsonText;
    jsonFromBson( obj->data, recBsonText );

    // run script
    string newjson = _lua.run( luascript,recBsonText, false );
    if( !newjson.empty() )
    { bson_destroy(obj );
      jsonToBson( obj, newjson );
    }
}

string AbstractIEFile::impexFieldOrganization( const string& impexFieldKey )
{
    auto itPair = block.pairs.find(impexFieldKey);
    if( itPair != block.pairs.end() )
         return itPair->second.organization;
    return "";
 }

string AbstractIEFile::impexFieldType( const string& impexFieldKey )
{
    auto itPair = block.pairs.find(impexFieldKey);
    if( itPair != block.pairs.end() )
         return itPair->second.datatype;
    return "";
 }

// export part-----------------------------------------


void AbstractIEFile::assembleGroups( map<string,vector<string>>& writeData  )
{
    string groupKey, itKey;
    vector<string> groupValues;
    uint groupKeySize;
    int ndx;

    auto itPair = block.pairs.begin();
    while( itPair != block.pairs.end() )
    {
       if( itPair->second.organization == "group" )
       {
          groupKey = itPair->first;
          groupValues = extractValue( groupKey, writeData ); // get old
          groupKeySize = groupKey.size();

          auto itv = writeData.begin();
          while( itv != writeData.end() )
          {
            itKey = itv->first;
            if( itKey.substr( 0, groupKeySize ) == groupKey )
            {
              if( is<int>( ndx, itKey.substr( groupKeySize ) ) )
              {
                if( ndx+1 > (int)groupValues.size() )
                  groupValues.resize(ndx+1, " ");
                if( itv->second.size() > 0 )
                 groupValues[ndx] =  itv->second[0];
                itv = writeData.erase(itv);
                continue;
              }
            }
            itv++;
           }
           writeData[groupKey]  = groupValues;
       }
       itPair++;
    }
}


// Insert key value paies to extern list
void AbstractIEFile::addMapKeyValue( const string& _key, const vector<string>& vec )
{
    auto itr = externData.find(_key);
    if( itr != externData.end() )
    {
        // add only for type "group"
        if( impexFieldOrganization( _key ) == "group")
        {
          itr->second.insert( itr->second.end(), vec.begin(), vec.end() );
          return;
        }
    }
    // can overload previous
    externData[_key] = vec;
}


// Extract data from bson
void AbstractIEFile::extractDataToWrite( bson *bsobj, map<string,vector<string>>& writeData )
{
    string impexKey, impexOrganization, nodeValue;
    vector<string> nodeValues;
    externData.clear();

    // Run script for operation on data values in block (if need some data preparation )
    run_block_script(block.script, bsobj);

    // extract bson data to internal structure
    structNode->setStructBson( bsobj->data );

    // set up predefined data
    auto itr1 = block.defaults.begin();
    while ( itr1 != block.defaults.end() )
    {
      impexKey = itr1->first;
      impexOrganization = impexFieldOrganization( impexKey );

      // we can split second to vector if type "list" | "set"
      if( impexOrganization=="list" || impexOrganization == "set"  )
        nodeValues = regexp_split( itr1->second, " "/*format.value_token_regexp*/ );
      else
         nodeValues =  { itr1->second };
      addMapKeyValue( impexKey, nodeValues );
      itr1++;
    }

    // extract fields from structNode
    auto idom = block.matches.begin();
    while ( idom != block.matches.end() )
    {
        impexKey = idom->first;
        string domkey = idom->second.field; //idom->second.fieldkey;
        if( idom->second.ignore || domkey.empty() )  // skip ignored
        {   idom++; continue; }

        auto fld = structNode->field(domkey);
        if( !fld )
           {   idom++; continue; } // field is not present

        if( fld->isArray() )
        {
            vector<int> sizes = fld->getArraySizes();
            nodeValues.resize(sizes[0]);
            fld->getArray(nodeValues);
        }
        else
         {
           if( !fld->getValue( nodeValue  ) )
             {   idom++; continue; }     //error extraction
           nodeValues = { nodeValue};
         }

        impexOrganization = impexFieldOrganization( impexKey );
        bool isArray = impexOrganization=="list" || impexOrganization == "set";

        run_convert( idom->second.convert, nodeValues );
         if( fld->elemTypeId() == T_STRING )
          run_lua_script_string( idom->second.script, nodeValues, isArray );
         else
           run_lua_script( idom->second.script, nodeValues, isArray );

        addMapKeyValue( impexKey, nodeValues );
        idom++;
    }

    // add to results
    writeData.insert( externData.begin(), externData.end());
}





//--------------------------------------------------------------------------
// Export/import of data records to/from text file in legacy JSON/YAML/XML (foreign keywords)

StructDataIEFile::StructDataIEFile( int amode, const ThriftSchema* aschema, const string& alualibPath,
     const FormatStructDataFile& defData ):
           AbstractIEFile( amode, aschema, alualibPath),  renderer(defData.renderer)
{
   // set up data from  FormatKeyValueFile !!!
    label = defData.label;
    comment = defData.comment;
    file_name = defData.fname;
    Nblocks = defData.Nblocks;
    Nlines = defData.Nlines;
    block = defData.block;
    structNode = SchemaNode::newSchemaStruct( label, 0 );

    // select file type
    if(renderer == "json" || renderer == "JSON" )
      _ftype = FileTypes::Json_;
    else  if(renderer == "yaml" || renderer == "YAML" )
            _ftype = FileTypes::Yaml_;
    else if(renderer == "xml" || renderer == "XML" )
            _ftype = FileTypes::XML_;
         else _ftype = FileTypes::Undef_;
}

// Open file to import
void StructDataIEFile::openFile( const string& inputFname )
{

  bsonioErrIf( _mode != modeImport  , file_name, "No export to format...");

  if( !inputFname.empty() )
    file_name = inputFname;

  if( _ftype == FileTypes::XML_ )
  {
      bson_init(&xmlobj);
      if( !ParserXML::parseXMLToBson( file_name, &xmlobj ))
             return;
      bson_finish(&xmlobj);
      bson_iterator_from_buffer(&ixml, xmlobj.data);
      return;
  }

 // open  file for other types
  if (_stream.is_open())
     _stream.close();
  _stream.open(file_name,  std::fstream::in );
  bsonioErrIf( !_stream.good() , file_name, "Fileopen error...");
}

// Close internal file
void StructDataIEFile::closeFile()
{
    // close file
    if (_stream.is_open())
    _stream.close();
}

// Reading work structure from YAML text file
bool StructDataIEFile::next_from_yaml_file( bson *obj )
{
  bson_init(obj);
  if( !ParserYAML::parseYAMLToBson( _stream, obj ) )
      return false;
  bson_finish(obj);
  return true;
}

// Reading work structure from json text file
bool StructDataIEFile::next_from_json_file( bson *obj )
{
    // read bson records array from file
    ParserJson parserJson;
    char b;
    string objStr;

    while( !_stream.eof() )
    {
     _stream.get(b);
      if( b == jsBeginObject ) // read one object
      {
           b= ' ';
           objStr =  parserJson.readObjectText( _stream );
           bson_init(obj);
           parserJson.parseObject( obj );
           bson_finish(obj);
           return true;  // next record
       }
    }
    return false;
}

// Reading work structure from xml text file
bool StructDataIEFile::next_from_xml_file( bson *obj )
{
  if (bson_iterator_next(&ixml))
  {
      bson_init(obj);
      bson_append_field_from_iterator(&ixml, obj );
      bson_finish(obj);
      return true;
  }
  return false;
}

// Read next bson object
bool StructDataIEFile::nextFromFile( bson *obj )
{
  switch( _ftype )
  {
   case  FileTypes::Json_: return next_from_json_file( obj );
   case  FileTypes::Yaml_: return next_from_yaml_file( obj );
   case  FileTypes::XML_:  return next_from_xml_file( obj );
   default: return false;
  }
}

// Read data from one block in file
bool StructDataIEFile::readBlockFromFile()
{
    bson obj;
    if( !nextFromFile( &obj ) )
       return false;

    // set data to readedData
    string key;
    vector<string> _value;
    auto itr = block.matches.begin();
    while ( itr != block.matches.end() )
    {
        key = itr->first;
        if( bson_read_array_path( obj.data, key.c_str(), _value ) )
         externData.insert( pair<string, vector<string>>(key, _value) );
        itr++;
    }
    bson_destroy(&obj);
    return true;
}

// Read data one block from bson data
bool StructDataIEFile::readBlockFromBson( bson *obj, string& secondKey, int ndx )
{
    if( !obj )
       return false;

    secondKey = "";

    // set data to readedData
    string key, key2;
    vector<string> _value;
    auto itr = block.matches.begin();
    while ( itr != block.matches.end() )
    {
        key2 = key = itr->first;

        if( ndx>=0)
         key2 = replace( key, "%", to_string(ndx).c_str());

        if( bson_read_array_path( obj->data, key2.c_str(), _value ) )
        { externData.insert( pair<string, vector<string>>(key, _value) );
          if(itr->second.field == "@key" )
          {   // convert key or get substring
              _lua.runFunc(itr->second.script, _value[0]);
              secondKey += _value[0];
          }
        } else
          {
              if(key != key2 )
               return false;
          }

        itr++;
    }
    return true;
}

/// Convert free format bson to schema defined
bool StructDataIEFile::readBlock(  bson *inobj, bson *bsobj, string& key, int ndx )
{
 // read data from block to readedData
 externData.clear();

 if( !readBlockFromBson(inobj, key, ndx ) )
   return false;
 // reset default structure
 structNode->clearNode();
 // write data to structNode
 setReadedData();

 // structNode to bson
 bson_destroy( bsobj );
 structNode->getStructBson(bsobj);
 // Run script for operation on data values in block
 run_block_script(block.script, bsobj);

 // Set data according schema (+4 to 4)
 structNode->setStructBson(bsobj->data);
 bson_destroy( bsobj );
 structNode->getStructBson(bsobj);

 return true;
}


//--------------------------------------------------------------------------
// Definition of text file with key-value pair data

KeyValueIEFile::KeyValueIEFile( int amode, const ThriftSchema* aschema, const string& alualibPath,
           const FormatKeyValueFile& defData ):
           AbstractIEFile( amode, aschema, alualibPath),
           format(defData.format), renderer(defData.renderer)
{
   // set up data from  FormatKeyValueFile !!!
    label = defData.label;
    comment = defData.comment;
    file_name = defData.fname;
    block = defData.block;
    Nblocks = defData.Nblocks;
    Nlines = defData.Nlines;
    structNode = SchemaNode::newSchemaStruct( label, 0 );
    str_buf = "";
    test_start = str_buf.begin();
    test_end = str_buf.end();

}

// Open file to import
void KeyValueIEFile::openFile( const string& inputFname )
{
  if( !inputFname.empty() )
    file_name = inputFname;

   // open  file
  if (_stream.is_open())
  _stream.close();

  if( _mode == modeImport )
     _stream.open(file_name,  std::fstream::in );
  else
     _stream.open(file_name,  std::fstream::out|std::fstream::app );

  bsonioErrIf( !_stream.good() , file_name, "Fileopen error...");
}

// Close internal file
void KeyValueIEFile::closeFile()
{
    // close file
    if (_stream.is_open())
    _stream.close();
}

/// Read next block of data from file
bool  KeyValueIEFile::readNextBlockfromFile()
{
    // clear analyzed block
    str_buf = string(test_start, test_end);
    // delete spaces
    if( !str_buf.empty() )
    { string::size_type pos1 = str_buf.find_first_not_of(" \t\r\n");
      if( pos1 != string::npos )
        str_buf = str_buf.substr(pos1);
      else
        str_buf = "";
    }
    test_start = str_buf.begin();
    test_end = str_buf.end();

    if( _stream.eof() || !_stream.good()  )
        return false;

    //let's say read by 2048 char block
    char buffer[2049];
    memset(buffer,'\0', 2049 );
    // read next block
    _stream.read( buffer, 2048 );
    str_buf += string(buffer);
    test_start = str_buf.begin();
    test_end = str_buf.end();
    //cout << endl << "!!!!" << _str_buf.c_str() << "\n<<<" << endl;

    return true;
}

/// Skip comments and blanks
void KeyValueIEFile::skipComments()
{
    if( str_buf.empty() )
      return;

    string::size_type posb;
    vector<string> mach_str;

    do{
        if( !mach_str.empty() )
          insertKeyValue( "comment", mach_str );

        posb = str_buf.find_first_not_of(" \t\r\n",test_start-str_buf.begin() );
        if( posb != string::npos )
          test_start = str_buf.begin()+posb;
        else
          test_start = test_end;

        if( test_start == test_end )
              break;
    } while( !format.comment_regexp.empty() && readRegexp( format.comment_regexp, mach_str ) );

}

///  Test/read next regexp
///  \param  re_str - string with Regular Expression
///  \param  mach_str - list of matched strings
///  \return true if matched Expression
bool KeyValueIEFile::readRegexp( const string& re_str, vector<string>& mach_str )
{
 try
  {
   mach_str.clear();

   if( test_start == test_end )
    return false;

   match_results<string::const_iterator> what;
   match_flag_type flags = boost::match_continuous| boost::match_partial;
   const regex expression( re_str.c_str() );

   while(regex_search(test_start, test_end, what, expression, flags))
   {
      // match only partial
      if( !what[0].matched  )
      {
          if( !readNextBlockfromFile() )
            return false; // end of file
      }
      else
      { // what[0] contains the whole string
        // what[>0] contains the tokens.

        for( uint ii=1; ii<what.size(); ii++ )
        { //cout <<  what[ii] << endl;
          mach_str.push_back(what[ii]);
        }
        // update search position:
        test_start = what[0].second;
        return true;
      }
   }

  }
   catch (regex_error &e)
    {
      cout << "what: " << e.what() << "; code: " << parseCode(e.code()) << endl;
      bsonioErr("Error in Regular Expression", e.what(), re_str);
    }
  return false;
}

///  Only Test next regexp
///  \param  re_str - string with Regular Expression
///  \return true if matched Expression
bool KeyValueIEFile::testRegexp( const string& re_str )
{
 try
  {
   //if( test_start == test_end )
   // return false;

   match_results<string::const_iterator> what;
   match_flag_type flags = boost::match_continuous| boost::match_partial;
   const regex expression( re_str.c_str() );
   while(regex_search(test_start, test_end, what, expression, flags))
   {
      // match only partial
      if( !what[0].matched  )
      {
        if( !readNextBlockfromFile())
            return false; // end of file
      }
      else
        return true;
   }
  }
   catch (regex_error &e)
    {
      cout << "what: " << e.what() << "; code: " << parseCode(e.code()) << endl;
      bsonioErr("Error in Regular Expression", e.what(), re_str);
    }
  return false;
}

/// Read header of block
bool KeyValueIEFile::readStartBlock()
{
  // read next block of data from file
  readNextBlockfromFile();

  // skip comments and blanks
  skipComments();

  // nothing to test
  if( str_buf.empty() )
    return false;

  // no head condition
  if( format.head_regexp.empty() )
    return true;

  // read header of block
  vector<string> mach_str;
  bool ret = readRegexp( format.head_regexp, mach_str );
  if( ret )
    for(uint ii=0; ii<mach_str.size(); ii++ )
    {
      string _key = "head"+to_string(ii);
      addKeyValue( _key, mach_str[ii] );
    }

  return ret;
}

/// Read&test end block
bool KeyValueIEFile::isEndBlock()
{
    bool ret;

    // skip comments and blanks
    skipComments();

    // no end condition (test next head condition or end of file )
    if( format.end_regexp.empty() )
    {
        if( test_start == test_end )
         ret = true;
        else
         ret = testRegexp(  format.head_regexp );
    } else
      { // read end of block
        vector<string> mach_str;
        ret = readRegexp(  format.end_regexp, mach_str );
        if( ret )
          for(uint ii=0; ii<mach_str.size(); ii++ )
          {
            string _key = "end"+to_string(ii);
            addKeyValue( _key, mach_str[ii] );
          }
    }
   return ret;
}

/// Read value untill next key or end of block
string KeyValueIEFile::readAllValue()
{
  string value = "";
  size_t posb;

  auto pos = test_start;
  while( pos != test_end )
  {
      // test next key;
      if( testRegexp(  format.key_regexp ) )
       break;

      // test next end of block
      if( format.end_regexp.empty() )
      {   if( testRegexp(  format.head_regexp ) )
            break;
      } else
          if( testRegexp(  format.end_regexp ) )
             break;

      if( format.value_next.empty() )
      {
        pos+=1;   // only next symbol
      }
      else
        {
          posb =  test_start-str_buf.begin();
          posb = str_buf.find( format.value_next, posb );
          if( posb != string::npos )
            pos = str_buf.begin()+posb+format.value_next.length();
          else
            pos = test_end;
        }
      value += string( test_start, pos );
      test_start = pos;

      if( pos == test_end )
      {  if( !readNextBlockfromFile() )
           break;
         pos = test_start;
      }
  }
  posb = value.rfind(format.value_next);
  return value.substr(0,posb );
}

/// Read key value pair
bool KeyValueIEFile::readKeyValuePair()
{
    string key, value="";
    vector<string> mach_str;

    // skip comments and blanks
    skipComments();

    // get key
    if( !readRegexp(  format.key_regexp, mach_str ))
      return false;
    key =mach_str[0];

    // skip comments and blanks
    skipComments();

    // get value
    if( !format.value_regexp.empty() )
    {
        if( readRegexp(  format.value_regexp, mach_str ))
           value =mach_str[0];
    }
    else  // value untill end of block or key
    {
       value = readAllValue();
    }

    addKeyValue( key, value );
    return true;
}

/// Inser key value paies to readed list
void KeyValueIEFile::insertKeyValue( const string& _key, const vector<string>& vec )
{
    auto itr = externData.find(_key);
    if( itr != externData.end() )
    {    for( uint ii=0; ii<vec.size(); ii++ )
           itr->second.push_back(vec[ii]);
    }
    else
     externData.insert( pair<string, vector<string>>(_key, vec) );

}

/// Add key value data to Readed Key, Value pairs
void KeyValueIEFile::addKeyValue( const string& _key, const string& _value )
{
    vector<string> vec;
    vec.push_back(_value);

    auto  organization = impexFieldOrganization( _key );

    //  here we test if key is array type in  block.pairs type
    //  we split _value to vector use format.value_part_regexp
    if( organization == "list" ||   organization == "set" ||
        organization == "map" ||     organization == "group"  )
      vec = regexp_split(_value, format.value_token_regexp );

    if( organization == "group"  )
    {
          for( uint ii=0; ii<vec.size(); ii++)
           insertKeyValue( _key+to_string(ii), { vec[ii] } );
          return;
    }
    insertKeyValue( _key, vec );
 }


/// Read data from one block in file
bool KeyValueIEFile::readBlockFromFile( )
{
  if( !readStartBlock() )
      return false;

  int ii=0;
  while( !isEndBlock() )
  {
      if( format.Ndata>0 && ii > format.Ndata )
        return true;
      if( !readKeyValuePair() )
         bsonioErr("ReadKeyValuePair -error ",
                   string(test_start, min(test_start+100, test_end)));
      ii++;
  }
  /* test output
  auto it = externData.begin();
  while( it != externData.end() )
  {
   cout << " key " << it->first ;
   for(uint ii=0; ii< it->second.size(); ii++ )
    cout << "         "  << it->second[ii].c_str() << endl;
   it++;
  }*/
  return true;
}


/// Convert free format bson to schema defined
bool KeyValueIEFile::initBlock(  bson *bsobj,  const map<string,vector<string>>& areadedData )
{
 // read data from block to readedData
 externData = areadedData;
 // reset default structure
 structNode->clearNode();
 // write data to structNode
 setReadedData();

 // structNode to bson
 bson_destroy( bsobj );
 structNode->getStructBson(bsobj);
 // Run script for operation on data values in block
 run_block_script(block.script, bsobj);

 // Set data according schema (+4 to 4)
 structNode->setStructBson(bsobj->data);
 bson_destroy( bsobj );
 structNode->getStructBson(bsobj);

 return true;
}

// export

bool KeyValueIEFile::writeBlockToFile( const map<string,vector<string>>& writeData_ )
{
    externData = writeData_;
    assembleGroups( externData  );
    vector<string> impexValues;
    string impexOrganization, impexType;
    uint impexSize;

    // write head
    vector<string> tokens = regexp_extract( format.head_regexp, "%head\\d+");
    string headstr = format.head_regexp;
    for( auto token: tokens )
    {
        impexValues = extractValue( token.substr(1), externData );
        if( impexValues.size() > 0 )
             headstr = regexp_replace( headstr, token, impexValues[0] );
    }
    _stream << headstr;

    //build order list
    auto orderFields = format.key_order;
    if( orderFields.empty() )
      for( auto item: externData )
      {
        auto impexKey = item.first;
        if( impexKey.find("head") == string::npos && impexKey.find("end") == string::npos)
         orderFields.push_back(impexKey);
      }

    // write matches (use orderFields )
    for( auto impexKey: orderFields )
    {
      auto idom = externData.find(impexKey);
      if( idom == externData.end() ) //not exist
         continue;
      impexValues = idom->second;
      if( impexValues.empty() )
         continue;

      _stream << regexp_replace( format.key_regexp, "%key", impexKey );

      impexOrganization = impexFieldOrganization( impexKey );
      if( impexOrganization=="list" || impexOrganization == "set" ||
              impexOrganization == "group" )
       impexSize = impexValues.size();
      else
       impexSize = 1;

      impexType = impexFieldType( impexKey );
      for( uint ii=0; ii< impexSize; ii++  )
       if( !format.strvalue_exp.empty()  && impexType == "string" )
         _stream << regexp_replace( format.strvalue_exp, "%value", impexValues[ii] ) << format.value_token_regexp;
       else
         _stream << regexp_replace( format.value_regexp, "%value", impexValues[ii] ) << format.value_token_regexp;

      _stream << format.value_next;
    }

    // write end of block
    tokens = regexp_extract( format.end_regexp, "%end\\d+");
    string endstr = format.end_regexp;
    for( auto token: tokens )
    {
        impexValues = extractValue( token.substr(1), externData );
        if( impexValues.size() > 0 )
             endstr = regexp_replace( endstr, token, impexValues[0] );
    }
    _stream << endstr;
   return true;
}



//-----------------------------------------------------------------------

TableIEFile::TableIEFile( int amode, const ThriftSchema* aschema, const string& alualibPath,
           const FormatTableFile& defData ):
           AbstractIEFile( amode, aschema, alualibPath),
           format(defData.format), renderer(defData.renderer)
{
   // set up data from  FormatKeyValueFile !!!
    label = defData.label;
    comment = defData.comment;
    file_name = defData.fname;
    block = defData.block;
    Nblocks = defData.Nblocks;
    Nlines = defData.Nlines;
    structNode = SchemaNode::newSchemaStruct( label, 0 );
    str_buf = "";
    test_start = str_buf.begin();
    test_end = str_buf.end();
}

// Open file to import
void TableIEFile::openFile( const string& inputFname )
{
  if( !inputFname.empty() )
    file_name = inputFname;

   // open  file
  if (_stream.is_open())
  _stream.close();

  if( _mode == modeImport )
     _stream.open(file_name,  std::fstream::in );
  else
     _stream.open(file_name,  std::fstream::out|std::fstream::app );

  bsonioErrIf( !_stream.good() , file_name, "Fileopen error...");

  if( _mode == modeImport )
    readHeaderFromFile();
///  else
///    writeHeaderToFile();

}

// Close internal file
void TableIEFile::closeFile()
{
    if (_stream.is_open())
    _stream.close();
}

/// Read data from one block in file
bool TableIEFile::readBlockFromFile( )
{
  string valuesRow = readNextRow();
  if( valuesRow.empty() )
      return false;

  if( !format.colsizes.empty() )
      splitFixedSize( valuesRow );
  else
      if( !format.value_regexp.empty() )
          splitRegExpr( valuesRow );
      else
          splitDelimiter( valuesRow );

  /* test output
  auto it = externData.begin();
  while( it != externData.end() )
  {
   cout << " key " << it->first ;
   for(uint ii=0; ii< it->second.size(); ii++ )
    cout << "         "  << it->second[ii].c_str() << endl;
   it++;
  }*/

  return true;
}


/// Read data from one block in file
bool TableIEFile::readHeaderFromFile( )
{
  if( format.Nhrows <= 0)  // no headers
    return  true;

  string valuesRow = readNextRow();
  if( valuesRow.empty() )
      return false;

  if( !format.colsizes.empty() )
      splitFixedSize( valuesRow, true );
  else
      //if( !format.value_regexp.empty() )
      //    splitRegExpr( valuesRow, true );
      //else
          splitDelimiter( valuesRow, true );

  if( format.Nhcols == 0 )
      format.Nhcols = format.headers.size();

  // skip rows
  for( int ii=1; ii<format.Nhrows; ii++)
    valuesRow = readNextRow();

  return true;
}

void TableIEFile::skipComments( string& row )
{
  try{
    regex re(format.comment_regexp);
    row = regex_replace(row, re, "");
    }
     catch (regex_error &e)
      {
        cout << "what: " << e.what() << "; code: " << parseCode(e.code()) << endl;
        bsonioErr("Error in Regular Expression", e.what(), format.comment_regexp);
      }
}



string TableIEFile::readNextRow( )
{
    string nextrow;
    while( nextrow.empty() )
    {
        if( _stream.eof() || !_stream.good()  )
            return "";

        char buffer[2049];
        memset(buffer,'\0', 2049 );
        // read next block
        _stream.getline( buffer, 2048, format.rowend[0] );
        nextrow = string(buffer);
        skipComments( nextrow );
    }
    return nextrow;
}

void TableIEFile::addValue( uint col, const string& _value, bool toheader )
{
    if( toheader )
    {
       if( format.headers.size() < col+1 )
       format.headers.resize(col+1);
       format.headers[col] = _value;
       return;
    }

    vector<string> vec;
    vec.push_back(_value);

    string key;
    if( col < format.headers.size() )
        key = format.headers[col];
    if( key.empty())
        key = "clm"+to_string(col);

    externData.insert( pair<string, vector<string>>(key, vec) );
}

void TableIEFile::splitFixedSize( const string& readedrow, bool toheader )
{
  string value, row = readedrow;
  auto colsizes = format.colsizes;
  while( (int)colsizes.size() < format.Nhcols )
    colsizes.push_back(colsizes[0]);

  for( uint ii=0; ii<colsizes.size(); ii++)
  {
      value = row.substr(0, colsizes[ii]);
      row = row.substr( min( colsizes[ii], (int)row.size()));
      addValue( ii, value, toheader );
  }
}

void TableIEFile::splitRegExpr( const string& readedrow, bool toheader )
{
  uint ii=0;
  string value, row = readedrow;
  auto rgex = format.value_regexp;
  while( (int)rgex.size() < format.Nhcols )
    rgex.push_back(rgex[0]);

 try{
    for( ii=0; ii<rgex.size(); ii++)
    {
      regex re(rgex[ii]);
      smatch sm;
      if(regex_search(row, sm, re))
      {
          for (smatch::iterator it = sm.begin(); it!=sm.end(); ++it)
          {
              value = *it;
          }
        //value = sm.str();
        row = sm.suffix();
        addValue( ii, value, toheader);
      }
      else
       addValue( ii, "", toheader);
    }
  }
   catch (regex_error &e)
    {
      cout << "what: " << e.what() << "; code: " << parseCode(e.code()) << endl;
      bsonioErr("Error in Regular Expression", e.what(), rgex[ii] );
    }

}

uint TableIEFile::extractQuoted( const string& row, string& value )
{
     boost::regex re("\"([^\"]+)\"");
     smatch sm;
     if(regex_search(row, sm, re))
     {  value = sm.str();
        return value.length()+2;
     }
     return 0;
}



void TableIEFile::splitDelimiter( const string& readedrow, bool toheader )
{
  uint ii=0;
  string value, row = readedrow;

  string::size_type start = 0;
  auto pos = row.find_first_of(format.colends, start);
  while(pos != string::npos)
  {
      if( !format.usemore || pos != start) // ignore empty tokens
      {
        if(format.usequotes && row[start]== '\"' )
         {
           auto dlt = extractQuoted( string( row, start), value );
           if( dlt )
            pos = start + dlt;
           else
            value = string(row, start, pos - start);
         }
         else
           value = string(row, start, pos - start);

        strip(value);
        addValue( ii++, value, toheader);
      }
      start = pos + 1;
      pos = row.find_first_of(format.colends, start);
  }
  if(start < row.length()) // ignore trailing delimiter
  {
      value =string (row, start, row.length() - start);
      strip(value);
      addValue( ii, value, toheader);
  }
}

bool TableIEFile::writeBlockToFile( const map<string,vector<string>>& writeData )
{
   vector<string> values;
   auto colsizes = format.colsizes;
   auto rgex = format.value_regexp;
   if( !format.colsizes.empty() )
    {
       while( colsizes.size() < format.headers.size() )
         colsizes.push_back(colsizes[0]);
    }
   else
       if( !format.value_regexp.empty() )
       {
         while( rgex.size() < format.headers.size() )
             rgex.push_back(rgex[0]);
       }

   int ii=0;
   for( auto colhead: format.headers )
   {
       values = extractValue( colhead, writeData );
       if( values.empty() )
          values.push_back(" ");

       if( !colsizes.empty() )
          {
            values[0].resize(colsizes[ii++], ' ');
           _stream << values[0];
          }
       else
           if( !rgex.empty() )
              _stream << regexp_replace( rgex[ii++], "%value", values[0] );
           else
              _stream << values[0] << format.colends;
   }
   _stream << format.rowend;
   return true;
}


//-------------------------------------------------------------------------------
// lib functions

string parseCode(/*regex_constants::error_type*/ int etype)
{
  switch (etype)
  {
    case regex_constants::error_collate:
        return "error_collate: invalid collating element request";
    case regex_constants::error_ctype:
        return "error_ctype: invalid character class";
    case regex_constants::error_escape:
        return "error_escape: invalid escape character or trailing escape";
    case regex_constants::error_backref:
        return "error_backref: invalid back reference";
    case regex_constants::error_brack:
        return "error_brack: mismatched bracket([ or ])";
    case regex_constants::error_paren:
        return "error_paren: mismatched parentheses(( or ))";
    case regex_constants::error_brace:
        return "error_brace: mismatched brace({ or })";
    case regex_constants::error_badbrace:
        return "error_badbrace: invalid range inside a { }";
    case regex_constants::error_range:
        return "erro_range: invalid character range(e.g., [z-a])";
    case regex_constants::error_space:
        return "error_space: insufficient memory to handle this regular expression";
    case regex_constants::error_badrepeat:
        return "error_badrepeat: a repetition character (*, ?, +, or {) was not preceded by a valid regular expression";
    case regex_constants::error_complexity:
        return "error_complexity: the requested match is too complex";
    case regex_constants::error_stack:
        return "error_stack: insufficient memory to evaluate a match";
    default:
        return "";
  }
}

} // namespace bsonio
