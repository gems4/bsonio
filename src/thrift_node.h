//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file thrift_node.h
/// Declarations of  SchemaNode class -
/// implementation an API for direct code access to internal
/// DOM based on our JSON schemas.
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef THRIFT_NODE_H
#define THRIFT_NODE_H

#include <queue>
#include "thrift_schema.h"
#include "ar2base.h"
#include "v_json.h"
#include <thrift/protocol/TVirtualProtocol.h>
using namespace ::apache::thrift::protocol;

namespace bsonio {

class SchemaNode
{
    static ThriftFieldDef topfield;

    ThriftStructDef* _strDef;   ///< Struct definition
    ThriftFieldDef*  _fldDef;   ///< Field definition
    bool _isSetUp;              ///< Value set up by user
    string _fldKey;             ///< Key of Node
    string _fldValue;           ///< Value of Node

    SchemaNode *_parent;                      ///< Parent node
    vector<unique_ptr<SchemaNode>> _children; ///< Children nodes for Object or Array

    // internal data
    int _level;                   ///< index into fldDef->fTypeId array
    vector<string> _noUsedFields; ///< Field names can be add

    // ???? internal for GUI
    int _ndx;                     ///< internal ndx

    /// Read string value from bson structure
    void valueFromBson(int type, bson_iterator *it, string& str );
    /// Add levels for objects and arrays
    void addChildren( const char* bson_data );
    /// Define Struct
    void struct2model( ThriftStructDef* strDef, const char *bson_data );
    /// Define Array ( LIST, SET, MAP )
    void list2model(  int level, const char *bson_data );
    /// add field to bson structure
    void list2bson( bson* bsonobj );

    /// Set up default value to field
    void setDefault(const string& newdef, int deflevel );
    /// Set up current time as default value
    void setCurrentTime();

    /// Constructor for struct (up level )
    SchemaNode( ThriftStructDef* aStrDef, const char *bsondata );
    /// Constructor for field
    SchemaNode( SchemaNode* aparent, ThriftFieldDef* afldDef, const char *bsondata );
    /// Constructor for array items
    SchemaNode( SchemaNode* aparent, bson_iterator* it, int alevel );
    /// Copy Constructor
    SchemaNode( const SchemaNode* data );

    /// Resize Array with all levels
    void resizeArray( uint level, const vector<int>& sizes, const string& defval  );
    /// Resize current Array
    void resizeArray( uint size, const string& defval  );

public:

    /// Destructor
    ~SchemaNode()
    {
      _children.clear();
    }

    /// Create new Node for struct name
   static  SchemaNode* newSchemaStruct( const string& className, const char *bsondata = 0 );

   const ThriftStructDef* getThriftStructDef() const
   { return _strDef; }

   // Access Struct node  ------------------------

   /// Set bson object to structure
   void setStructBson( const char *bsondata );
   /// Get bson object from structure
   void getStructBson(  bson* bdata );
   /// Get bson object from field
   void getFieldBson(  bson* bdata );
   /// Get json string from field
   string getFieldJSON();


   /// Get field by fieldpath ("name1.name2.name3")
   SchemaNode *field(  const string& fieldpath );
   /// Get field by fieldpath
   SchemaNode *field( queue<string> names );
   /// Get field by idspath
   SchemaNode *field( queue<int> ids );

   // Work with arrays   ----------------------------

   /// get top level Array sizes
   vector<int> getArraySizes();

   /// Resize top level Array
   void resizeArray( const vector<int>& sizes, const string& defval  = "" )
   {
     if( isArray() && _level == 0 )
     {
         addField();
         resizeArray( 0, sizes, defval  );
     }
     else
      bsonio::bsonioErr("resizeArray" , _fldKey+" is not a top level array" );
   }

   /// Reset top level Array
   void resetArray( const vector<string> vals )
   {
     if( !isArray() || _level != 0 )
      bsonio::bsonioErr("resetArray" , _fldKey+" is not a top level array" );

     if( vals.size()<1 &&  _children.size()<1 )
        return;  // no add empty array

     vector<int> sizes;
     sizes.push_back(vals.size());
     resizeArray( sizes );
     for(uint ii=0; ii< _children.size(); ii++ )
       _children[ii]->setDefault(vals[ii], 2 );
   }

   /// Set Array to Node (must be resized before )
   template <class T>
   void setArray( const vector<T>& value  )
   {
       if( !isArray() )
         bsonio::bsonioErr("setArray" , _fldKey+" is not an array" );

       for( uint ii =0; ii<_children.size(); ii++)
       {
          if( _children[ii]->isArray() )
            _children[ii]->setArray( value[ii]  );
          else
            if( _children[ii]->isStruct() )
                ; // ??????
            else
              _children[ii]->setValue( value[ii] );
       }
   }

   /// Get Array from Node (must be resized before )
   template <class T>
   void getArray( vector<T>& value  )
   {
       if( !isArray() )
         bsonio::bsonioErr("getArray" , _fldKey+" is not an array" );

       for( uint ii =0; ii<_children.size(); ii++)
       {
          if( _children[ii]->isArray() )
            ; // _children[ii]->getArray( value[ii]  ); DM 17.06.2016 - not compiling
          else
            if( _children[ii]->isStruct() )
                ; // ??????
            else
              _children[ii]->getValue( value[ii] );
       }
   }

   // Access current node  ---------------------------

   /// Get Current key from Node
   const string& getKey() const
   {
     return _fldKey;
   }
   /// Get Field Path from Node
   string getFieldPath() const
   {
     if( _parent->isTop() )
        return _fldKey;
     else
        return  _parent->getFieldPath()+"."+_fldKey;
   }


   /// Get field key from Node
   const string& getFieldKey() const
   {
     return _fldDef->fName;
   }

   /// Get field type from Node
   int getType( bool key = false ) const
   {
     if( key )
       return  _fldDef->fTypeId[_parent->_level+1];
     return _fldDef->fTypeId[_level];
   }

   /// Get field elements type from Node
   int elemTypeId() const
   {
     return _fldDef->fTypeId.back();
   }

   /// Get Description from Node
   string getDescription() const
   {
      if( _level == 0 )
        return  _fldDef->fDoc;
     return "";
   }

   /// Return the Node size
   int nodeSize ()
   {
       return _children.size();
   }

   /// Get Description from Node
   string getFullDescription() const;

   /// Get Value from Node
   /// If field is not type T, the false will be returned.
   template <class T>
   bool getValue( T& value  )
   {  return  bsonio::TArray2Base::string2value( _fldValue, value );  }

   /// Set Value to Node
   template <class T>
   void setValue( const T& value  )
   {   string def  =  bsonio::TArray2Base::value2string( value );
      // convert string to internal type
       _fldValue = getDefValue( _fldDef->fTypeId[_level], def );
       addField();
   }


   /// Set Value to Map Key
   template <class T>
   void setMapKey( T& value  )
   {
       if( _parent->isMap() )
       { string def  =  bsonio::TArray2Base::value2string( value );
         // convert string to internal type
         _fldKey = getDefValue( _fldDef->fTypeId[_parent->_level+1], def );
       }
   }

   /// Set Value to Map Key
   template <class T>
   void addMapKeyValue( T& key, const string& value  )
   {
       if( isMap() )
       {
           vector<int> sizes;
           sizes.push_back(_children.size()+1);
           resizeArray( sizes, value );
           _children.back()->setMapKey( key );
       }
   }

   /// Delete Key from map
   void delMapKey( const string& key  )
   {
       if( !isMap() )
        return;

       bool move = false;
       auto it = _children.begin();

       while( it != _children.end() )
       {
          if( move )
             it++->get()->_ndx--;
          else
            if( it->get()->_fldKey == key )
            {   it =  _children.erase( it );
                move = true;
            }
            else
               it++;
        }
   }


   /// Set up string json value to array or struct field
   void setComplexValue(const string& newval )
   {
     setDefault( newval, 2 );
   }

   /// Get min Value to Node
   double minValue() const
   {
     return _fldDef->minval;
   }

   /// Get max Value to Node
   double maxValue() const
   {
     return _fldDef->maxval;
   }

   /// Get def Value to Node
   string getDefault() const
   {
      if( _level == 0 )
       return _fldDef->fDefault;
      return "";
   }

  /// Add field to bson with default values
  void addField()
  {
    _isSetUp = true;
    if( _parent != nullptr )
     _parent->addField();
  }

  /// Remove field from bson, set field to defValue
  /// return false if requred field
  bool removeField()
  {
    if( _parent->isMap() )
    {
      _parent->delMapKey( _fldKey  );
      return true;
    }
    else
    {
      if( _fldDef->fRequired == fld_required )
        return false;

      setDefault("", 0 );
      _isSetUp = false;
      return true;
    }
  }

   /// Test top Node
   bool isTop() const
   { return( _parent==nullptr); }

   /// Test Node as Map
   bool isMap() const
   {
      if( isTop() )
        return false;
      return( _fldDef->fTypeId[_level] == T_MAP );
    }

    /// Test Node as Struct
    bool isStruct() const
    { return( isTop() ||  _fldDef->fTypeId[_level] == T_STRUCT ); }

    /// Test Node as Array
    bool isArray() const
    {
        if( isTop() )
          return false;
        return( _fldDef->fTypeId[_level] == T_MAP ||
                _fldDef->fTypeId[_level] == T_SET ||
                _fldDef->fTypeId[_level] == T_LIST );
    }

    /// Test Node as numeric value
    bool isNumber( ) const
    {
      if( isTop() )
          return false;
      return( _fldDef->fTypeId[_level] >= T_I08 &&
              _fldDef->fTypeId[_level] <= T_I64 );
    }

    bool isUnion( ) const
    {
        return ( _parent->isStruct() && _parent->_strDef->testUnion() );
    }

    bool isReqiered( ) const
    {
        return ( _level==0 && _fldDef->fRequired == fld_required );
    }

    bool isSetUp() const
    {
        return _isSetUp;
    }

    string getEnumName() const
    {
        if( _fldDef->fTypeId[_level] == T_I32  )
           return  _fldDef->className;
        else
           return "";
    }

    /// Reset Node to default values
    void clearNode()
    {
       setDefault("", 2 );
    }

    /// Get children number
    uint getChildNum() const
    { return _children.size(); }

    /// Get children by id
    SchemaNode *children( int id )
    { return _children[id].get(); }

};

template <>  void SchemaNode::setValue( const string& value  );

} // namespace bsonio

#endif // THRIFT_NODE_H
