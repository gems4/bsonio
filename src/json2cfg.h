//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file json2cfg.h
/// Declarations&Implementation of TFileBase, FJson, ConfigJson,
/// ConfigArray - classes for file manipulation
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef JSON2CFG_H
#define JSON2CFG_H

#include <unistd.h>
#include <memory>
#include "v_json.h"
#include "nservice.h"

namespace bsonio {

typedef ios::openmode FileStatus;
const int 	MAX_FILENAME_LEN = 20;

enum FileTypes {

 Undef_ = 'u', Txt_ = 't', Json_ = 'j', Yaml_ = 'y',
 XML_ = 'x', EJDB_ = 'e', Binary_ = 'b', GDS_ = 'g' // GemDataStream
};

/// This enum is used to describe the mode in which a device is opened
enum OpenModeTypes {
  NotOpen,       ///<	The device is not open.
  ReadOnly,      ///<	The device is open for reading.
  WriteOnly, 	 ///<	The device is open for writing.
  ReadWrite,     ///<	ReadOnly | WriteOnly	The device is open for reading and writing.
  Append	     ///<	The device is opened in append mode so that all data is written to the end of the file.
};

/// Base class for file manipulation
class TFileBase
{

protected:

    char Keywd_[MAX_FILENAME_LEN+1];
    bool  isopened_ = false;    ///<  true, if file is opened
    string dir_;
    string name_;
    string ext_;
    string path_;
    char type_ = FileTypes::Undef_;
    int  mode_ = OpenModeTypes::ReadWrite;

    virtual void makeKeyword();
    void makePath()
    { path_ = u_makepath( dir_, name_, ext_ ); }

public:

    /// Constructor
    //TFileBase();
    /// Constructor
    TFileBase(const string& fName, const string& fExt, const string& fDir="" );
    /// Constructor from path
    TFileBase(const string& path);
    /// Constructor from bson data
    TFileBase( const char* bsobj );
    virtual ~TFileBase()
    {
      //if( isopened_ )
      //  Close();
    }

    virtual void toBson( bson *obj ) const;
    virtual void fromBson( const char *obj );

    //--- Selectors

    const char* getKeywd() const
    { return Keywd_;  }
    bool testOpen() const
    { return isopened_; }

    const char*  Name() const
    { return name_.c_str();  }
    const char*  Ext() const
    { return ext_.c_str();    }
    const char*  Dir() const
    { return dir_.c_str();    }
    const char*  Path() const
    { return path_.c_str();  }
    char Type() const
    { return type_;  }

    //--- Manipulation

    bool Exist();

    void reOpen( const string& path  )
    {
      if( !path.empty() && path != path_ )
      {
        Close();
        path_ = path;
        u_splitpath(path_, dir_, name_, ext_);
        makeKeyword();
      }
      Open(mode_);
    }

    void reOpen( const string& fName, const string& fExt )
    {
      if( ( !fName.empty() && fName !=name_ ) || (!fExt.empty()  && fExt !=ext_ ) )
      {
        Close();
        name_ = fName;
        ext_ = fExt;
        makePath();
        makeKeyword();
      }
      Open(mode_);
    }

    virtual void Close() { isopened_ = false; }
    virtual void Open(int amode ) { mode_ = amode; isopened_ = true; }
};

/// Class for read/write json/yaml/xml files
class FJson : public TFileBase
{

protected:

   /// Load data from json file to bson object
   void loadJson( bson *bobj );

   /// Save data from bson object to  json file
   void saveJson( const char *bobj );

   /// Load data from yaml file to bson object
   void loadYaml( bson *bobj );

   /// Save data from bson object to yaml file
   void saveYaml( const char *bobj );

   /// Load data from xml file to bson object
   void loadXml(  bson *bobj );

   /// Save data from bson object to xml file
   void saveXml(  const char *bobj );


public:

   static char defType;	///< Default cfg file type

   /// Constructor
    FJson( const string& fName, const string& fExt, const string& fDir=""):
       TFileBase( fName, fExt, fDir )
    { setType(getType(ext_));}
   /// Constructor from path
    FJson( const string& path): TFileBase( path )
    { setType(getType(ext_));}
   /// Constructor from bson data
    FJson( const char* bsobj ):TFileBase( bsobj ) {}
   /// Destructor
    ~FJson() { }

   /// Setup file type
   void setType(char ftype )
   {
     if( !( ftype == FileTypes::Json_ ||  ftype == FileTypes::Yaml_ || ftype == FileTypes::XML_ ) )
       bsonioErr( "FJson", "Illegal file type" );
       type_ = ftype;
   }

   /// Get file type from extension
   static char getType(const string& fExt )
   {
      char type__ =  defType;
      if(fExt == "json" )
        type__ = FileTypes::Json_;
      else  if(fExt == "yaml" )
              type__ = FileTypes::Yaml_;
      else if(fExt == "xml" )
              type__ = FileTypes::XML_;
      return type__;
   }


   /// Load data from file to bson object
   void LoadBson( bson *bobj );

   /// Save data from bson object to file
   void SaveBson( const char *bobj );

};

enum cfgval_types
{
   cfg_string = 's',
   cfg_int    = 'i',
   cfg_double = 'd',
   cgf_bool   = 'b'
};

/// Values in ConfigArray
class CfgVal
{
  char _type;     // type of value ( cfgval_types )
  string _svalue;
  double _dvalue;

public:

  CfgVal( const string& str = "" ):
      _type(cfg_string), _svalue(str), _dvalue(0.)
  { }
  CfgVal( int val ):
      _type(cfg_int), _svalue(""), _dvalue(val)
  { }
  CfgVal( double val ):
      _type(cfg_double), _svalue(""), _dvalue(val)
  { }
  CfgVal( bool val ):
      _type(cgf_bool), _svalue("false"), _dvalue(val)
  {
    _svalue = ( val ? "true" : "false");
  }

  string toString() const
  {
    if( _type == cfg_string )
      return  _svalue;
    else
      return to_string(_dvalue);
  }

  int toInt() const
  {
    return (int) _dvalue;
  }

  double toDouble() const
  {
    return _dvalue;
  }

  bool toBool() const
  {
    return (_svalue=="true");
  }

  char type() const
  {
    return _type;
  }

};

// After changing the TObject class we must change ConfigJson class
// In future may be used sections (json levels, arrays and other)

/// Class for read/write configuration files
class ConfigJson : public FJson
{
   map<string,CfgVal>  cfgData;

   /// Load configuration data from file
   void loadCfg();

public:

    ConfigJson( const string& fname ): FJson(fname)
    {
      if( !access(Path(), 0 ) ) //file exists
        loadCfg();
    }

    virtual ~ConfigJson()
    {
      saveCfg();
    }

    /// Save configuration data to file
    void saveCfg();

    /// Sets the value of setting key to value.
    /// If the key already exists, the previous value is overwritten.
    template <class T>
    void setValue( const string& key, const T& value )
    {
       cfgData[key] = value;
    }

    /// Gets the value of setting key
    /// If the key not exists, the defvalue returned.
    template <class T>
    T getValue( const string& key, const T& defvalue ) const
    {
        auto itr = cfgData.find(key);
        if ( itr == cfgData.end() )
             return defvalue;
        return itr->second.toDouble();
    }

    /// Gets the value of setting key
    /// If the key not exists, the defvalue returned.
    string getValue( const string& key, const string& defvalue ) const
    {
        auto itr = cfgData.find(key);
        if ( itr == cfgData.end() )
             return defvalue;
        return itr->second.toString();
    }

    /// Gets the value of setting key
    /// If the key not exists, the defvalue returned.
    string getValue( const char *key, const char* defvalue ) const
    {
        auto itr = cfgData.find(key);
        if ( itr == cfgData.end() )
             return defvalue;
        return itr->second.toString();
    }

    /// Gets the value of setting key
    /// If the key not exists, the defvalue returned.
    string getString( const char *key, const string& defvalue ) const
    {
        auto itr = cfgData.find(key);
        if ( itr == cfgData.end() )
             return defvalue;
        return itr->second.toString();
    }

    /// Gets the value of setting key
    /// If the key not exists, the defvalue returned.
    bool getBool( const char *key, const bool& defvalue ) const
    {
        auto itr = cfgData.find(key);
        if ( itr == cfgData.end() )
             return defvalue;
        return itr->second.toBool();
    }

};

/// Class for read/write configuration arrays
template <class T>
class ConfigArray : public FJson
{

    /// Save configuration data to file
    void array2Bson( bson *obj, const vector<T>& arr )
    {
        int ii=0;
        for(auto itr = arr.begin(); itr != arr.end(); ++itr, ++ii )
        {
            //IsUnloadToBson* itt = dynamic_cast<IsUnloadToBson>(itr);
            //if(itt)
            {    bson_append_start_object( obj, to_string(ii).c_str() );
                 /*itt*/ itr->toBson(obj);
                 bson_append_finish_object(obj);
            }
        }
    }

    /// Load configuration data to array
    void bson2Array( const char* bsobj, vector<T>& arr )
    {
        bson_iterator i;
        const char *key;
        bson_iterator_from_buffer(&i, bsobj);
        while (bson_iterator_next(&i))
        {
            bson_type t = bson_iterator_type(&i);
            bsonioErrIf( t != BSON_OBJECT, "bson2Array", "Error reading cfg file..." );
            key = bson_iterator_key(&i);
            arr.push_back( T(bson_iterator_value(&i)) );
        }
    }

    /// Save configuration data to file
    void array2Bson( bson *obj, const vector<unique_ptr<T>>& arr )
    {
        int ii=0;
        for(auto itr = arr.begin(); itr != arr.end(); ++itr, ++ii )
        {
            //IsUnloadToBson* itt = dynamic_cast<IsUnloadToBson>(itr);
            //if(itt)
            {    bson_append_start_object( obj, to_string(ii).c_str() );
                 /*itt*/ itr->get()->toBson(obj);
                 bson_append_finish_object(obj);
            }
        }
    }

    /// Load configuration data to array
    void bson2Array( const char* bsobj, vector<unique_ptr<T>>& arr )
    {
        bson_iterator i;
        const char *key;
        bson_iterator_from_buffer(&i, bsobj);
        while (bson_iterator_next(&i))
        {
            bson_type t = bson_iterator_type(&i);
            bsonioErrIf( t != BSON_OBJECT, "bson2Array", "Error reading cfg file..." );
            key = bson_iterator_key(&i);
            arr.push_back( unique_ptr<T>(new T(bson_iterator_value(&i))) );
        }
    }

 public:

    ConfigArray( const string& fname ):FJson(fname) {}
    /// Constructor from bson data
    ConfigArray( const char* bsobj ):FJson( bsobj ) {}

    virtual ~ConfigArray() {}

    /// Save configuration data to file
    void saveArray2Cfg( const vector<T>& arr )
    {
        bson obj;
        bson_init( &obj );
        array2Bson( &obj,  arr );
        bson_finish( &obj );

        // save data
        SaveBson( obj.data );
        bson_destroy(&obj);
    }

    /// Load configuration data to array
    void loadCfg2Array( vector<T>& arr )
    {
        // get data from file
        bson obj;
        LoadBson( &obj );
        bson2Array( obj.data, arr );
        bson_destroy(&obj);
    }

    /// Save configuration data to file
    void saveArray2Cfg( const vector<unique_ptr<T>>& arr )
    {
        bson obj;
        bson_init( &obj );
        array2Bson( &obj,  arr );
        bson_finish( &obj );

        // save data
        SaveBson( obj.data );
        bson_destroy(&obj);
    }

    /// Load configuration data to array
    void loadCfg2Array( vector<unique_ptr<T>>& arr )
    {
        // get data from file
        bson obj;
        LoadBson( &obj );
        bson2Array( obj.data, arr );
        bson_destroy(&obj);
    }

};


/// Class for read/write json/yaml/xml arrays files
class FJsonArray : public TFileBase
{

   fstream _stream;      ///< Stream to input/output
   int ndx_ = 0;

   // read bson records array from xml file
   bson xmlobj;
   bson_iterator ixml;

protected:

   /// Reading object from YAML text file
   bool next_from_yaml_file( bson *obj );
   /// Reading object from json text file
   bool next_from_json_file( bson *obj );
   /// Reading object from xml text file
   bool next_from_xml_file( bson *obj );

   /// Saving object to YAML text file
   void next_to_yaml_file( const bson *bobj );
   /// Saving object to json text file
   void next_to_json_file( const bson *bobj );
   /// Saving object to xml text file
   void next_to_xml_file( const bson *bobj );

public:

    /// Constructor
    FJsonArray( const string& fName, const string& fExt, const string& fDir=""):
       TFileBase( fName, fExt, fDir )
    { setType(FJson::getType(ext_));}
   /// Constructor from path
    FJsonArray( const string& path): TFileBase( path )
    { setType(FJson::getType(ext_));}
   /// Constructor from bson data
    FJsonArray( const char* bsobj ):TFileBase( bsobj ) {}
   /// Destructor
    ~FJsonArray()
    {
        if( testOpen() )
            Close();
    }

   /// Setup file type
   void setType(char ftype )
   {
     if( !( ftype == FileTypes::Json_ ||
            ftype == FileTypes::Yaml_ ||
            ftype == FileTypes::XML_ ) )
       bsonioErr( "FJsonArray", "Illegal file type" );
       type_ = ftype;
   }

   /// Load data from file to bson object
   bool LoadNext( bson *bobj );

   /// Save data from bson object to file
   void SaveNext( const bson *bobj );

   void Close();
   void Open(int amode );

};


} // namespace bsonio

#endif // JSON2CFG_H
