//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbedgedoc.h
/// Declarations of class TDBGraph - working with graph databases (OLTP)
/// Used  SchemaNode class - API for direct code access to internal
/// DOM based on our JSON schemas.
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TDBEDGE_H
#define TDBEDGE_H

#include "dbvertexdoc.h"

namespace bsonio {


/// Definition of graph databases chain
class TDBEdgeDocument : public TDBVertexDocument
{
    virtual void beforeRm( const char *  ) {}

 protected:

    /// Build default json query string for collection
    string makeDefaultQuery();

    /// Type constructor
    TDBEdgeDocument( const string& schemaName, const TDataBase* dbconnect,
                       const string& coltype, const string& colname,
                       const vector<KeyFldsData>& keyFldsInf ):
      TDBVertexDocument( schemaName, dbconnect, coltype, colname, keyFldsInf )
   { }

 public:

    static TDBEdgeDocument* newDBEdgeDocument( const TDataBase* dbconnect,
                  const string& schemaName, const string& queryString = ""  );

    ///  Constructor collection&document
    TDBEdgeDocument( const string& aschemaName, const TDataBase* dbconnect );
    ///  Constructor document
    TDBEdgeDocument( const string& schemaName, TDBCollection* collection  );
    /// Constructor from bson data
    TDBEdgeDocument( TDBCollection* collection, const char* bsobj );

    ///  Destructor
    virtual ~TDBEdgeDocument(){}

    /// Make Follow Outgoing Edges query
    string outEdgesQuery( const string& id ) const
    {
        return string("{\"_type\": \"edge\", \"_outV\": \"")+ id + "\" }";
    }
    /// Make Follow Incoming Edges query
    string inEdgesQuery( const string& id ) const
    {
        return string("{\"_type\": \"edge\", \"_inV\": \"")+ id + "\" }";
    }

    /// Test existence Outgoing Edges
    bool existOutEdges( const string& id )
    {
      return existKeysByQuery( outEdgesQuery( id ) );
    }
    /// Build Outgoing Edges keys list
    vector<string> getOutEdgesKeys( const string& id )
    {
      return getKeysByQuery( outEdgesQuery( id ) );
    }

    /// Test existence Incoming Edges
    bool existInEdges( const string& id )
    {
       return existKeysByQuery( inEdgesQuery( id ) );
    }
    /// Build Incoming Edges keys list
    vector<string> getInEdgesKeys( const string& id )
    {
       return getKeysByQuery( inEdgesQuery( id ) );
    }

    // build functions

    /// Define new Edge
    void setEdge( const string& aschemaName, const string& outV,
                  const string& inV, const FieldSetMap& fldvalues );
    /// Add new Edge to database
    /// Return oid of new record
    string addNewEdge( const string& aschemaName, const string& outV,
                         const string& inV, const FieldSetMap& fldvalues, bool testValues )
    {
        setEdge( aschemaName, outV, inV, fldvalues );
        return InsertCurrent( testValues );
    }

};

TDBEdgeDocument* documentAllEdges( const TDataBase* dbconnect );

} // namespace bsonio

#endif // TDBEDGE_H
