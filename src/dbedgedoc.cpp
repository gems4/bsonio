//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbgraph.cpp
/// Implementation of classes TGraphAbstract - working with graph databases (OLTP)
/// TGraphEJDB - working with internal EJDB data base in graph mode
/// Used  SchemaNode class - API for direct code access to internal
/// DOM based on our JSON schemas.
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include "dbedgedoc.h"
#include "io_settings.h"

namespace bsonio {

TDBEdgeDocument* TDBEdgeDocument::newDBEdgeDocument( const TDataBase* dbconnect,
              const string& schemaName, const string& queryString  )
{
   if( schemaName.empty()  )
          return nullptr;
   TDBEdgeDocument* newDov = new TDBEdgeDocument( schemaName, dbconnect );
   if( !queryString.empty() )
            newDov->SetQueryJson(queryString);
   newDov->runQuery();
   return newDov;
}

TDBEdgeDocument* documentAllEdges( const TDataBase* dbconnect )
{
   string schemaName = ioSettings().Schema()->getEdgesList()[0];
   TDBEdgeDocument* edgeDoc = new TDBEdgeDocument( schemaName, dbconnect  );
   edgeDoc->resetMode(true);
   return edgeDoc;
}


TDBEdgeDocument::TDBEdgeDocument( const string& aschemaName, TDBCollection* collection  ):
  TDBVertexDocument( aschemaName, collection )
{
   resetSchema( aschemaName, true );
   // set query data
}

TDBEdgeDocument::TDBEdgeDocument( const string& aschemaName, const TDataBase* dbconnect ):
    TDBEdgeDocument( aschemaName,  dbconnect, "edge",
               collectionNameFromSchema( aschemaName ).c_str(), graphKeyFldsInf )
{
   resetSchema( aschemaName, true );
   // set query data
}


/// Constructor from bson data
TDBEdgeDocument::TDBEdgeDocument( TDBCollection* collection, const char* bsobj ):
    TDBVertexDocument( collection, bsobj )
{
    resetSchema( _schemaName, true );
    // set query data
    SetDefaultQueryFields(makeDefaultQueryFields(_schemaName));
    SetDefaultQueryJson(makeDefaultQuery());
}

string TDBEdgeDocument::makeDefaultQuery()
{
   string queryJson =  "{'_label': '";
    queryJson += _label;
    queryJson += "' }";

   return queryJson;
}


// Define new Edge
void TDBEdgeDocument::setEdge( const string& aschemaName, const string& outV,
              const string& inV, const FieldSetMap& fldvalues )
{
    // check schema
    if( _schemaName != aschemaName )
    {
      if( _changeSchemaMode )
          resetSchema( aschemaName, false );
      else
        bsonioErr("TGRDB0007", "Illegal record schame: " + aschemaName + " current schema: " + _schemaName );
    }

   _currentData->clearNode(); // set default values
   setValue("_inV", inV);
   setValue("_outV", outV);
   for(auto const &ent : fldvalues)
     setValue( ent.first, ent.second  );
}


} // namespace bsonio
