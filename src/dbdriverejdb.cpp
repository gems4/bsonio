//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbdriverejdb.cpp
/// Implementation of classes
/// TEJDBFile - working with internal EJDB data base
/// TEJDBDrive - DB driver for working with EJDB data base
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include <cstring>
#include "dbdriverejdb.h"
#include "dbclient.h"

namespace bsonio {

string TEJDBDriveOne::flEJPath =  "ejdbdata";

TEJDBDriveOne& ejdbDriveOne()
{
 static TEJDBDriveOne drive_ejdb;
 return drive_ejdb;
}

EJCOLL *TEJDBDrive::openCollectionFile( const char* collname, bool createifempty )
{
    // Test and open file  (name of ejdb must be take from nFile)
    flEJ.Open();
    if( !flEJ.ejDB )
    {
        string err= "Cannot open EJDB ";
        err += flEJ.Name();
        bsonioErr("TGRDB0010", err ); //return NULL;
    }

    EJCOLL *coll;
    if( createifempty )
    {  // Get or create collection 'module name'
       coll = ejdbcreatecoll(flEJ.ejDB, collname, NULL );
       if( !coll )
       {
         closeCollectionFile(collname);
         string err= "Cannot open EJDB collection ";
         err += flEJ.Name();
         err += collname;
         bsonioErr("TGRDB0011", err );
       }
     }
     else
        coll = ejdbgetcoll(flEJ.ejDB, collname);

     return coll;
}

void TEJDBDrive::closeCollectionFile(const char* )
{
    flEJ.Close();
}

void TEJDBDrive::getBsonOid( bson_oid_t *oid, char* pars )
{
   bson_oid_from_string( oid, (char *)pars );
}

void TEJDBDrive::setBsonOid( bson_oid_t *oid, KeysSet::iterator& itr )
{
  char *bytes = new char[25];
  bson_oid_to_string( oid, bytes );
  itr->second = unique_ptr<char>(bytes);
}

// Retrive one record from the collection
bson *TEJDBDrive::loadbson( const char* collname, KeysSet::iterator& itr )
{
    // Get oid of record
    bson_oid_t oid;
    getBsonOid( &oid, itr->second.get() );
    // Get current collection file
    EJCOLL *coll = openCollectionFile( collname, false );
    // Read bson structure from collection by index oid
    bson *bsrec = ejdbloadbson(coll, &oid);
    // Close database
    closeCollectionFile( collname );
    return bsrec;
}

// Removes record from the collection
bool TEJDBDrive::rmbson( const char* collname, KeysSet::iterator& itr )
{
    // Get oid of record
    bson_oid_t oid;
    getBsonOid( &oid, itr->second.get() );
    // Get current collection file
    EJCOLL *coll = openCollectionFile( collname, true );
    // Remove BSON object from collection.
    bool iRet = ejdbrmbson( coll,  &oid );
    // Close database
    closeCollectionFile( collname );
    return iRet;
}

// Save/update record in the collection
bool TEJDBDrive::savebson( const char* collname, KeysSet::iterator& itr, bson *bsrec, string& errejdb )
{
   bson_oid_t oid;
   // Get current collection file
   EJCOLL *coll = openCollectionFile( collname, true );
   // Persist BSON object in the collection
   bool retSave = ejdbsavebson( coll, bsrec, &oid );
   // Close database
   closeCollectionFile( collname );

   if( !retSave )
       errejdb = bson_first_errormsg(bsrec);
   else
       if( itr->second.get() == nullptr ) // new record
           setBsonOid(&oid, itr);
   return retSave;
}

// Run query
//  \param _queryJson - json string with where condition
//  \param  queryFields - list fields
//  \return _resultData - list of json strings with query result
void TEJDBDrive::selectQuery( const char* collname, const set<string>& fields,
                           const string&  queryJson,  SetReadedFunction setfnc )
{
    flEJ.Open();

    EJDB *ejDB = flEJ.ejDB;
    if( !flEJ.ejDB )
     return;

    EJCOLL *coll = ejdbgetcoll( ejDB, collname );
    if( !coll )
     return;

    // select view fields
    bson bshits1;
    bson_init_as_query(&bshits1);
    bson_append_start_object(&bshits1, "$fields");
    bson_append_int(&bshits1, JDBIDKEYNAME, 1);
    auto it = fields.begin();
    while( it!= fields.end())
        bson_append_int(&bshits1, (it++)->c_str(), 1);
     bson_append_finish_object(&bshits1);
     bson_finish(&bshits1);

    // select all records
    string _queryJson = replace_all( queryJson, "\'", "\"");
    bson bsq1;
    bson_init_as_query(&bsq1);
    if( !_queryJson.empty() )
    { ParserJson pars;
      pars.setJsonText( _queryJson.substr( _queryJson.find_first_of('{')+1 ) );
      pars.parseObject( &bsq1 );
    }
    bson_finish(&bsq1);

    EJQ *q1 = ejdbcreatequery( ejDB, &bsq1, NULL, 0, &bshits1 );
    bsonioErrIf( !q1, "TGRDB0014", "Error in query (test)" );

    uint32_t count = 0;
    TCXSTR *log = tcxstrnew();
    TCLIST *q1res = ejdbqryexecute(coll, q1, &count, 0, log);

    //cout << count << " records in collection " << getKeywd() << endl;
    for (int i = 0; i < TCLISTNUM(q1res); ++i)
    {
        void *bsdata = TCLISTVALPTR(q1res, i);
        // save readed
        setfnc( (const char *)bsdata );
    }

    bson_destroy(&bshits1);
    bson_destroy(&bsq1);
    tclistdel(q1res);
    tcxstrdel(log);
    ejdbquerydel(q1);

    //?? flEJ.Close();
}


void TEJDBDrive::allQuery( const char* collname, const set<string>& fields,
                            SetReadedFunctionKey setfnc )
{
    flEJ.Open();

    EJDB *ejDB = flEJ.ejDB;
    if( !flEJ.ejDB )
     return;

    EJCOLL *coll = ejdbgetcoll( ejDB, collname );
    if( !coll )
     return;

    // select view fields
    bson bshits1;
    bson_init_as_query(&bshits1);
    bson_append_start_object(&bshits1, "$fields");
    bson_append_int(&bshits1, JDBIDKEYNAME, 1);
    auto it = fields.begin();
    while( it!= fields.end())
        bson_append_int(&bshits1, (it++)->c_str(), 1);
     bson_append_finish_object(&bshits1);
     bson_finish(&bshits1);

    // select all records
    bson bsq1;
    bson_init_as_query(&bsq1);
    bson_finish(&bsq1);

    EJQ *q1 = ejdbcreatequery( ejDB, &bsq1, NULL, 0, &bshits1 );
    bsonioErrIf( !q1, "TGRDB0014", "Error in query (test)" );

    uint32_t count = 0;
    TCXSTR *log = tcxstrnew();
    TCLIST *q1res = ejdbqryexecute(coll, q1, &count, 0, log);

    //cout << count << " records in collection " << getKeywd() << endl;
    for (int i = 0; i < TCLISTNUM(q1res); ++i)
    {
        void *bsdata = TCLISTVALPTR(q1res, i);
        // make oid
        bson_iterator it;
        bson_find_from_buffer(&it, (const char *)bsdata, JDBIDKEYNAME );
        char *oidhex = new char[25];
        bson_oid_to_string(bson_iterator_oid(&it), oidhex);

        // save readed
        setfnc( (const char *)bsdata, oidhex );
    }

    bson_destroy(&bshits1);
    bson_destroy(&bsq1);
    tclistdel(q1res);
    tcxstrdel(log);
    ejdbquerydel(q1);

    //?? flEJ.Close();
}


void TEJDBDrive::delQuery(const char* collname, bson* bsq1 )
{
    flEJ.Open();

    EJDB *ejDB = flEJ.ejDB;
    if( !flEJ.ejDB )
     return;

    EJCOLL *coll = ejdbgetcoll( ejDB, collname);
    if( !coll )
     return;

    // bshits1 init query to fields
    // select view fields
    bson bshits1;
    bson_init_as_query(&bshits1);
    bson_append_start_object(&bshits1, "$fields");
    bson_append_int(&bshits1, JDBIDKEYNAME, 1);
    bson_append_finish_object(&bshits1);
    bson_finish(&bshits1);
    // bsq1 delete all edges query

    EJQ *q1 = ejdbcreatequery( ejDB, bsq1, NULL, 0, &bshits1 );
    bsonioErrIf( !q1, "TGRDB0014", "Error in query (test)" );

    uint32_t count = 0;
    TCXSTR *log = tcxstrnew();
    TCLIST *q1res = ejdbqryexecute(coll, q1, &count, 0, log);

    cout << count << " !!!! Delete edges records into collection " << collname << endl;
    /*for (int i = 0; i < TCLISTNUM(q1res); ++i)
    {
        void *bsdata = TCLISTVALPTR(q1res, i);
    }*/

    bson_destroy(&bshits1);
    tclistdel(q1res);
    tcxstrdel(log);
    ejdbquerydel(q1);
    //?? flEJ.Close();
}


void TEJDBDrive::fpathQuery( const char* collname, const string& fpath, const string& queryJson, vector<string>& values )
{
   flEJ.Open();

   EJDB *ejDB = flEJ.ejDB;
   if( !flEJ.ejDB )
    return;

   EJCOLL *coll = ejdbgetcoll( ejDB, collname );
   if( !coll )
    return;

   // select all records
   string _queryJson = replace_all( queryJson, "\'", "\"");
   bson bsq1;
   bson_init_as_query(&bsq1);
   if( !_queryJson.empty() )
   { ParserJson pars;
     pars.setJsonText( _queryJson.substr( _queryJson.find_first_of('{')+1 ) );
     pars.parseObject( &bsq1 );
   }
   bson_finish(&bsq1);

   uint32_t count = 0;
   TCXSTR *log = tcxstrnew();
   bson *q1res;
   q1res = ejdbqrydistinct(coll, fpath.c_str(), &bsq1, NULL, 0, &count, log);
   bsonioErrIf( !q1res, "TGRDB0015", "Error in query (test)" );
   //

   values.clear();
   string value;
   bson_iterator i;
   bson_iterator_from_buffer(&i, q1res->data);
   while (bson_iterator_next(&i))
   {
       const char* key = bson_iterator_key(&i);
       if( !bson_to_string( q1res->data, key, value ) )
         value = "";
       values.push_back(value);
   }

   bson_del(q1res);
   bson_destroy(&bsq1);
   tcxstrdel(log);
}


// Change current bson before save
void TEJDBDrive::addtobson( bson* bsdata, char* pars )
{

    bson_oid_t oid;
    bson_type idtype = _bsonoidkey(bsdata, &oid);

    if( pars )
    {
        if( idtype ==  BSON_OID )
        {
          char bytes[25];
          bson_oid_to_string( &oid, bytes );
          if( string(bytes) != string((char *)pars) )
            bsonioErr("TEJDB0045", string("Changed record oid ") + pars+ " : " +bytes );
        } else // add oid
          {
            bson_oid_from_string( &oid, (char *)pars );
            bson_append_oid( bsdata, JDBIDKEYNAME, &oid);
          }
     } else
       {
         if( idtype !=  BSON_OID )
         {
            bson_oid_gen( &oid );
            bson_append_oid( bsdata, JDBIDKEYNAME, &oid);
         }
       }
}



//-- TEJDBFile -----------------------------------------------------

TEJDBFile::TEJDBFile( const string& fName, const string& fExt, const string& fDir ):
   TFileBase( fName, fExt, fDir ),  ejDB(0)
{
     type_ = FileTypes::EJDB_;
     makeKeyword();
}

// Constructor from path
TEJDBFile::TEJDBFile( const string& path ):
    TFileBase( path ),  ejDB(0)
{
     type_ = FileTypes::EJDB_;
     makeKeyword();
}

// Configurations from file path
TEJDBFile::TEJDBFile( const char* bsobj ):
        TFileBase(bsobj),  ejDB(0)
{}


TEJDBFile::~TEJDBFile()
{
    if( testOpen() )
        Close();
}

// Make keyword of DB internal file
void TEJDBFile::makeKeyword()
{
    string key;
    if( name_.empty() )
        return;

    string fname = name_;

    key = string(fname, 0, 2);
    size_t npos = 0;
    size_t npos2 = fname.find("_", npos);
    while( npos2 != string::npos )
    {   npos = npos2+1;
        key += string(fname, npos, 2);
        npos2 = fname.find("_", npos);
    }
    key += string(fname, npos+2);

    strncpy( Keywd_, key.c_str(), MAX_FILENAME_LEN-2);
    Keywd_[MAX_FILENAME_LEN-2]='\0';
}

/// Open file in special mode
void TEJDBFile::Open( int amode )
{
    mode_ = amode;
    if( !testOpen() )
    {
      // Test and open file  (name of ejdb must be take from nFile)
      ejDB = ejdbnew();
      if (!ejdbopen(ejDB, Path(), JBOWRITER | JBOCREAT ))
      {
        ejdbdel(ejDB);
        cout << " ejdbopen error " << Path() << endl;
        ejDB = 0;
        isopened_ = false;
        return;
       }
       isopened_ = true;
    }
}


// Close EJ DataBase
void TEJDBFile::Close()
{
    if(ejDB )
    { ejdbclose(ejDB);
      ejdbdel(ejDB);
      ejDB = 0;
    }
    isopened_ = false;
}

// Create PDB file
void TEJDBFile::Create( bool /*isDel*/ )
{
    Open( mode_ );
    Close();
    // make changelog.txt file
    string clfile = dir_ + "/Changelog.txt";
    fstream ff( clfile.c_str(), ios::out);
    ff << "File " << name_.c_str() << " created on " << curDate().c_str() << " " << curTime().c_str() << endl;
    ff << "<Version> = v0.1" << endl;
}


// Read version from Changelog.txt file
void TEJDBFile::readVersion( )
{
    // open changelog.txt file
    string clfile = dir_ + "/Changelog.txt";
    fstream ff( clfile.c_str(), ios::in);
    string fbuf;
    size_t pos;
    version = "not versioned";

    while( ff.good() )
    {
      getline ( ff ,fbuf);
      pos = fbuf.find("<Version>");
      if( pos != string::npos )
      {
         pos = fbuf.find("=", pos+1);
         if( pos != string::npos )
         {
           version = fbuf.substr(pos+1);
           strip( version);
           break;
         }
      }
    }
}


} // namespace bsonio

