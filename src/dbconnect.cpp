//  This is BSONIO library+API (https://bitbucket.org/gems4/bsonio)
//
/// \file dbbase.cpp
/// Implementation of class TDBAbstract - base interface of abstract DB chain
//
// BSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// BSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include <cstring>
#include "io_settings.h"
#include "dbconnect.h"
#include "dbclient.h"
#include "dbdriverejdb.h"

namespace bsonio {


void TDataBase::getFromSettings()
{
    _isLocalDB = ioSettings().isLocalDB();
    _dbfilePath = ioSettings().localDBPath();
    _host = ioSettings().dbSocketHost();
    _port = ioSettings().dbSocketPort();
}

// Update DataBase connections using settings
void TDataBase::updateSettings()
{
  getFromSettings();
  TAbstractDBDriver* adriver;
  for( auto coll:  _collections )
  {
    adriver = newDrive();
    coll.second->changeDriver( adriver );
  }
}


TAbstractDBDriver* TDataBase::newDrive() const
{
    TAbstractDBDriver* dbdriver;
    switch( _isLocalDB )
    {
      case 1:  dbdriver = &ejdbDriveOne(); // new TEJDBDrive( _dbfilePath);
               break;
      case 0:  dbdriver = &dbClientOne();  // new TDBClient( _host, _port );
               break;
      default: dbdriver = 0;
    }
    return  dbdriver;
}

TDBCollection *TDataBase::addCollection( const string& type, const string& colname,
                              const vector<KeyFldsData>& keyFldsInf  ) const
{
  //TAbstractDBDriver* adriver = newDrive();
  TDBCollection *col = new TDBCollection( this, colname.c_str(), keyFldsInf  );
  col->setType( type );
  col->Open();
  _collections[colname] = shared_ptr<TDBCollection>(col);
  return col;
}



} // namespace bsonio
